﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Application.Entities;
using Application.DAL.Migrations;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System;
using System.Data.Entity.Infrastructure;
using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Entities.AuditTrailSupportEntities;

namespace Application.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        private IAuditTrailBS _auditTrailBS;
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public ApplicationDbContext(IAuditTrailBS auditTrailBS)
        {
            _auditTrailBS = auditTrailBS;
        }
        static ApplicationDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
            //Database.SetInitializer(new ApplicationDbInitializer());


        }

        public static ApplicationDbContext Create()
        {

            var context = new ApplicationDbContext();
            context.Database.Initialize(false);
            return context;
        }

        public virtual IDbSet<BaselineVariable> BaselineVariables { get; set; }
        public virtual IDbSet<CalculationScenario> CalculationScenarios { get; set; }
        public virtual IDbSet<UserBlock> UserBlocks { get; set; }
        public virtual IDbSet<Project> Projects { get; set; }
        public virtual IDbSet<ProjectValues> ProjectValues { get; set; }
        public virtual IDbSet<AuditTrail> AuditTrails { get; set; }
        public virtual IDbSet<AuditTrailEntity> AuditTrailEntitys { get; set; }
        public virtual IDbSet<AuditTrailValues> AuditTrailValues { get; set; }
        public virtual IDbSet<CampusInfo> CampusInfos { get; set; }
        public virtual IDbSet<Entities.FileInfo> FileInfos { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ConfigureBaselineVariableEntity(modelBuilder);
            ConfigureCalculationScenarioEntity(modelBuilder);
            ConfigureUserBlockStatusEntity(modelBuilder);
            ConfigureProjectEntity(modelBuilder);
            ConfigureProjectValuesEntity(modelBuilder);
            ConfigureAuditTrailEntity(modelBuilder);
            ConfigureGeneratedPDFEntity(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void ConfigureBaselineVariableEntity(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BaselineVariable>().HasKey(t => t.Id);
            modelBuilder.Entity<BaselineVariable>().Property(e => e.Name)
                .IsRequired().HasMaxLength(250).HasColumnType("nvarchar");
            modelBuilder.Entity<BaselineVariable>().Property(e => e.Value)
                .IsRequired().HasMaxLength(50).HasColumnType("nvarchar");


            modelBuilder.Entity<BaselineVariable>()
                          .HasMany<CalculationScenario>(c => c.CalculationScenarios)
                          .WithMany(b => b.BaselineVariables)
                          .Map(cb =>
                          {
                              cb.MapLeftKey("BaselineVariableId");
                              cb.MapRightKey("CalculationScenarioId");
                              cb.ToTable("BaselineVariableCalculationScenario");
                          });

        }
        private void ConfigureCalculationScenarioEntity(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CalculationScenario>().HasKey(t => t.Id);
            modelBuilder.Entity<CalculationScenario>().Property(e => e.Name)
                .IsRequired().HasMaxLength(250).HasColumnType("nvarchar");

        }
        private void ConfigureUserBlockStatusEntity(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserBlock>().Property(us => us.UserId).
                IsRequired();
        }
        private void ConfigureProjectEntity(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().HasRequired(p => p.User)
                .WithMany(b => b.Projects)
                .HasForeignKey(p => p.UserId);
        }
        private void ConfigureProjectValuesEntity(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectValues>().HasRequired(p => p.Project)
                .WithMany(b => b.ProjectValues)
                .HasForeignKey(p => p.ProjectId);

        }

        private void ConfigureAuditTrailEntity(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuditTrailEntity>().HasRequired(p => p.AuditTrails)
               .WithMany(b => b.AuditTrailEntities)
               .HasForeignKey(p => p.AuditTrailId);
            modelBuilder.Entity<AuditTrailValues>().HasRequired(p => p.AuditTrailEntity)
              .WithMany(b => b.AuditTrailValues)
              .HasForeignKey(p => p.AuditTrailEntityId);
            modelBuilder.Entity<AuditTrailEntity>().HasOptional(ate => ate.Parent)
                .WithMany().HasForeignKey(ate => ate.AuditTrailEntityParentId);

        }
        

        private T CreateWithValues<T>(DbPropertyValues values) where T : new()
        {
            T entity = new T();
            Type type = typeof(T);

            foreach (var name in values.PropertyNames)
            {
                var property = type.GetProperty(name);
                property.SetValue(entity, values.GetValue<object>(name));
            }

            return entity;
        }

        public override int SaveChanges()
        {

            var addedEntities = new List<DbEntityEntry>();
            var modifiedEntities = new List<CustomDbEntityEntry>();
            var deletedEntities = new List<CustomDbEntityEntry>();
            foreach (var entity in this.ChangeTracker.Entries())
            {
                if (entity.State == EntityState.Added)
                    addedEntities.Add(entity); // hasn't have Id's yet
                else if (entity.State == EntityState.Modified)
                    modifiedEntities.Add(new CustomDbEntityEntry(entity)); // will change OriginalValues to CurrentValues after base.SaveChanges()
                else if (entity.State == EntityState.Deleted)
                {
                    deletedEntities.Add(new CustomDbEntityEntry(entity)); // will change OriginalValues to CurrentValues and delete CurrentValues after base.SaveChanges()
                }
            }
            //for deleted records like UserBlock we have no parent entity - it comes like dedicated row
            var userEntity = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Unchanged && e.Entity.GetType().ToString().StartsWith("System.Data.Entity.DynamicProxies.ApplicationUser_")).FirstOrDefault();
            ApplicationUser user = null;
            if (userEntity != null)
                user = (ApplicationUser)userEntity.Entity;

            int retValue = base.SaveChanges();

            // on the beginning _auditTrailBS is null
            // do nothing if we have no added or modified entities
            if (_auditTrailBS != null && (addedEntities.Count > 0 || modifiedEntities.Count > 0 || deletedEntities.Count > 0))
            {
                var auditData = _auditTrailBS.FormAuditData(addedEntities, modifiedEntities, deletedEntities, user);
                if (auditData != null)
                {
                    foreach (var entity in auditData.AuditTrailEntities)
                    {
                        foreach (var value in entity.AuditTrailValues)
                        {
                            this.AuditTrailValues.Add(value);
                        }
                        this.AuditTrailEntitys.Add(entity);
                    }
                    this.AuditTrails.Add(auditData);
                    retValue = base.SaveChanges();
                }
            }
            return retValue;
        }

        private void ConfigureGeneratedPDFEntity(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Entities.FileInfo>().HasRequired(p => p.User)
                .WithMany(b => b.FileInfo)
                .HasForeignKey(p => p.UserId);

        }
    }
}