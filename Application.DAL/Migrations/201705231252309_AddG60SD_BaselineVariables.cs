namespace Application.DAL.Migrations
{
    using Application.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    
    public partial class AddG60SD_BaselineVariables : DbMigration
    {
        public override void Up()
        {
            List<string> args = new List<string>();

            args.Add(CalculationScenarioNames.G60SD);
            args.Add(BaselineVariableNames.LocationFactorNationalAverage);
            args.Add(BaselineVariableNames.LocationFactorLosAngeles);
            args.Add(BaselineVariableNames.LocationFactorTBD);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
            //Add G60-calculation and links of G60-calc to locations, startegy factors, all captions and isUsing variables
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}','{7}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", args.ToArray()
));

            //Add variables for F60-calculation
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.G60SD
, BaselineVariableNames.MiscellaneousSiteConstruction, "2.5"


));



        }

        public override void Down()
        {
            //It will also delete all related rows from BaselineVariableCalculationScenario because we have cascade delete flag
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.G60SD));

            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.MiscellaneousSiteConstruction));
         
        }
    }
}
