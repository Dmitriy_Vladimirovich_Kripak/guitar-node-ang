namespace Application.DAL.Migrations
{
    using Common;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddB20BaselineVariables : DbMigration
    {
        public override void Up()
        {
            //Insert Calculation Scenarios and their links to BaselineVariables

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int;
INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{7}', '{8}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{9}', '{10}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{11}', '{12}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{13}', '{14}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{15}', '{16}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{17}', '{18}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{19}', '{20}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{21}', '{22}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{23}', '{24}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{25}', '{26}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{27}', '{28}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{29}', '{30}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{31}', '{32}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{33}', '{34}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{35}', '{36}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

"
, CalculationScenarioNames.B20
, BaselineVariableNames.DesignFactorArchitecturalExpectationIconicB20, "1.5"
, BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardB20, "1.0"
, BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostB20, "0.8"
, BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB20, "1.5"
, BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentB20, "1.15"
, BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentB20, "1"
, BaselineVariableNames.AcademicClassroomBuildingSolidWallSF, "80"
, BaselineVariableNames.AcademicClassroomBuildingGlazedWallSF, "100"
, BaselineVariableNames.LaboratoryBuildingSolidWallSF, "80"
, BaselineVariableNames.LaboratoryBuildingGlazedWallSF, "100"
, BaselineVariableNames.StudentHousingSolidWallSF, "60"
, BaselineVariableNames.StudentHousingGlazedWallSF, "80"
, BaselineVariableNames.DiningSolidWallSF, "70"
, BaselineVariableNames.DiningGlazedWallSF, "90"
, BaselineVariableNames.ParkingGarageBelowGradeSolidWallSF, "80"
, BaselineVariableNames.ParkingGarageBelowGradeGlazedWallSF, "100"
, BaselineVariableNames.ParkingGarageAboveGradeSolidWallSF, "80"
, BaselineVariableNames.ParkingGarageAboveGradeGlazedWallSF, "100"
));
        }

        public override void Down()
        {
            
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.B20));

            Func<string, string, string> sqlCmd = (Name, Value) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicB20, "1.5"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardB20, "1.0"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostB20, "0.8"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB20, "1.5"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentB20, "1.15"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentB20, "1"));
            Sql(sqlCmd(BaselineVariableNames.AcademicClassroomBuildingSolidWallSF, "80"));
            Sql(sqlCmd(BaselineVariableNames.AcademicClassroomBuildingGlazedWallSF, "100"));
            Sql(sqlCmd(BaselineVariableNames.LaboratoryBuildingSolidWallSF, "80"));
            Sql(sqlCmd(BaselineVariableNames.LaboratoryBuildingGlazedWallSF, "100"));
            Sql(sqlCmd(BaselineVariableNames.StudentHousingSolidWallSF, "60"));
            Sql(sqlCmd(BaselineVariableNames.StudentHousingGlazedWallSF, "80"));
            Sql(sqlCmd(BaselineVariableNames.DiningSolidWallSF, "70"));
            Sql(sqlCmd(BaselineVariableNames.DiningGlazedWallSF, "90"));
            Sql(sqlCmd(BaselineVariableNames.ParkingGarageBelowGradeSolidWallSF, "80"));
            Sql(sqlCmd(BaselineVariableNames.ParkingGarageBelowGradeGlazedWallSF, "100"));
            Sql(sqlCmd(BaselineVariableNames.ParkingGarageAboveGradeSolidWallSF, "80"));
            Sql(sqlCmd(BaselineVariableNames.ParkingGarageAboveGradeGlazedWallSF, "100"));


        }
    }
}
