namespace Application.DAL.Migrations
{
    using Common;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddZ920BC_BaselineVariables : DbMigration
    {
        public override void Up()
        {
            //Insert Calculation Scenarios and their links to BaselineVariables

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int;
INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{7}', '{8}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{9}', '{10}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{11}', '{12}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
"
, CalculationScenarioNames.Z920BC
, BaselineVariableNames.DefaultEscalationRate1Year, "0.06"
, BaselineVariableNames.DefaultEscalationRate2Year, "0.05"
, BaselineVariableNames.DefaultEscalationRate3Year, "0.04"
, BaselineVariableNames.DefaultEscalationRate4Year, "0.04"
, BaselineVariableNames.DefaultEscalationRate5Year, "0.035"
, BaselineVariableNames.DataDate, "01/01/2017"
));
        }
        
        public override void Down()
        {
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.Z920BC));

            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.DefaultEscalationRate1Year));
            Sql(sqlCmd(BaselineVariableNames.DefaultEscalationRate2Year));
            Sql(sqlCmd(BaselineVariableNames.DefaultEscalationRate3Year));
            Sql(sqlCmd(BaselineVariableNames.DefaultEscalationRate4Year));
            Sql(sqlCmd(BaselineVariableNames.DefaultEscalationRate5Year));
            Sql(sqlCmd(BaselineVariableNames.DataDate));
        }
    }
}
