namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAuditTrail_SetParentChildInAuditTrailEntity : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AuditTrailEntities", "AuditTrailEntityParentId", c => c.Int());
            CreateIndex("dbo.AuditTrailEntities", "AuditTrailEntityParentId");
            AddForeignKey("dbo.AuditTrailEntities", "AuditTrailEntityParentId", "dbo.AuditTrailEntities", "Id");
            DropColumn("dbo.AuditTrails", "Action");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AuditTrails", "Action", c => c.String());
            DropForeignKey("dbo.AuditTrailEntities", "AuditTrailEntityParentId", "dbo.AuditTrailEntities");
            DropIndex("dbo.AuditTrailEntities", new[] { "AuditTrailEntityParentId" });
            AlterColumn("dbo.AuditTrailEntities", "AuditTrailEntityParentId", c => c.Int(nullable: false));
        }
    }
}
