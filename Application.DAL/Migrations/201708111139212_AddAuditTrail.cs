namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAuditTrail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditTrailEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AuditTrailId = c.Int(nullable: false),
                        EntityId = c.Int(nullable: false),
                        EntityName = c.String(),
                        DisplayName = c.String(),
                        ParentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AuditTrails", t => t.AuditTrailId, cascadeDelete: true)
                .Index(t => t.AuditTrailId);
            
            CreateTable(
                "dbo.AuditTrails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Date = c.DateTime(nullable: false),
                        Action = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AuditTrailValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AuditTrailEntityId = c.Int(nullable: false),
                        PropertyName = c.String(),
                        DisplayName = c.String(),
                        OldValue = c.String(),
                        NewValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AuditTrailEntities", t => t.AuditTrailEntityId, cascadeDelete: true)
                .Index(t => t.AuditTrailEntityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AuditTrailValues", "AuditTrailEntityId", "dbo.AuditTrailEntities");
            DropForeignKey("dbo.AuditTrailEntities", "AuditTrailId", "dbo.AuditTrails");
            DropIndex("dbo.AuditTrailValues", new[] { "AuditTrailEntityId" });
            DropIndex("dbo.AuditTrailEntities", new[] { "AuditTrailId" });
            DropTable("dbo.AuditTrailValues");
            DropTable("dbo.AuditTrails");
            DropTable("dbo.AuditTrailEntities");
        }
    }
}
