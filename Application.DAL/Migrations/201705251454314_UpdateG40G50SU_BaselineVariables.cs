namespace Application.DAL.Migrations
{
    using Application.Common;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateG40G50SU_BaselineVariables : DbMigration
    {
        public override void Up()
        {
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.G40SU
, BaselineVariableNames.SiteLighting, "3.5"
));
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.G50SU
, BaselineVariableNames.SiteCommunications, "1.0"
           ));
        }

        public override void Down()
        {
            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };
            Sql(sqlCmd(BaselineVariableNames.SiteLighting));

            Func<string, string> sqlCmdTwo = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };
            Sql(sqlCmdTwo(BaselineVariableNames.SiteCommunications));
        }
    }
}
