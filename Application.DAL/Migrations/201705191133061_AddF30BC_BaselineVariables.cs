namespace Application.DAL.Migrations
{
    using Application.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    
    public partial class AddF30BC_BaselineVariables : DbMigration
    {
        public override void Up()
        {
            List<string> args = new List<string>();

            args.Add(CalculationScenarioNames.F30BC);
            args.Add(BaselineVariableNames.LocationFactorNationalAverage);
            args.Add(BaselineVariableNames.LocationFactorLosAngeles);
            args.Add(BaselineVariableNames.LocationFactorTBD);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
            //Add C10-calculation and links of C10-calc to locations, startegy factors, all captions and isUsing variables
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}','{7}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", args.ToArray()
));

            //Add variables for F30-calculation
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.F30BC
, BaselineVariableNames.FacilityDemolitionRate, "12"
, BaselineVariableNames.FacilityDemolitionSimple, "1.0"
, BaselineVariableNames.FacilityDemolitionComplex, "1.5"
));

        }

        public override void Down()
        {
            //It will also delete all related rows from BaselineVariableCalculationScenario because we have cascade delete flag
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.F30BC));

            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.FacilityDemolitionRate));
            Sql(sqlCmd(BaselineVariableNames.FacilityDemolitionSimple));
            Sql(sqlCmd(BaselineVariableNames.FacilityDemolitionComplex));
        }
    }
}
