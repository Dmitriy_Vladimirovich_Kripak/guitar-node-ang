namespace Application.DAL.Migrations
{
    using Common;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedRelationsToA10B10 : DbMigration
    {
        public override void Up()
        {
            //Insert Calculation Scenarios and their links to BaselineVariables

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", CalculationScenarioNames.A10
, BaselineVariableNames.NumberOfLevelsFactor
, BaselineVariableNames.AcademicClassroomBuildingDeepGSF
, BaselineVariableNames.AcademicClassroomBuildingShallowGSF
, BaselineVariableNames.LaboratoryBuildingDeepGSF
, BaselineVariableNames.LaboratoryBuildingShallowGSF
, BaselineVariableNames.StudentHousingDeepGSF
, BaselineVariableNames.StudentHousingShallowGSF
, BaselineVariableNames.DiningDeepGSF
, BaselineVariableNames.DiningShallowGSF
, BaselineVariableNames.ParkingGarageBelowGradeDeepGSF
, BaselineVariableNames.ParkingGarageBelowGradeShallowGSF
, BaselineVariableNames.ParkingGarageAboveGradeDeepGSF
, BaselineVariableNames.ParkingGarageAboveGradeShallowGSF
));

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", CalculationScenarioNames.A20
, BaselineVariableNames.BasementWallsSF
, BaselineVariableNames.RetentionSF
, BaselineVariableNames.EarthworkExcavationBackfillCY
));

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", CalculationScenarioNames.A40
, BaselineVariableNames.AcademicClassroomBuildingSlabSF
, BaselineVariableNames.LaboratoryBuildingSlabSF
, BaselineVariableNames.StudentHousingSlabSF
, BaselineVariableNames.DiningSlabSF
, BaselineVariableNames.ParkingGarageBelowGradeSlabSF
, BaselineVariableNames.ParkingGarageAboveGradeSlabSF
));

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", CalculationScenarioNames.A90
, BaselineVariableNames.EarthworkExcavationHaulAwayCYSoil
, BaselineVariableNames.EarthworkExcavationHaulAwayCYRippableRock
, BaselineVariableNames.EarthworkExcavationHaulAwayCYSolidRock
));

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}',
'{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',
'{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}',
'{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}',
'{41}','{42}','{43}','{44}','{45}','{46}','{47}','{48}','{49}','{50}',
'{51}','{52}','{53}','{54}','{55}','{56}','{57}','{58}','{59}','{60}',
'{61}','{62}','{63}','{64}','{65}','{66}','{67}','{68}','{69}','{70}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", CalculationScenarioNames.B10
, BaselineVariableNames.FloorConstructionAcademicClassroomConcreteFlatSlabSF
, BaselineVariableNames.FloorConstructionAcademicClassroomConcreteSlabAndBeamSF
, BaselineVariableNames.FloorConstructionAcademicClassroomSteelBracedFrameSF
, BaselineVariableNames.FloorConstructionAcademicClassroomSteelMomentFrameSF
, BaselineVariableNames.FloorConstructionLaboratoryConcreteFlatSlabSF
, BaselineVariableNames.FloorConstructionLaboratoryConcreteSlabAndBeamSF
, BaselineVariableNames.FloorConstructionLaboratorySteelBracedFrameSF
, BaselineVariableNames.FloorConstructionLaboratorySteelMomentFrameSF
, BaselineVariableNames.FloorConstructionStudentHousingConcreteFlatSlabSF
, BaselineVariableNames.FloorConstructionStudentHousingConcreteSlabAndBeamSF
, BaselineVariableNames.FloorConstructionStudentHousingSteelBracedFrameSF
, BaselineVariableNames.FloorConstructionStudentHousingSteelMomentFrameSF
, BaselineVariableNames.FloorConstructionStudentHousingWoodFrameSF
, BaselineVariableNames.FloorConstructionDiningConcreteFlatSlabSF
, BaselineVariableNames.FloorConstructionDiningConcreteSlabAndBeamSF
, BaselineVariableNames.FloorConstructionDiningSteelBracedFrameSF
, BaselineVariableNames.FloorConstructionDiningSteelMomentFrameSF
, BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteFlatSlabSF
, BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF
, BaselineVariableNames.FloorConstructionParkingGarageBelowGradePrecastSF
, BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteFlatSlabSF
, BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF
, BaselineVariableNames.FloorConstructionParkingGarageAboveGradePrecastSF

, BaselineVariableNames.RoofConstructionAcademicClassroomConcreteFlatSlabSF
, BaselineVariableNames.RoofConstructionAcademicClassroomConcreteSlabAndBeamSF
, BaselineVariableNames.RoofConstructionAcademicClassroomSteelBracedFrameSF
, BaselineVariableNames.RoofConstructionAcademicClassroomSteelMomentFrameSF
, BaselineVariableNames.RoofConstructionLaboratoryConcreteFlatSlabSF
, BaselineVariableNames.RoofConstructionLaboratoryConcreteSlabAndBeamSF
, BaselineVariableNames.RoofConstructionLaboratorySteelBracedFrameSF
, BaselineVariableNames.RoofConstructionLaboratorySteelMomentFrameSF
, BaselineVariableNames.RoofConstructionStudentHousingConcreteFlatSlabSF
, BaselineVariableNames.RoofConstructionStudentHousingConcreteSlabAndBeamSF
, BaselineVariableNames.RoofConstructionStudentHousingSteelBracedFrameSF
, BaselineVariableNames.RoofConstructionStudentHousingSteelMomentFrameSF
, BaselineVariableNames.RoofConstructionStudentHousingWoodFrameSF
, BaselineVariableNames.RoofConstructionDiningConcreteFlatSlabSF
, BaselineVariableNames.RoofConstructionDiningConcreteSlabAndBeamSF
, BaselineVariableNames.RoofConstructionDiningSteelBracedFrameSF
, BaselineVariableNames.RoofConstructionDiningSteelMomentFrameSF
, BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteFlatSlabSF
, BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF
, BaselineVariableNames.RoofConstructionParkingGarageBelowGradePrecastSF
, BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteFlatSlabSF
, BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF
, BaselineVariableNames.RoofConstructionParkingGarageAboveGradePrecastSF

, BaselineVariableNames.StairAcademicClassroomEgressFLT
, BaselineVariableNames.StairAcademicClassroomOrnamentalFLT
, BaselineVariableNames.StairLaboratoryEgressFLT
, BaselineVariableNames.StairLaboratoryOrnamentalFLT 
, BaselineVariableNames.StairStudentHousingEgressFLT
, BaselineVariableNames.StairStudentHousingOrnamentalFLT 
, BaselineVariableNames.StairDiningEgressFLT 
, BaselineVariableNames.StairDiningOrnamentalFLT 
, BaselineVariableNames.StairParkingGarageBelowGradeEgressFLT 
, BaselineVariableNames.StairParkingGarageBelowGradeOrnamentalFLT 
, BaselineVariableNames.StairParkingGarageAboveGradeEgressFLT
, BaselineVariableNames.StairParkingGarageAboveGradeOrnamentalFLT

, BaselineVariableNames.FlightAcademicClassroomEgressRatio 
, BaselineVariableNames.FlightAcademicClassroomOrnamentalRatio
, BaselineVariableNames.FlightLaboratoryEgressRatio 
, BaselineVariableNames.FlightLaboratoryOrnamentalRatio 
, BaselineVariableNames.FlightStudentHousingEgressRatio 
, BaselineVariableNames.FlightStudentHousingOrnamentalRatio 
, BaselineVariableNames.FlightDiningEgressRatio
, BaselineVariableNames.FlightDiningOrnamentalRatio 
, BaselineVariableNames.FlightParkingGarageBelowGradeEgressRatio 
, BaselineVariableNames.FlightParkingGarageBelowGradeOrnamentalRatio
, BaselineVariableNames.FlightParkingGarageAboveGradeEgressRatio
, BaselineVariableNames.FlightParkingGarageAboveGradeOrnamentalRatio
));
        }

        public override void Down()
        {
            //It will also delete all related rows from BaselineVariableCalculationScenario because we have cascade delete flag
            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(CalculationScenarioNames.A10));
            Sql(sqlCmd(CalculationScenarioNames.A20));
            Sql(sqlCmd(CalculationScenarioNames.A40));
            Sql(sqlCmd(CalculationScenarioNames.A90));
            Sql(sqlCmd(CalculationScenarioNames.B10));
        }
    }
}
