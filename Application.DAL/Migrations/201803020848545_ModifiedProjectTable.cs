namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedProjectTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "CampusPrecint", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "CampusPrecint");
        }
    }
}
