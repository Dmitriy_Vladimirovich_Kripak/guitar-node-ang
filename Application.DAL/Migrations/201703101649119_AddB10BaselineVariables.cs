namespace Application.DAL.Migrations
{
    using Common;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddB10BaselineVariables : DbMigration
    {
        public override void Up()
        {
            Func<string, string, string> sqlCmd = (Name, Value) =>
            { return string.Format("INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}', '{1}');", Name, Value); };

            Sql(sqlCmd(BaselineVariableNames.FloorConstructionAcademicClassroomConcreteFlatSlabSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionAcademicClassroomConcreteSlabAndBeamSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionAcademicClassroomSteelBracedFrameSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionAcademicClassroomSteelMomentFrameSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionLaboratoryConcreteFlatSlabSF, "60"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionLaboratoryConcreteSlabAndBeamSF, "65"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionLaboratorySteelBracedFrameSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionLaboratorySteelMomentFrameSF, "60"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingConcreteFlatSlabSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingConcreteSlabAndBeamSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingSteelBracedFrameSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingSteelMomentFrameSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingWoodFrameSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionDiningConcreteFlatSlabSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionDiningConcreteSlabAndBeamSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionDiningSteelBracedFrameSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionDiningSteelMomentFrameSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteFlatSlabSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageBelowGradePrecastSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteFlatSlabSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageAboveGradePrecastSF, "30"));

            Sql(sqlCmd(BaselineVariableNames.RoofConstructionAcademicClassroomConcreteFlatSlabSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionAcademicClassroomConcreteSlabAndBeamSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionAcademicClassroomSteelBracedFrameSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionAcademicClassroomSteelMomentFrameSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionLaboratoryConcreteFlatSlabSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionLaboratoryConcreteSlabAndBeamSF, "60"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionLaboratorySteelBracedFrameSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionLaboratorySteelMomentFrameSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingConcreteFlatSlabSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingSteelBracedFrameSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingSteelMomentFrameSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingWoodFrameSF, "25"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionDiningConcreteFlatSlabSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionDiningConcreteSlabAndBeamSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionDiningSteelBracedFrameSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionDiningSteelMomentFrameSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteFlatSlabSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageBelowGradePrecastSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteFlatSlabSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageAboveGradePrecastSF, "30"));

            Sql(sqlCmd(BaselineVariableNames.StairAcademicClassroomEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairAcademicClassroomOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairLaboratoryEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairLaboratoryOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairStudentHousingEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairStudentHousingOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairDiningEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairDiningOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairParkingGarageBelowGradeEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairParkingGarageBelowGradeOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairParkingGarageAboveGradeEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairParkingGarageAboveGradeOrnamentalFLT, "120000"));

            Sql(sqlCmd(BaselineVariableNames.FlightAcademicClassroomEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightAcademicClassroomOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightLaboratoryEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightLaboratoryOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightStudentHousingEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightStudentHousingOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightDiningEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightDiningOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightParkingGarageBelowGradeEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightParkingGarageBelowGradeOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightParkingGarageAboveGradeEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightParkingGarageAboveGradeOrnamentalRatio, "0.00003"));

        }

        public override void Down()
        {
            //don't use value but keep for consistency with Up?
            Func<string, string, string> sqlCmd = (Name, Value) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.FloorConstructionAcademicClassroomConcreteFlatSlabSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionAcademicClassroomConcreteSlabAndBeamSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionAcademicClassroomSteelBracedFrameSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionAcademicClassroomSteelMomentFrameSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionLaboratoryConcreteFlatSlabSF, "60"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionLaboratoryConcreteSlabAndBeamSF, "65"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionLaboratorySteelBracedFrameSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionLaboratorySteelMomentFrameSF, "60"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingConcreteFlatSlabSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingConcreteSlabAndBeamSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingSteelBracedFrameSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingSteelMomentFrameSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionStudentHousingWoodFrameSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionDiningConcreteFlatSlabSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionDiningConcreteSlabAndBeamSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionDiningSteelBracedFrameSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionDiningSteelMomentFrameSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteFlatSlabSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageBelowGradePrecastSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteFlatSlabSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.FloorConstructionParkingGarageAboveGradePrecastSF, "30"));

            Sql(sqlCmd(BaselineVariableNames.RoofConstructionAcademicClassroomConcreteFlatSlabSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionAcademicClassroomConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionAcademicClassroomSteelBracedFrameSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionAcademicClassroomSteelMomentFrameSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionLaboratoryConcreteFlatSlabSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionLaboratoryConcreteSlabAndBeamSF, "60"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionLaboratorySteelBracedFrameSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionLaboratorySteelMomentFrameSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingConcreteFlatSlabSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingSteelBracedFrameSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingSteelMomentFrameSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionStudentHousingWoodFrameSF, "25"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionDiningConcreteFlatSlabSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionDiningConcreteSlabAndBeamSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionDiningSteelBracedFrameSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionDiningSteelMomentFrameSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteFlatSlabSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF, "55"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageBelowGradePrecastSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteFlatSlabSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF, "40"));
            Sql(sqlCmd(BaselineVariableNames.RoofConstructionParkingGarageAboveGradePrecastSF, "30"));

            Sql(sqlCmd(BaselineVariableNames.StairAcademicClassroomEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairAcademicClassroomOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairLaboratoryEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairLaboratoryOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairStudentHousingEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairStudentHousingOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairDiningEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairDiningOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairParkingGarageBelowGradeEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairParkingGarageBelowGradeOrnamentalFLT, "120000"));
            Sql(sqlCmd(BaselineVariableNames.StairParkingGarageAboveGradeEgressFLT, "40000"));
            Sql(sqlCmd(BaselineVariableNames.StairParkingGarageAboveGradeOrnamentalFLT, "120000"));

            Sql(sqlCmd(BaselineVariableNames.FlightAcademicClassroomEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightAcademicClassroomOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightLaboratoryEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightLaboratoryOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightStudentHousingEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightStudentHousingOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightDiningEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightDiningOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightParkingGarageBelowGradeEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightParkingGarageBelowGradeOrnamentalRatio, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.FlightParkingGarageAboveGradeEgressRatio, "0.00007"));
            Sql(sqlCmd(BaselineVariableNames.FlightParkingGarageAboveGradeOrnamentalRatio, "0.00003"));
        }
    }
}
