namespace Application.DAL.Migrations
{
    using Application.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    
    public partial class AddZ10BC_BaselineVariables : DbMigration
    {
        public override void Up()
        {
            List<string> args = new List<string>();

            args.Add(CalculationScenarioNames.Z10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC);

            //Add Z10BC-calculation and links of Z10BC-calc to locations, startegy factors, all captions and isUsing variables
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}',
'{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',
'{21}','{22}','{23}','{24}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", args.ToArray()
));

            //Add variables for Z10BC-calculation
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{7}', '{8}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{9}', '{10}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{11}', '{12}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{13}', '{14}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{15}', '{16}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{17}', '{18}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{19}', '{20}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{21}', '{22}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{23}', '{24}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{25}', '{26}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{27}', '{28}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{29}', '{30}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{31}', '{32}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{33}', '{34}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{35}', '{36}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{37}', '{38}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{39}', '{40}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{41}', '{42}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{43}', '{44}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{45}', '{46}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{47}', '{48}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.Z10BC
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC, "0.08"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC, "0.1"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC, "0.085"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC, "0.115"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC, "0.08"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC, "0.1"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC, "0.085"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC, "0.115"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC, "0.06"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC, "0.08"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC, "0.065"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC, "0.085"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC, "0.07"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC, "0.09"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC, "0.075"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC, "0.095"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC, "0.06"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC, "0.08"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC, "0.065"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC, "0.085"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC, "0.06"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC, "0.08"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC, "0.065"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC, "0.085"
));
        }

        public override void Down()
        {
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.Z10BC));

            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC));
        }
    }
}
