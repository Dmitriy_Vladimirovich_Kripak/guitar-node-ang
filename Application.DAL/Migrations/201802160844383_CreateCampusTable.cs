namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateCampusTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CampusInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InstitutionName = c.String(),
                        InstitutionType = c.String(),
                        Location = c.String(),
                        ArchitechName = c.String(),
                        CampusProject = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CampusInfoes");
        }
    }
}
