namespace Application.DAL.Migrations
{
    using Common;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddB30BaselineVariables : DbMigration
    {
        public override void Up()
        {
            //Insert Calculation Scenarios and their links to BaselineVariables

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int;
INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{7}', '{8}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{9}', '{10}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{11}', '{12}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{13}', '{14}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{15}', '{16}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{17}', '{18}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{19}', '{20}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{21}', '{22}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{23}', '{24}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{25}', '{26}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{27}', '{28}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{29}', '{30}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{31}', '{32}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{33}', '{34}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{35}', '{36}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{37}', '{38}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{39}', '{40}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{41}', '{42}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{43}', '{44}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{45}', '{46}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

INSERT INTO BaselineVariables(Name, Value) VALUES ('{47}', '{48}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
"
, CalculationScenarioNames.B30
, BaselineVariableNames.DesignFactorArchitecturalExpectationIconicB30, "1.5"
, BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardB30, "1.0"
, BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostB30, "0.8"
, BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB30, "1.5"
, BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentB30, "1.15"
, BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentB30, "1"
, BaselineVariableNames.AcademicClassroomBuildingSolidRoofSF, "17.5"
, BaselineVariableNames.AcademicClassroomBuildingGlazedRoofSF, "175"
, BaselineVariableNames.AcademicClassroomBuildingTerraceRoofSF, "50"
, BaselineVariableNames.AcademicClassroomBuildingSoffitRoofSF, "45"
, BaselineVariableNames.LaboratoryBuildingSolidRoofSF, "20"
, BaselineVariableNames.LaboratoryBuildingGlazedRoofSF, "175"
, BaselineVariableNames.LaboratoryBuildingTerraceRoofSF, "50"
, BaselineVariableNames.LaboratoryBuildingSoffitRoofSF, "50"
, BaselineVariableNames.StudentHousingSolidRoofSF, "15"
, BaselineVariableNames.StudentHousingGlazedRoofSF, "125"
, BaselineVariableNames.StudentHousingTerraceRoofSF, "35"
, BaselineVariableNames.StudentHousingSoffitRoofSF, "30"
, BaselineVariableNames.DiningSolidRoofSF, "20"
, BaselineVariableNames.DiningGlazedRoofSF, "150"
, BaselineVariableNames.DiningTerraceRoofSF, "50"
, BaselineVariableNames.DiningSoffitRoofSF, "45"
, BaselineVariableNames.ParkingGarageBelowGradeSolidRoofSF, "35"
, BaselineVariableNames.ParkingGarageBelowGradeGlazedRoofSF, "175"
, BaselineVariableNames.ParkingGarageAboveGradeSolidRoofSF, "2.5"
));
        }

        public override void Down()
        {

            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.B30));

            Func<string, string, string> sqlCmd = (Name, Value) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicB30, "1.5"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardB30, "1.0"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostB30, "0.8"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB30, "1.5"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentB30, "1.15"));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentB30, "1"));
            Sql(sqlCmd(BaselineVariableNames.AcademicClassroomBuildingSolidRoofSF, "17.5"));
            Sql(sqlCmd(BaselineVariableNames.AcademicClassroomBuildingGlazedRoofSF, "175"));
            Sql(sqlCmd(BaselineVariableNames.AcademicClassroomBuildingTerraceRoofSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.AcademicClassroomBuildingSoffitRoofSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.LaboratoryBuildingSolidRoofSF, "20"));
            Sql(sqlCmd(BaselineVariableNames.LaboratoryBuildingGlazedRoofSF, "175"));
            Sql(sqlCmd(BaselineVariableNames.LaboratoryBuildingTerraceRoofSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.LaboratoryBuildingSoffitRoofSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.StudentHousingSolidRoofSF, "15"));
            Sql(sqlCmd(BaselineVariableNames.StudentHousingGlazedRoofSF, "125"));
            Sql(sqlCmd(BaselineVariableNames.StudentHousingTerraceRoofSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.StudentHousingSoffitRoofSF, "30"));
            Sql(sqlCmd(BaselineVariableNames.DiningSolidRoofSF, "20"));
            Sql(sqlCmd(BaselineVariableNames.DiningGlazedRoofSF, "150"));
            Sql(sqlCmd(BaselineVariableNames.DiningTerraceRoofSF, "50"));
            Sql(sqlCmd(BaselineVariableNames.DiningSoffitRoofSF, "45"));
            Sql(sqlCmd(BaselineVariableNames.ParkingGarageBelowGradeSolidRoofSF, "35"));
            Sql(sqlCmd(BaselineVariableNames.ParkingGarageBelowGradeGlazedRoofSF, "175"));
            Sql(sqlCmd(BaselineVariableNames.ParkingGarageAboveGradeSolidRoofSF, "2.5"));

        }
    }
}
