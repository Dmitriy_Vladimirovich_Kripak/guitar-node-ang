namespace Application.DAL.Migrations
{
    using Application.Common;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateZ930SUZ930SDBaselineVariables : DbMigration
    {
        public override void Up()
        {
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int;
SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{1}' ) ; 
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{2}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{3}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{4}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{5}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{6}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{7}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{8}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{9}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{10}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{11}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{12}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{13}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{14}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{15}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{16}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{17}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{18}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{19}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{20}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{21}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{22}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{23}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{24}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
"
    , CalculationScenarioNames.Z930SU

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ930BC
));

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int;
SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{1}' ) ; 
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{2}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{3}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{4}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{5}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{6}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{7}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{8}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{9}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{10}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{11}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{12}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{13}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{14}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{15}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{16}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{17}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{18}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{19}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{20}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)



SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{21}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{22}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{23}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{24}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
"
    , CalculationScenarioNames.Z930SD

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ930BC

    , BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ930BC
    , BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ930BC
));
        }
        
        public override void Down()
        {
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.Z930SU));
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.Z930SD));
        }
    }
}
