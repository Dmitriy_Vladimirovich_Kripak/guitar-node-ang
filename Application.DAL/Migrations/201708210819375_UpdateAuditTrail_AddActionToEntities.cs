namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAuditTrail_AddActionToEntities : DbMigration
    {
        public override void Up()
        {
            Sql(@"DELETE FROM dbo.AuditTrailValues;");
            Sql(@"DELETE FROM dbo.AuditTrailEntities;");
            Sql(@"DELETE FROM dbo.AuditTrails;");


            AddColumn("dbo.AuditTrailEntities", "AuditTrailEntityParentId", c => c.Int(nullable: false));
            AddColumn("dbo.AuditTrailEntities", "Action", c => c.String());
            DropColumn("dbo.AuditTrailEntities", "ParentId");
          
        }

        public override void Down()
        {
            AddColumn("dbo.AuditTrailEntities", "ParentId", c => c.Int(nullable: false));
            DropColumn("dbo.AuditTrailEntities", "Action");
            DropColumn("dbo.AuditTrailEntities", "AuditTrailEntityParentId");
          
        }
    }
}
