namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProjectModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Institution", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Institution");
        }
    }
}
