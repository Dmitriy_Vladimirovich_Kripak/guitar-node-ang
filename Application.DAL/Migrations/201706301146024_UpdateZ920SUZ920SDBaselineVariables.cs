namespace Application.DAL.Migrations
{
    using Application.Common;
    using System;
    using System.Data.Entity.Migrations;

    public partial class UpdateZ920SUZ920SDBaselineVariables : DbMigration
    {
        public override void Up()
        {
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int;
SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{1}' ) ; 
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{2}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{3}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{4}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{5}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{6}');
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
"
    , CalculationScenarioNames.Z920SU
    , BaselineVariableNames.DefaultEscalationRate1Year
    , BaselineVariableNames.DefaultEscalationRate2Year
    , BaselineVariableNames.DefaultEscalationRate3Year
    , BaselineVariableNames.DefaultEscalationRate4Year
    , BaselineVariableNames.DefaultEscalationRate5Year
    , BaselineVariableNames.DataDate, "01/01/2017"
));
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int;
SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{1}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{2}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{3}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{4}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{5}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

SELECT @BaselineVariableId = (SELECT Id FROM BaselineVariables WHERE Name = '{6}' );
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
"
 , CalculationScenarioNames.Z920SD
 , BaselineVariableNames.DefaultEscalationRate1Year
 , BaselineVariableNames.DefaultEscalationRate2Year
 , BaselineVariableNames.DefaultEscalationRate3Year
 , BaselineVariableNames.DefaultEscalationRate4Year
 , BaselineVariableNames.DefaultEscalationRate5Year
 , BaselineVariableNames.DataDate, "01/01/2017"
 ));
        }

        public override void Down()
        {
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.Z920SU));
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.Z920SD));
        }
    }
}
