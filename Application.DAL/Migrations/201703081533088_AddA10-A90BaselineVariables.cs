namespace Application.DAL.Migrations
{
    using Common;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddA10A90BaselineVariables : DbMigration
    {   
        public override void Up()
        {

            Func<string, string, string> sqlInsert = (Name, Value) => 
                { return string.Format("INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}', '{1}');", Name, Value); };

            Sql(sqlInsert(BaselineVariableNames.NumberOfLevelsFactor,"0.95"));
            Sql(sqlInsert(BaselineVariableNames.AcademicClassroomBuildingDeepGSF, "20"));
            Sql(sqlInsert(BaselineVariableNames.AcademicClassroomBuildingShallowGSF, "10"));
            Sql(sqlInsert(BaselineVariableNames.LaboratoryBuildingDeepGSF, "20"));
            Sql(sqlInsert(BaselineVariableNames.LaboratoryBuildingShallowGSF, "10"));
            Sql(sqlInsert(BaselineVariableNames.StudentHousingDeepGSF, "20"));
            Sql(sqlInsert(BaselineVariableNames.StudentHousingShallowGSF, "10"));
            Sql(sqlInsert(BaselineVariableNames.DiningDeepGSF, "20"));
            Sql(sqlInsert(BaselineVariableNames.DiningShallowGSF, "10"));
            Sql(sqlInsert(BaselineVariableNames.ParkingGarageBelowGradeDeepGSF, "20"));
            Sql(sqlInsert(BaselineVariableNames.ParkingGarageBelowGradeShallowGSF, "10"));
            Sql(sqlInsert(BaselineVariableNames.ParkingGarageAboveGradeDeepGSF, "20"));
            Sql(sqlInsert(BaselineVariableNames.ParkingGarageAboveGradeShallowGSF, "10"));
            Sql(sqlInsert(BaselineVariableNames.BasementWallsSF, "50"));
            Sql(sqlInsert(BaselineVariableNames.RetentionSF, "65"));
            Sql(sqlInsert(BaselineVariableNames.EarthworkExcavationBackfillCY, "40"));

            Sql(sqlInsert(BaselineVariableNames.AcademicClassroomBuildingSlabSF, "10"));
            Sql(sqlInsert(BaselineVariableNames.LaboratoryBuildingSlabSF, "15"));
            Sql(sqlInsert(BaselineVariableNames.StudentHousingSlabSF, "8"));
            Sql(sqlInsert(BaselineVariableNames.DiningSlabSF, "12"));
            Sql(sqlInsert(BaselineVariableNames.ParkingGarageBelowGradeSlabSF, "12"));
            Sql(sqlInsert(BaselineVariableNames.ParkingGarageAboveGradeSlabSF, "10"));

            Sql(sqlInsert(BaselineVariableNames.EarthworkExcavationHaulAwayCYSoil, "25"));
            Sql(sqlInsert(BaselineVariableNames.EarthworkExcavationHaulAwayCYRippableRock, "35"));
            Sql(sqlInsert(BaselineVariableNames.EarthworkExcavationHaulAwayCYSolidRock, "70"));
        }

        public override void Down()
        {
            //don't use value but keep for consistency with Up?
            Func<string, string, string> sqlDelete = (Name, Value) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlDelete(BaselineVariableNames.NumberOfLevelsFactor, "0.95"));
            Sql(sqlDelete(BaselineVariableNames.AcademicClassroomBuildingDeepGSF, "20"));
            Sql(sqlDelete(BaselineVariableNames.AcademicClassroomBuildingShallowGSF, "10"));
            Sql(sqlDelete(BaselineVariableNames.LaboratoryBuildingDeepGSF, "20"));
            Sql(sqlDelete(BaselineVariableNames.LaboratoryBuildingShallowGSF, "10"));
            Sql(sqlDelete(BaselineVariableNames.StudentHousingDeepGSF, "20"));
            Sql(sqlDelete(BaselineVariableNames.StudentHousingShallowGSF, "10"));
            Sql(sqlDelete(BaselineVariableNames.DiningDeepGSF, "20"));
            Sql(sqlDelete(BaselineVariableNames.DiningShallowGSF, "10"));
            Sql(sqlDelete(BaselineVariableNames.ParkingGarageBelowGradeDeepGSF, "20"));
            Sql(sqlDelete(BaselineVariableNames.ParkingGarageBelowGradeShallowGSF, "10"));
            Sql(sqlDelete(BaselineVariableNames.ParkingGarageAboveGradeDeepGSF, "20"));
            Sql(sqlDelete(BaselineVariableNames.ParkingGarageAboveGradeShallowGSF, "10"));
            Sql(sqlDelete(BaselineVariableNames.BasementWallsSF, "50"));
            Sql(sqlDelete(BaselineVariableNames.RetentionSF, "65"));
            Sql(sqlDelete(BaselineVariableNames.EarthworkExcavationBackfillCY, "40"));
            Sql(sqlDelete(BaselineVariableNames.AcademicClassroomBuildingSlabSF, "10"));
            Sql(sqlDelete(BaselineVariableNames.LaboratoryBuildingSlabSF, "15"));
            Sql(sqlDelete(BaselineVariableNames.StudentHousingSlabSF, "8"));
            Sql(sqlDelete(BaselineVariableNames.DiningSlabSF, "12"));
            Sql(sqlDelete(BaselineVariableNames.ParkingGarageBelowGradeSlabSF, "12"));
            Sql(sqlDelete(BaselineVariableNames.ParkingGarageAboveGradeSlabSF, "10"));
            Sql(sqlDelete(BaselineVariableNames.EarthworkExcavationHaulAwayCYSoil, "25"));
            Sql(sqlDelete(BaselineVariableNames.EarthworkExcavationHaulAwayCYRippableRock, "35"));
            Sql(sqlDelete(BaselineVariableNames.EarthworkExcavationHaulAwayCYSolidRock, "70"));
        }
    }
}
