namespace Application.DAL.Migrations
{
    using Application.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    
    public partial class AddA60BaselineVariables : DbMigration
    {
        public override void Up()
        {
            List<string> args = new List<string>();

            args.Add(CalculationScenarioNames.A60);
            args.Add(BaselineVariableNames.LocationFactorNationalAverage);
            args.Add(BaselineVariableNames.LocationFactorLosAngeles);
            args.Add(BaselineVariableNames.LocationFactorTBD);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);           

            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", args.ToArray()
));            
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.A60
, BaselineVariableNames.FoundationDrainage, "25"
));
        }
        
        public override void Down()
        {
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.A60));

            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.FoundationDrainage));
           
        }
    }
}
