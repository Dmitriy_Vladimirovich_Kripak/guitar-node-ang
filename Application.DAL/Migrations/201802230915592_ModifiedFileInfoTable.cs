namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedFileInfoTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileInfoes", "Label", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileInfoes", "Label");
        }
    }
}
