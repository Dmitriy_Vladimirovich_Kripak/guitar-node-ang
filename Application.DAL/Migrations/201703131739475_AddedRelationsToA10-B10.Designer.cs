// <auto-generated />
namespace Application.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedRelationsToA10B10 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedRelationsToA10B10));
        
        string IMigrationMetadata.Id
        {
            get { return "201703131739475_AddedRelationsToA10-B10"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
