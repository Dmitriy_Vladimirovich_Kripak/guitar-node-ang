namespace Application.DAL.Migrations
{
    using Application.Common;
    using Application.Models.Calculation;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedValueBaselineVariables : DbMigration
    {
        public override void Up()
        {
            Func<string, string, string> sqlCmd = (Name, Value) =>
            { return string.Format("UPDATE BaselineVariables SET Value = '{0}' WHERE Name = '{1}'", Value, Name); };

            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.D60, BuildingType.LaboratoryBuilding.ToString(), 1), "0.5"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.D60, BuildingType.LaboratoryBuilding.ToString(), 3), "0.5"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.D60, BuildingType.LaboratoryBuilding.ToString(), 5), "0.5"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.D60, BuildingType.LaboratoryBuilding.ToString(), 2), "0.5"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.D60, BuildingType.LaboratoryBuilding.ToString(), 4), "0.5"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.D60, BuildingType.StudentHousing.ToString(), 1), "0.5"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.D60, BuildingType.StudentHousing.ToString(), 3), "0.5"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.D60, BuildingType.StudentHousing.ToString(), 5), "0.5"));
        }
        
        public override void Down()
        {
        }
    }
}
