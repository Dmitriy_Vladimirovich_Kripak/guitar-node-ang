namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserBlockWithoutRelation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserBlocks", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserBlocks", new[] { "UserId" });
            AlterColumn("dbo.UserBlocks", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserBlocks", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.UserBlocks", "UserId");
            AddForeignKey("dbo.UserBlocks", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
