namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjectValues : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        PropertyName = c.String(),
                        PropertyValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectValues", "ProjectId", "dbo.Projects");
            DropIndex("dbo.ProjectValues", new[] { "ProjectId" });
            DropTable("dbo.ProjectValues");
        }
    }
}
