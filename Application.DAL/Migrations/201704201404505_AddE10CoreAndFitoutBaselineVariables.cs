namespace Application.DAL.Migrations
{
    using Common;
    using Models.Calculation;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    public partial class AddE10CoreAndFitoutBaselineVariables : DbMigration
    {
        public override void Up()
        {
            List<string> args;

            #region E10
            args = new List<string>();

            args.Add(CalculationScenarioNames.E10);
            args.Add(BaselineVariableNames.LocationFactorNationalAverage);
            args.Add(BaselineVariableNames.LocationFactorLosAngeles);
            args.Add(BaselineVariableNames.LocationFactorTBD);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);

            //6 building types, 8+4 programs, caption + isUsing variables = 144 variables
            foreach (var buildingType in Enum.GetNames(typeof(BuildingType)))
            {
                //The maximum 8 Assignable Programs 
                for (int i = 1; i <= 8; i++)
                {
                    args.Add(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, true, true));
                    args.Add(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, true, false));
                }
                //There are always 4 Gross Up Programs
                for (int i = 1; i <= 4; i++)
                {
                    args.Add(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, false, true));
                    args.Add(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, false, false));
                }
            }

            //Add calculation and links to locations, startegy factors, all captions and isUsing variables
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}',
'{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',
'{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}',
'{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}',
'{41}','{42}','{43}','{44}','{45}','{46}','{47}','{48}','{49}','{50}',
'{51}','{52}','{53}','{54}','{55}','{56}','{57}','{58}','{59}','{60}',
'{61}','{62}','{63}','{64}','{65}','{66}','{67}','{68}','{69}','{70}',
'{71}','{72}','{73}','{74}','{75}','{76}','{77}','{78}','{79}','{80}',
'{81}','{82}','{83}','{84}','{85}','{86}','{87}','{88}','{89}','{90}',
'{91}','{92}','{93}','{94}','{95}','{96}','{97}','{98}','{99}','{100}',
'{101}','{102}','{103}','{104}','{105}','{106}','{107}','{108}','{109}','{110}',
'{111}','{112}','{113}','{114}','{115}','{116}','{117}','{118}','{119}','{120}',
'{121}','{122}','{123}','{124}','{125}','{126}','{127}','{128}','{129}','{130}',
'{131}','{132}','{133}','{134}','{135}','{136}','{137}','{138}','{139}','{140}',
'{141}','{142}','{143}','{144}','{145}','{146}','{147}','{148}','{149}','{150}',
'{151}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", args.ToArray()
));

            //Add program values
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{7}', '{8}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{9}', '{10}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{11}', '{12}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{13}', '{14}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{15}', '{16}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{17}', '{18}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{19}', '{20}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{21}', '{22}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{23}', '{24}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{25}', '{26}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{27}', '{28}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{29}', '{30}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{31}', '{32}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{33}', '{34}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{35}', '{36}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{37}', '{38}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{39}', '{40}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{41}', '{42}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{43}', '{44}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{45}', '{46}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{47}', '{48}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{49}', '{50}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{51}', '{52}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{53}', '{54}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{55}', '{56}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{57}', '{58}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{59}', '{60}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{61}', '{62}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{63}', '{64}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{65}', '{66}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{67}', '{68}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{69}', '{70}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{71}', '{72}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{73}', '{74}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{75}', '{76}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{77}', '{78}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{79}', '{80}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{81}', '{82}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{83}', '{84}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{85}', '{86}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{87}', '{88}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{89}', '{90}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{91}', '{92}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{93}', '{94}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{95}', '{96}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{97}', '{98}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{99}', '{100}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{101}', '{102}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{103}', '{104}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{105}', '{106}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{107}', '{108}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{109}', '{110}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{111}', '{112}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{113}', '{114}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{115}', '{116}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{117}', '{118}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{119}', '{120}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{121}', '{122}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{123}', '{124}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{125}', '{126}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{127}', '{128}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{129}', '{130}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{131}', '{132}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{133}', '{134}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{135}', '{136}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{137}', '{138}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{139}', '{140}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{141}', '{142}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{143}', '{144}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.E10
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 1), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 2), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 3), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 4), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 5), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 1), "0.5"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 2), "0.5"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 3), "0.5"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 4), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 4), "0"
));


            //Add variables for calculation
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.E10
, BaselineVariableNames.DesignFactorArchitecturalExpectationIconicE10, "1.5"
, BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardE10, "1.0"
, BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostE10, "0.8"
));

            #endregion E10

            #region E10Fitout
            args = new List<string>();

            args.Add(CalculationScenarioNames.E10Fitout);
            args.Add(BaselineVariableNames.LocationFactorNationalAverage);
            args.Add(BaselineVariableNames.LocationFactorLosAngeles);
            args.Add(BaselineVariableNames.LocationFactorTBD);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);

            //6 building types, 8+4 programs, caption + isUsing variables = 144 variables
            foreach (var buildingType in Enum.GetNames(typeof(BuildingType)))
            {
                //The maximum 8 Assignable Programs 
                for (int i = 1; i <= 8; i++)
                {
                    args.Add(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, true, true));
                    args.Add(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, true, false));
                }
                //There are always 4 Gross Up Programs
                for (int i = 1; i <= 4; i++)
                {
                    args.Add(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, false, true));
                    args.Add(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, false, false));
                }
            }

            //Add calculation and links to locations, startegy factors, all captions and isUsing variables
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}',
'{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',
'{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}',
'{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}',
'{41}','{42}','{43}','{44}','{45}','{46}','{47}','{48}','{49}','{50}',
'{51}','{52}','{53}','{54}','{55}','{56}','{57}','{58}','{59}','{60}',
'{61}','{62}','{63}','{64}','{65}','{66}','{67}','{68}','{69}','{70}',
'{71}','{72}','{73}','{74}','{75}','{76}','{77}','{78}','{79}','{80}',
'{81}','{82}','{83}','{84}','{85}','{86}','{87}','{88}','{89}','{90}',
'{91}','{92}','{93}','{94}','{95}','{96}','{97}','{98}','{99}','{100}',
'{101}','{102}','{103}','{104}','{105}','{106}','{107}','{108}','{109}','{110}',
'{111}','{112}','{113}','{114}','{115}','{116}','{117}','{118}','{119}','{120}',
'{121}','{122}','{123}','{124}','{125}','{126}','{127}','{128}','{129}','{130}',
'{131}','{132}','{133}','{134}','{135}','{136}','{137}','{138}','{139}','{140}',
'{141}','{142}','{143}','{144}','{145}','{146}','{147}','{148}','{149}','{150}',
'{151}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", args.ToArray()
));

            //Add program values
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{7}', '{8}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{9}', '{10}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{11}', '{12}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{13}', '{14}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{15}', '{16}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{17}', '{18}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{19}', '{20}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{21}', '{22}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{23}', '{24}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{25}', '{26}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{27}', '{28}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{29}', '{30}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{31}', '{32}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{33}', '{34}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{35}', '{36}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{37}', '{38}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{39}', '{40}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{41}', '{42}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{43}', '{44}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{45}', '{46}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{47}', '{48}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{49}', '{50}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{51}', '{52}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{53}', '{54}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{55}', '{56}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{57}', '{58}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{59}', '{60}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{61}', '{62}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{63}', '{64}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{65}', '{66}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{67}', '{68}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{69}', '{70}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{71}', '{72}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{73}', '{74}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{75}', '{76}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{77}', '{78}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{79}', '{80}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{81}', '{82}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{83}', '{84}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{85}', '{86}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{87}', '{88}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{89}', '{90}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{91}', '{92}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{93}', '{94}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{95}', '{96}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{97}', '{98}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{99}', '{100}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{101}', '{102}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{103}', '{104}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{105}', '{106}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{107}', '{108}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{109}', '{110}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{111}', '{112}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{113}', '{114}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{115}', '{116}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{117}', '{118}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{119}', '{120}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{121}', '{122}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{123}', '{124}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{125}', '{126}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{127}', '{128}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{129}', '{130}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{131}', '{132}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{133}', '{134}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{135}', '{136}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{137}', '{138}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{139}', '{140}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{141}', '{142}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{143}', '{144}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.E10Fitout
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 1), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 2), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 3), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 4), "50"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 5), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 1), "0.5"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 2), "0.5"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 3), "0.5"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 4), "0.5"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 4), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 5), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 6), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 7), "0"
, BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 8), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 1), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 2), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 3), "0"
, BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 4), "0"
));


            //Add variables for calculation
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.E10Fitout
, BaselineVariableNames.DesignFactorArchitecturalExpectationIconicE10Fitout, "1.5"
, BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardE10Fitout, "1.0"
, BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostE10Fitout, "0.8"
));


            #endregion E10Fitout
        }

        public override void Down()
        {
            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            #region E10
            //It will also delete all related rows from BaselineVariableCalculationScenario because we have cascade delete flag
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.E10));

            //E10
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.AcademicClassroomBuilding.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.LaboratoryBuilding.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.StudentHousing.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.Dining.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageBelowGrade.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10, BuildingType.ParkingGarageAboveGrade.ToString(), 4)));

            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicE10));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardE10));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostE10));

            #endregion E10

            #region E10Fitout
            //It will also delete all related rows from BaselineVariableCalculationScenario because we have cascade delete flag
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.E10Fitout));

            //E10Fitout
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.AcademicClassroomBuilding.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.LaboratoryBuilding.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.StudentHousing.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.Dining.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageBelowGrade.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 4)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 5)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 6)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 7)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20AssignableVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 8)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 1)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 2)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 3)));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20GrossUpVarName(CalculationScenarioNames.E10Fitout, BuildingType.ParkingGarageAboveGrade.ToString(), 4)));

            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicE10Fitout));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardE10Fitout));
            Sql(sqlCmd(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostE10Fitout));

            #endregion E10Fitout
        }
    }
}
