namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertDefaultValueCampusInfo : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO CampusInfoes(InstitutionName, InstitutionType,Location, ArchitechName, CampusProject) " +
                "VALUES ('Some Text', 'Some Text', 'Some Text', 'Some Text', 'Some Text');");
        }
        
        public override void Down()
        {
            Sql(" DELETE TOP(1) FROM CampusInfoes");           
        }
    }
}
