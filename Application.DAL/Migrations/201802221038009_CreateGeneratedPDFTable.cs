namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateGeneratedPDFTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        File = c.Binary(),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ProjectId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FileInfoes", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.FileInfoes", new[] { "UserId" });
            DropTable("dbo.FileInfoes");
        }
    }
}
