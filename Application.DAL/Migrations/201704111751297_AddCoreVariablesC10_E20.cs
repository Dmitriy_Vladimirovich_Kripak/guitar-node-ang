namespace Application.DAL.Migrations
{
    using Common;
    using Models.Calculation;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddCoreVariablesC10_E20 : DbMigration
    {
        public override void Up()
        {
            Sql(string.Format(@"
            INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); 
            INSERT INTO BaselineVariables(Name, Value) VALUES ('{2}',  '{3}'); 
            INSERT INTO BaselineVariables(Name, Value) VALUES ('{4}',  '{5}'); 
            INSERT INTO BaselineVariables(Name, Value) VALUES ('{6}',  '{7}'); 
            INSERT INTO BaselineVariables(Name, Value) VALUES ('{8}',  '{9}'); 
            INSERT INTO BaselineVariables(Name, Value) VALUES ('{10}', '{11}'); 
            INSERT INTO BaselineVariables(Name, Value) VALUES ('{12}', '{13}'); 
            "
, BaselineVariableNames.LocationFactorNationalAverage, "1.0"
, BaselineVariableNames.LocationFactorLosAngeles, "1.1"
, BaselineVariableNames.LocationFactorTBD, "1.0"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03"
));
            foreach (var buildingType in Enum.GetNames(typeof(BuildingType)))
            {
                //The maximum 8 Assignable Programs 
                for (int i = 1; i <= 8; i++)
                {
                    //Assignable Program Caption, by default like: AssignableProgram1
                    Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                        BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, true, true),
                        "AssignableProgram" + i.ToString()
                        ));
                    //Assignable Program IsUsing value, by default like: False (all variables ARE NOT USING by default)
                    Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                        BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, true, false),
                        "False"
                        ));
                }
                // There are always 4 Gross Up Programs : Restrooms, Circulation, MEP / Building Support, Walls and Shafts
                Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                    BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, 1, false, true), "Restrooms"));
                Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                    BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, 1, false, false), "True"));
                Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                    BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, 2, false, true), "Circulation"));
                Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                    BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, 2, false, false), "True"));
                Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                    BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, 3, false, true), "MEP / Building Support"));
                Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                    BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, 3, false, false), "True"));
                Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                    BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, 4, false, true), "Walls and Shafts"));
                Sql(string.Format(@"INSERT INTO BaselineVariables(Name, Value) VALUES ('{0}',  '{1}'); ",
                    BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, 4, false, false), "True"));
            }


            Func<string, string, string> sqlCmd = (Name, Value) =>
            { return string.Format("UPDATE BaselineVariables SET Value = '{0}' WHERE Name = '{1}'", Value, Name); };

            // Setup known values. We know that BuildingType.AcademicClassroomBuilding is using 5 programs 
            // with the following captions:

            // Assignable: Teaching, Enclosed Office / Conference, Open Office / Conference, Kitchenette
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 1, true, true), "Teaching"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 1, true, false), "True"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 2, true, true), "Enclosed Office / Conference"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 2, true, false), "True"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 3, true, true), "Open Office / Conference"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 3, true, false), "True"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 4, true, true), "Kitchenette"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 4, true, false), "True"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 5, true, true), "Shell Space"));
            Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(BuildingType.AcademicClassroomBuilding.ToString(), 5, true, false), "True")); 

        }

        public override void Down()
        {
            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.LocationFactorNationalAverage));
            Sql(sqlCmd(BaselineVariableNames.LocationFactorLosAngeles));
            Sql(sqlCmd(BaselineVariableNames.LocationFactorTBD));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational));

            foreach (var buildingType in Enum.GetNames(typeof(BuildingType)))
            {
                //The maximum 8 Assignable Programs 
                for (int i = 1; i <= 8; i++)
                {
                    Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, true, true)));
                    Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, true, false)));
                }
                //There are always 4 Gross Up Programs
                for (int i = 1; i <= 4; i++)
                {
                    Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, false, true)));
                    Sql(sqlCmd(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, i, false, false)));
                }
            }
        }
    }
}
