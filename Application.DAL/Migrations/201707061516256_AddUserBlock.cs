namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserBlock : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserBlocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserBlocks", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserBlocks", new[] { "Id" });
            DropTable("dbo.UserBlocks");
        }
    }
}
