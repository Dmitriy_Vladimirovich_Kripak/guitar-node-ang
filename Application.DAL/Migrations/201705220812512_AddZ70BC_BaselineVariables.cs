namespace Application.DAL.Migrations
{
    using Application.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    
    public partial class AddZ70BC_BaselineVariables : DbMigration
    {
        public override void Up()
        {
            List<string> args = new List<string>();

            args.Add(CalculationScenarioNames.Z70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC);

            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC);
            args.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC);

            //Add Z70BC-calculation and links of Z70BC-calc to locations, startegy factors, all captions and isUsing variables
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
SELECT @CalculationScenarioId = @@identity;

DECLARE @BaselineVariableId int
DECLARE READ_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
SELECT Id FROM BaselineVariables WHERE Name in (
'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}',
'{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',
'{21}','{22}','{23}','{24}'
)
OPEN READ_CURSOR FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId WHILE @@FETCH_STATUS = 0
BEGIN 
    INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
    FETCH NEXT FROM READ_CURSOR INTO @BaselineVariableId
END CLOSE READ_CURSOR DEALLOCATE READ_CURSOR
            ", args.ToArray()
));

            //Add variables for Z70BC-calculation
            Sql(string.Format(@"
DECLARE @CalculationScenarioId int;
SELECT @CalculationScenarioId = Id FROM CalculationScenarios WHERE Name = '{0}';

DECLARE @BaselineVariableId int;

INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{7}', '{8}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{9}', '{10}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{11}', '{12}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{13}', '{14}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{15}', '{16}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{17}', '{18}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{19}', '{20}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{21}', '{22}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{23}', '{24}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{25}', '{26}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{27}', '{28}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{29}', '{30}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{31}', '{32}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{33}', '{34}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{35}', '{36}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{37}', '{38}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{39}', '{40}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{41}', '{42}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{43}', '{44}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{45}', '{46}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
INSERT INTO BaselineVariables(Name, Value) VALUES ('{47}', '{48}'); SELECT @BaselineVariableId = @@identity;
INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)
", CalculationScenarioNames.Z70BC
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC, "0.01"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC, "0.02"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC, "0.015"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC, "0.025"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC, "0.01"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC, "0.02"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC, "0.015"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC, "0.025"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC, "0.01"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC, "0.02"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC, "0.015"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC, "0.025"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC, "0.01"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC, "0.02"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC, "0.015"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC, "0.025"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC, "0.01"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC, "0.02"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC, "0.015"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC, "0.025"
, BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC, "0.01"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC, "0.02"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC, "0.015"
, BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC, "0.025"
));
        }
        
        public override void Down()
        {
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.Z70BC));

            Func<string, string> sqlCmd = (Name) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC));
            Sql(sqlCmd(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC));
        }
    }
}
