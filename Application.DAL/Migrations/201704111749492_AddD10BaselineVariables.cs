namespace Application.DAL.Migrations
{
    using Common;
    using Models.Calculation;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddD10BaselineVariables : DbMigration
    {
        public override void Up()
        {
            //D10
            Sql(string.Format(@"
            DECLARE @CalculationScenarioId int;
            INSERT INTO CalculationScenarios(Name) VALUES ('{0}');
            SELECT @CalculationScenarioId = @@identity;

            DECLARE @BaselineVariableId int;
            INSERT INTO BaselineVariables(Name, Value) VALUES ('{1}', '{2}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{3}', '{4}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{5}', '{6}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{7}', '{8}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{9}', '{10}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{11}', '{12}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{13}', '{14}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{15}', '{16}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{17}', '{18}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{19}', '{20}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{21}', '{22}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{23}', '{24}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{25}', '{26}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{27}', '{28}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{29}', '{30}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{31}', '{32}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{33}', '{34}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{35}', '{36}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{37}', '{38}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{39}', '{40}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{41}', '{42}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{43}', '{44}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{45}', '{46}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            INSERT INTO BaselineVariables(Name, Value) VALUES ('{47}', '{48}'); SELECT @BaselineVariableId = @@identity;
            INSERT INTO BaselineVariableCalculationScenario(BaselineVariableId, CalculationScenarioId) VALUES (@BaselineVariableId, @CalculationScenarioId)

            "
, CalculationScenarioNames.D10
, BaselineVariableNames.ElevatorAcademicClassroomPassengerSTP, "50000"
, BaselineVariableNames.ElevatorAcademicClassroomServiceSTP, "70000"
, BaselineVariableNames.ElevatorLaboratoryPassengerSTP, "60000"
, BaselineVariableNames.ElevatorLaboratoryServiceSTP, "75000"
, BaselineVariableNames.ElevatorStudentHousingPassengerSTP, "45000"
, BaselineVariableNames.ElevatorStudentHousingServiceSTP, "60000"
, BaselineVariableNames.ElevatorDiningPassengerSTP, "50000"
, BaselineVariableNames.ElevatorDiningServiceSTP, "70000"
, BaselineVariableNames.ElevatorParkingGarageBelowGradePassengerSTP, "45000"
, BaselineVariableNames.ElevatorParkingGarageBelowGradeServiceSTP, "60000"
, BaselineVariableNames.ElevatorParkingGarageAboveGradePassengerSTP, "45000"
, BaselineVariableNames.ElevatorParkingGarageAboveGradeServiceSTP, "60000"
, BaselineVariableNames.ElevatorAcademicClassroomPassengerSTP_SF, "0.00005"
, BaselineVariableNames.ElevatorAcademicClassroomServiceSTP_SF, "0.00003"
, BaselineVariableNames.ElevatorLaboratoryPassengerSTP_SF, "0.00005"
, BaselineVariableNames.ElevatorLaboratoryServiceSTP_SF, "0.00003"
, BaselineVariableNames.ElevatorStudentHousingPassengerSTP_SF, "0.00005"
, BaselineVariableNames.ElevatorStudentHousingServiceSTP_SF, "0.00003"
, BaselineVariableNames.ElevatorDiningPassengerSTP_SF, "0.00005"
, BaselineVariableNames.ElevatorDiningServiceSTP_SF, "0.00003"
, BaselineVariableNames.ElevatorParkingGarageBelowGradePassengerSTP_SF, "0.00005"
, BaselineVariableNames.ElevatorParkingGarageBelowGradeServiceSTP_SF, "0.00003"
, BaselineVariableNames.ElevatorParkingGarageAboveGradePassengerSTP_SF, "0.00005"
, BaselineVariableNames.ElevatorParkingGarageAboveGradeServiceSTP_SF, "0.00003"
));
        }

        public override void Down()
        {
            Sql(string.Format("DELETE FROM CalculationScenarios WHERE Name = '{0}'", CalculationScenarioNames.D10));

            Func<string, string, string> sqlCmd = (Name, Value) =>
            { return string.Format("DELETE FROM BaselineVariables WHERE Name = '{0}'", Name); };

            //D10
            Sql(sqlCmd(BaselineVariableNames.ElevatorAcademicClassroomPassengerSTP, "50000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorAcademicClassroomServiceSTP, "70000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorLaboratoryPassengerSTP, "60000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorLaboratoryServiceSTP, "75000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorStudentHousingPassengerSTP, "45000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorStudentHousingServiceSTP, "60000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorDiningPassengerSTP, "50000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorDiningServiceSTP, "70000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorParkingGarageBelowGradePassengerSTP, "45000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorParkingGarageBelowGradeServiceSTP, "60000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorParkingGarageAboveGradePassengerSTP, "45000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorParkingGarageAboveGradeServiceSTP, "60000"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorAcademicClassroomPassengerSTP_SF, "0.00005"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorAcademicClassroomServiceSTP_SF, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorLaboratoryPassengerSTP_SF, "0.00005"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorLaboratoryServiceSTP_SF, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorStudentHousingPassengerSTP_SF, "0.00005"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorStudentHousingServiceSTP_SF, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorDiningPassengerSTP_SF, "0.00005"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorDiningServiceSTP_SF, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorParkingGarageBelowGradePassengerSTP_SF, "0.00005"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorParkingGarageBelowGradeServiceSTP_SF, "0.00003"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorParkingGarageAboveGradePassengerSTP_SF, "0.00005"));
            Sql(sqlCmd(BaselineVariableNames.ElevatorParkingGarageAboveGradeServiceSTP_SF, "0.00003"));


        }
    }
}
