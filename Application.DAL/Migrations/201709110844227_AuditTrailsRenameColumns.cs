namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuditTrailsRenameColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AuditTrailEntities", "EntityType", c => c.String());
            AddColumn("dbo.AuditTrailValues", "EntityType", c => c.String());
            DropColumn("dbo.AuditTrailEntities", "DisplayName");
            DropColumn("dbo.AuditTrailValues", "DisplayName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AuditTrailValues", "DisplayName", c => c.String());
            AddColumn("dbo.AuditTrailEntities", "DisplayName", c => c.String());
            DropColumn("dbo.AuditTrailValues", "EntityType");
            DropColumn("dbo.AuditTrailEntities", "EntityType");
        }
    }
}
