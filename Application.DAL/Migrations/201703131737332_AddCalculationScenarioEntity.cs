namespace Application.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCalculationScenarioEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CalculationScenarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BaselineVariableCalculationScenario",
                c => new
                    {
                        BaselineVariableId = c.Int(nullable: false),
                        CalculationScenarioId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BaselineVariableId, t.CalculationScenarioId })
                .ForeignKey("dbo.BaselineVariables", t => t.BaselineVariableId, cascadeDelete: true)
                .ForeignKey("dbo.CalculationScenarios", t => t.CalculationScenarioId, cascadeDelete: true)
                .Index(t => t.BaselineVariableId)
                .Index(t => t.CalculationScenarioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BaselineVariableCalculationScenario", "CalculationScenarioId", "dbo.CalculationScenarios");
            DropForeignKey("dbo.BaselineVariableCalculationScenario", "BaselineVariableId", "dbo.BaselineVariables");
            DropIndex("dbo.BaselineVariableCalculationScenario", new[] { "CalculationScenarioId" });
            DropIndex("dbo.BaselineVariableCalculationScenario", new[] { "BaselineVariableId" });
            DropTable("dbo.BaselineVariableCalculationScenario");
            DropTable("dbo.CalculationScenarios");
        }
    }
}
