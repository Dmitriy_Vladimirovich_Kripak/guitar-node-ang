﻿using Application.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.DAL
{
    public interface IApplicationDbContext :  IDisposable
    {
        IDbSet<BaselineVariable> BaselineVariables { get; set; }
        IDbSet<CalculationScenario> CalculationScenarios { get; set; }
        IDbSet<ApplicationUser>  Users { get; set; }
        IDbSet<UserBlock> UserBlocks { get; set; }
        IDbSet<Project>  Projects { get; set; }
        IDbSet<ProjectValues> ProjectValues { get; set; }
        IDbSet<AuditTrail> AuditTrails { get; set; }
        IDbSet<AuditTrailEntity> AuditTrailEntitys { get; set; }
        IDbSet<AuditTrailValues> AuditTrailValues { get; set; }
        IDbSet<CampusInfo> CampusInfos { get; set; }
        IDbSet<FileInfo> FileInfos { get; set; }
        int SaveChanges();
    }
}