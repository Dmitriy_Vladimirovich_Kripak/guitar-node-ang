﻿using Application.Models.Calculation;
using Application.Models.Manage;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static iTextSharp.text.Font;

namespace Application.BLL.Common
{
    class GeneratePDF
    {
        public void CreatePDF(Document document, MemoryStream ms, ProjectResultModel resultModel, ProjectModel projectModel)
        {
            var writer = PdfWriter.GetInstance(document, ms);
            document.Open();

            List<double> coreValueList = new List<double>();
            List<double> fitoutValueList = new List<double>();
            List<double> coreValueListST = new List<double>();
            List<double> fitoutValueListST = new List<double>();
            List<double> currentCoreValueList = new List<double>();
            List<double> currentFitoutValueList = new List<double>();
            List<double> sdValueList = new List<double>();
            List<double> suValueList = new List<double>();
            List<double> sdValueListST = new List<double>();
            List<double> suValueListST = new List<double>();
            List<double> softCost = new List<double>();
            List<double> softCostTotalCore = new List<double>();
            List<double> softCostTotalFitout = new List<double>();
            List<double> softCostTotalSD = new List<double>();
            List<double> softCostTotalSU = new List<double>();

            AddHeaderOfPDF(document, projectModel);
            AddHeaderTableOfPDF(document, resultModel, projectModel);

            AddTitleOfRow("A SUBSTRUCTURE", document);
            AddRowBC(resultModel.A10, 0, resultModel.GrossSquareFeet, "A10 Foundations", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.A20, 0, resultModel.GrossSquareFeet, "A20 Subgrade Enclosures", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.A40, 0, resultModel.GrossSquareFeet, "A40 Slabs on Grade ", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.A60, 0, resultModel.GrossSquareFeet, "A60 Water and Gas Mitigation", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.A90, 0, resultModel.GrossSquareFeet, "A90 Substructure Related Activities ", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddSubtotalBCRow("A SUBTOTAL", currentCoreValueList, currentFitoutValueList, sdValueList, suValueList, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, document);

            currentCoreValueList.Clear(); currentFitoutValueList.Clear();
            AddTitleOfRow("B SHELL", document);
            AddRowBC(resultModel.B10, 0, resultModel.GrossSquareFeet, "B10 Superstructure + Stairs", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.B20, 0, resultModel.GrossSquareFeet, "B20 Exterior Vertical Enclosure", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.B30, 0, resultModel.GrossSquareFeet, "B30 Exterior Horizontal Enclosure", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddSubtotalBCRow("B SUBTOTAL", currentCoreValueList, currentFitoutValueList, sdValueList, suValueList, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, document);

            currentCoreValueList.Clear(); currentFitoutValueList.Clear();
            AddTitleOfRow("C INTERIORS", document);
            AddRowBC(resultModel.C10, resultModel.C10Fitout, resultModel.GrossSquareFeet, "C10 Interior Construction", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.C20, resultModel.C20Fitout, resultModel.GrossSquareFeet, "C20 Interior Finishes", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddSubtotalBCRow("C SUBTOTAL", currentCoreValueList, currentFitoutValueList, sdValueList, suValueList, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, document);

            currentCoreValueList.Clear(); currentFitoutValueList.Clear();
            AddTitleOfRow("D SERVICES", document);
            AddRowBC(resultModel.D10, 0, resultModel.GrossSquareFeet, "D10 Conveying", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.D20, resultModel.D20Fitout, resultModel.GrossSquareFeet, "D20 Plumbing", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.D30, resultModel.D30Fitout, resultModel.GrossSquareFeet, "D30 HVAC", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.D40, resultModel.D40Fitout, resultModel.GrossSquareFeet, "D40 Fire Protection", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.D50, resultModel.D50Fitout, resultModel.GrossSquareFeet, "D50 Electrical", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.D60, resultModel.D60Fitout, resultModel.GrossSquareFeet, "D60 Communications", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.D70, resultModel.D70Fitout, resultModel.GrossSquareFeet, "D70 Electronic Safety & Security", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.D80, resultModel.D80Fitout, resultModel.GrossSquareFeet, "D80 Integrated Automation", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddSubtotalBCRow("D SUBTOTAL", currentCoreValueList, currentFitoutValueList, sdValueList, suValueList, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, document);

            currentCoreValueList.Clear(); currentFitoutValueList.Clear();
            AddTitleOfRow("E EQUIPMENT & FURNISHINGS", document);
            AddRowBC(resultModel.E10, resultModel.E10Fitout, resultModel.GrossSquareFeet, "E10 Equipment", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.E20, resultModel.E20Fitout, resultModel.GrossSquareFeet, "E20 Furnishings", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddSubtotalBCRow("E SUBTOTAL", currentCoreValueList, currentFitoutValueList, sdValueList, suValueList, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, document);

            currentCoreValueList.Clear(); currentFitoutValueList.Clear();
            AddTitleOfRow("F SPECIAL CONSTRUCTION & DEMOLITION", document);
            AddRowBC(resultModel.F10BC, 0, resultModel.GrossSquareFeet, "F10 Special Construction", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.F20BC, 0, resultModel.GrossSquareFeet, "F20 Facility Remediation", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddRowBC(resultModel.F30BC, 0, resultModel.GrossSquareFeet, "F30 Demolition", document, currentCoreValueList, currentFitoutValueList, coreValueList, fitoutValueList);
            AddSubtotalBCRow("F SUBTOTAL", currentCoreValueList, currentFitoutValueList, sdValueList, suValueList, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, document);

            currentCoreValueList.Clear(); currentFitoutValueList.Clear();
            AddTitleOfRow("G SITEWORK", document);
            AddRowSD(resultModel.G10SD, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, "G10 Site Preparation", document, sdValueList);
            AddRowSD(resultModel.G20SD, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, "G20 Site Improvements", document, sdValueList);
            AddRowSU(resultModel.G30SU, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, "G30 Liquid & Gas Site Utilities", document, suValueList);
            AddRowSU(resultModel.G40SU, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, "G40 Electrical Site Improvements", document, suValueList);
            AddRowSU(resultModel.G50SU, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, "G50 Site Communications", document, suValueList);
            AddRowSD(resultModel.G60SD, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, "G60 Miscellaneous Site Construction", document, sdValueList);
            AddSubtotalBCRow("G SUBTOTAL", currentCoreValueList, currentFitoutValueList, sdValueList, suValueList, resultModel.GrossSiteArea, resultModel.GrossSquareFeet, document);

            AddSubtotalDirectCostRow("SUBTOTAL DIRECT CONSTRUCTION COSTS", coreValueList.Sum(), fitoutValueList.Sum(), sdValueList.Sum(), suValueList.Sum(), resultModel.GrossSiteArea, resultModel.GrossSquareFeet, document);

            AddTemplatesItems(document, writer, "1/2");

            AddIndirectConstructionCost("INDIRECT CONSTRUCTION COSTS", document);
            AddRowTC(resultModel.Z10Percent, resultModel.Z10BC, resultModel.Z10Fitout, resultModel.Z10SD, resultModel.Z10SU, coreValueListST, fitoutValueList, sdValueListST, suValueListST, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Z10 General Requirements", document);
            AddRowTC(resultModel.Z70Percent, resultModel.Z70BC, resultModel.Z70Fitout, resultModel.Z70SD, resultModel.Z70SU, coreValueListST, fitoutValueList, sdValueListST, suValueListST, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Z70 Taxes, Permits, Ins., and Bonds", document);
            AddRowTC(resultModel.Z910Percent, resultModel.Z910BC, resultModel.Z910Fitout, resultModel.Z910SD, resultModel.Z910SU, coreValueListST, fitoutValueList, sdValueListST, suValueListST, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Z910 Contingency (Design)", document);
            AddRowTC(Math.Round(resultModel.Z920Percent, 4), resultModel.Z920BC, resultModel.Z920Fitout, resultModel.Z920SD, resultModel.Z920SU, coreValueListST, fitoutValueList, sdValueListST, suValueListST, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Z920 Contingency (Escalation)", document);
            AddRowTC(resultModel.Z930Percent, resultModel.Z930BC, resultModel.Z930Fitout, resultModel.Z930SD, resultModel.Z930SU, coreValueListST, fitoutValueList, sdValueListST, suValueListST, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Z930 Contingency (Construction)", document);
            AddRowTC(resultModel.Z940Percent, resultModel.Z940BC, resultModel.Z940Fitout, resultModel.Z940SD, resultModel.Z940SU, coreValueListST, fitoutValueList, sdValueListST, suValueListST, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Z940 Fees", document);

            AddSubtotalCostRowTC("TOTAL CONSTRUCTION COSTS", coreValueList, fitoutValueList, coreValueListST, fitoutValueListST, sdValueList, suValueList, sdValueListST, suValueListST, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, document);

            AddIndirectConstructionCost("SOFT COSTS", document);
            AddRowSoft(resultModel.DesignFee, softCost, coreValueListST.Sum() + coreValueList.Sum(), fitoutValueList.Sum() + fitoutValueListST.Sum(), sdValueList.Sum() + sdValueListST.Sum(), suValueList.Sum() + suValueListST.Sum(), softCostTotalCore, softCostTotalFitout, softCostTotalSD, softCostTotalSU, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Design Fees", document);
            AddRowSoft(resultModel.FFE, softCost, coreValueListST.Sum() + coreValueList.Sum(), fitoutValueList.Sum() + fitoutValueListST.Sum(), sdValueList.Sum() + sdValueListST.Sum(), suValueList.Sum() + suValueListST.Sum(), softCostTotalCore, softCostTotalFitout, softCostTotalSD, softCostTotalSU, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "FFE", document);
            AddRowSoft(resultModel.Technology, softCost, coreValueListST.Sum() + coreValueList.Sum(), fitoutValueList.Sum() + fitoutValueListST.Sum(), sdValueList.Sum() + sdValueListST.Sum(), suValueList.Sum() + suValueListST.Sum(), softCostTotalCore, softCostTotalFitout, softCostTotalSD, softCostTotalSU, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Technology", document);
            AddRowSoft(resultModel.Contingency, softCost, coreValueListST.Sum() + coreValueList.Sum(), fitoutValueList.Sum() + fitoutValueListST.Sum(), sdValueList.Sum() + sdValueListST.Sum(), suValueList.Sum() + suValueListST.Sum(), softCostTotalCore, softCostTotalFitout, softCostTotalSD, softCostTotalSU, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Contingency", document);
            AddRowSoft(resultModel.Misc, softCost, coreValueListST.Sum() + coreValueList.Sum(), fitoutValueList.Sum() + fitoutValueListST.Sum(), sdValueList.Sum() + sdValueListST.Sum(), suValueList.Sum() + suValueListST.Sum(), softCostTotalCore, softCostTotalFitout, softCostTotalSD, softCostTotalSU, resultModel.GrossSquareFeet, resultModel.GrossSiteArea, "Misc", document);
            AddSubtotalCostRow("TOTAL PROJECT COST", softCostTotalCore.Sum() + coreValueListST.Sum() + coreValueList.Sum(), softCostTotalFitout.Sum() + fitoutValueList.Sum() + fitoutValueListST.Sum(),
                softCostTotalSD.Sum() + sdValueList.Sum() + sdValueListST.Sum(), softCostTotalSU.Sum() + suValueList.Sum() + suValueListST.Sum(), resultModel.GrossSquareFeet, resultModel.GrossSiteArea, document);
            AddTemplatesItems(document, writer, "2/2");

        }

        /*----------------------------------There are constants for tune PDF doc size params------------------*/

        /*=============FIRST_COLUMN====================*/
        private const int FIRST_COLUMN_ALL = FIRST_COLUMN_MAIN + FIRST_COLUMN_SUB;
        private const int FIRST_COLUMN_MAIN = 12;
        private const int FIRST_COLUMN_SUB = 4;

        /*============Building==================*/
        private const int BUILDING_ALL = BUILDING_CORE_AND_SHEL_ALL + BUILDING_FITOUT_ALL + BUILDING_SUBTOTAL_ALL;
        /*------------Building/Core and Shell--------------------*/
        private const int BUILDING_CORE_AND_SHEL_ALL = BUILDING_CORE_AND_SHEL_FIRST_COLL + BUILDING_CORE_AND_SHEL_SECOND_COLL;
        private const int BUILDING_CORE_AND_SHEL_FIRST_COLL = 3;
        private const int BUILDING_CORE_AND_SHEL_SECOND_COLL = 5;
        /*------------Building/Fitout--------------------*/
        private const int BUILDING_FITOUT_ALL = BUILDING_FITOUT_FIRST_COLL + BUILDING_FITOUT_SECOND_COLL;
        private const int BUILDING_FITOUT_FIRST_COLL = 3;
        private const int BUILDING_FITOUT_SECOND_COLL = 5;
        /*------------Building/Subtotal--------------------*/
        private const int BUILDING_SUBTOTAL_ALL = BUILDING_SUBTOTAL_FIRST_COLL + BUILDING_SUBTOTAL_SECOND_COLL;
        private const int BUILDING_SUBTOTAL_FIRST_COLL = 3;
        private const int BUILDING_SUBTOTAL_SECOND_COLL = 5;

        /*============Site==================*/
        private const int SITE_ALL = SITE_SITE_DEVELOPMENT_ALL + SITE_SITE_UTILITIES_ALL + SITE_SUBTOTAL_ALL;
        /*------------Site/Site Development--------------------*/
        private const int SITE_SITE_DEVELOPMENT_ALL = SITE_SITE_DEVELOPMENT_FIRST_COLL + SITE_SITE_DEVELOPMENT_SECOND_COLL;
        private const int SITE_SITE_DEVELOPMENT_FIRST_COLL = 3;
        private const int SITE_SITE_DEVELOPMENT_SECOND_COLL = 5;
        /*------------Site/Site Utilities--------------------*/
        private const int SITE_SITE_UTILITIES_ALL = SITE_SITE_UTILITIES_FIRST_COLL + SITE_SITE_UTILITIES_SECOND_COLL;
        private const int SITE_SITE_UTILITIES_FIRST_COLL = 3;
        private const int SITE_SITE_UTILITIES_SECOND_COLL = 5;
        /*------------Site/Subtotal--------------------*/
        private const int SITE_SUBTOTAL_ALL = SITE_SUBTOTAL_FIRST_COLL + SITE_SUBTOTAL_SECOND_COLL;
        private const int SITE_SUBTOTAL_FIRST_COLL = 3;
        private const int SITE_SUBTOTAL_SECOND_COLL = 5;

        /*============Building + Site==================*/
        private const int BUILDING_SITE_ALL = BUILDING_SITE_FIRST_COLL + BUILDING_SITE_SECOND_COLL;
        private const int BUILDING_SITE_FIRST_COLL = 3;
        private const int BUILDING_SITE_SECOND_COLL = 5;

        //===============Total width==================================;
        private const int COLUMN_ALL_COUNT = BUILDING_SITE_ALL + SITE_ALL + BUILDING_ALL + FIRST_COLUMN_ALL;



        private void AddHeaderOfPDF(Document document, ProjectModel projectModel)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;
            float firstRowFixedHeight = 9f;
            /*-----------------------------------First Row-----------------------------------*/
            Paragraph p = new Paragraph();
            p.Add(new Chunk("Project Title :", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            PdfPCell cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.FixedHeight = firstRowFixedHeight;
            cell.Border = 0;
            cell.PaddingBottom = 0;
            cell.Colspan = FIRST_COLUMN_ALL / 2;

            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(projectModel.Name, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.FixedHeight = firstRowFixedHeight;
            cell.Colspan = FIRST_COLUMN_ALL / 2;
            cell.Border = 0;
            cell.PaddingBottom = 0;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.Colspan = BUILDING_ALL + SITE_ALL - BUILDING_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = firstRowFixedHeight;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Cost Model :", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Border = 0;
            cell.Colspan = BUILDING_SITE_FIRST_COLL + BUILDING_SUBTOTAL_SECOND_COLL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.FixedHeight = firstRowFixedHeight;
            cell.PaddingBottom = 0;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(projectModel.CampusPrecint != null ? projectModel.CampusPrecint.ToString() : " ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Border = 0;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.FixedHeight = firstRowFixedHeight;
            cell.PaddingBottom = 0;
            table.AddCell(cell);
            /*-----------------------------------END First Row-----------------------------------*/

            /*-----------------------------------Second Row-----------------------------------*/
            p = new Paragraph();
            p.Add(new Chunk("Institution :", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.PaddingTop = 0;
            cell.Border = 0;
            cell.Colspan = FIRST_COLUMN_ALL / 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(projectModel.Institution != null ? projectModel.Institution.ToString() : " ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.PaddingTop = 0;
            cell.Border = 0;
            cell.Colspan = FIRST_COLUMN_ALL / 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.Colspan = BUILDING_ALL + SITE_ALL - SITE_SUBTOTAL_SECOND_COLL;
            cell.PaddingTop = 0;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Date :", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.PaddingTop = 0;
            cell.Border = 0;
            cell.Colspan = BUILDING_SITE_FIRST_COLL + SITE_SUBTOTAL_SECOND_COLL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(projectModel.Date, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.PaddingTop = 0;
            cell.Border = 0;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);
            /*-----------------------------------END Second Row-----------------------------------*/



            document.Add(table);
        }

        private void AddHeaderTableOfPDF(Document document, ProjectResultModel resultModel, ProjectModel projectModel)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;
            /*-----------------------------------First Row-----------------------------------*/
            Paragraph p = new Paragraph();
            p.Add(new Chunk("Overall Component Summary", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
            PdfPCell cell = new PdfPCell(new Phrase(p));
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Building", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_ALL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Site", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = SITE_ALL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Building + Site", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_SITE_ALL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);
            /*-----------------------------------END First Row-----------------------------------*/

            /*-----------------------------------Second Row-----------------------------------*/
            float secondRowFixedHeight = 8f;

            cell = new PdfPCell(new Phrase(" "));
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.FixedHeight = secondRowFixedHeight;
            cell.Padding = 0;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Core and Shell ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_CORE_AND_SHEL_ALL;
            cell.Border = 0;
            cell.Padding = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = secondRowFixedHeight;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" Fitout ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = BUILDING_FITOUT_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = secondRowFixedHeight;
            cell.Padding = 0;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" Subtotal ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = BUILDING_SUBTOTAL_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = secondRowFixedHeight;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.Padding = 0;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" Site Development ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = SITE_SITE_DEVELOPMENT_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = secondRowFixedHeight;
            cell.Padding = 0;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Site Utilities", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = SITE_SITE_UTILITIES_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = secondRowFixedHeight;
            cell.Padding = 0;
            table.AddCell(cell);


            p = new Paragraph();
            p.Add(new Chunk("Subtotal", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = SITE_SUBTOTAL_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = secondRowFixedHeight;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 0;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);


            p = new Paragraph();
            p.Add(new Chunk("Total", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = BUILDING_SITE_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = secondRowFixedHeight;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 0;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);
            /*-----------------------------------END Second Row-----------------------------------*/

            /*-----------------------------------Third Row-----------------------------------*/
            cell = new PdfPCell(new Phrase(" "));
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 0;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0} SF", resultModel.GrossSquareFeet), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_CORE_AND_SHEL_ALL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.Padding = 0;
            table.AddCell(cell);


            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = BUILDING_FITOUT_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.Padding = 0;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = BUILDING_SUBTOTAL_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 0;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0} SF", resultModel.GrossSiteArea), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = SITE_SITE_DEVELOPMENT_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.Padding = 0;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = SITE_SITE_UTILITIES_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.Padding = 0;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = SITE_SUBTOTAL_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 0;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0} SF", resultModel.GrossSquareFeet), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Border = 0;
            cell.Colspan = BUILDING_SITE_ALL;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 0;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);
            /*-----------------------------------END Thrid Row-----------------------------------*/

            /*-----------------------------------Fourth Row-----------------------------------*/

            cell = new PdfPCell(new Phrase(""));
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" $/SF ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Total ($'s)", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" $/SF ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Total ($'s)", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" $/SF ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" Total ($'s)", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" $/SF ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Total ($'s)", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" $/SF ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Total ($'s)", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" $/SF ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Total ($'s)", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(" $/SF ", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk("Total ($'s)", new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = 2;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthTop = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);


            /*-----------------------------------END Fourth Row-----------------------------------*/

            document.Add(table);
        }

        private void AddSubtotalBCRow(string title, List<double> valuesCore, List<double> valueFitout, List<double> sdValueList,
            List<double> suValueList, double grossSiteArea, double grossSquareFeet, Document document)
        {

            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;

            Font fontTitle = new Font(FontFamily.HELVETICA, 7, Font.BOLD),
                 font = new Font(FontFamily.HELVETICA, 7, Font.BOLD);

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, fontTitle));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Padding = 2;
            cell.Border = 0;
            table.AddCell(cell);

            var coreSum = valuesCore.Sum() > 0 ? String.Format("{0:0.00}", valuesCore.Sum()) : "-";
            p = new Paragraph();
            p.Add(new Chunk(coreSum, font));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);


            var totalCoreSum = valuesCore.Sum() > 0 ? String.Format("{0:0,0}", valuesCore.Sum() * grossSquareFeet) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalCoreSum, font));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var fitoutSum = valueFitout.Sum() > 0 ? String.Format("{0:0.00}", valueFitout.Sum()) : "-";
            p = new Paragraph();
            p.Add(new Chunk(fitoutSum, font));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);


            var totalFitoutSum = valueFitout.Sum() > 0 ? String.Format("{0:0,0}", valueFitout.Sum() * grossSquareFeet) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalFitoutSum, font));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);


            var buildingTotal = (valuesCore.Sum() > 0 || valueFitout.Sum() > 0)
                ? String.Format("{0:0.00}", valuesCore.Sum() + valueFitout.Sum()) : "-";
            p = new Paragraph();
            p.Add(new Chunk(buildingTotal, font));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var buildingTotalGSF = (valuesCore.Sum() > 0 || valueFitout.Sum() > 0)
                ? String.Format("{0:0,0}", (valuesCore.Sum() + valueFitout.Sum()) * grossSquareFeet) : "-";
            p = new Paragraph();
            p.Add(new Chunk(buildingTotalGSF, font));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var totalSD = sdValueList.Sum() > 0 ? String.Format("{0:0.00}", sdValueList.Sum()) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalSD, font));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var rounded = RoundToNearestThousand(sdValueList.Sum() * grossSiteArea);
            var totalSDGSF = sdValueList.Sum() > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalSDGSF, font));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var totalSU = suValueList.Sum() > 0 ? String.Format("{0:0.00}", suValueList.Sum()) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalSU, font));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(suValueList.Sum() * grossSiteArea);
            var totalSUGSF = suValueList.Sum() > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalSUGSF, font));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var totalSU_SD = (suValueList.Sum() > 0 || sdValueList.Sum() > 0)
                ? String.Format("{0:0,0}", (suValueList.Sum() + sdValueList.Sum())) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalSU_SD, font));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand((suValueList.Sum() + sdValueList.Sum()) * grossSiteArea);
            var totalSU_SDGSF = (suValueList.Sum() > 0 || sdValueList.Sum() > 0)
                ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalSU_SDGSF, font));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var total = (valuesCore.Sum() > 0 || valueFitout.Sum() > 0 || suValueList.Sum() > 0 || sdValueList.Sum() > 0)
                ? String.Format("{0:0.00}", (valuesCore.Sum() + valueFitout.Sum() + suValueList.Sum() + sdValueList.Sum())) : "-";
            p = new Paragraph();
            p.Add(new Chunk(total, font));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand((valuesCore.Sum() + valueFitout.Sum() + suValueList.Sum() + sdValueList.Sum()) * grossSquareFeet);
            var totalGSF = (valuesCore.Sum() > 0 || valueFitout.Sum() > 0 || suValueList.Sum() > 0 || sdValueList.Sum() > 0)
                ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(totalGSF, font));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }

        private void AddIndirectConstructionCost(string title, Document document)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;
            string emptyFiller = "";
            float paddingTop = 8, paddingBottom = 3;


            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 9, Font.BOLD)));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);


            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);


            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            table.AddCell(cell);


            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(emptyFiller, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1f;
            cell.BorderColorBottom = new BaseColor(0, 0, 0);
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            document.Add(table);

        }

        private void AddSubtotalDirectCostRow(string title, double valuesTotalCore, double valueTotalFitout, double valuesSiteSD, double valuesSiteSU, double grossSiteArea, double grossSquareFeet, Document document)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;
            double valueSumSD = valuesSiteSD;
            double valueSumSU = valuesSiteSU;
            double valueSumCore = valuesTotalCore;
            double valueSumFitout = valueTotalFitout;

            float paddingTop = 1, paddingBottom = 3;

            double GSA = grossSiteArea;
            double GSF = grossSquareFeet;

            double subTotalCore = Math.Round(valueSumCore, 2) * GSF;
            double subTotalSFCore = subTotalCore / GSF;

            double subTotalFitout = Math.Round(valueSumFitout, 2) * GSF;
            double subTotalSFFitout = subTotalFitout / GSF;

            double subTotalSD = valueSumSD * GSA;
            double subSFSD = subTotalSD / GSA;

            double subTotalSU = valueSumSU * GSA;
            double subSFSU = subTotalSU / GSA;

            double siteTotal = subTotalSU + subTotalSD;
            double siteSFTotal = siteTotal / GSA;

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            table.AddCell(cell);

            var strSFCore = subTotalSFCore > 0 ? String.Format("{0:0,0.00}", subTotalSFCore) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFCore, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            var rounded = RoundToNearestThousand(subTotalCore);
            var strCore = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strCore, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);


            var strSFFitout = subTotalSFFitout > 0 ? String.Format("{0:0,0.00}", subTotalSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalFitout);
            var strFitout = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSumSFCoreFitout = subTotalSFCore + subTotalSFFitout > 0 ? String.Format("{0:0,0.00}", subTotalSFCore + subTotalSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSFCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalCore + subTotalFitout);
            var strSumCoreFitout = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);


            var strSFSD = subSFSD > 0 ? String.Format("{0:0,0.00}", subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFSD, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalSD);
            var strTotalSD = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalSD, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSFSU = subSFSU > 0 ? String.Format("{0:0,0.00}", subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalSU);
            var strTotalSU = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSumSFSDSU = (subSFSD + subSFSU) > 0 ? String.Format("{0:0,0.00}", subSFSD + subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSFSDSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalSD + subTotalSU);
            var strSumSDSU = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSDSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0.00}", subTotalSFCore + subTotalSFFitout + subSFSD + subSFSU), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand((subTotalSFCore + subTotalSFFitout + subSFSD + subSFSU) * grossSquareFeet);
            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0}", rounded), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingTop = paddingTop;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }

        private void AddSubtotalCostRow(string title, double valuesTotalCore, double valueTotalFitout, double valuesSiteSD, double valuesSiteSU, double grossSquareFeet, double grossSiteArea, Document document)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;
            float paddingTop = 1;

            double valueSumSD = valuesSiteSD;
            double valueSumSU = valuesSiteSU;
            double valueSumCore = valuesTotalCore;
            double valueSumFitout = valueTotalFitout;

            double GSF = grossSquareFeet;
            double GSA = grossSiteArea;

            double subTotalCore = Math.Round(valueSumCore, 2) * GSF;
            double subTotalSFCore = subTotalCore / GSF;

            double subTotalFitout = Math.Round(valueSumFitout, 2) * GSF;
            double subTotalSFFitout = subTotalFitout / GSF;

            double subTotalSD = valueSumSD * GSA;
            double subSFSD = subTotalSD / GSA;

            double subTotalSU = valueSumSU * GSA;
            double subSFSU = subTotalSU / GSA;

            double siteTotal = subTotalSU + subTotalSD;
            double siteSFTotal = siteTotal / GSF;

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.PaddingTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            table.AddCell(cell);

            var strSFCore = subTotalSFCore > 0 ? String.Format("{0:0,0.00}", subTotalSFCore) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFCore, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var rounded = RoundToNearestThousand(subTotalCore);
            var strCore = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strCore, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);


            var strSFFitout = subTotalSFFitout > 0 ? String.Format("{0:0,0.00}", subTotalSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalFitout);
            var strFitout = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSumSFCoreFitout = subTotalSFCore + subTotalSFFitout > 0 ? String.Format("{0:0,0.00}", subTotalSFCore + subTotalSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSFCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalCore + subTotalFitout);
            var strSumCoreFitout = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.PaddingTop = paddingTop;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);


            var strSFSD = subSFSD > 0 ? String.Format("{0:0,0.00}", subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFSD, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalSD);
            var strTotalSD = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalSD, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSFSU = subSFSU > 0 ? String.Format("{0:0,0.00}", subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalSU);
            var strTotalSU = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSumSFSDSU = (subSFSD + subSFSU) > 0 ? String.Format("{0:0,0.00}", subSFSD + subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSFSDSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalSD + subTotalSU);
            var strSumSDSU = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSDSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.PaddingTop = paddingTop;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0.00}", subTotalSFCore + subTotalSFFitout + subSFSD + subSFSU), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand((subTotalSFCore + subTotalSFFitout + subSFSD + subSFSU) * GSF);
            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0}", rounded), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.PaddingTop = paddingTop;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }

        /*==================================SUBTOTAL DIRECT CONSTRUCTION COSTS=======================================================*/

        private void AddSubtotalCostRowTC(string title, List<double> valuesTotalCore, List<double> valueTotalFitout, List<double> valuesTotalCoreTC, List<double> valueTotalFitoutTC, List<double> valuesSiteSD, List<double> valuesSiteSU, List<double> valuesSiteSDTC, List<double> valuesSiteSUTC, double grossSquareFeet, double grossSiteArea, Document document)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;
            float paddingTop = 1;

            double valueSumSD = valuesSiteSD.Sum() + valuesSiteSDTC.Sum();
            double valueSumSU = valuesSiteSU.Sum() + valuesSiteSUTC.Sum();
            double valueSumCore = valuesTotalCore.Sum() + valuesTotalCoreTC.Sum();
            double valueSumFitout = valueTotalFitout.Sum() + valueTotalFitoutTC.Sum();

            double GSF = grossSquareFeet;
            double GSA = grossSiteArea;

            double subTotalCore = Math.Round(valueSumCore, 2) * GSF;
            double subTotalSFCore = subTotalCore / GSF;

            double subTotalFitout = Math.Round(valueSumFitout, 2) * GSF;
            double subTotalSFFitout = subTotalFitout / GSF;

            double subTotalSD = valueSumSD * GSA;
            double subSFSD = subTotalSD / GSA;

            double subTotalSU = valueSumSU * GSA;
            double subSFSU = subTotalSU / GSA;

            double siteTotal = subTotalSU + subTotalSD;
            double siteSFTotal = siteTotal / GSA;

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 9, Font.BOLD)));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.PaddingTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            table.AddCell(cell);

            var strSFCore = subTotalSFCore > 0 ? String.Format("{0:0,0.00}", subTotalSFCore) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFCore, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var rounded = RoundToNearestThousand(subTotalCore);
            var strCore = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strCore, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);


            var strSFFitout = subTotalSFFitout > 0 ? String.Format("{0:0,0.00}", subTotalSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalFitout);
            var strFitout = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSumSFCoreFitout = subTotalSFCore + subTotalSFFitout > 0 ? String.Format("{0:0,0.00}", subTotalSFCore + subTotalSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSFCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalCore + subTotalFitout);
            var strSumCoreFitout = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.PaddingTop = paddingTop;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);


            var strSFSD = subSFSD > 0 ? String.Format("{0:0,0.00}", subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFSD, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalSD);
            var strTotalSD = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalSD, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSFSU = subSFSU > 0 ? String.Format("{0:0,0.00}", subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subTotalSU);
            var strTotalSU = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            var strSumSFSDSU = (subSFSD + subSFSU) > 0 ? String.Format("{0:0,0.00}", subSFSD + subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSFSDSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand((subSFSD + subSFSU) * GSA);
            var strSumSDSU = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSumSDSU, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.PaddingTop = paddingTop;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0.00}", subTotalSFCore + subTotalSFFitout + subSFSD + subSFSU), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.PaddingTop = paddingTop;
            table.AddCell(cell);

            rounded = RoundToNearestThousand((subTotalSFCore + subTotalSFFitout + subSFSD + subSFSU) * GSF);
            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0}", rounded), new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.PaddingTop = paddingTop;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }


        private void AddTitleOfRow(string title, Document document)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 7, Font.BOLD)));
            PdfPCell cell = new PdfPCell(new Phrase(p));
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Padding = 2;
            cell.Border = 0;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.Colspan = BUILDING_CORE_AND_SHEL_ALL + BUILDING_FITOUT_ALL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.Colspan = BUILDING_SUBTOTAL_ALL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.Colspan = SITE_SITE_DEVELOPMENT_ALL + SITE_SITE_UTILITIES_ALL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.Colspan = SITE_SUBTOTAL_ALL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = 0;
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.Colspan = BUILDING_SITE_ALL;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);


        }

        /*===================================================================================*/

        private void AddRowBC(double core, double fitout, double grossSquareFeet, string title, Document document, List<double> coreValueList, List<double> fitoutValueList, List<double> allCoreValue, List<double> allFitoutValue)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;

            coreValueList.Add(core);
            fitoutValueList.Add(fitout);
            allCoreValue.Add(core);
            allFitoutValue.Add(fitout);

            double coreValue = core;
            double fitoutValue = fitout;
            double GSF = grossSquareFeet;

            double subtotalBS = coreValue * GSF;
            double subSFBS = subtotalBS / GSF;

            double subFitoutTotalBS = fitoutValue * GSF;
            double subFitoutSFBS = subFitoutTotalBS / GSF;


            double buildingTotal = subtotalBS + subFitoutTotalBS;
            double buildingSFTotal = buildingTotal / GSF;

            Font font = new Font(FontFamily.HELVETICA, 7, Font.NORMAL);

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, font));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Padding = 2;
            cell.Border = 0;
            table.AddCell(cell);

            var strCore = coreValue > 0 ? String.Format("{0:0.00}", coreValue) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strCore, font));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);


            var rounded = RoundToNearestThousand(subtotalBS);
            var strTotalCore = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalCore, font));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var strFitout = fitoutValue > 0 ? String.Format("{0:0.00}", fitoutValue) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strFitout, font));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subFitoutTotalBS);
            var strTotalFitout = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalFitout, font));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);


            var strSFTotal = buildingSFTotal > 0 ? String.Format("{0:0.00}", buildingSFTotal) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFTotal, font));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            rounded = RoundToNearestThousand(buildingTotal);
            var strTotal = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotal, font));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var strBuildingSFTotal = buildingSFTotal > 0 ? String.Format("{0:0.00}", buildingSFTotal) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strBuildingSFTotal, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(new Phrase(p));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(buildingTotal);
            var strBuildingTotal = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strBuildingTotal, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(new Phrase(p));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }

        /*========================================================================================*/

        private void AddRowSD(double value, double grossSiteArea, double grossSquareFeet, string title, Document document, List<double> valueListSite)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;

            valueListSite.Add(value);

            double valueSD = value;
            double GSA = grossSiteArea;
            double GSF = grossSquareFeet;

            double subtotalSD = valueSD * GSA;
            double subtotalSDplusBC = valueSD * GSF;
            double subSFSD = valueSD;


            double subFitoutSFSD = subtotalSD / GSA;

            Font font = new Font(FontFamily.HELVETICA, 7, Font.NORMAL);

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Padding = 2;
            cell.Border = 0;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);


            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var strSubSFSD = Math.Round(subSFSD, 2) > 0 ? String.Format("{0:0.00}", subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFSD, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var rounded = RoundToNearestThousand(subtotalSD);
            var strSubtotalSD = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSD, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var _strSubSFSD = Math.Round(subSFSD, 2) > 0 ? String.Format("{0:0.00}", subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(_strSubSFSD, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSD);
            var _strSubtotalSD = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(_strSubtotalSD, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var _strSubSFSD_ = Math.Round(subSFSD, 2) > 0 ? String.Format("{0:0.00}", subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(_strSubSFSD_, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSDplusBC);
            p = new Paragraph();
            p.Add(new Chunk(String.Format("{0:0,0}", rounded), new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }

        /*=============================================================*/

        private void AddRowSU(double value, double grossSiteArea, double grossSquareFeet, string title, Document document, List<double> valueListSite)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;

            valueListSite.Add(value);

            double valueSU = value;
            double GSA = grossSiteArea;
            double GSF = grossSquareFeet;

            double subtotalSU = valueSU * GSA;
            double subtotalSUplusBC = valueSU * GSF;
            double subSFSU = valueSU;

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Padding = 2;
            cell.Border = 0;
            table.AddCell(cell);

            Font font = new Font(FontFamily.HELVETICA, 7, Font.NORMAL);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);


            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);


            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("-", font));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var strSubSFSD = Math.Round(subSFSU, 2) > 0 ? String.Format("{0:0.00}", subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFSD, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var rounded = RoundToNearestThousand(subtotalSU);
            var strSubtotalSU = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var strSubSFSU = Math.Round(subSFSU, 2) > 0 ? String.Format("{0:0.00}", subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);
           
            var _strSubtotalSU = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(_strSubtotalSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var _strSubSFSU = Math.Round(subSFSU, 2) > 0 ? String.Format("{0:0.00}", subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(_strSubSFSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSUplusBC);
            var _strSubtotalSUplusBC = rounded > 0 ? String.Format("{0:0,0}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(_strSubtotalSUplusBC, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }

        /*============================================================================================*/

        private void AddRowTC(double percent, double valueCoreBC, double valueFitoutBC, double valueSD, double valueSU, List<double> valuesTotalCore, List<double> valueTotalFitout, List<double> valuesSiteSD, List<double> valuesSiteSU, double grossSquareFeet, double grossSiteArea, string title, Document document)
        {
            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;

            valuesTotalCore.Add(valueCoreBC);
            if (valueFitoutBC != 0)
                valueTotalFitout.Add(valueFitoutBC);
            valuesSiteSD.Add(valueSD);
            valuesSiteSU.Add(valueSU);

            double GSF = grossSquareFeet;
            double GSA = grossSiteArea;

            double subtotalCore = valueCoreBC * GSF;
            double subSFCore = subtotalCore / GSF;

            double subtotalFitout = valueFitoutBC * GSF;
            double subSFFitout = subtotalFitout / GSF;

            double subtotalSD = valueSD * GSA;
            double subSFSD = valueSD;

            double subtotalSU = valueSU * GSA;
            double subSFSU = valueSU;

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL - FIRST_COLUMN_SUB;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Padding = 2;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var strPercent = Math.Round(percent * 100, 2) > 0 ? String.Format("{0:0.00}%", percent * 100) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strPercent, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = FIRST_COLUMN_SUB;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            var strSubSFCore = Math.Round(subSFCore, 2) > 0 ? String.Format("{0:0.00}", subSFCore) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFCore, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            var rounded = RoundToNearestThousand(subtotalCore);
            var strSubtotalCore = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalCore, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            var strSFFitout = subSFFitout > 0 ? String.Format("{0:0.00}", subSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFFitout, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalFitout);
            var strtotalFitout = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strtotalFitout, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            var strCoreFitout = Math.Round(subSFCore + subSFFitout, 2) > 0 ? String.Format("{0:0.00}", subSFCore + subSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalCore + subtotalFitout);
            var strSubtotalCoreFitout = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColorRight = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var strSubSFSC = Math.Round(subSFSD, 2) > 0 ? String.Format("{0:0.00}", subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFSC, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSD);
            var strSubtotalSD = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSD, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            var strSubSFSU = Math.Round(subSFSU, 2) > 0 ? String.Format("{0:0.00}", subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSU);
            var strSubtotalSU = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.Padding = 2;
            table.AddCell(cell);

            var strSubSFSD_SFSU = Math.Round(subSFSD + subSFSU, 2) > 0 ? String.Format("{0:0.00}", subSFSD + subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFSD_SFSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSU + subtotalSD);
            var strSubtotalSD_SU = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSD_SU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var strSubtotalCoreFitout_SDSU = Math.Round(subSFCore + subSFFitout + subSFSD + subSFSU, 2) > 0 ?
                    String.Format("{0:0.00}", subSFCore + subSFFitout + subSFSD + subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalCoreFitout_SDSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand((subSFCore + subSFFitout + subSFSD + subSFSU) * GSF);
            var strTotalCoreFitout_SDSU = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotalCoreFitout_SDSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }

        /*========================================================================================*/

        private void AddRowSoft(double value, List<double> softCost, double valuesTotalCore, double valueTotalFitout, double valuesSiteSD, double valuesSiteSU,
        List<double> softCostTotalCore, List<double> softCostTotalFitout, List<double> softCostTotalSD, List<double> softCostTotalSU, double grossSquareFeet, double grossSiteArea, string title, Document document)
        {

            PdfPTable table = new PdfPTable(COLUMN_ALL_COUNT);
            table.WidthPercentage = 100;

            double GSF = grossSquareFeet;
            double GSA = grossSiteArea;

            softCostTotalCore.Add(valuesTotalCore * value);
            double subtotalCore = valuesTotalCore * value * GSF;
            double subSFCore = valuesTotalCore * value;

            double sumTotalFitout = valueTotalFitout;
            softCostTotalFitout.Add(sumTotalFitout * value);
            double subtotalFitout = sumTotalFitout * value * GSF;
            double subSFFitout = sumTotalFitout * value;

            softCostTotalSD.Add(valuesSiteSD * value);
            double subtotalSD = valuesSiteSD * value * GSA;
            double subSFSD = valuesSiteSD * value;

            softCostTotalSU.Add(valuesSiteSU * value);
            double subtotalSU = valuesSiteSU * value * GSA;
            double subSFSU = valuesSiteSU * value;

            Paragraph p = new Paragraph();
            p.Add(new Chunk(title, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            PdfPCell cell = new PdfPCell(p);
            cell.Colspan = FIRST_COLUMN_ALL - FIRST_COLUMN_SUB;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Padding = 2;
            cell.Border = 0;
            table.AddCell(cell);

            var strValue = Math.Round(value * 100, 2) > 0 ? String.Format("{0:0.0}%", value * 100) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strValue, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = FIRST_COLUMN_SUB;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var strSubSFCore = Math.Round(subSFCore, 2) > 0 ? String.Format("{0:0.00}", subSFCore) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFCore, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var rounded = RoundToNearestThousand(subtotalCore);
            var strSubtotalCore = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";            
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalCore, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_CORE_AND_SHEL_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);


            var strSFFitout = Math.Round(subSFFitout, 2) > 0 ? String.Format("{0:0.00}", subSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSFFitout, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalFitout);
            var strtotalFitout = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strtotalFitout, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_FITOUT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var strCoreFitout = Math.Round(subSFCore + subSFFitout, 2) > 0 ? String.Format("{0:0.00}", subSFCore + subSFFitout) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalCore + subtotalFitout);
            var strSubtotalCoreFitout = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalCoreFitout, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var strSubSFSD = Math.Round(subSFSD, 2) > 0 ? String.Format("{0:0.00}", subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFSD, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSD);
            var strSubtotalSD = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSD, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_DEVELOPMENT_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var strSubSFSU = Math.Round(subSFSU, 2) > 0 ? String.Format("{0:0.00}", subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubSFSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_FIRST_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSD);
            var strSubtotalSU = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SITE_UTILITIES_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            table.AddCell(cell);

            var strSubtotalSFSDSFSU = Math.Round(subSFSU + subSFSD, 2) > 0 ? String.Format("{0:0.00}", subSFSU + subSFSD) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSFSDSFSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand(subtotalSD + subtotalSU);
            var strSubtotalSDSU = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubtotalSDSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = SITE_SUBTOTAL_SECOND_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            var strSubCoreFitoutSDSU = Math.Round(subSFCore + subSFFitout + subSFSD + subSFSU, 2) > 0 ?
                        String.Format("{0:0.00}", subSFCore + subSFFitout + subSFSD + subSFSU) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strSubCoreFitoutSDSU, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_FIRST_COLL;
            cell.Border = 0;
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.Padding = 2;
            table.AddCell(cell);

            rounded = RoundToNearestThousand((subSFCore + subSFFitout + subSFSD + subSFSU) * GSF);
            var strTotal = rounded > 0 ? String.Format("{0:0,00}", rounded) : "-";
            p = new Paragraph();
            p.Add(new Chunk(strTotal, new Font(FontFamily.HELVETICA, 7, Font.NORMAL)));
            cell = new PdfPCell(p);
            cell.BackgroundColor = new BaseColor(219, 229, 241);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = BUILDING_SITE_SECOND_COLL;
            cell.Border = 0;
            cell.Padding = 2;
            cell.Border = Rectangle.BOX;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderColor = new BaseColor(255, 255, 255);
            table.AddCell(cell);

            document.Add(table);
        }

        private void AddTemplatesItems(Document document, PdfWriter writer, string currentPage)
        {

            var logo = Image.GetInstance(System.Drawing.Image.FromFile(System.Web.Hosting.HostingEnvironment.MapPath("~/Images/logo.png")), System.Drawing.Imaging.ImageFormat.Png);
            logo.ScalePercent(40f);
            logo.SetAbsolutePosition(40f, 10f);
            document.Add(logo);
            ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, new Phrase(currentPage), 795f, 10f, 0);
            document.NewPage();
        }

        private double RoundToNearestThousand(double inputValue)
        {
            var interResult = Math.Round(inputValue, 2);

            return (Math.Round(interResult / 1000d, 0) * 1000);
        }
    }
}
