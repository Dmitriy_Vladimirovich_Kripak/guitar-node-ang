﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Models.Calculation;
using CalculationEngine;
using AutoMapper;
using Application.Models.Calculation.C10_E20;

namespace Application.BLL.Common
{
    public class ProjectModelsMapper
    {
        IMapper mapper;
        

        public ProjectModelsMapper()
        {
            Init();
        }

        public T MapTo<T>(ProjectViewModel projectModel)
        {      
            return mapper.Map<ProjectViewModel, T>(projectModel);
        }

        public T MapTo<T>(ProjectResultModel projectModel, T viewModel)
        {
            return mapper.Map<ProjectResultModel, T>(projectModel, viewModel);
        }

        private void Init()
        {

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProjectViewModel, A10ViewModel>();
                cfg.CreateMap<ProjectViewModel, A20ViewModel>();
                cfg.CreateMap<ProjectViewModel, A40ViewModel>();
                cfg.CreateMap<ProjectViewModel, A60ViewModel>();
                cfg.CreateMap<ProjectViewModel, A90ViewModel>();
                cfg.CreateMap<ProjectViewModel, B10ViewModel>()
                .ForMember(dest => dest.FloorConstructionType, opt => opt.MapFrom(src => src.FloorConstructionType.Replace(" ", "")))
                .ForMember(dest => dest.RoofConstructionType, opt => opt.MapFrom(src => src.RoofConstructionType.Replace(" ", "")));
                cfg.CreateMap<ProjectViewModel, B20ViewModel>();
                cfg.CreateMap<ProjectViewModel, B30ViewModel>();
                cfg.CreateMap<ProjectViewModel, C10ViewModel>();
                cfg.CreateMap<ProjectViewModel, C20ViewModel>();
                cfg.CreateMap<ProjectViewModel, C10FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, C20FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, D10ViewModel>();
                cfg.CreateMap<ProjectViewModel, D20ViewModel>();
                cfg.CreateMap<ProjectViewModel, D20FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, D30ViewModel>();
                cfg.CreateMap<ProjectViewModel, D30FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, D40ViewModel>();
                cfg.CreateMap<ProjectViewModel, D40FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, D50ViewModel>();
                cfg.CreateMap<ProjectViewModel, D50FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, D60FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, D60ViewModel>();
                cfg.CreateMap<ProjectViewModel, D70ViewModel>();
                cfg.CreateMap<ProjectViewModel, D70FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, D80ViewModel>();
                cfg.CreateMap<ProjectViewModel, D80FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, E10ViewModel>();
                cfg.CreateMap<ProjectViewModel, E10FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, E20ViewModel>();
                cfg.CreateMap<ProjectViewModel, E20FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, F10BCViewModel>();
                cfg.CreateMap<ProjectViewModel, F20BCViewModel>();
                cfg.CreateMap<ProjectViewModel, F30BCViewModel>();
                cfg.CreateMap<ProjectViewModel, G10SDViewModel>()/*.ForMember(dest => dest.DevelopedSiteArea, opt => opt.MapFrom(src => src.GrossSiteArea - src.BuildingFootprint))*/;
                cfg.CreateMap<ProjectViewModel, G20SDViewModel>()/*.ForMember(dest => dest.DevelopedSiteArea, opt => opt.MapFrom(src => src.GrossSiteArea - src.BuildingFootprint))*/;
                cfg.CreateMap<ProjectViewModel, G60SDViewModel>()/*.ForMember(dest => dest.DevelopedSiteArea, opt => opt.MapFrom(src => src.GrossSiteArea - src.BuildingFootprint))*/;
                cfg.CreateMap<ProjectViewModel, G30SUViewModel>()/*.ForMember(dest => dest.DevelopedSiteArea, opt => opt.MapFrom(src => src.GrossSiteArea - src.BuildingFootprint))*/; 
                cfg.CreateMap<ProjectViewModel, G40SUViewModel>()/*.ForMember(dest => dest.DevelopedSiteArea, opt => opt.MapFrom(src => src.GrossSiteArea - src.BuildingFootprint))*/; 
                cfg.CreateMap<ProjectViewModel, G50SUViewModel>()/*.ForMember(dest => dest.DevelopedSiteArea, opt => opt.MapFrom(src => src.GrossSiteArea - src.BuildingFootprint))*/; 

                cfg.CreateMap<ProjectViewModel, Z10BCViewModel>();
                 //.ForMember(dest => dest.A10F30Sum, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                 //src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout + 
                 //src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC))
                 
                cfg.CreateMap<ProjectViewModel, Z10SDViewModel>()/*.ForMember(dest => dest.G10G60Sum, opt => opt.MapFrom(src => src.G10SD + src.G20SD + src.G60SD))*/;
                cfg.CreateMap<ProjectViewModel, Z10SUViewModel>()/*.ForMember(dest => dest.G30G50SUSum, opt => opt.MapFrom(src => src.G30SU + src.G40SU + src.G50SU))*/;

                cfg.CreateMap<ProjectResultModel, Z10SDViewModel>();
                cfg.CreateMap<ProjectResultModel, Z10SUViewModel>();                
                cfg.CreateMap<ProjectResultModel, Z70SDViewModel>();
                cfg.CreateMap<ProjectResultModel, Z70SUViewModel>();
                cfg.CreateMap<ProjectResultModel, Z910BCViewModel>();
                cfg.CreateMap<ProjectResultModel, Z910SDViewModel>();
                cfg.CreateMap<ProjectResultModel, Z910SUViewModel>();
                cfg.CreateMap<ProjectResultModel, Z920BCViewModel>();
                cfg.CreateMap<ProjectResultModel, Z920SDViewModel>();
                cfg.CreateMap<ProjectResultModel, Z920SUViewModel>();
                cfg.CreateMap<ProjectResultModel, Z930BCViewModel>();
                cfg.CreateMap<ProjectResultModel, Z930SDViewModel>();
                cfg.CreateMap<ProjectResultModel, Z930SUViewModel>();
                cfg.CreateMap<ProjectResultModel, Z940BCViewModel>();                 
                cfg.CreateMap<ProjectResultModel, Z940SDViewModel>();
                cfg.CreateMap<ProjectResultModel, Z940SUViewModel>();
                cfg.CreateMap<ProjectResultModel, SoftCostViewModel>();
                cfg.CreateMap<ProjectResultModel, TotalProjectCostViewModel>();

                cfg.CreateMap<ProjectResultModel, Z10FitoutViewModel>();
                cfg.CreateMap<ProjectResultModel, Z70FitoutViewModel>();
                cfg.CreateMap<ProjectResultModel, Z910FitoutViewModel>();
                cfg.CreateMap<ProjectResultModel, Z920FitoutViewModel>();
                cfg.CreateMap<ProjectResultModel, Z930FitoutViewModel>();
                cfg.CreateMap<ProjectResultModel, Z940FitoutViewModel>();

                cfg.CreateMap<ProjectViewModel, Z10FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, Z70FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, Z910FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, Z920FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, Z930FitoutViewModel>();
                cfg.CreateMap<ProjectViewModel, Z940FitoutViewModel>();

                cfg.CreateMap<ProjectViewModel, Z70BCViewModel>();
                cfg.CreateMap<ProjectResultModel, Z70BCViewModel>()/*.ForMember(dest => dest.A10F30Sum, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                 src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout +
                 src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC))*/;
                cfg.CreateMap<ProjectViewModel, Z70SDViewModel>()/*.ForMember(dest => dest.G10G60Sum, opt => opt.MapFrom(src => src.G10SD + src.G20SD + src.G60SD))*/; 
                cfg.CreateMap<ProjectViewModel, Z70SUViewModel>()/*.ForMember(dest => dest.G30G50SUSum, opt => opt.MapFrom(src => src.G30SU + src.G40SU + src.G50SU))*/;
                cfg.CreateMap<ProjectViewModel, Z910BCViewModel>()/*.ForMember(dest => dest.A10F30Sum, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                // src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout +
                // src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC))*/;

                cfg.CreateMap<ProjectViewModel, Z910SDViewModel>()/*.ForMember(dest => dest.G10G60Sum, opt => opt.MapFrom(src => src.G10SD + src.G20SD + src.G60SD))*/ ;
                cfg.CreateMap<ProjectViewModel, Z910SUViewModel>()/*.ForMember(dest => dest.G30G50SUSum, opt => opt.MapFrom(src => src.G30SU + src.G40SU + src.G50SU))*/;
                cfg.CreateMap<ProjectViewModel, Z920BCViewModel>()/*.ForMember(dest => dest.A10F30Sum, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                 src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout +
                  src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC))*/;

                cfg.CreateMap<ProjectViewModel, Z920SDViewModel>()/*.ForMember(dest => dest.G10G60Sum, opt => opt.MapFrom(src => src.G10SD + src.G20SD + src.G60SD))*/ ;
                cfg.CreateMap<ProjectViewModel, Z920SUViewModel>()/*.ForMember(dest => dest.G30G50SUSum, opt => opt.MapFrom(src => src.G30SU + src.G40SU + src.G50SU))*/;
                cfg.CreateMap<ProjectViewModel, Z930BCViewModel>()/*.ForMember(dest => dest.A10F30Sum, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                 src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout +
                 src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC))*/;

                cfg.CreateMap<ProjectViewModel, Z930SDViewModel>()/*.ForMember(dest => dest.G10G60Sum, opt => opt.MapFrom(src => src.G10SD + src.G20SD + src.G60SD))*/; 
                cfg.CreateMap<ProjectViewModel, Z930SUViewModel>()/*.ForMember(dest => dest.G30G50SUSum, opt => opt.MapFrom(src => src.G30SU + src.G40SU + src.G50SU))*/;
                cfg.CreateMap<ProjectViewModel, Z940BCViewModel>()/*.ForMember(dest => dest.A10F30Sum, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                 src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout +
                 src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC))*/;
                cfg.CreateMap<ProjectViewModel, Z940SDViewModel>()/*.ForMember(dest => dest.G10G60Sum, opt => opt.MapFrom(src => src.G10SD + src.G20SD + src.G60SD))*/; 
                cfg.CreateMap<ProjectViewModel, Z940SUViewModel>()/*.ForMember(dest => dest.G30G50SUSum, opt => opt.MapFrom(src => src.G30SU + src.G40SU + src.G50SU))*/;

                cfg.CreateMap<ProjectViewModel, SoftCostViewModel>()/*.ForMember(dest => dest.TotalConstructionCosts, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                 src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout +
                 src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC + src.Z10BC + src.Z10SD +src.Z10SU + src.Z70BC + src.Z70SD + src.Z70SU + src.Z910BC + src.Z910SD + src.Z910SU + 
                 + src.Z920BC + src.Z920SD + src.Z920SU + src.Z930BC + src.Z930SD + src.Z930SU + src.Z940BC + src.Z940SD + src.Z940SU))*/;
                cfg.CreateMap<ProjectViewModel, SoftCostViewModel>();

                cfg.CreateMap<ProjectViewModel, TotalProjectCostViewModel>()/*.ForMember(dest => dest.TotalConstructionCosts, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                 src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout +
                 src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC + src.Z10BC + src.Z10SD + src.Z10SU + src.Z70BC + src.Z70SD + src.Z70SU + src.Z910BC + src.Z910SD + src.Z910SU +
                 +src.Z920BC + src.Z920SD + src.Z920SU + src.Z930BC + src.Z930SD + src.Z930SU + src.Z940BC + src.Z940SD + src.Z940SU))*/;
                cfg.CreateMap<ProjectResultModel, TotalProjectCostViewModel>();

                cfg.CreateMap<ProjectResultModel, Z10BCViewModel>();
                 //.ForMember(dest => dest.A10F30Sum, opt => opt.MapFrom(src => src.A10 + src.A20 + src.A40 + src.A90 + src.B10 + src.B20 + src.B30 + src.C10 + src.C10Fitout + src.C20 + src.C20Fitout +
                 //src.D10 + src.D20 + src.D20Fitout + src.D30 + src.D30Fitout + src.D40 + src.D40Fitout + src.D50 + src.D50Fitout + src.D60 + src.D60Fitout + src.D70 + src.D70Fitout + src.D80 + src.D80Fitout +
                 //src.E10 + src.E10Fitout + src.E20 + src.E20Fitout + src.F10BC + src.F20BC + src.F30BC));

                cfg.CreateMap<ProjectViewModel, ProjectResultModel>();
            });
            mapper = config.CreateMapper();


        }
       
    }
}
