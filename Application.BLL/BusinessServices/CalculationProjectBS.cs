﻿using Application.BLL.Common;
using Application.Common;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using CalculationEngine;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.BLLInterfaces.BusinessServicesInterfaces;

namespace Application.BLL.BusinessServices
{
    public class CalculationProjectBS : ICalculationProjectBS
    {
        private IFormulasContainer _formulasContainer;
        private ProjectModelsMapper _mapper;
        public CalculationProjectBS(IFormulasContainer formulasContainer, ProjectModelsMapper mapper)
        {
            _formulasContainer = formulasContainer;
            _mapper = mapper;
        }
        public ProjectResultModel CalcProject(ProjectViewModel model)
        {
            return calcAllFormul(model);
        }
        private ProjectResultModel calcAllFormul(ProjectViewModel model)
        {
            ProjectResultModel resultModel = new ProjectResultModel();
            resultModel = _mapper.MapTo<ProjectResultModel>(model);

            try
            {
                model.ConstructionStartDate = DateTime.ParseExact(model.ConstructionStartDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
                model.EstimateDate = DateTime.ParseExact(model.EstimateDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
                model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
                resultModel.GrossSquareFeet = model.GrossSquareFeet;
                resultModel.DevelopedSiteArea = model.DevelopedSiteArea;
                resultModel.GrossSiteArea = model.GrossSiteArea;
                resultModel.DesignFee = model.DesignFee;
                resultModel.FFE = model.FFE;
                resultModel.Technology = model.Technology;
                resultModel.Contingency = model.Contingency;
                resultModel.Misc = resultModel.Misc;
                

                resultModel.A10 = _formulasContainer.CalculateA10Foundations(_mapper.MapTo<A10ViewModel>(model));
                resultModel.A20 = _formulasContainer.CalculateA20Foundations(_mapper.MapTo<A20ViewModel>(model));
                resultModel.A40 = _formulasContainer.CalculateA40Foundations(_mapper.MapTo<A40ViewModel>(model));
                resultModel.A60 = _formulasContainer.CalculateA60Foundations(_mapper.MapTo<A60ViewModel>(model));
                resultModel.A90 = _formulasContainer.CalculateA90Foundations(_mapper.MapTo<A90ViewModel>(model));
                resultModel.B10 = _formulasContainer.CalculateB10Foundations(_mapper.MapTo<B10ViewModel>(model));
                resultModel.B20 = _formulasContainer.CalculateB20Foundations(_mapper.MapTo<B20ViewModel>(model));
                resultModel.B30 = _formulasContainer.CalculateB30Foundations(_mapper.MapTo<B30ViewModel>(model));
                resultModel.C10 = _formulasContainer.CalculateC10Foundations(_mapper.MapTo<C10ViewModel>(model));
                resultModel.C10Fitout = _formulasContainer.CalculateC10FitoutFoundations(_mapper.MapTo<C10FitoutViewModel>(model));
                resultModel.C20 = _formulasContainer.CalculateC20Foundations(_mapper.MapTo<C20ViewModel>(model));
                resultModel.C20Fitout = _formulasContainer.CalculateC20FitoutFoundations(_mapper.MapTo<C20FitoutViewModel>(model));
                resultModel.D10 = _formulasContainer.CalculateD10Foundations(_mapper.MapTo<D10ViewModel>(model));
                resultModel.D20 = _formulasContainer.CalculateD20Foundations(_mapper.MapTo<D20ViewModel>(model));
                resultModel.D20Fitout = _formulasContainer.CalculateD20FitoutFoundations(_mapper.MapTo<D20FitoutViewModel>(model));
                resultModel.D30 = _formulasContainer.CalculateD30Foundations(_mapper.MapTo<D30ViewModel>(model));
                resultModel.D30Fitout = _formulasContainer.CalculateD30FitoutFoundations(_mapper.MapTo<D30FitoutViewModel>(model));
                resultModel.D40 = _formulasContainer.CalculateD40Foundations(_mapper.MapTo<D40ViewModel>(model));
                resultModel.D40Fitout = _formulasContainer.CalculateD40FitoutFoundations(_mapper.MapTo<D40FitoutViewModel>(model));
                resultModel.D50 = _formulasContainer.CalculateD50Foundations(_mapper.MapTo<D50ViewModel>(model));
                resultModel.D50Fitout = _formulasContainer.CalculateD50FitoutFoundations(_mapper.MapTo<D50FitoutViewModel>(model));
                resultModel.D60 = _formulasContainer.CalculateD60Foundations(_mapper.MapTo<D60ViewModel>(model));
                resultModel.D60Fitout = _formulasContainer.CalculateD60FitoutFoundations(_mapper.MapTo<D60FitoutViewModel>(model));
                resultModel.D70 = _formulasContainer.CalculateD70Foundations(_mapper.MapTo<D70ViewModel>(model));
                resultModel.D70Fitout = _formulasContainer.CalculateD70FitoutFoundations(_mapper.MapTo<D70FitoutViewModel>(model));
                resultModel.D80 = _formulasContainer.CalculateD80Foundations(_mapper.MapTo<D80ViewModel>(model));
                resultModel.D80Fitout = _formulasContainer.CalculateD80FitoutFoundations(_mapper.MapTo<D80FitoutViewModel>(model));
                resultModel.E10 = _formulasContainer.CalculateE10Foundations(_mapper.MapTo<E10ViewModel>(model));
                resultModel.E10Fitout = _formulasContainer.CalculateE10FitoutFoundations(_mapper.MapTo<E10FitoutViewModel>(model));
                resultModel.E20 = _formulasContainer.CalculateE20Foundations(_mapper.MapTo<E20ViewModel>(model));
                resultModel.E20Fitout = _formulasContainer.CalculateE20FitoutFoundations(_mapper.MapTo<E20FitoutViewModel>(model));
                resultModel.F10BC = _formulasContainer.CalculateF10BC(_mapper.MapTo<F10BCViewModel>(model));
                resultModel.F20BC = _formulasContainer.CalculateF20BC(_mapper.MapTo<F20BCViewModel>(model));
                resultModel.F30BC = _formulasContainer.CalculateF30BC(_mapper.MapTo<F30BCViewModel>(model));
                resultModel.G10SD = _formulasContainer.CalculateG10SD(_mapper.MapTo<G10SDViewModel>(model));
                resultModel.G20SD = _formulasContainer.CalculateG20SD(_mapper.MapTo<G20SDViewModel>(model));
                resultModel.G60SD = _formulasContainer.CalculateG60SD(_mapper.MapTo<G60SDViewModel>(model));
                resultModel.G30SU = _formulasContainer.CalculateG30SU(_mapper.MapTo<G30SUViewModel>(model));
                resultModel.G40SU = _formulasContainer.CalculateG40SU(_mapper.MapTo<G40SUViewModel>(model));
                resultModel.G50SU = _formulasContainer.CalculateG50SU(_mapper.MapTo<G50SUViewModel>(model));

                resultModel.A10F30SumOfBC = _formulasContainer.CalculateA10F30SumOfBC(resultModel);
                resultModel.A10F30SumOfFitouts = _formulasContainer.CalculateA10F30SumOfFitouts(resultModel);
                resultModel.A10F30Sum = _formulasContainer.CalculateA10F30Sum(resultModel);
                resultModel.Z910BC = _formulasContainer.CalculateZ910BC(_mapper.MapTo<Z910BCViewModel>(resultModel, _mapper.MapTo<Z910BCViewModel>(model)));
                resultModel.Z920BC = _formulasContainer.CalculateZ920BC(_mapper.MapTo<Z920BCViewModel>(resultModel, _mapper.MapTo<Z920BCViewModel>(model)));
                resultModel.Z930BC = _formulasContainer.CalculateZ930BC(_mapper.MapTo<Z930BCViewModel>(resultModel, _mapper.MapTo<Z930BCViewModel>(model)));
                resultModel.Z10BC = _formulasContainer.CalculateZ10BC(_mapper.MapTo<Z10BCViewModel>(resultModel, _mapper.MapTo<Z10BCViewModel>(model)));
                resultModel.Z70BC = _formulasContainer.CalculateZ70BC(_mapper.MapTo<Z70BCViewModel>(resultModel, _mapper.MapTo<Z70BCViewModel>(model)));
                resultModel.Z940BC = _formulasContainer.CalculateZ940BC(_mapper.MapTo<Z940BCViewModel>(resultModel, _mapper.MapTo<Z940BCViewModel>(model)));

                resultModel.Z910Fitout = _formulasContainer.CalculateZ910Fitout(_mapper.MapTo<Z910FitoutViewModel>(resultModel, _mapper.MapTo<Z910FitoutViewModel>(model)));
                resultModel.Z920Fitout = _formulasContainer.CalculateZ920Fitout(_mapper.MapTo<Z920FitoutViewModel>(resultModel, _mapper.MapTo<Z920FitoutViewModel>(model)));
                resultModel.Z930Fitout = _formulasContainer.CalculateZ930Fitout(_mapper.MapTo<Z930FitoutViewModel>(resultModel, _mapper.MapTo<Z930FitoutViewModel>(model)));
                resultModel.Z10Fitout = _formulasContainer.CalculateZ10Fitout(_mapper.MapTo<Z10FitoutViewModel>(resultModel, _mapper.MapTo<Z10FitoutViewModel>(model)));
                resultModel.Z70Fitout = _formulasContainer.CalculateZ70Fitout(_mapper.MapTo<Z70FitoutViewModel>(resultModel, _mapper.MapTo<Z70FitoutViewModel>(model)));
                resultModel.Z940Fitout = _formulasContainer.CalculateZ940Fitout(_mapper.MapTo<Z940FitoutViewModel>(resultModel, _mapper.MapTo<Z940FitoutViewModel>(model)));

                resultModel.Z10Percent = _formulasContainer.GetBuildingTypeProjectStrategyFactor(_mapper.MapTo<Z10BCViewModel>(resultModel, _mapper.MapTo<Z10BCViewModel>(model)));
                resultModel.Z70Percent = _formulasContainer.GetBuildingTypeProjectStrategyFactor(_mapper.MapTo<Z70BCViewModel>(resultModel, _mapper.MapTo<Z70BCViewModel>(model)));
                resultModel.Z910Percent = _mapper.MapTo<Z910BCViewModel>(resultModel, _mapper.MapTo<Z910BCViewModel>(model)).ContingenciesDesign;
                resultModel.Z920Percent = _formulasContainer.CalculateZ920Rate(_mapper.MapTo<Z920BCViewModel>(resultModel, _mapper.MapTo<Z920BCViewModel>(model)));
                resultModel.Z930Percent = _formulasContainer.GetBuildingTypeProjectStrategyFactor(_mapper.MapTo<Z930BCViewModel>(resultModel, _mapper.MapTo<Z930BCViewModel>(model)));
                resultModel.Z940Percent = _formulasContainer.GetBuildingTypeProjectStrategyFactor(_mapper.MapTo<Z940BCViewModel>(resultModel, _mapper.MapTo<Z940BCViewModel>(model)));

                resultModel.G10G60Sum = _formulasContainer.CalculateG10G60Sum(resultModel);
                resultModel.Z910SD = _formulasContainer.CalculateZ910SD(_mapper.MapTo<Z910SDViewModel>(resultModel, _mapper.MapTo<Z910SDViewModel>(model)));
                resultModel.Z920SD = _formulasContainer.CalculateZ920SD(_mapper.MapTo<Z920SDViewModel>(resultModel, _mapper.MapTo<Z920SDViewModel>(model)));
                resultModel.Z930SD = _formulasContainer.CalculateZ930SD(_mapper.MapTo<Z930SDViewModel>(resultModel, _mapper.MapTo<Z930SDViewModel>(model)));
                resultModel.Z10SD = _formulasContainer.CalculateZ10SD(_mapper.MapTo<Z10SDViewModel>(resultModel, _mapper.MapTo<Z10SDViewModel>(model)));
                resultModel.Z70SD = _formulasContainer.CalculateZ70SD(_mapper.MapTo<Z70SDViewModel>(resultModel, _mapper.MapTo<Z70SDViewModel>(model)));
                resultModel.Z940SD = _formulasContainer.CalculateZ940SD(_mapper.MapTo<Z940SDViewModel>(resultModel, _mapper.MapTo<Z940SDViewModel>(model)));
                resultModel.G30G50SUSum = _formulasContainer.CalculateG30G50SUSum(resultModel);
                resultModel.Z910SU = _formulasContainer.CalculateZ910SU(_mapper.MapTo<Z910SUViewModel>(resultModel, _mapper.MapTo<Z910SUViewModel>(model)));
                resultModel.Z920SU = _formulasContainer.CalculateZ920SU(_mapper.MapTo<Z920SUViewModel>(resultModel, _mapper.MapTo<Z920SUViewModel>(model)));
                resultModel.Z930SU = _formulasContainer.CalculateZ930SU(_mapper.MapTo<Z930SUViewModel>(resultModel, _mapper.MapTo<Z930SUViewModel>(model)));
                resultModel.Z10SU = _formulasContainer.CalculateZ10SU(_mapper.MapTo<Z10SUViewModel>(resultModel, _mapper.MapTo<Z10SUViewModel>(model)));
                resultModel.Z70SU = _formulasContainer.CalculateZ70SU(_mapper.MapTo<Z70SUViewModel>(resultModel, _mapper.MapTo<Z70SUViewModel>(model)));
                resultModel.Z940SU = _formulasContainer.CalculateZ940SU(_mapper.MapTo<Z940SUViewModel>(resultModel, _mapper.MapTo<Z940SUViewModel>(model)));

                resultModel.DataDate = _formulasContainer.CalculateDataDate(_mapper.MapTo<Z920BCViewModel>(resultModel, _mapper.MapTo<Z920BCViewModel>(model)));
                resultModel.StrDataDate = _formulasContainer.CalculateStrDataDate(_mapper.MapTo<Z920BCViewModel>(resultModel, _mapper.MapTo<Z920BCViewModel>(model)));

                // Viewer BUILDING CONSTRUCTION
                resultModel.A_Substructure = _formulasContainer.CalculateA_Substructure(resultModel);
                resultModel.B_Shell = _formulasContainer.CalculateB_Shell(resultModel);
                resultModel.C_Interiors = _formulasContainer.CalculateC_Interiors(resultModel);
                resultModel.D_Services = _formulasContainer.CalculateD_Services(resultModel);
                resultModel.E_EquipmentFurnishings = _formulasContainer.CalculateE_EquipmentFurnishings(resultModel);
                resultModel.F_ConstructionDemolition = _formulasContainer.CalculateF_ConstructionDemolition(resultModel);
                resultModel.SubtotalDirectCost_building = _formulasContainer.CalculateSubtotalDirectCost_building(resultModel);
                resultModel.ContractorMarkups_building = _formulasContainer.CalculateContractorMarkups_building(resultModel);
                resultModel.Contingencies_building = _formulasContainer.CalculateContingencies_building(resultModel);
                resultModel.TotalBuildingConstruction = _formulasContainer.CalculateTotalBuildingConstruction(resultModel);

                // Viewer SITE CONSTRUCTION
                resultModel.SiteDevelopment_site = _formulasContainer.CalculateSiteDevelopment_site(resultModel);
                resultModel.SiteUtilities_site = _formulasContainer.CalculateSiteUtilities_site(resultModel);
                resultModel.SubtotalDirectCost_site = _formulasContainer.CalculateSubtotalDirectCost_site(resultModel);
                resultModel.ContractorMarkups_site = _formulasContainer.CalculateContractorMarkups_site(resultModel);
                resultModel.Contingencies_site = _formulasContainer.CalculateContingencies_site(resultModel);
                resultModel.TotalSiteConstruction_site = _formulasContainer.CalculateTotalSiteConstruction_site(resultModel);

                //Viewer TOTALS
                resultModel.TotalConstructionCosts = _formulasContainer.CalculateTotalConstructionCosts(resultModel);
                resultModel.SoftCosts = _formulasContainer.CalculateSoftCosts(resultModel);
                resultModel.TotalProjectCosts = _formulasContainer.CalculateTotalProjectCosts(resultModel);

            }
            catch (Exception e)
            {
                resultModel.status = e.Message;
                throw new Exception("Project wasn't calculated", e);
            }

            return resultModel;
        }
    }
}
