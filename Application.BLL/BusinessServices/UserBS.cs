﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Models;
using Application.DAL;
using Application.Models.Calculation;
using CalculationEngine;
using Application.Entities;
using Application.Common;
using Application.Models.Calculation.C10_E20;
using System.Globalization;
using Application.Models.Manage;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Models.DTO;
using System.Web;

namespace Application.BLL.BusinessServices
{
    public class UserBS: IUserBS
    {
        private IDbContextFactory _dbContextFactory;
        private IAuditTrailBS _auditTrailBS;

        public UserBS(IDbContextFactory dbContextFactory, IAuditTrailBS auditTrailBS)
        {
            _dbContextFactory = dbContextFactory;
            _auditTrailBS = auditTrailBS;
        }

        public List<UserView> GetAllUsers()
        { 
            using (var context = _dbContextFactory.Create())
            {
                return context.Users.ToList().Select((user) =>
                {
                    return new UserView
                    {
                        Id = user.Id,
                        Name = user.UserName,
                        Email = user.Email,
                        Status = context.UserBlocks.FirstOrDefault(x => x.UserId == user.Id) == null ? false : true
                    };
                }).ToList();              
            }
        }
      
        public UserView GetUserById(string id)
        {
            using (var context = _dbContextFactory.Create())
            {
                return context.Users.Where(a => a.Id == id).ToList().Select(user =>
                new UserView
                {
                    Id = user.Id,
                    Name = user.UserName,
                    Email = user.Email
                }).FirstOrDefault();  
            }
        }

        public List<UserView> ChangeUserBlockStatus(UserView model)
        {          
            using (var context = _dbContextFactory.Create())
            {
                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));
                foreach (var role in manage.GetRoles(model.Id))
                {
                    if (role == "Admin")
                        return context.Users.ToList().Select((users) =>
                        {
                            return new UserView
                            {
                                Id = users.Id,
                                Name = users.UserName,
                                Email = users.Email,
                                Status = context.UserBlocks.FirstOrDefault(x => x.UserId == users.Id) == null ? false : true
                            };
                        }).ToList();
                }
                try
                {                   
                    if (model.Status)
                    {
                        var user = new UserBlock
                        {
                            //User = manage.FindById(model.Id)
                            UserId = model.Id
                        };
                        context.UserBlocks.Add(user);
                    }
                  
                    else
                    {
                        //context.UserBlocks.Attach(user);
                        var user = context.UserBlocks.FirstOrDefault(x => x.UserId == model.Id );
                        context.UserBlocks.Remove(user);
                    }
                    context.SaveChanges();
                    return context.Users.ToList().Select((users) =>
                    {
                        return new UserView
                        {
                            Id = users.Id,
                            Name = users.UserName,
                            Email = users.Email,
                            Status = context.UserBlocks.FirstOrDefault(x => x.UserId == users.Id) == null ? false : true                        
                        };
                    }).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception("User Block  Status wasn't changed", e);
                }
            }
          
        }

        public bool IsUserBlock(ApplicationUser user)
        {
            if (user == null)
                return false;

            using (var context = _dbContextFactory.Create())
            {
                var blockUser = context.UserBlocks.FirstOrDefault(x => x.UserId ==  user.Id);
                if (blockUser != null)
                     return true;
                return false;
            }
        }

        public string GetUserRole(string userName)
        {
            using (var context = _dbContextFactory.Create())
            {
                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));
                var user = manage.FindByName(userName);
                foreach (var role in manage.GetRoles(user.Id))
                {
                    if (role == "Admin")
                        return "Admin";
                }
                return "User";
            }
        }

        public UserView UpdateUserInfo(UserView model)
        {
            using (var context = _dbContextFactory.Create())
            {
                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));
                ApplicationUser user = manage.FindById(model.Id);
                if (user == null)
                {
                    
                    user = new ApplicationUser { UserName = model.Email, Email = model.Email};
                    var creationResult = manage.CreateAsync(user, model.Password);
                    if (creationResult.Result.Succeeded)
                    {
                        model.Id = manage.Find(model.Email, model.Password).Id;
                        return model;
                    }
                    else
                    {
                        throw new Exception(creationResult.Result.Errors.FirstOrDefault());
                    }
                }
                user.PasswordHash = manage.PasswordHasher.HashPassword(model.Password);
                var result = manage.Update(user);
                if (result.Succeeded)
                {
                    _auditTrailBS.ProcessUser(user, false);

                    //return "Update";
                    return model;
                }
                else
                {
                    if(result.Errors != null)
                    {
                        string resMsg = string.Empty;
                        result.Errors.All(e =>
                        {
                            resMsg += e + Environment.NewLine;
                            return true;
                        });
                        //return resMsg;
                        return model;
                    }
                    else
                    {
                        //return "User was not updated";
                        return model;
                    }                    
                }
                
            }
        }

        public ApplicationUser GetUserByName(string userName)
        {
            using (var context = _dbContextFactory.Create())
            {
                return context.Users.FirstOrDefault(x => x.UserName == userName);
            }
        }

        public UserInfoDTO GetUserInfo(string userName)
        {
            using (var context = _dbContextFactory.Create())
            {
                var user = context.Users.FirstOrDefault(x => x.UserName == userName);

                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));

                var roles = manage.GetRoles(user.Id);

                return new UserInfoDTO()
                {
                    Id = user.Id,

                    Username = user.UserName,

                    Email = user.UserName,

                    Roles = roles.ToArray()
                };
            }
        }

    }
}
