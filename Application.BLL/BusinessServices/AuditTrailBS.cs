﻿using Application.BLL.Common;
using Application.Common;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using CalculationEngine;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Infrastructure;
using Application.DAL;
using Application.Models.AuditTrail;
using Application.Models;
using System.Data.Entity.Core.Objects;
using Application.Entities.AuditTrailSupportEntities;

namespace Application.BLL.BusinessServices
{
    public class AuditTrailBS : IAuditTrailBS
    {
        private IDbContextFactory _dbContextFactory;


        public AuditTrailBS(IDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public PaginationAuditTrailsViewModel GetAuditTrailInfoForPaging(PaginationRequest paging)
        {
            int totalCount;
            var resultModel = new PaginationAuditTrailsViewModel();
            using (var context = _dbContextFactory.Create())
            {
                var auditTrailValuesQuery = from auditTrailValues in context.AuditTrailValues
                                            select auditTrailValues;

                var auditTrailEntitiesQuery = from auditTrailEntities in context.AuditTrailEntitys
                                              where auditTrailEntities.AuditTrailValues.All(q => auditTrailValuesQuery.Contains(q))
                                              select auditTrailEntities;

                var query = from auditTrails in context.AuditTrails
                            where auditTrails.AuditTrailEntities.All(q => auditTrailEntitiesQuery.Contains(q))
                            select new AuditTrailViewModel
                            {
                                Id = auditTrails.Id,
                                UserName = auditTrails.UserName,
                                Date = auditTrails.Date,
                                Action = auditTrails.AuditTrailEntities.FirstOrDefault(a => a.AuditTrailId == auditTrails.Id && a.AuditTrailEntityParentId == null).Action,
                                EntityName = auditTrails.AuditTrailEntities.FirstOrDefault(a => a.AuditTrailId == auditTrails.Id && a.AuditTrailEntityParentId == null).EntityName,
                                EntityType = auditTrails.AuditTrailEntities.FirstOrDefault(a => a.AuditTrailId == auditTrails.Id && a.AuditTrailEntityParentId == null).EntityType
                            };

                var predicate = HelperInstancePredicateForFilter.GetPredicate(paging);

                query = query.Where(predicate);

                IList<AuditTrailViewModel> resultList = GenericSorterPager.GetSortedPagedList<AuditTrailViewModel>(query, paging, out totalCount);

                var auditTrailIds = resultList.Select(r => r.Id).ToArray();

                foreach (var auditTrailEntity in context.AuditTrailEntitys.Where(ate => auditTrailIds.Contains(ate.AuditTrailId)).OrderBy(ate => ate.AuditTrailId).ToList())
                {
                    var auditTrailViewModel = resultList.FirstOrDefault(rl => rl.Id == auditTrailEntity.AuditTrailId);
                    if (auditTrailViewModel.AuditTrailValues == null)
                    {
                        auditTrailViewModel.AuditTrailValues = new List<AuditTrailValuesViewModel>();
                    }
                    foreach (var auditTrailValue in auditTrailEntity.AuditTrailValues)
                    {
                        auditTrailViewModel.AuditTrailValues.Add(new AuditTrailValuesViewModel()
                        {
                            Action = auditTrailValue.AuditTrailEntity.Action,
                            EntityType = auditTrailValue.AuditTrailEntity.EntityType,
                            PropertyName = auditTrailValue.PropertyName,
                            NewValue = auditTrailValue.NewValue,
                            OldValue = auditTrailValue.OldValue,
                        });
                    }
                }

                resultModel.AuditTrails = resultList;
                resultModel.TotalCount = totalCount;

                return resultModel;
            }
        }

        public IList<AuditTrailViewModel> GetAuditTrailInfo()
        {
            IList<AuditTrailViewModel> complexAudit = null;

            using (var context = _dbContextFactory.Create())
            {
                complexAudit = context.AuditTrails.ToList().Select((audit) =>
                {
                    return new AuditTrailViewModel
                    {
                        Id = audit.Id,
                        Date = audit.Date,
                        UserName = audit.UserName,
                        EntityName = audit.AuditTrailEntities.FirstOrDefault(a => a.AuditTrailId == audit.Id && a.AuditTrailEntityParentId == null).EntityName,
                        EntityType = audit.AuditTrailEntities.FirstOrDefault(a => a.AuditTrailId == audit.Id && a.AuditTrailEntityParentId == null).EntityType,
                        Action = audit.AuditTrailEntities.Where(e => e.AuditTrailId == audit.Id && e.AuditTrailEntityParentId == null).First().Action,
                        AuditTrailValues = audit.AuditTrailEntities.SelectMany(ate => ate.AuditTrailValues).Select((value) =>
                                           {
                                               return new AuditTrailValuesViewModel
                                               {
                                                   Action = value.AuditTrailEntity.Action,
                                                   EntityType = value.AuditTrailEntity.EntityType,
                                                   PropertyName = value.PropertyName,
                                                   NewValue = value.NewValue,
                                                   OldValue = value.OldValue,
                                               };
                                           }).ToList()
                    };
                }).ToList();
            }

            return complexAudit;
        }

        public void ProcessUser(ApplicationUser user, bool adding)
        {
            using (var context = _dbContextFactory.Create())
            {
                var userName = System.Web.HttpContext.Current.User.Identity.Name;
                AuditTrail auditTrail = new AuditTrail()
                {
                    UserName = userName,
                    Date = DateTime.Now,
                };
                AuditTrailEntity projectAuditEntity = new AuditTrailEntity()
                {

                    AuditTrails = auditTrail,
                    EntityName = userName,
                    Action = adding ? "Added" : "Modified",
                    EntityType = "User"

                };
                context.AuditTrails.Add(auditTrail);
                context.AuditTrailEntitys.Add(projectAuditEntity);
                var projectAuditValues = new AuditTrailValues()
                {
                    AuditTrailEntity = projectAuditEntity,
                    NewValue = "********",
                    PropertyName = "Password"
                };
                context.AuditTrailValues.Add(projectAuditValues);
                if (adding)
                {
                    projectAuditValues = new AuditTrailValues()
                    {
                        AuditTrailEntity = projectAuditEntity,
                        NewValue = user.Email,
                        PropertyName = "Email"
                    };
                    context.AuditTrailValues.Add(projectAuditValues);

                }
                context.SaveChanges();
            }
        }

        public AuditTrail FormAuditData(IList<DbEntityEntry> added, IList<CustomDbEntityEntry> modified, IList<CustomDbEntityEntry> deleted, ApplicationUser user)
        {
            AuditTrail audit = null;

            var entities = new List<CustomDbEntityEntry>();
            entities.AddRange(modified);
            entities.AddRange(deleted);
            added.All(a =>
            {
                entities.Add(new CustomDbEntityEntry(a));
                return true;
            });

            // only one audit entity is possible, so when method forms auditData we will stop processing and return formed data

            audit = ProcessProject(entities, added.Count > 0, deleted.Count > 0);
            if (audit != null)
            {
                return audit;
            }
            audit = ProcessBaselineVariables(entities, added.Count > 0);
            if (audit != null)
            {
                return audit;
            }
            audit = ProcessUsers(entities, added.Count > 0, deleted.Count > 0, user);
            if (audit != null)
            {
                return audit;
            }

            return audit;
        }

        private AuditTrail ProcessUsers(IList<CustomDbEntityEntry> entities, bool adding, bool deleting, ApplicationUser user)
        {
            AuditTrail auditTrail = null;
            if (adding || deleting)
            {
                var userBlockEntityData = entities.Where(p => p.EntityType == typeof(UserBlock)).FirstOrDefault();
                //added block to user
                if (userBlockEntityData != null)
                {
                    string entityName = userBlockEntityData.ParentName;
                    if (string.IsNullOrEmpty(entityName) && user != null)
                        entityName = user.Email;

                    auditTrail = new AuditTrail()
                    {
                        UserName = System.Web.HttpContext.Current.User.Identity.Name,
                        Date = DateTime.Now,
                        AuditTrailEntities = new List<AuditTrailEntity>() {
                            new AuditTrailEntity()
                                {
                                    AuditTrails = auditTrail,
                                    EntityId = int.Parse(AsStringOrEmpty(userBlockEntityData.OriginalValues, "Id")),
                                    EntityName = entityName,
                                    Action = "Modified",
                                    EntityType = "User",
                                    AuditTrailValues = new List<AuditTrailValues>()
                                }
                        }
                    };
                    auditTrail.AuditTrailEntities.First().AuditTrailValues.Add(
                        new AuditTrailValues()
                        {
                            AuditTrailEntity = auditTrail.AuditTrailEntities.First(),
                            OldValue = adding ? "Unblocked" : "Blocked",
                            NewValue = adding ? "Blocked" : "Unblocked",
                            PropertyName = "Block Status",
                        }
                    );
                }
            }
            return auditTrail;
        }

        private AuditTrail ProcessBaselineVariables(IList<CustomDbEntityEntry> entities, bool adding)
        {
            AuditTrail auditTrail = null;
            //BaselineVariables can be modified only
            if (!adding)
            {
                var baselineVariableEntityData = entities.Where(p => p.EntityType == typeof(BaselineVariable)).FirstOrDefault();
                if (baselineVariableEntityData != null)
                {
                    auditTrail = new AuditTrail()
                    {
                        UserName = System.Web.HttpContext.Current.User.Identity.Name,
                        Date = DateTime.Now,
                        AuditTrailEntities = new List<AuditTrailEntity>() {
                            new AuditTrailEntity()
                                {
                                    AuditTrails = auditTrail,
                                    EntityId = int.Parse(AsStringOrEmpty(baselineVariableEntityData.CurrentValues, "Id")),
                                    EntityName = AsStringOrEmpty(baselineVariableEntityData.CurrentValues, "Name"),
                                    Action = "Modified",
                                    EntityType = "Baseline Variable",
                                    AuditTrailValues = new List<AuditTrailValues>()
                                }
                        }
                    };
                    auditTrail.AuditTrailEntities.First().AuditTrailValues.Add(
                        new AuditTrailValues()
                        {
                            AuditTrailEntity = auditTrail.AuditTrailEntities.First(),
                            OldValue = AsStringOrEmpty(baselineVariableEntityData.OriginalValues, "Value"),
                            NewValue = AsStringOrEmpty(baselineVariableEntityData.CurrentValues, "Value"),
                            PropertyName = AsStringOrEmpty(baselineVariableEntityData.CurrentValues, "Name"),
                        }
                    );

                }
            }
            return auditTrail;
        }

        private AuditTrail ProcessProject(IList<CustomDbEntityEntry> entities, bool adding, bool deleting)
        {

            AuditTrail auditTrail = null;
            var projectEntityData = entities.Where(p => p.EntityType == typeof(Project)).FirstOrDefault();

            if (projectEntityData != null && deleting)
            {
                auditTrail = new AuditTrail() { UserName = System.Web.HttpContext.Current.User.Identity.Name, Date = DateTime.Now };
                int currentProjectId = int.Parse(projectEntityData.OriginalValues["Id"]);
                string currentProjectName = projectEntityData.OriginalValues["Name"];

                var projectAuditEntity = CreateProjectAuditTrailEntity(
                    auditTrail, currentProjectId, currentProjectName, adding, deleting);

                auditTrail.AuditTrailEntities = new List<AuditTrailEntity>() { projectAuditEntity };

                return auditTrail;
            }

            if (projectEntityData != null)
            {
                auditTrail = new AuditTrail() { UserName = System.Web.HttpContext.Current.User.Identity.Name, Date = DateTime.Now };

                var projectAuditEntity = CreateProjectAuditTrailEntity(
                    auditTrail, int.Parse(AsStringOrEmpty(projectEntityData.CurrentValues, "Id")), AsStringOrEmpty(projectEntityData.CurrentValues, "Name"), adding);

                auditTrail.AuditTrailEntities = new List<AuditTrailEntity>() { projectAuditEntity };

                FillAuditTrailProjectProperties(projectEntityData, projectAuditEntity, adding);

                if (entities.Where(x => x.EntityType == typeof(ProjectValues)).Count() > 0)
                {
                    var projectValuesAuditEntity = CreateProjectValuesAuditTrailEntity(auditTrail, projectAuditEntity, entities, adding);
                    auditTrail.AuditTrailEntities.Add(projectValuesAuditEntity);
                }
            }
            // we have ProjectValues but haven't Project entities - user modified ProjectValues only
            else if (entities.Where(x => x.EntityType == typeof(ProjectValues)).Count() > 0)
            {
                auditTrail = new AuditTrail() { UserName = System.Web.HttpContext.Current.User.Identity.Name, Date = DateTime.Now };
                bool addingProject = false;
                //get parent information from any ProjectValues entity, for example from first entity
                var projectValueFirstEntity = entities.Where(x => x.EntityType == typeof(ProjectValues)).First();
                var projectAuditEntity = CreateProjectAuditTrailEntity(
                    auditTrail, projectValueFirstEntity.ParentId, projectValueFirstEntity.ParentName, addingProject);

                var projectValuesAuditEntity = CreateProjectValuesAuditTrailEntity(auditTrail, projectAuditEntity, entities, adding);
                auditTrail.AuditTrailEntities = new List<AuditTrailEntity>() { projectAuditEntity, projectValuesAuditEntity };
            }
            return auditTrail;
        }

        private AuditTrailEntity CreateProjectAuditTrailEntity(AuditTrail auditTrail, int entityId, string entityName, bool addingProject, bool deletingProject = false)
        {
            if(deletingProject)
            {
                return new AuditTrailEntity()
                {
                    AuditTrails = auditTrail,
                    EntityId = entityId,
                    EntityName = entityName,
                    Action = "Deleted",
                    EntityType = "Project",
                    AuditTrailValues = new List<AuditTrailValues>()
                };
            }

            return new AuditTrailEntity()
            {
                AuditTrails = auditTrail,
                EntityId = entityId,
                EntityName = entityName,
                Action = addingProject ? "Added" : "Modified",
                EntityType = "Project",
                AuditTrailValues = new List<AuditTrailValues>()
            };
        }

        private AuditTrailEntity CreateProjectValuesAuditTrailEntity(AuditTrail auditTrail, AuditTrailEntity projectAuditEntity, IList<CustomDbEntityEntry> entities, bool addingProject)
        {
            var projectValueEntities = entities.Where(x => x.EntityType == typeof(ProjectValues));
            if (projectValueEntities.Count() > 0)
            {
                AuditTrailEntity projectValuesAuditEntity = new AuditTrailEntity()
                {
                    AuditTrails = auditTrail,
                    EntityId = projectAuditEntity.EntityId,
                    EntityName = "ProjectValues",
                    Parent = projectAuditEntity,
                    Action = addingProject ? "Added" : "Modified",
                    EntityType = "User Inputs"
                };
                projectValuesAuditEntity.AuditTrailValues = new List<AuditTrailValues>();
                foreach (var projectValueEntity in projectValueEntities)
                {
                    var value = CreateAuditTrailValueFromProjectValueDbEntityEntry(projectValueEntity, projectValuesAuditEntity, addingProject);
                    if (value != null)
                        projectValuesAuditEntity.AuditTrailValues.Add(value);
                }

                return projectValuesAuditEntity;
            }
            return null;
        }

        private void FillAuditTrailProjectProperties(CustomDbEntityEntry dbEntityEntry, AuditTrailEntity auditTrailEntity, bool addingProject)
        {
            auditTrailEntity.AuditTrailValues = dbEntityEntry.CurrentValues.PropertyNames
                .Where(p => addingProject || AsStringOrEmpty(dbEntityEntry.CurrentValues, p) != AsStringOrEmpty(dbEntityEntry.OriginalValues, p))
                .Select(p =>
                       new AuditTrailValues()
                       {
                           AuditTrailEntity = auditTrailEntity,
                           OldValue = addingProject ? string.Empty : AsStringOrEmpty(dbEntityEntry.OriginalValues, p),
                           NewValue = AsStringOrEmpty(dbEntityEntry.CurrentValues, p),
                           PropertyName = p
                       }
                ).ToList();
        }

        private AuditTrailValues CreateAuditTrailValueFromProjectValueDbEntityEntry(CustomDbEntityEntry dbEntityEntry, AuditTrailEntity auditTrailEntity, bool addingProject)
        {
            if (addingProject)
            {
                return new AuditTrailValues()
                {
                    AuditTrailEntity = auditTrailEntity,
                    OldValue = string.Empty,
                    NewValue = AsStringOrEmpty(dbEntityEntry.CurrentValues, "PropertyValue"),
                    PropertyName = AsStringOrEmpty(dbEntityEntry.CurrentValues, "PropertyName"),
                };
            }
            else if (AsStringOrEmpty(dbEntityEntry.CurrentValues, "PropertyValue") != AsStringOrEmpty(dbEntityEntry.OriginalValues, "PropertyValue"))
            {
                return new AuditTrailValues()
                {
                    AuditTrailEntity = auditTrailEntity,
                    OldValue = AsStringOrEmpty(dbEntityEntry.OriginalValues, "PropertyValue"),
                    NewValue = AsStringOrEmpty(dbEntityEntry.CurrentValues, "PropertyValue"),
                    PropertyName = AsStringOrEmpty(dbEntityEntry.CurrentValues, "PropertyName"),
                };
            }
            else return null;
        }

        private string AsStringOrEmpty(CustomDbPropertyValues values, string propertyName)
        {
            if (values != null)
            {
                if (values[propertyName] == null)
                    return string.Empty;
                else
                    return values[propertyName].ToString();
            }

            return string.Empty;
        }

    }

}