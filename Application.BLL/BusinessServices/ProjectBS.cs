﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Models;
using Application.DAL;
using Application.Models.Calculation;
using CalculationEngine;
using Application.Entities;
using Application.Common;
using Application.Models.Calculation.C10_E20;
using System.Globalization;
using Application.Models.Manage;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Reflection;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using static iTextSharp.text.Font;
using Application.BLL.Common;
using Application.BLLInterfaces.BusinessServicesInterfaces;

namespace Application.BLL.BusinessServices
{
    public class ProjectBS : IProjectBS
    {
        private IDbContextFactory _dbContextFactory;
        private ICalculationProjectBS _calculationBMS;

        public ProjectBS(IDbContextFactory dbContextFactory, ICalculationProjectBS calculationBMS)
        {
            _dbContextFactory = dbContextFactory;
            _calculationBMS = calculationBMS;
        }

        public ComplexProjectValue AddProject(ComplexProjectValue model)
        {
            try
            {
                model.CalcResult = _calculationBMS.CalcProject(model.ProjectValue);
            }
            catch (Exception e)
            {
                throw new Exception("Project was not added. Reason: " + e.Message);
            }
            using (var context = _dbContextFactory.Create())
            {
                if (model.Project.Id > 0)
                {
                    var otherProjects = context.Projects.Where(x => x.Id != model.Project.Id).ToList();
                    foreach (var item in otherProjects)
                    {
                        if (item.Name == model.Project.Name)
                            throw new Exception("Project exist");

                        else if (model.Project.Name == string.Empty)
                            throw new Exception("Project name is empty");
                    }

                    var existProjectValue = context.ProjectValues.Where(x => x.ProjectId == model.Project.Id).ToList();
                    if (existProjectValue != null)
                    {
                        foreach (var value in existProjectValue)
                        {
                            PropertyInfo prop = model.ProjectValue.GetType().GetProperty(value.PropertyName, BindingFlags.Public | BindingFlags.Instance);
                            value.PropertyValue = prop.GetValue(model.ProjectValue).ToString();
                            var property = context.ProjectValues.Where(p => p.PropertyName == value.PropertyName && p.ProjectId == value.ProjectId).FirstOrDefault();
                            property.PropertyValue = value.PropertyValue;
                        }

                        var currentProject = context.Projects.FirstOrDefault(x => x.Id == model.Project.Id);


                        currentProject.Institution = model.Project?.Institution;

                        currentProject.Name = model.Project?.Name;

                        currentProject.CampusPrecint = model.Project?.CampusPrecint;

                        context.SaveChanges();
                    }
                    return model;
                }
                var isProjectExist = context.Projects.FirstOrDefault(x => x.Name == model.Project.Name);

                if (isProjectExist != null)
                    throw new Exception("Project exist");

                else if (model.Project.Name == "" || string.IsNullOrWhiteSpace(model.Project.Name))
                    throw new Exception("Project name is empty");

                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));
                model.Project.UserId = manage.FindByName(model.Project.UserName).Id;
                var project = new Project
                {
                    Name = model.Project.Name,
                    UserId = model.Project.UserId,
                    Date = DateTime.Now.Date,
                    Institution = model.Project?.Institution,
                    CampusPrecint = model.Project?.CampusPrecint


                };
                context.Projects.Add(project);
                model.ProjectValue.ConstructionStartDate = DateTime.ParseExact(model.ProjectValue.ConstructionStartDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
                model.ProjectValue.EstimateDate = DateTime.ParseExact(model.ProjectValue.EstimateDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
                model.ProjectValue.DevelopedSiteArea = model.ProjectValue.GrossSiteArea - model.ProjectValue.BuildingFootprint;

                foreach (var property in model.ProjectValue.GetType().GetProperties())
                {
                    var projectValue = new ProjectValues
                    {
                        ProjectId = project.Id,
                        PropertyName = property.Name,
                        PropertyValue = property.GetValue(model.ProjectValue).ToString()
                    };
                    context.ProjectValues.Add(projectValue);
                }
                context.SaveChanges();

                model.Project = new ProjectModel
                {
                    Id = project.Id,
                    Date = project.Date.ToShortDateString(),
                    Name = project.Name,
                    UserId = project.UserId,
                    CampusPrecint = project?.CampusPrecint,
                    Institution = project?.Institution
                };

                return model;
            }
        }

        public bool DeleteProject(int projectId)
        {
            using (var context = _dbContextFactory.Create())
            {
                var existProject = context.Projects.FirstOrDefault(x => x.Id == projectId);

                if (existProject == null)
                    throw new Exception("The project wasn't deleted!!");


                context.FileInfos.Where(w => w.ProjectId == existProject.Id)
                                .ToList().ForEach(f => context.FileInfos.Remove(f));

                context.Projects.Remove(existProject);

                context.SaveChanges();

                return true;
            }
        }

        public List<ProjectModel> GetAllProject(string userName)
        {
            bool isAdmin = false;
            using (var context = _dbContextFactory.Create())
            {
                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));
                var currentUser = manage.FindByName(userName);
                foreach (var role in manage.GetRoles(currentUser.Id))
                {
                    if (role == "Admin")
                        isAdmin = true;

                }
                if (isAdmin)
                    return context.Projects.ToList().Select((projects) =>
                    {
                        return new ProjectModel
                        {
                            Id = projects.Id,
                            Name = projects.Name,
                            Date = projects.Date.ToShortDateString(),
                            UserId = projects.UserId,
                            UserName = manage.FindById(projects.UserId).UserName,
                        };
                    }).ToList();
                else
                    return context.Projects.ToList().Select((projects) =>
                    {
                        return new ProjectModel
                        {
                            Id = projects.Id,
                            Name = projects.Name,
                            Date = projects.Date.ToShortDateString(),
                            UserId = projects.UserId,
                        };
                    }).Where(x => x.UserId == currentUser.Id).ToList();
            }
        }

        public PaginationProjectViewModel GetPagedListProject(PaginationRequest paginationModel, string userName)
        {
            PaginationProjectViewModel resultModel = new PaginationProjectViewModel();
            List<Project> projectCollection = new List<Project>();

            bool isAdmin = false;

            using (var context = _dbContextFactory.Create())
            {
                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));
                var currentUser = manage.FindByName(userName);

                foreach (var role in manage.GetRoles(currentUser.Id))
                {
                    if (role == "Admin")
                        isAdmin = true;
                }
                if (isAdmin)
                {
                    if (paginationModel.Filters.Count > 0)
                    {
                        projectCollection = GetFilteredProjects(paginationModel, context);
                        resultModel.TotalCount = projectCollection.Count();
                    }
                    else
                    {
                        projectCollection = context.Projects.OrderByDescending(p => p.Date).ToList();
                        resultModel.TotalCount = projectCollection.Count();
                    }

                    var pagedProjectCollection = projectCollection.ToList().Skip(paginationModel.PageIndex)
                        .Take(paginationModel.PageSize);
                    var projectsCollection = pagedProjectCollection.Select((projects) =>
                    {
                        return new ProjectModel
                        {
                            Id = projects.Id,
                            Name = projects.Name,
                            Date = projects.Date.ToShortDateString(),
                            UserId = projects.UserId,
                            UserName = manage.FindById(projects.UserId).UserName,
                        };
                    }).ToList();

                    foreach (var item in projectsCollection)
                    {
                        resultModel.Projects.Add(item);
                    }

                    return resultModel;
                }
                else
                {
                    if (paginationModel.Filters.Count > 0)
                    {
                        projectCollection = GetFilteredProjects(paginationModel, context);
                        resultModel.TotalCount = projectCollection.Count();
                    }
                    else
                    {
                        projectCollection = context.Projects.OrderByDescending(p => p.Date).ToList();
                        resultModel.TotalCount = projectCollection.Count();
                    }

                    var pagedProjectCollection = projectCollection.ToList().Skip(paginationModel.PageIndex)
                        .Take(paginationModel.PageSize);

                    var projectsCollection = pagedProjectCollection.Select((projects) =>
                    {
                        return new ProjectModel
                        {
                            Id = projects.Id,
                            Name = projects.Name,
                            Date = projects.Date.ToShortDateString(),
                            UserId = projects.UserId,
                        };
                    }).Where(x => x.UserId == currentUser.Id).ToList();

                    foreach (var item in projectsCollection)
                    {
                        resultModel.Projects.Add(item);
                    }

                    return resultModel;
                }
            }
        }

        public Project GetProjectById(int projectId, string userId)
        {
            throw new NotImplementedException();
        }

        public ComplexProjectValue GetProjectValues(ProjectModel model)
        {
            try
            {
                using (var context = _dbContextFactory.Create())
                {
                    var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));
                    var inputsValue = context.ProjectValues.ToList().Where(x => x.ProjectId == model.Id);
                    ProjectViewModel resultInputs = new ProjectViewModel();
                    foreach (var value in inputsValue)
                    {
                        PropertyInfo prop = resultInputs.GetType().GetProperty(value.PropertyName, BindingFlags.Public | BindingFlags.Instance);
                        if (null != prop && prop.CanWrite)
                        {
                            if (prop.PropertyType.IsEnum)
                            {
                                Type enumType = prop.PropertyType;
                                if (Enum.IsDefined(enumType, value.PropertyValue))
                                    prop.SetValue(resultInputs, Enum.Parse(enumType, value.PropertyValue));
                            }
                            else if (prop.PropertyType == typeof(int))
                                prop.SetValue(resultInputs, Int32.Parse(value.PropertyValue));
                            else if (prop.PropertyType == typeof(double))
                                prop.SetValue(resultInputs, Double.Parse(value.PropertyValue));
                            else if (prop.PropertyType == typeof(bool))
                                prop.SetValue(resultInputs, value.PropertyValue == "True" ? true : false);
                            else if (prop.PropertyType == typeof(DateTime))
                                prop.SetValue(resultInputs, DateTime.Parse(value.PropertyValue));
                            else
                                prop.SetValue(resultInputs, value.PropertyValue);
                        }
                    }
                    var editeProjectResult = new ComplexProjectValue
                    {
                        ProjectValue = resultInputs
                    };

                    var project = new Project();
                    if (manage.IsInRole(model.UserId, "Admin"))
                    {
                        project = context.Projects.FirstOrDefault(x => x.Id == model.Id);
                    }
                    else
                    {
                        project = context.Projects.FirstOrDefault(x => x.Id == model.Id && x.UserId == model.UserId);
                    }

                    editeProjectResult.Project = new ProjectModel
                    {
                        Id = project.Id,
                        Date = project.Date.ToShortDateString(),
                        Institution = project?.Institution,
                        UserId = project.UserId,
                        Name = project.Name,
                        CampusPrecint = project?.CampusPrecint
                    };
                    return editeProjectResult;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Can't get user's project. ", e);

            }
        }

        public string SaveAsProject(ComplexProjectValue model)
        {
            using (var context = _dbContextFactory.Create())
            {
                if (model.Project.Name == "" || model.Project.Name == null)
                    throw new Exception("Project name is empty");
                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));
                model.Project.UserId = manage.FindByName(model.Project.UserName).Id;
                var project = new Project
                {
                    Name = model.Project.Name,
                    UserId = model.Project.UserId,
                    Date = DateTime.Now.Date,

                };
                context.Projects.Add(project);
                context.SaveChanges();

                int projectId = project.Id;
                model.ProjectValue.ConstructionStartDate = DateTime.ParseExact(model.ProjectValue.ConstructionStartDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
                model.ProjectValue.EstimateDate = DateTime.ParseExact(model.ProjectValue.EstimateDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
                model.ProjectValue.DevelopedSiteArea = model.ProjectValue.GrossSiteArea - model.ProjectValue.BuildingFootprint;

                foreach (var property in model.ProjectValue.GetType().GetProperties())
                {
                    string value = "";
                    value = property.GetValue(model.ProjectValue).ToString();
                    var projectValue = new ProjectValues
                    {
                        ProjectId = projectId,
                        PropertyName = property.Name,
                        PropertyValue = value
                    };
                    context.ProjectValues.Add(projectValue);
                }
                context.SaveChanges();
                return "Add";

            }
        }

        public Project GetProjectByUserId(string userId)
        {
            throw new NotImplementedException();
        }

        public byte[] GetPDFByProject(ProjectResultModel resultModel, ProjectModel projectModel)
        {
            GeneratePDF pdf = new GeneratePDF();
            using (MemoryStream ms = new MemoryStream())
            {
                using (Document document = new Document(PageSize.A4.Rotate(), 40, 40, 10, 5))
                {
                    using (var context = _dbContextFactory.Create())
                    {
                        var project = context.Projects.Where(x => x.Id == projectModel.Id).ToList().Select((result) =>
                        {
                            return new ProjectModel
                            {
                                Id = result.Id,
                                Name = result.Name,
                                Date = result.Date.ToShortDateString(),
                                Institution = result.Institution,
                                CampusPrecint = result.CampusPrecint,

                            };
                        }).First();
                        pdf.CreatePDF(document, ms, resultModel, project);
                    }

                }
                return ms.ToArray();
            }

        }

        public bool IsProjectExist(ProjectModel project)
        {
            if (project?.Id <= 0)
                return false;

            using (var context = _dbContextFactory.Create())
            {
                var existProject = context.Projects.FirstOrDefault(x => x.Id == project.Id);

                return existProject == null ? false : true;
            }
        }

        private List<Project> GetFilteredProjects(PaginationRequest paginationModel, IApplicationDbContext context)
        {
            List<Project> result = new List<Project>();
            var projects = context.Projects.OrderByDescending(p => p.Date).ToList();

            foreach (var item in projects)
            {
                switch (paginationModel.Filters[0].Type)
                {
                    case "contains":
                        if (item.Name.ToLower().Contains(paginationModel.Filters[0].FilterValue))
                        {
                            result.Add(item);
                        }
                        break;

                    case "equals":
                        if (item.Name.ToLower() == paginationModel.Filters[0].FilterValue)
                        {
                            result.Add(item);
                        }
                        break;

                    case "notEqual":
                        if (item.Name.ToLower() != paginationModel.Filters[0].FilterValue)
                        {
                            result.Add(item);
                        }
                        break;

                    case "startsWith":
                        if (item.Name.ToLower().StartsWith(paginationModel.Filters[0].FilterValue))
                        {
                            result.Add(item);
                        }
                        break;

                    case "endsWith":
                        if (item.Name.ToLower().EndsWith(paginationModel.Filters[0].FilterValue))
                        {
                            result.Add(item);
                        }
                        break;

                    case "notContains":
                        if (!item.Name.ToLower().Contains(paginationModel.Filters[0].FilterValue))
                        {
                            result.Add(item);
                        }
                        break;

                    default:
                        result = context.Projects.ToList();
                        break;
                }
            }

            return result;
        }
    }
}
