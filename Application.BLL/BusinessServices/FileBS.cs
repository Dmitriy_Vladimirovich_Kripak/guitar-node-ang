﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.DAL;
using Application.Entities;
using Application.Models.DTO;
using Application.Models.Manage;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLL.BusinessServices
{
    public class FileBS : IFileBS
    {
        private IDbContextFactory _dbContextFactory;
        private IProjectBS _projectService;
        private ICalculationProjectBS _calculationBMS;

        public FileBS(IDbContextFactory dbContextFactory, IProjectBS projectService, ICalculationProjectBS calculationBMS)
        {
            _dbContextFactory = dbContextFactory;
            _projectService = projectService;
            _calculationBMS = calculationBMS;
        }       

        public List<FileInfoDTO> GetAll(bool showAll, string userName)
        {
           
            using (var context = _dbContextFactory.Create())
            {
                var manage = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>((DbContext)context));

                var user = manage.FindByName(userName);

                var isAdmin = manage.IsInRole(user.Id, "Admin");

                if (showAll)
                {
                    if(isAdmin)
                    {
                        return context.FileInfos.ToList().Select(s => new FileInfoDTO
                        {
                            Id = s.Id,
                            Project = SelectProjectModel(context, s.ProjectId),
                            UserId = s.UserId,
                            Label = s.Label
                        }).ToList();
                    }
                    else
                    {
                        return context.FileInfos.Where(x=>x.UserId == user.Id).ToList().Select(s => new FileInfoDTO
                        {
                            Id = s.Id,
                            Project = SelectProjectModel(context, s.ProjectId),
                            UserId = s.UserId,
                            Label = s.Label
                        }).ToList();
                    }
                }
                   
                if(isAdmin)
                    return context.FileInfos.Take(10).ToList().Select(s => new FileInfoDTO
                    {
                        Id = s.Id,
                        Project = SelectProjectModel(context, s.ProjectId),
                        UserId = s.UserId,
                        Label = s?.Label
                    }).ToList();
                else
                    return context.FileInfos.Where(x=>x.UserId == user.Id).Take(10).ToList().Select(s => new FileInfoDTO
                    {
                        Id = s.Id,
                        Project = SelectProjectModel(context, s.ProjectId),
                        UserId = s.UserId,
                        Label = s?.Label
                    }).ToList();

            }
        }

        public FileInfoDTO GetById(int fileId)
        {
            using (var context = _dbContextFactory.Create())
            {
                var fileInfo = context.FileInfos.FirstOrDefault( f => f.Id == fileId);

                return new FileInfoDTO
                {
                    Id = fileInfo.Id,
                    Project = SelectProjectModel(context, fileInfo.ProjectId),
                    UserId = fileInfo.UserId,
                    File = fileInfo.File,
                    Date = fileInfo.CreatedDate.ToShortDateString(),
                    Label = fileInfo.Label,                    
                };
            }
        }

        public FileInfoDTO SaveFile(ProjectModel project, string fileName)
        {
            using (var context = _dbContextFactory.Create())
            {
                if (project?.Id <= 0)
                    throw new Exception("Wrong Project Id");

                var existProject = context.Projects.FirstOrDefault(x => x.Id == project.Id);

                if (existProject == null)
                    throw new Exception("Project was not found");

                var complexProjectInfo = _projectService.GetProjectValues(new ProjectModel
                {
                    Id = project?.Id ?? 0,
                    UserId = project.UserId
                });

                var file = _projectService.GetPDFByProject(_calculationBMS.CalcProject(complexProjectInfo.ProjectValue), complexProjectInfo.Project);

                var fileInfo = new Entities.FileInfo
                {
                    ProjectId = project.Id,
                    UserId = project.UserId,
                    File = file,                    
                    CreatedDate = DateTime.Now,
                    Label = String.IsNullOrEmpty(fileName) ? project.Name + " - " + DateTime.Now.ToShortDateString() : fileName
                };

                context.FileInfos.Add(fileInfo);
                context.SaveChanges();

                return new FileInfoDTO
                {
                    Id = fileInfo.Id,
                    UserId = fileInfo.UserId,
                    Project = new ProjectModel
                    {
                        Id = fileInfo.ProjectId
                    }

                };
            }          
        }

        public bool DeleteFile(int fileInfoId)
        {
            using (var context = _dbContextFactory.Create())
            {
                var fileInfo = context.FileInfos.FirstOrDefault(x => x.Id == fileInfoId);

                context.FileInfos.Remove(fileInfo);

                context.SaveChanges();

                return true;
            }
        }

        private ProjectModel SelectProjectModel(IApplicationDbContext context, int projectId)
        {
            var project = context.Projects.FirstOrDefault(x => x.Id == projectId);

            if (project == null)
                return null;

            return new ProjectModel
            {
                Id = project.Id,
                Name = project?.Name,
                Date = project.Date.ToShortDateString()
            };
        }

        
    }
}
