﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.DAL;
using Application.Entities;
using Application.Models.DTO;
using Application.Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLL.BusinessServices
{
    public class CampusInfoBS : ICampusInfo
    {
        private readonly IDbContextFactory _dbContextFactory;

        public CampusInfoBS(IDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }  

        public CampusInfoDTO Delete(CampusInfoDTO campusInfo)
        {
            throw new NotImplementedException();
        }

        public CampusInfoDTO Update(CampusInfoDTO campusInfo)
        {
            using (var context = _dbContextFactory.Create())
            {
                var campus = context.CampusInfos.FirstOrDefault(c => c.Id == campusInfo.Id);

                campus.InstitutionName = campusInfo.InstitutionName;

                campus.InstitutionType = campusInfo.InstitutionType;

                campus.Location = campusInfo.Location;

                campus.CampusProject = campusInfo.CampusProject;

                campus.ArchitechName = campusInfo.ArchitechName;

                context.SaveChanges();

                return campusInfo;
            }
        }
        public CampusInfoDTO Get(int campusId)
        {
            using (var context = _dbContextFactory.Create())
            {
                return context.CampusInfos.Where(x => x.Id == campusId).Select(s => new CampusInfoDTO
                {
                    Id = s.Id,
                    ArchitechName = s.ArchitechName,
                    InstitutionName = s.InstitutionName,
                    InstitutionType = s.InstitutionType,
                    Location = s.Location
                }).FirstOrDefault();
            }
        }

        public CampusInfoDTO Get()
        {
            using (var context = _dbContextFactory.Create())
            {
                return context.CampusInfos.Select(s => new CampusInfoDTO
                {
                    Id = s.Id,
                    ArchitechName = s.ArchitechName,
                    InstitutionName = s.InstitutionName,
                    InstitutionType = s.InstitutionType,
                    Location = s.Location,          
                    CampusProject = s.CampusProject
                }).FirstOrDefault();
            }
        }
    }
}
