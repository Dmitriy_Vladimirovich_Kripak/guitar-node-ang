﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Models;
using Application.DAL;
using Application.Models.Calculation;
using CalculationEngine;
using Application.Entities;
using Application.Common;
using Application.Models.Calculation.C10_E20;
using System.Globalization;

namespace Application.BLL.BusinessServices
{
    public class CalculationBS : ICalculationBS
    {
        private IDbContextFactory _dbContextFactory;
        private IFormulasContainer _formulasContainer;
        private IUpdateLocalStorage _updateLocalStorage;
        private IReadLocalStorage _readLocalStorage;
        public CalculationBS(IDbContextFactory dbContextFactory, IFormulasContainer formulasContainer, IUpdateLocalStorage updateLocalStorage, IReadLocalStorage readLocalStorage)
        {
            _dbContextFactory = dbContextFactory;
            _formulasContainer = formulasContainer;
            _updateLocalStorage = updateLocalStorage;
            _readLocalStorage = readLocalStorage;
        }

        public void ConfigureCalculationBS()
        {
            BaselineVariable[] all;
            Dictionary<string, string> allAsPairs = new Dictionary<string, string>();
            if (!_updateLocalStorage.Initialized)
            {
                using (var context = _dbContextFactory.Create())
                {
                    all = context.BaselineVariables.ToArray();
                }
                foreach(var bv in all)
                {
                    allAsPairs.Add(bv.Name, bv.Value);
                }
                _updateLocalStorage.Update(allAsPairs);
            }
        }

        public BaselineVariablesViewModel GetBaselineVariables(CalculationType calculationType)
        {
            var model = new BaselineVariablesViewModel();

            CalculationScenario scenario;
            model.BaselineVariables = new List<BaselineVariableViewModel>();
            IList<BaselineVariable> variables = null;
            using (var context = _dbContextFactory.Create())
            {
                if (calculationType == CalculationType.All)
                {
                    variables = context.BaselineVariables.ToList();
                }
                else
                {
                    scenario = context.CalculationScenarios.FirstOrDefault(cs => cs.Name == calculationType.ToString());
                    if (scenario != null) variables = scenario.BaselineVariables.ToList();
                    variables = variables ?? new List<BaselineVariable>();
                }
            }
            foreach (var bv in variables)
            {
                model.BaselineVariables.Add(new BaselineVariableViewModel() { Name = bv.Name, Value = bv.Value });
            }
            return model;
        }

        public C10_E20SettingsViewModel GetC10_E20ViewSettings(BuildingType buildingType)
        {
            var model = new C10_E20SettingsViewModel()
            {
                AssignableProgram1Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 1, true, true)),
                AssignableProgram2Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 2, true, true)),
                AssignableProgram3Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 3, true, true)),
                AssignableProgram4Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 4, true, true)),
                AssignableProgram5Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 5, true, true)),
                AssignableProgram6Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 6, true, true)),
                AssignableProgram7Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 7, true, true)),
                AssignableProgram8Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 8, true, true)),
                AssignableProgram1IsUsing = _readLocalStorage.GetValueAsBool(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 1, true, false)),
                AssignableProgram2IsUsing = _readLocalStorage.GetValueAsBool(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 2, true, false)),
                AssignableProgram3IsUsing = _readLocalStorage.GetValueAsBool(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 3, true, false)),
                AssignableProgram4IsUsing = _readLocalStorage.GetValueAsBool(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 4, true, false)),
                AssignableProgram5IsUsing = _readLocalStorage.GetValueAsBool(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 5, true, false)),
                AssignableProgram6IsUsing = _readLocalStorage.GetValueAsBool(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 6, true, false)),
                AssignableProgram7IsUsing = _readLocalStorage.GetValueAsBool(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 7, true, false)),
                AssignableProgram8IsUsing = _readLocalStorage.GetValueAsBool(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 8, true, false)),
                GrossUpProgram1Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 1, false, true)),
                GrossUpProgram2Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 2, false, true)),
                GrossUpProgram3Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 3, false, true)),
                GrossUpProgram4Caption = _readLocalStorage.GetValueAsString(BaselineVariableNames.GetC10_E20CaptionVarName(buildingType.ToString(), 4, false, true)),
            };           
            return model;
        }


        public string UpdateVariable(BaselineVariableViewModel model)
        {
            if(string.IsNullOrWhiteSpace(model.Name) || string.IsNullOrWhiteSpace(model.Value))
            {
                throw new Exception("Can't save new value for variable with empty Name or empty Value");
            }

            model.Name = model.Name.Trim();
            model.Value = model.Value.Trim();

            using (var context = _dbContextFactory.Create())
            {
                var variable = context.BaselineVariables.FirstOrDefault(bv => bv.Name == model.Name);
                if(variable != null)
                {
                    variable.Value = model.Value;
                    context.SaveChanges();
                    _updateLocalStorage.Update(new Tuple<string, string>(model.Name, model.Value));
                }
                else
                {
                    throw new Exception(string.Format("Can't save new value for unknown variable with Name = {0}", model.Name));
                }
            }
            return "Successfully updated";
        }

        public string CalcA10(A10ViewModel model)
        {
            var result  = _formulasContainer.CalculateA10Foundations(model);
            return string.Format("A10 Foundations =  ${0}/GSF", result);
        }

        public string CalcA20(A20ViewModel model)
        {
            var result = _formulasContainer.CalculateA20Foundations(model);
            return string.Format("A20 Subgrade Enclosures = ${0}/GSF", result);
        }

        public string CalcA40(A40ViewModel model)
        {
            var result = _formulasContainer.CalculateA40Foundations(model);
            return string.Format("A40 Slabs-on-Grade = ${0}/GSF", result);
        }
        public string CalcA60(A60ViewModel model)
        {
            var result = _formulasContainer.CalculateA60Foundations(model);
            return string.Format("A60 Water and Gas Mitigation = ${0}/GSF", result);
        }

        public string CalcA90(A90ViewModel model)
        {
            var result = _formulasContainer.CalculateA90Foundations(model);
            return string.Format("A90 Substructure Related Activities = ${0}/GSF", result);
        }
                
        public string CalcB10(B10ViewModel model)
        {
            var result = _formulasContainer.CalculateB10Foundations(model);
            return string.Format("B10 Superstructure = ${0}/GSF", result);
        }

        public string CalcB20(B20ViewModel model)
        {
            var result = _formulasContainer.CalculateB20Foundations(model);
            return string.Format("B20 Exterior Vertical Enclosures = ${0}/GSF", result);
        }

        public string CalcB30(B30ViewModel model)
        {
            var result = _formulasContainer.CalculateB30Foundations(model);
            return string.Format("B30 Exterior Horizontal Enclosures = ${0}/GSF", result);
        }

        public string CalcD10(D10ViewModel model)
        {
            var result = _formulasContainer.CalculateD10Foundations(model);
            return string.Format("D10 Conveying = ${0}/GSF", result);
        }

        public string CalcC10(C10ViewModel model)
        {
            var result = _formulasContainer.CalculateC10Foundations(model);
            return string.Format("C10 Interior Construction – Core and Shell = ${0}/GSF", result);
        }

        public string CalcC10Fitout(C10FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateC10FitoutFoundations(model);
            return string.Format("C10 Interior Construction – Fitout = ${0}/GSF", result);
        }

        public string CalcC20(C20ViewModel model)
        {
            var result = _formulasContainer.CalculateC20Foundations(model);
            return string.Format("C20 Interior Finishes – Core and Shell = ${0}/GSF", result);
        }

        public string CalcC20Fitout(C20FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateC20FitoutFoundations(model);
            return string.Format("C20 Interior Finishes – Fitout = ${0}/GSF", result);
        }

        public string CalcD20(D20ViewModel model)
        {
            var result = _formulasContainer.CalculateD20Foundations(model);
            return string.Format("D20 Plumbing – Core and Shell = ${0}/GSF", result);
        }

        public string CalcD20Fitout(D20FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateD20FitoutFoundations(model);
            return string.Format("D20 Plumbing – Fitout = ${0}/GSF", result);
        }

        public string CalcD30(D30ViewModel model)
        {
            var result = _formulasContainer.CalculateD30Foundations(model);
            return string.Format("D30 HVAC – Core and Shell = ${0}/GSF", result);
        }

        public string CalcD30Fitout(D30FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateD30FitoutFoundations(model);
            return string.Format("D30 HVAC – Fitout = ${0}/GSF", result);
        }

        public string CalcD40(D40ViewModel model)
        {
            var result = _formulasContainer.CalculateD40Foundations(model);
            return string.Format("D40 Fire Protection – Core and Shell = ${0}/GSF", result);
        }

        public string CalcD40Fitout(D40FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateD40FitoutFoundations(model);
            return string.Format("D40 Fire Protection – Fitout = ${0}/GSF", result);
        }

        public string CalcD50(D50ViewModel model)
        {
            var result = _formulasContainer.CalculateD50Foundations(model);
            return string.Format("D50 Electrical – Core and Shell = ${0}/GSF", result);
        }

        public string CalcD50Fitout(D50FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateD50FitoutFoundations(model);
            return string.Format("D50 Electrical – Fitout = ${0}/GSF", result);
        }

        public string CalcD60(D60ViewModel model)
        {
            var result = _formulasContainer.CalculateD60Foundations(model);
            return string.Format("D60 Communications – Core and Shell = ${0}/GSF", result);
        }

        public string CalcD60Fitout(D60FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateD60FitoutFoundations(model);
            return string.Format("D60 Communications – Fitout = ${0}/GSF", result);
        }

        public string CalcD70(D70ViewModel model)
        {
            var result = _formulasContainer.CalculateD70Foundations(model);
            return string.Format("D70 Electronic Safety and Security – Core and Shell = ${0}/GSF", result);
        }

        public string CalcD70Fitout(D70FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateD70FitoutFoundations(model);
            return string.Format("D70 Electronic Safety and Security – Fitout = ${0}/GSF", result);
        }

        public string CalcD80(D80ViewModel model)
        {
            var result = _formulasContainer.CalculateD80Foundations(model);
            return string.Format("D80 Integrated Automation – Core and Shell = ${0}/GSF", result);
        }

        public string CalcD80Fitout(D80FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateD80FitoutFoundations(model);
            return string.Format("D80 Integrated Automation – Fitout = ${0}/GSF", result);
        }

        public string CalcE10(E10ViewModel model)
        {
            var result = _formulasContainer.CalculateE10Foundations(model);
            return string.Format("E10 Equipment – Core and Shell = ${0}/GSF", result);
        }

        public string CalcE10Fitout(E10FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateE10FitoutFoundations(model);
            return string.Format("E10 Equipment – Fitout = ${0}/GSF", result);
        }

        public string CalcE20(E20ViewModel model)
        {
            var result = _formulasContainer.CalculateE20Foundations(model);
            return string.Format("E20 Furnishings – Core and Shell = ${0}/GSF", result);
        }

        public string CalcE20Fitout(E20FitoutViewModel model)
        {
            var result = _formulasContainer.CalculateE20FitoutFoundations(model);
            return string.Format("E20 Furnishings – Fitout = ${0}/GSF", result);
        }

        public string CalcF10BC(F10BCViewModel model)
        {
            var result = _formulasContainer.CalculateF10BC(model);
            return string.Format("F10 Special Construction = ${0}/GSF", result);
        }

        public string CalcF20BC(F20BCViewModel model)
        {
            var result = _formulasContainer.CalculateF20BC(model);
            return string.Format("F20 Facility Remediation  = ${0}/GSF", result);
        }

        public string CalcF30BC(F30BCViewModel model)
        {
            var result = _formulasContainer.CalculateF30BC(model);
            return string.Format("F30 Facility Demolition  = ${0}/GSF", result);
        }

        public string CalcZ10BC(Z10BCViewModel model)
        {
            var result = _formulasContainer.CalculateZ10BC(model);
            return string.Format("Z10 General Requirements = ${0}/GSF", result);
        }

        public string CalcZ70BC(Z70BCViewModel model)
        {
            var result = _formulasContainer.CalculateZ70BC(model);
            return string.Format("Z70 Taxes, Permits, Insurance and Bonds  = ${0}/GSF", result);
        }


        public string CalcZ910BC(Z910BCViewModel model)
        {
            var result = _formulasContainer.CalculateZ910BC(model);
            return string.Format("Z910 Contingencies (Design) = ${0}/GSF", result);
        }

        public string CalcZ920BC(Z920BCViewModel model)
        {
            model.ConstructionStartDate = DateTime.ParseExact(model.ConstructionStartDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
            model.EstimateDate = DateTime.ParseExact(model.EstimateDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
            var result = _formulasContainer.CalculateZ920BC(model);
            return string.Format("Z920 Contingencies (Escalation)= ${0}/GSF", result);
        }
        public string CalcZ930BC(Z930BCViewModel model)
        {
            var result = _formulasContainer.CalculateZ930BC(model);
            return string.Format("Z930 Contingencies (Construction) $/GSF = ${0}/GSF", result);
        }
        public string CalcZ940BC(Z940BCViewModel model)
        {
            var result = _formulasContainer.CalculateZ940BC(model);
            return string.Format("Z940 Fees  $/GSF = ${0}/GSF", result);
        }
        public string CalcG10SD(G10SDViewModel model)
        {
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            var result = _formulasContainer.CalculateG10SD(model);
            return string.Format("G10 Site Preparation  = ${0}/DSA", result);
        }
        public string CalcG20SD(G20SDViewModel model)
        {
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            var result = _formulasContainer.CalculateG20SD(model);
            return string.Format("G20 Site Improvements  = ${0}/DSA", result);
        }
        public string CalcG60SD(G60SDViewModel model)
        {
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            var result = _formulasContainer.CalculateG60SD(model);
            return string.Format("G60 Miscellaneous Site Construction   = ${0}/DSA", result);
        }
        public string CalcZ10SD(Z10SDViewModel model)
        {            
            var result = _formulasContainer.CalculateZ10SD(model);
            return string.Format("Z10 General Requirements  = ${0}/DSA", result);
        }
        public string CalcZ70SD(Z70SDViewModel model)
        {
            var result = _formulasContainer.CalculateZ70SD(model);
            return string.Format("Z70 Taxes, Permits, Insurance and Bonds = ${0}/DSA", result);
        }
        public string CalcZ910SD(Z910SDViewModel model)
        {
            var result = _formulasContainer.CalculateZ910SD(model);
            return string.Format("Z910 Contingencies (Design)  = ${0}/DSA", result);
        }
        public string CalcZ920SD(Z920SDViewModel model)
        {
            model.ConstructionStartDate = DateTime.ParseExact(model.ConstructionStartDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
            model.EstimateDate = DateTime.ParseExact(model.EstimateDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
            var result = _formulasContainer.CalculateZ920SD(model);
            return string.Format("Z920 Contingencies (Escalation)  = ${0}/DSA", result);
        }

        public string CalcZ930SD(Z930SDViewModel model)
        {
            var result = _formulasContainer.CalculateZ930SD(model);
            return string.Format("Z930 Contingencies (Construction) = ${0}/DSA", result);
        }
        public string CalcZ940SD(Z940SDViewModel model)
        {
            var result = _formulasContainer.CalculateZ940SD(model);
            return string.Format("Z940 Fees $/DSA = ${0}/DSA", result);
        }
        public string CalcG30SU(G30SUViewModel model)
        {
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            var result = _formulasContainer.CalculateG30SU(model);
            return string.Format("G30 Liquid and Gas Site Utilities  = ${0}/DSA", result);
        }
        public string CalcG40SU(G40SUViewModel model)
        {
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            var result = _formulasContainer.CalculateG40SU(model);
            return string.Format("G40 Electrical Site Improvements = ${0}/DSA", result);
        }
        public string CalcG50SU(G50SUViewModel model)
        {
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            var result = _formulasContainer.CalculateG50SU(model);
            return string.Format("G50 Site Communications  = ${0}/DSA", result);
        }
        public string CalcZ10SU(Z10SUViewModel model)
        {            
            var result = _formulasContainer.CalculateZ10SU(model);
            return string.Format("Z10 General Requirements   = ${0}/DSA", result);
        }
        public string CalcZ70SU(Z70SUViewModel model)
        {
            var result = _formulasContainer.CalculateZ70SU(model);
            return string.Format("Z70 Taxes, Permits, Insurance and Bonds   = ${0}/DSA", result);
        }
        public string CalcZ910SU(Z910SUViewModel model)
        {
            var result = _formulasContainer.CalculateZ910SU(model);
            return string.Format("Z910 Contingencies (Design)  = ${0}/DSA", result);
        }
        public string CalcZ920SU(Z920SUViewModel model)
        {
            model.ConstructionStartDate = DateTime.ParseExact(model.ConstructionStartDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
            model.EstimateDate = DateTime.ParseExact(model.EstimateDateStr, Application_Constants.webClientDataFormat, CultureInfo.InvariantCulture);
            var result = _formulasContainer.CalculateZ920SU(model);
            return string.Format("Z920 Contingencies (Escalation) = ${0}/DSA", result);
        }
        public string CalcZ930SU(Z930SUViewModel model)
        {            
            var result = _formulasContainer.CalculateZ930SU(model);
            return string.Format("Z930 Contingencies (Construction)  = ${0}/DSA", result);
        }
        public string CalcZ940SU(Z940SUViewModel model)
        {
            var result = _formulasContainer.CalculateZ940SU(model);
            return string.Format("Z940 Fees = ${0}/DSA", result);
        }
        public string CalcSoftCost(SoftCostViewModel model)
        {
            var result = _formulasContainer.CalculateSoftCost(model);
            return string.Format("Soft Costs  = ${0}/GSF", result);
        }
        public string CalcTotalProjectCost(TotalProjectCostViewModel model)
        {
            var result = _formulasContainer.CalculateTotalProjectCost(model);
            return string.Format("Total Project Cost  = ${0}/GSF", result);
        }
        
    }
}
