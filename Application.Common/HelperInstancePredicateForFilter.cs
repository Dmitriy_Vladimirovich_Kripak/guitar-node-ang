﻿using Application.Models;
using Application.Models.AuditTrail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common
{
    public class HelperInstancePredicateForFilter
    {
        public static Expression<Func<AuditTrailViewModel, bool>> GetPredicate(PaginationRequest paging)
        {
            var predicate = PredicateBuilder.True<AuditTrailViewModel>();
            foreach (var filter in paging.Filters)
            {
                switch (filter.Type)
                {
                    case "equals":
                        switch (filter.ColumnName)
                        {
                            case "UserName":
                                predicate = predicate.And(p => p.UserName == filter.FilterValue);
                                break;
                            case "EntityName":
                                predicate = predicate.And(p => p.EntityName == filter.FilterValue);
                                break;;
                            case "EntityType":
                                predicate = predicate.And(p => p.EntityType == filter.FilterValue);
                                break;
                            case "Action":
                                predicate = predicate.And(p => p.Action == filter.FilterValue);
                                break;
                            case "Date":
                                var tempDate = Convert.ToDateTime(filter.FilterValue);
                                predicate = predicate.And(p => p.Date.Date == tempDate.Date);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "notEqual":
                        switch (filter.ColumnName)
                        {
                            case "UserName":
                                predicate = predicate.And(p => p.UserName != filter.FilterValue);
                                break;
                            case "EntityName":
                                predicate = predicate.And(p => p.EntityName != filter.FilterValue);
                                break;
                            case "EntityType":
                                predicate = predicate.And(p => p.EntityType != filter.FilterValue);
                                break;
                            case "Action":
                                predicate = predicate.And(p => p.Action != filter.FilterValue);
                                break;
                            case "Date":
                                var tempDate = Convert.ToDateTime(filter.FilterValue);
                                predicate = predicate.And(p => p.Date.Date != tempDate.Date);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "startsWith":
                        switch (filter.ColumnName)
                        {
                            case "UserName":
                                predicate = predicate.And(p => p.UserName.StartsWith(filter.FilterValue));
                                break;
                            case "EntityName":
                                predicate = predicate.And(p => p.EntityName.StartsWith(filter.FilterValue));
                                break;
                            case "EntityType":
                                predicate = predicate.And(p => p.EntityType.StartsWith(filter.FilterValue));
                                break;
                            case "Action":
                                predicate = predicate.And(p => p.Action.StartsWith(filter.FilterValue));
                                break;
                            default:
                                break;
                        }
                        break;
                    case "endsWith":
                        switch (filter.ColumnName)
                        {
                            case "UserName":
                                predicate = predicate.And(p => p.UserName.EndsWith(filter.FilterValue));
                                break;
                            case "EntityName":
                                predicate = predicate.And(p => p.EntityName.EndsWith(filter.FilterValue));
                                break;
                            case "EntityType":
                                predicate = predicate.And(p => p.EntityType.EndsWith(filter.FilterValue));
                                break;
                            case "Action":
                                predicate = predicate.And(p => p.Action.EndsWith(filter.FilterValue));
                                break;
                            default:
                                break;
                        }
                        break;
                    case "contains":
                        switch (filter.ColumnName)
                        {
                            case "UserName":
                                predicate = predicate.And(p => p.UserName.ToLower().Contains(filter.FilterValue.ToLower()));
                                break;
                            case "EntityName":
                                predicate = predicate.And(p => p.EntityName.ToLower().Contains(filter.FilterValue.ToLower()));
                                break;
                            case "EntityType":
                                predicate = predicate.And(p => p.EntityType.ToLower().Contains(filter.FilterValue.ToLower()));
                                break;
                            case "Action":
                                predicate = predicate.And(p => p.Action.ToLower().Contains(filter.FilterValue.ToLower()));
                                break;
                            default:
                                break;
                        }
                        break;
                    case "notContains":
                        switch (filter.ColumnName)
                        {
                            case "UserName":
                                predicate = predicate.And(p => !p.UserName.ToLower().Contains(filter.FilterValue.ToLower()));
                                break;
                            case "EntityName":
                                predicate = predicate.And(p => !p.EntityName.ToLower().Contains(filter.FilterValue.ToLower()));
                                break;
                            case "EntityType":
                                predicate = predicate.And(p => !p.EntityType.ToLower().Contains(filter.FilterValue.ToLower()));
                                break;
                            case "Action":
                                predicate = predicate.And(p => !p.Action.ToLower().Contains(filter.FilterValue.ToLower()));
                                break;
                            default:
                                break;
                        }
                        break;

                    case "greaterThan":
                        switch (filter.ColumnName)
                        {
                            case "Date":
                                var tempDate = Convert.ToDateTime(filter.FilterValue);
                                predicate = predicate.And(p => p.Date > tempDate);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "lessThan":
                        switch (filter.ColumnName)
                        {
                            case "Date":
                                var tempDate = Convert.ToDateTime(filter.FilterValue);
                                predicate = predicate.And(p => p.Date < tempDate);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "inRange":
                        switch (filter.ColumnName)
                        {
                            case "Date":
                                var tempDate = Convert.ToDateTime(filter.FilterValue);
                                var tempDateTo = Convert.ToDateTime(filter.FilterValueTo);
                                predicate = predicate.And(p => p.Date >= tempDate && p.Date <= tempDateTo);
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            return predicate;
        }
    }
}
