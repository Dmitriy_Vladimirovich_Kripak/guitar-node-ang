﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common
{
    public class CalculationScenarioNames
    {
        public const string A10 = "A10";
        public const string A20 = "A20";
        public const string A40 = "A40";
        public const string A60 = "A60";
        public const string A90 = "A90";
        public const string B10 = "B10";
        public const string B20 = "B20";
        public const string B30 = "B30";
        public const string D10 = "D10";
        //universal formula
        public const string C10 = "C10";
        public const string C20 = "C20";
        public const string C10Fitout = "C10Fitout";
        public const string C20Fitout = "C20Fitout";
        public const string D20 = "D20";
        public const string D20Fitout = "D20Fitout";
        public const string D30 = "D30";
        public const string D30Fitout = "D30Fitout";
        public const string D40 = "D40";
        public const string D40Fitout = "D40Fitout";
        public const string D50 = "D50";
        public const string D50Fitout = "D50Fitout";
        public const string D60 = "D60";
        public const string D60Fitout = "D60Fitout";
        public const string D70 = "D70";
        public const string D70Fitout = "D70Fitout";
        public const string D80 = "D80";
        public const string D80Fitout = "D80Fitout";
        public const string E10 = "E10";
        public const string E10Fitout = "E10Fitout";
        public const string E20 = "E20";
        public const string E20Fitout = "E20Fitout";
        public const string F10BC = "F10BC";
        public const string F20BC = "F20BC";
        public const string F30BC = "F30BC";
        public const string Z10BC = "Z10BC";
        public const string Z70BC = "Z70BC";
        public const string Z910BC = "Z910BC";
        public const string Z920BC = "Z920BC";
        public const string Z930BC = "Z930BC";
        public const string Z940BC = "Z940BC";
        public const string G10SD = "G10SD";
        public const string G20SD = "G20SD";
        public const string G60SD = "G60SD";
        public const string Z10SD = "Z10SD";
        public const string Z920SD = "Z920SD";
        public const string Z930SD = "Z930SD";
        public const string G30SU = "G30SU";
        public const string G40SU = "G40SU";
        public const string G50SU = "G50SU";
        public const string Z920SU = "Z920SU";
        public const string Z930SU = "Z930SU";


    }
}
