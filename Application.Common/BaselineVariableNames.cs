﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common
{
    public class BaselineVariableNames
    {
        //A10
        public const string NumberOfLevelsFactor = "NumberOfLevelsFactor";

        public const string AcademicClassroomBuildingDeepGSF = "AcademicClassroomBuildingDeep$/GSF";
        public const string AcademicClassroomBuildingShallowGSF = "AcademicClassroomBuildingShallow$/GSF";
        public const string LaboratoryBuildingDeepGSF = "LaboratoryBuildingDeep$/GSF";
        public const string LaboratoryBuildingShallowGSF = "LaboratoryBuildingShallow$/GSF";
        public const string StudentHousingDeepGSF = "StudentHousingDeep$/GSF";
        public const string StudentHousingShallowGSF = "StudentHousingShallow$/GSF";
        public const string DiningDeepGSF = "DiningDeep$/GSF";
        public const string DiningShallowGSF = "DiningShallow$/GSF";
        public const string ParkingGarageBelowGradeDeepGSF = "ParkingGarageBelowGradeDeep$/GSF";
        public const string ParkingGarageBelowGradeShallowGSF = "ParkingGarageBelowGradeShallow$/GSF";
        public const string ParkingGarageAboveGradeDeepGSF = "ParkingGarageAboveGradeDeep$/GSF";
        public const string ParkingGarageAboveGradeShallowGSF = "ParkingGarageAboveGradeShallow$/GSF";
        //A20
        public const string BasementWallsSF = "BasementWalls$/SF";
        public const string RetentionSF = "Retention$/SF";
        public const string EarthworkExcavationBackfillCY = "EarthworkExcavation&Backfill$/CY";
        //A40
        public const string AcademicClassroomBuildingSlabSF = "AcademicClassroomBuildingSlab$/SF";
        public const string LaboratoryBuildingSlabSF = "LaboratoryBuildingSlab$";
        public const string StudentHousingSlabSF = "StudentHousingSlab$/SF";
        public const string DiningSlabSF = "DiningSlab$/SF";
        public const string ParkingGarageBelowGradeSlabSF = "ParkingGarageBelowGradeSlab$/SF";
        public const string ParkingGarageAboveGradeSlabSF = "ParkingGarageAboveGradeSlab$/SF";

        //A60
        public const string FoundationDrainage = " FoundationDrainage$/LF";
        //A90
        public const string EarthworkExcavationHaulAwayCYSoil = "EarthworkExcavationHaulAway$/CY-Soil";
        public const string EarthworkExcavationHaulAwayCYRippableRock = "EarthworkExcavationHaulAway$/CY-RippableRock";
        public const string EarthworkExcavationHaulAwayCYSolidRock = "EarthworkExcavationHaulAway$/CY-SolidRock";
        //B10
        public const string FloorConstructionAcademicClassroomConcreteFlatSlabSF = "FloorConstructionAcademicClassroomConcreteFlatSlab$/SF";
        public const string FloorConstructionAcademicClassroomConcreteSlabAndBeamSF = "FloorConstructionAcademicClassroomConcreteSlabAndBeam$/SF";
        public const string FloorConstructionAcademicClassroomSteelBracedFrameSF = "FloorConstructionAcademicClassroomSteelBracedFrame$/SF";
        public const string FloorConstructionAcademicClassroomSteelMomentFrameSF = "FloorConstructionAcademicClassroomSteelMomentFrame$/SF";
        public const string FloorConstructionLaboratoryConcreteFlatSlabSF = "FloorConstructionLaboratoryConcreteFlatSlab$/SF";
        public const string FloorConstructionLaboratoryConcreteSlabAndBeamSF = "FloorConstructionLaboratoryConcreteSlabAndBeam$/SF";
        public const string FloorConstructionLaboratorySteelBracedFrameSF = "FloorConstructionLaboratorySteelBracedFrame$/SF";
        public const string FloorConstructionLaboratorySteelMomentFrameSF = "FloorConstructionLaboratorySteelMomentFrame$/SF";
        public const string FloorConstructionStudentHousingConcreteFlatSlabSF = "FloorConstructionStudentHousingConcreteFlatSlab$/SF";
        public const string FloorConstructionStudentHousingConcreteSlabAndBeamSF = "FloorConstructionStudentHousingConcreteSlabAndBeam$/SF";
        public const string FloorConstructionStudentHousingSteelBracedFrameSF = "FloorConstructionStudentHousingSteelBracedFrame$/SF";
        public const string FloorConstructionStudentHousingSteelMomentFrameSF = "FloorConstructionStudentHousingSteelMomentFrame$/SF";
        public const string FloorConstructionStudentHousingWoodFrameSF = "FloorConstructionStudentHousingWoodFrame$/SF";
        public const string FloorConstructionDiningConcreteFlatSlabSF = "FloorConstructionDiningConcreteFlatSlab$/SF";
        public const string FloorConstructionDiningConcreteSlabAndBeamSF = "FloorConstructionDiningConcreteSlabAndBeam$/SF";
        public const string FloorConstructionDiningSteelBracedFrameSF = "FloorConstructionDiningSteelBracedFrame$/SF";
        public const string FloorConstructionDiningSteelMomentFrameSF = "FloorConstructionDiningSteelMomentFrame$/SF";
        public const string FloorConstructionParkingGarageBelowGradeConcreteFlatSlabSF = "FloorConstructionParkingGarageBelowGradeConcreteFlatSlab$/SF";
        public const string FloorConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF = "FloorConstructionParkingGarageBelowGradeConcreteSlabAndBeam$/SF";
        public const string FloorConstructionParkingGarageBelowGradePrecastSF = "FloorConstructionParkingGarageBelowGradePrecast$/SF";
        public const string FloorConstructionParkingGarageAboveGradeConcreteFlatSlabSF = "FloorConstructionParkingGarageAboveGradeConcreteFlatSlab$/SF";
        public const string FloorConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF = "FloorConstructionParkingGarageAboveGradeConcreteSlabAndBeam$/SF";
        public const string FloorConstructionParkingGarageAboveGradePrecastSF = "FloorConstructionParkingGarageAboveGradePrecast$/SF";

        public const string RoofConstructionAcademicClassroomConcreteFlatSlabSF = "RoofConstructionAcademicClassroomConcreteFlatSlab$/SF";
        public const string RoofConstructionAcademicClassroomConcreteSlabAndBeamSF = "RoofConstructionAcademicClassroomConcreteSlabAndBeam$/SF";
        public const string RoofConstructionAcademicClassroomSteelBracedFrameSF = "RoofConstructionAcademicClassroomSteelBracedFrame$/SF";
        public const string RoofConstructionAcademicClassroomSteelMomentFrameSF = "RoofConstructionAcademicClassroomSteelMomentFrame$/SF";
        public const string RoofConstructionLaboratoryConcreteFlatSlabSF = "RoofConstructionLaboratoryConcreteFlatSlab$/SF";
        public const string RoofConstructionLaboratoryConcreteSlabAndBeamSF = "RoofConstructionLaboratoryConcreteSlabAndBeam$/SF";
        public const string RoofConstructionLaboratorySteelBracedFrameSF = "RoofConstructionLaboratorySteelBracedFrame$/SF";
        public const string RoofConstructionLaboratorySteelMomentFrameSF = "RoofConstructionLaboratorySteelMomentFrame$/SF";
        public const string RoofConstructionStudentHousingConcreteFlatSlabSF = "RoofConstructionStudentHousingConcreteFlatSlab$/SF";
        public const string RoofConstructionStudentHousingConcreteSlabAndBeamSF = "RoofConstructionStudentHousingConcreteSlabAndBeam$/SF";
        public const string RoofConstructionStudentHousingSteelBracedFrameSF = "RoofConstructionStudentHousingSteelBracedFrame$/SF";
        public const string RoofConstructionStudentHousingSteelMomentFrameSF = "RoofConstructionStudentHousingSteelMomentFrame$/SF";
        public const string RoofConstructionStudentHousingWoodFrameSF = "RoofConstructionStudentHousingWoodFrame$/SF";
        public const string RoofConstructionDiningConcreteFlatSlabSF = "RoofConstructionDiningConcreteFlatSlab$/SF";
        public const string RoofConstructionDiningConcreteSlabAndBeamSF = "RoofConstructionDiningConcreteSlabAndBeam$/SF";
        public const string RoofConstructionDiningSteelBracedFrameSF = "RoofConstructionDiningSteelBracedFrame$/SF";
        public const string RoofConstructionDiningSteelMomentFrameSF = "RoofConstructionDiningSteelMomentFrame$/SF";
        public const string RoofConstructionParkingGarageBelowGradeConcreteFlatSlabSF = "RoofConstructionParkingGarageBelowGradeConcreteFlatSlab$/SF";
        public const string RoofConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF = "RoofConstructionParkingGarageBelowGradeConcreteSlabAndBeam$/SF";
        public const string RoofConstructionParkingGarageBelowGradePrecastSF = "RoofConstructionParkingGarageBelowGradePrecast$/SF";
        public const string RoofConstructionParkingGarageAboveGradeConcreteFlatSlabSF = "RoofConstructionParkingGarageAboveGradeConcreteFlatSlab$/SF";
        public const string RoofConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF = "RoofConstructionParkingGarageAboveGradeConcreteSlabAndBeam$/SF";
        public const string RoofConstructionParkingGarageAboveGradePrecastSF = "RoofConstructionParkingGarageAboveGradePrecast$/SF";

        public const string StairAcademicClassroomEgressFLT = "StairAcademicClassroomEgress$/FLT";
        public const string StairAcademicClassroomOrnamentalFLT = "StairAcademicClassroomOrnamental$/FLT";
        public const string StairLaboratoryEgressFLT = "StairLaboratoryEgress$/FLT";
        public const string StairLaboratoryOrnamentalFLT = "StairLaboratoryOrnamental$/FLT";
        public const string StairStudentHousingEgressFLT = "StairStudentHousingEgress$/FLT";
        public const string StairStudentHousingOrnamentalFLT = "StairStudentHousingOrnamental$/FLT";
        public const string StairDiningEgressFLT = "StairDiningEgress$/FLT";
        public const string StairDiningOrnamentalFLT = "StairDiningOrnamental$/FLT";
        public const string StairParkingGarageBelowGradeEgressFLT = "StairParkingGarageBelowGradeEgress$/FLT";
        public const string StairParkingGarageBelowGradeOrnamentalFLT = "StairParkingGarageBelowGradeOrnamental$/FLT";
        public const string StairParkingGarageAboveGradeEgressFLT = "StairParkingGarageAboveGradeEgress$/FLT";
        public const string StairParkingGarageAboveGradeOrnamentalFLT = "StairParkingGarageAboveGradeOrnamental$/FLT";

        public const string FlightAcademicClassroomEgressRatio = "FlightAcademicClassroomEgressRatio";
        public const string FlightAcademicClassroomOrnamentalRatio = "FlightAcademicClassroomOrnamentalRatio";
        public const string FlightLaboratoryEgressRatio = "FlightLaboratoryEgressRatio";
        public const string FlightLaboratoryOrnamentalRatio = "FlightLaboratoryOrnamentalRatio";
        public const string FlightStudentHousingEgressRatio = "FlightStudentHousingEgressRatio";
        public const string FlightStudentHousingOrnamentalRatio = "FlightStudentHousingOrnamentalRatio";
        public const string FlightDiningEgressRatio = "FlightDiningEgressRatio";
        public const string FlightDiningOrnamentalRatio = "FlightDiningOrnamentalRatio";
        public const string FlightParkingGarageBelowGradeEgressRatio = "FlightParkingGarageBelowGradeEgressRatio";
        public const string FlightParkingGarageBelowGradeOrnamentalRatio = "FlightParkingGarageBelowGradeOrnamentalRatio";
        public const string FlightParkingGarageAboveGradeEgressRatio = "FlightParkingGarageAboveGradeEgressRatio";
        public const string FlightParkingGarageAboveGradeOrnamentalRatio = "FlightParkingGarageAboveGradeOrnamentalRatio";
        // B20
        public const string DesignFactorArchitecturalExpectationIconicB20  = "DesignFactorArchitecturalExpectationIconicB20";
        public const string DesignFactorArchitecturalExpectationCampusStandardB20 = "DesignFactorArchitecturalExpectationCampusStandardB20";
        public const string DesignFactorArchitecturalExpectationLowCostB20 = "DesignFactorArchitecturalExpectationLowCostB20";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB20  = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB20";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentB20 = "DesignFactorPerformanceExpectationLEEDGoldEquivalentB20";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentB20 = "DesignFactorPerformanceExpectationLEEDSilverEquivalentB20";

        public const string AcademicClassroomBuildingSolidWallSF = "AcademicClassroomBuildingSolidWall$/SF";
        public const string AcademicClassroomBuildingGlazedWallSF = "AcademicClassroomBuildingGlazedWall$/SF";
        public const string LaboratoryBuildingSolidWallSF = "LaboratoryBuildingSolidWall$/SF";
        public const string LaboratoryBuildingGlazedWallSF = "LaboratoryBuildingGlazedWall$/SF";
        public const string StudentHousingSolidWallSF = "StudentHousingSolidWall$/SF";
        public const string StudentHousingGlazedWallSF = "StudentHousingGlazedWall$/SF";
        public const string DiningSolidWallSF = "DiningSolidWall$/SF";
        public const string DiningGlazedWallSF = "DiningGlazedWall$/SF";
        public const string ParkingGarageBelowGradeSolidWallSF = "ParkingGarageBelowGradeSolidWall$/SF";
        public const string ParkingGarageBelowGradeGlazedWallSF = "ParkingGarageBelowGradeGlazedWall$/SF";
        public const string ParkingGarageAboveGradeSolidWallSF = "ParkingGarageAboveGradeSolidWall$/SF";
        public const string ParkingGarageAboveGradeGlazedWallSF = "ParkingGarageAboveGradeGlazedWall$/SF";

        // B30
        public const string DesignFactorArchitecturalExpectationIconicB30 = "DesignFactorArchitecturalExpectationIconicB30";
        public const string DesignFactorArchitecturalExpectationCampusStandardB30 = "DesignFactorArchitecturalExpectationCampusStandardB30";
        public const string DesignFactorArchitecturalExpectationLowCostB30 = "DesignFactorArchitecturalExpectationLowCostB30";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB30 = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB30";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentB30 = "DesignFactorPerformanceExpectationLEEDGoldEquivalentB30";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentB30 = "DesignFactorPerformanceExpectationLEEDSilverEquivalentB30";

        public const string AcademicClassroomBuildingSolidRoofSF = "AcademicClassroomBuildingSolidRoof$/SF";
        public const string AcademicClassroomBuildingGlazedRoofSF = "AcademicClassroomBuildingGlazedRoof$/SF";
        public const string AcademicClassroomBuildingTerraceRoofSF = "AcademicClassroomBuildingTerraceRoof$/SF";
        public const string AcademicClassroomBuildingSoffitRoofSF = "AcademicClassroomBuildingSoffitRoof$/SF";
        public const string LaboratoryBuildingSolidRoofSF = "LaboratoryBuildingSolidRoof$/SF";
        public const string LaboratoryBuildingGlazedRoofSF = "LaboratoryBuildingGlazedRoof$/SF";
        public const string LaboratoryBuildingTerraceRoofSF = "LaboratoryBuildingTerraceRoof$/SF";
        public const string LaboratoryBuildingSoffitRoofSF = "LaboratoryBuildingSoffitRoof$/SF";
        public const string StudentHousingSolidRoofSF = "StudentHousingSolidRoof$/SF";
        public const string StudentHousingGlazedRoofSF = "StudentHousingGlazedRoof$/SF";
        public const string StudentHousingTerraceRoofSF = "StudentHousingTerraceRoof$/SF";
        public const string StudentHousingSoffitRoofSF = "StudentHousingSoffitRoof$/SF";
        public const string DiningSolidRoofSF = "DiningSolidRoof$/SF";
        public const string DiningGlazedRoofSF = "DiningGlazedRoof$/SF";
        public const string DiningTerraceRoofSF = "DiningTerraceRoof$/SF";
        public const string DiningSoffitRoofSF = "DiningSoffitRoof$/SF";
        public const string ParkingGarageBelowGradeSolidRoofSF = "ParkingGarageBelowGradeSolidRoof$/SF";
        public const string ParkingGarageBelowGradeGlazedRoofSF = "ParkingGarageBelowGradeGlazedRoof$/SF";
        public const string ParkingGarageAboveGradeSolidRoofSF = "ParkingGarageAboveGradeSolidRoof$/SF";

        //D10
        public const string ElevatorAcademicClassroomPassengerSTP = "ElevatorAcademicClassroomPassenger$/STP";
        public const string ElevatorAcademicClassroomServiceSTP = "ElevatorAcademicClassroomService$/STP";
        public const string ElevatorLaboratoryPassengerSTP = "ElevatorLaboratoryPassenger$/STP";
        public const string ElevatorLaboratoryServiceSTP = "ElevatorLaboratoryService$/STP";
        public const string ElevatorStudentHousingPassengerSTP = "ElevatorStudentHousingPassenger$/STP";
        public const string ElevatorStudentHousingServiceSTP = "ElevatorStudentHousingService$/STP";
        public const string ElevatorDiningPassengerSTP = "ElevatorDiningPassenger$/STP";
        public const string ElevatorDiningServiceSTP = "ElevatorDiningService$/STP";
        public const string ElevatorParkingGarageBelowGradePassengerSTP = "ElevatorParkingGarageBelowGradePassenger$/STP";
        public const string ElevatorParkingGarageBelowGradeServiceSTP = "ElevatorParkingGarageBelowGradeService$/STP";
        public const string ElevatorParkingGarageAboveGradePassengerSTP = "ElevatorParkingGarageAboveGradePassenger$/STP";
        public const string ElevatorParkingGarageAboveGradeServiceSTP = "ElevatorParkingGarageAboveGradeService$/STP";

        public const string ElevatorAcademicClassroomPassengerSTP_SF = "ElevatorAcademicClassroomPassengerSTP/SF";
        public const string ElevatorAcademicClassroomServiceSTP_SF = "ElevatorAcademicClassroomServiceSTP/SF";
        public const string ElevatorLaboratoryPassengerSTP_SF = "ElevatorLaboratoryPassengerSTP/SF";
        public const string ElevatorLaboratoryServiceSTP_SF = "ElevatorLaboratoryServiceSTP/SF";
        public const string ElevatorStudentHousingPassengerSTP_SF = "ElevatorStudentHousingPassengerSTP/SF";
        public const string ElevatorStudentHousingServiceSTP_SF = "ElevatorStudentHousingServiceSTP/SF";
        public const string ElevatorDiningPassengerSTP_SF = "ElevatorDiningPassengerSTP/SF";
        public const string ElevatorDiningServiceSTP_SF = "ElevatorDiningServiceSTP/SF";
        public const string ElevatorParkingGarageBelowGradePassengerSTP_SF = "ElevatorParkingGarageBelowGradePassengerSTP/SF";
        public const string ElevatorParkingGarageBelowGradeServiceSTP_SF = "ElevatorParkingGarageBelowGradeServiceSTP/SF";
        public const string ElevatorParkingGarageAboveGradePassengerSTP_SF = "ElevatorParkingGarageAboveGradePassengerSTP/SF";
        public const string ElevatorParkingGarageAboveGradeServiceSTP_SF = "ElevatorParkingGarageAboveGradeServiceSTP/SF";

        // C10 - Z940
        public const string LocationFactorNationalAverage = "LocationFactorNationalAverage";
        public const string LocationFactorLosAngeles = "LocationFactorLosAngeles";
        public const string LocationFactorTBD = "LocationFactorTBD";
        public const string ProjectDeliveryStrategyFactorDesignBidBuild = "ProjectDeliveryStrategyFactorDesignBidBuild";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1 = "ProjectDeliveryStrategyFactorCMARRegionalTier1";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2 = "ProjectDeliveryStrategyFactorCMARRegionalTier2";
        public const string ProjectDeliveryStrategyFactorCMARNational = "ProjectDeliveryStrategyFactorCMARNational";

        // C10
        public const string DesignFactorArchitecturalExpectationIconicC10 = "DesignFactorArchitecturalExpectationIconicC10";
        public const string DesignFactorArchitecturalExpectationCampusStandardC10 = "DesignFactorArchitecturalExpectationCampusStandardC10";
        public const string DesignFactorArchitecturalExpectationLowCostC10 = "DesignFactorArchitecturalExpectationLowCostC10";
        // C20
        public const string DesignFactorArchitecturalExpectationIconicC20 = "DesignFactorArchitecturalExpectationIconicC20";
        public const string DesignFactorArchitecturalExpectationCampusStandardC20 = "DesignFactorArchitecturalExpectationCampusStandardC20";
        public const string DesignFactorArchitecturalExpectationLowCostC20 = "DesignFactorArchitecturalExpectationLowCostC20";

        // C10Fitout
        public const string DesignFactorArchitecturalExpectationIconicC10Fitout = "DesignFactorArchitecturalExpectationIconicC10Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardC10Fitout = "DesignFactorArchitecturalExpectationCampusStandardC10Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostC10Fitout = "DesignFactorArchitecturalExpectationLowCostC10Fitout";
        // C20Fitout
        public const string DesignFactorArchitecturalExpectationIconicC20Fitout = "DesignFactorArchitecturalExpectationIconicC20Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardC20Fitout = "DesignFactorArchitecturalExpectationCampusStandardC20Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostC20Fitout = "DesignFactorArchitecturalExpectationLowCostC20Fitout";

        // D20
        public const string DesignFactorArchitecturalExpectationIconicD20 = "DesignFactorArchitecturalExpectationIconicD20";
        public const string DesignFactorArchitecturalExpectationCampusStandardD20 = "DesignFactorArchitecturalExpectationCampusStandardD20";
        public const string DesignFactorArchitecturalExpectationLowCostD20 = "DesignFactorArchitecturalExpectationLowCostD20";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD20 = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD20";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD20 = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD20";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD20 = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD20";
        public const string PlumbingTypical = "PlumbingTypical";
        public const string PlumbingFlexible = "PlumbingFlexible";
        // D20Fitout
        public const string DesignFactorArchitecturalExpectationIconicD20Fitout = "DesignFactorArchitecturalExpectationIconicD20Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardD20Fitout = "DesignFactorArchitecturalExpectationCampusStandardD20Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostD20Fitout = "DesignFactorArchitecturalExpectationLowCostD20Fitout";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD20Fitout = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD20Fitout";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD20Fitout = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD20Fitout";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD20Fitout = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD20Fitout";

        // D30
        public const string DesignFactorArchitecturalExpectationIconicD30 = "DesignFactorArchitecturalExpectationIconicD30";
        public const string DesignFactorArchitecturalExpectationCampusStandardD30 = "DesignFactorArchitecturalExpectationCampusStandardD30";
        public const string DesignFactorArchitecturalExpectationLowCostD30 = "DesignFactorArchitecturalExpectationLowCostD30";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD30 = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD30";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD30 = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD30";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD30 = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD30";
        public const string HVACAir = "HVACAir";
        public const string HVACHydronic = "HVACHydronic";
        public const string HVACHybrid = "HVACHybrid";
        public const string BuildingPlantLocal = "BuildingPlantLocal";
        public const string BuildingPlantRemote = "BuildingPlantRemote";
        // D30Fitout
        public const string DesignFactorArchitecturalExpectationIconicD30Fitout = "DesignFactorArchitecturalExpectationIconicD30Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardD30Fitout = "DesignFactorArchitecturalExpectationCampusStandardD30Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostD30Fitout = "DesignFactorArchitecturalExpectationLowCostD30Fitout";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD30Fitout = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD30Fitout";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD30Fitout = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD30Fitout";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD30Fitout = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD30Fitout";

        // D40
        public const string DesignFactorArchitecturalExpectationIconicD40 = "DesignFactorArchitecturalExpectationIconicD40";
        public const string DesignFactorArchitecturalExpectationCampusStandardD40 = "DesignFactorArchitecturalExpectationCampusStandardD40";
        public const string DesignFactorArchitecturalExpectationLowCostD40 = "DesignFactorArchitecturalExpectationLowCostD40";
        public const string FireProtectionWet = "FireProtectionWet";
        public const string FireProtectionWetPreAction = "FireProtectionWetPreAction";
        public const string FireProtectionDry = "FireProtectionDry";
        // D40Fitout
        public const string DesignFactorArchitecturalExpectationIconicD40Fitout = "DesignFactorArchitecturalExpectationIconicD40Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardD40Fitout = "DesignFactorArchitecturalExpectationCampusStandardD40Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostD40Fitout = "DesignFactorArchitecturalExpectationLowCostD40Fitout";

        // D50
        public const string DesignFactorArchitecturalExpectationIconicD50 = "DesignFactorArchitecturalExpectationIconicD50";
        public const string DesignFactorArchitecturalExpectationCampusStandardD50 = "DesignFactorArchitecturalExpectationCampusStandardD50";
        public const string DesignFactorArchitecturalExpectationLowCostD50 = "DesignFactorArchitecturalExpectationLowCostD50";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD50 = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD50";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD50 = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD50";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD50 = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD50";
        public const string ElectricalTypical = "ElectricalTypical";
        public const string ElectricalComprehensive = "ElectricalComprehensive";
        // D50Fitout
        public const string DesignFactorArchitecturalExpectationIconicD50Fitout = "DesignFactorArchitecturalExpectationIconicD50Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardD50Fitout = "DesignFactorArchitecturalExpectationCampusStandardD50Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostD50Fitout = "DesignFactorArchitecturalExpectationLowCostD50Fitout";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD50Fitout = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD50Fitout";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD50Fitout = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD50Fitout";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD50Fitout = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD50Fitout";

        // D60
        public const string DesignFactorArchitecturalExpectationIconicD60 = "DesignFactorArchitecturalExpectationIconicD60";
        public const string DesignFactorArchitecturalExpectationCampusStandardD60 = "DesignFactorArchitecturalExpectationCampusStandardD60";
        public const string DesignFactorArchitecturalExpectationLowCostD60 = "DesignFactorArchitecturalExpectationLowCostD60";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD60 = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD60";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD60 = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD60";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD60 = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD60";
        // D60Fitout
        public const string DesignFactorArchitecturalExpectationIconicD60Fitout = "DesignFactorArchitecturalExpectationIconicD60Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardD60Fitout = "DesignFactorArchitecturalExpectationCampusStandardD60Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostD60Fitout = "DesignFactorArchitecturalExpectationLowCostD60Fitout";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD60Fitout = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD60Fitout";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD60Fitout = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD60Fitout";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD60Fitout = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD60Fitout";

        // D70
        public const string DesignFactorArchitecturalExpectationIconicD70 = "DesignFactorArchitecturalExpectationIconicD70";
        public const string DesignFactorArchitecturalExpectationCampusStandardD70 = "DesignFactorArchitecturalExpectationCampusStandardD70";
        public const string DesignFactorArchitecturalExpectationLowCostD70 = "DesignFactorArchitecturalExpectationLowCostD70";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD70 = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD70";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD70 = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD70";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD70 = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD70";
        // D70Fitout
        public const string DesignFactorArchitecturalExpectationIconicD70Fitout = "DesignFactorArchitecturalExpectationIconicD70Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardD70Fitout = "DesignFactorArchitecturalExpectationCampusStandardD70Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostD70Fitout = "DesignFactorArchitecturalExpectationLowCostD70Fitout";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD70Fitout = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD70Fitout";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD70Fitout = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD70Fitout";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD70Fitout = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD70Fitout";

        // D80
        public const string DesignFactorArchitecturalExpectationIconicD80 = "DesignFactorArchitecturalExpectationIconicD80";
        public const string DesignFactorArchitecturalExpectationCampusStandardD80 = "DesignFactorArchitecturalExpectationCampusStandardD80";
        public const string DesignFactorArchitecturalExpectationLowCostD80 = "DesignFactorArchitecturalExpectationLowCostD80";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD80 = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD80";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD80 = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD80";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD80 = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD80";
        // D80Fitout
        public const string DesignFactorArchitecturalExpectationIconicD80Fitout = "DesignFactorArchitecturalExpectationIconicD80Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardD80Fitout = "DesignFactorArchitecturalExpectationCampusStandardD80Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostD80Fitout = "DesignFactorArchitecturalExpectationLowCostD80Fitout";
        public const string DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD80Fitout = "DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD80Fitout";
        public const string DesignFactorPerformanceExpectationLEEDGoldEquivalentD80Fitout = "DesignFactorPerformanceExpectationLEEDGoldEquivalentD80Fitout";
        public const string DesignFactorPerformanceExpectationLEEDSilverEquivalentD80Fitout = "DesignFactorPerformanceExpectationLEEDSilverEquivalentD80Fitout";

        // E10
        public const string DesignFactorArchitecturalExpectationIconicE10 = "DesignFactorArchitecturalExpectationIconicE10";
        public const string DesignFactorArchitecturalExpectationCampusStandardE10 = "DesignFactorArchitecturalExpectationCampusStandardE10";
        public const string DesignFactorArchitecturalExpectationLowCostE10 = "DesignFactorArchitecturalExpectationLowCostE10";
        // E10Fitout
        public const string DesignFactorArchitecturalExpectationIconicE10Fitout = "DesignFactorArchitecturalExpectationIconicE10Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardE10Fitout = "DesignFactorArchitecturalExpectationCampusStandardE10Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostE10Fitout = "DesignFactorArchitecturalExpectationLowCostE10Fitout";

        // E20
        public const string DesignFactorArchitecturalExpectationIconicE20 = "DesignFactorArchitecturalExpectationIconicE20";
        public const string DesignFactorArchitecturalExpectationCampusStandardE20 = "DesignFactorArchitecturalExpectationCampusStandardE20";
        public const string DesignFactorArchitecturalExpectationLowCostE20 = "DesignFactorArchitecturalExpectationLowCostE20";
        // E20Fitout
        public const string DesignFactorArchitecturalExpectationIconicE20Fitout = "DesignFactorArchitecturalExpectationIconicE20Fitout";
        public const string DesignFactorArchitecturalExpectationCampusStandardE20Fitout = "DesignFactorArchitecturalExpectationCampusStandardE20Fitout";
        public const string DesignFactorArchitecturalExpectationLowCostE20Fitout = "DesignFactorArchitecturalExpectationLowCostE20Fitout";

        // F20BC
        public const string FacilityRemediationRate = "FacilityRemediationRate";
        public const string FacilityRemediationSimple = "FacilityRemediationSimple";
        public const string FacilityRemediationComplex = "FacilityRemediationComplex";

        // F30BC
        public const string FacilityDemolitionRate = "FacilityDemolitionRate";
        public const string FacilityDemolitionSimple = "FacilityDemolitionSimple";
        public const string FacilityDemolitionComplex = "FacilityDemolitionComplex";

        //Z10BC
        public const string ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC = "ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC = "ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC = "ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC = "ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC = "ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC = "ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC = "ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC = "ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC = "ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC = "ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC = "ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC = "ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC";

        //Z70BC
        public const string ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC = "ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC = "ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC = "ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC = "ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC = "ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC = "ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC = "ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC = "ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC = "ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC = "ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC = "ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC = "ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC";

        // Z920BC
        public const string DataDate = "DataDate";
        public const string DefaultEscalationRate1Year = "DefaultEscalationRate1Year";
        public const string DefaultEscalationRate2Year = "DefaultEscalationRate2Year";
        public const string DefaultEscalationRate3Year = "DefaultEscalationRate3Year";
        public const string DefaultEscalationRate4Year = "DefaultEscalationRate4Year";
        public const string DefaultEscalationRate5Year = "DefaultEscalationRate5Year";

        //Z930BC
        public const string ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ930BC = "ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ930BC = "ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ930BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ930BC = "ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ930BC = "ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ930BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ930BC = "ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ930BC = "ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ930BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildDiningZ930BC = "ProjectDeliveryStrategyFactorDesignBidBuildDiningZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalDiningZ930BC = "ProjectDeliveryStrategyFactorCMARNationalDiningZ930BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ930BC = "ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ930BC = "ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ930BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ930BC = "ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ930BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ930BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ930BC = "ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ930BC";
       
        //Z940BC
        public const string ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ940BC = "ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ940BC = "ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ940BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ940BC = "ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ940BC = "ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ940BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ940BC = "ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ940BC = "ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ940BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildDiningZ940BC = "ProjectDeliveryStrategyFactorDesignBidBuildDiningZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalDiningZ940BC = "ProjectDeliveryStrategyFactorCMARNationalDiningZ940BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ940BC = "ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ940BC = "ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ940BC";

        public const string ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ940BC = "ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ940BC = "ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ940BC";
        public const string ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ940BC = "ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ940BC";

        //G10SD

        public const string SiteDemolitionGreenfield= "SiteDemolitionGreenfield";
        public const string SiteDemolitionBrownfield = "SiteDemolitionBrownfield ";
        public const string SitePreparationGreenfield = "SitePreparationGreenfield";
        public const string SitePreparationBrownfield = "SitePreparationBrownfield";

        //G20SD

        public const string SiteDevelopmentSimple = "SiteDevelopmentSimple";
        public const string SiteDevelopmentModerate = "SiteDevelopmentModerate ";
        public const string SiteDevelopmentExtensive = "SiteDevelopmentExtensive";

        //G60SD

        public const string MiscellaneousSiteConstruction = "SiteDevelopmentMiscellaneousSiteConstruction";

        //G40SD

        public const string SiteInfrastructureServicesProximate = "SiteInfrastructureServicesProximate";
        public const string SiteInfrastructureServicesRemote = "SiteInfrastructureServicesRemote ";
        public const string SiteInfrastructureElectrical= "SiteInfrastructureElectrical";
        public const string SiteLighting = "SiteLighting";

        //G30SU
        public const string BuildingPlantLocalG30SU = "BuildingPlantLocalG30SU";
        public const string BuildingPlantRemoteG30SU = "BuildingPlantRemoteG30SU";
        public const string SiteServicesSteamCondensate = "SiteServicesSteamCondensate";
        public const string SiteServicesHotWaterSupply = "SiteServicesHotWaterSupply";
        public const string SiteServicesChilledWaterSupply = "SiteServicesChilledWaterSupply";
        public const string SiteInfrastructureCivilProximate = "SiteInfrastructureCivilProximate";
        public const string SiteInfrastructureCivilRemote = "SiteInfrastructureCivilRemote";
        public const string SiteCivilProximate = "SiteCivilProximate";
        public const string SiteCivilRemote = "SiteCivilRemote";
        public const string SiteCivilWaterFire  = "SiteCivilWaterFire";
        public const string SiteCivilSanitarySewer = "SiteCivilSanitarySewer";
        public const string SiteCivilStormSewer = "SiteCivilStormSewer";
        public const string SiteCivilGas = "SiteCivilGas";

        //G50SU

        public const string SiteInfrastructureCommunications = "SiteInfrastructureCommunications";
        public const string SiteCommunications = "SiteCommunications";

        /// <summary>
        /// Generate names of baseline variables for core part of C10-E20 formulas
        /// name will looks like: 
        /// AcademicClassroomTeachingFitout$/ASF_C20 or DiningMEPCoreAndShell$/ASF_E10 and so on.
        /// </summary>
        public static string GetC10_E20VarName(string buildingType, string programType, string calculationName, bool isFitout)
        {
            if (isFitout)
            {
                return string.Format("{0}{1}{2}{3}{4}", buildingType, programType, "Fitout", "$/ASF_", calculationName);
            }
            else
            {
                return string.Format("{0}{1}{2}{3}{4}", buildingType, programType, "CoreAndShell", "$/ASF_", calculationName);
            }
        }

        public static string GetC10_E20CaptionVarName(string buildingType, int programNumber, bool assignableOrGrossUp, bool captionOrIsUsing)
        {
            return string.Format("{0}{1}{2}{3}", 
                buildingType,
                assignableOrGrossUp ? "Assignable" : "GrossUp",
                "Program" + programNumber.ToString(),
                captionOrIsUsing ? "Caption" : "IsUsing");
        }

        public static string GetC10_E20GrossUpVarName(string calculationName, string buildingType, int programNumber)
        {
            return string.Format("{0}_{1}{2}{3}", calculationName, buildingType, "Program" + programNumber.ToString(), "GrossUp$/ASF");
        }

        public static string GetC10_E20AssignableVarName(string calculationName, string buildingType, int programNumber)
        {
            return string.Format("{0}_{1}{2}{3}", calculationName, buildingType, "Program" + programNumber.ToString(), "Assignable$/ASF");
        }
    }
}
