﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class Z920SDViewModel: BaseZ920ViewModel
    {
        public double G10G60Sum { get; set; }
        public double Z910SD { get; set; }
    }

}
