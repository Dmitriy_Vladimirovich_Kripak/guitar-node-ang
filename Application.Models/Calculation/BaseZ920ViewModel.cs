﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class BaseZ920ViewModel
    {
        public EscalateTo EscalateTo { get; set; }
        public DateTime EstimateDate { get; set; }
        public string EstimateDateStr { get; set; }
        public DateTime ConstructionStartDate { get; set; }
        public string ConstructionStartDateStr { get; set; }
        public int DurationInMonths { get; set; }
    }
}
