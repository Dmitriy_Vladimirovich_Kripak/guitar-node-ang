﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class TotalProjectCostViewModel
    {
        public double TotalConstructionCosts { get; set; }
        public double SoftCosts  { get; set; }
        
    }

}
