﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class BaselineVariablesViewModel
    {
        public List<BaselineVariableViewModel> BaselineVariables { get; set; }

    }
}
