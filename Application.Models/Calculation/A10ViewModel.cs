﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class A10ViewModel
    {
        public SoilCondition SoilCondition { get; set; }
        public int NumberOfLevels { get; set; }
        public BuildingType BuildingType { get; set; }
        public LocationFactor LocationFactor { get; set; }
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }

    }

}
