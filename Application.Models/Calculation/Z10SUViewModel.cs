﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class Z10SUViewModel
    {
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }
        public BuildingType BuildingType { get; set; }       
        public double G30G50SUSum { get; set; }
        public double Z910SU { get; set; }
        public double Z920SU { get; set; }
        public double Z930SU { get; set; }        

    }

}
