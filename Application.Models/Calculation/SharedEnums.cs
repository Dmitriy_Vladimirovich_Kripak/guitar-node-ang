﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{

    public enum CalculationType
    {
        All = 0,
        A10,
        A20,
        A40,
        A60,
        A90,
        B10,
        B20,
        B30,
        D10,
        C10,
        C10Fitout,
        C20,
        C20Fitout,
        D20,
        D20Fitout,
        D30,
        D30Fitout,
        D40,
        D40Fitout,
        D50,
        D50Fitout,
        D60,
        D60Fitout,
        D70,
        D70Fitout,
        D80,
        D80Fitout,
        E10,
        E10Fitout,
        E20,
        E20Fitout,
        Z10BC,
        Z70BC,
        Z910BC,
        Z920BC,
        Z930BC,
        Z940BC,
        F10BC,
        F20BC,
        F30BC,
        G10SD,
        G20SD,
        G60SD,
        Z10SD,
        Z70SD,
        Z910SD,
        Z920SD,
        Z930SD,
        Z940SD,
        G30SU,
        G40SU,
        G50SU,
        Z10SU,
        Z70SU,
        Z910SU,
        Z920SU,
        Z930SU,
        Z940SU,
        SoftCost,
        TotalProjectCost,

    }

    public enum BuildingType
    {
        AcademicClassroomBuilding = 0,
        LaboratoryBuilding,
        StudentHousing,
        Dining,
        ParkingGarageBelowGrade,
        ParkingGarageAboveGrade,
    }
    public enum SoilCondition
    {
        Soil = 0,
        RippableRock,
        SolidRock,
    }
    public enum SiteConstraint
    {
        Constrained = 0,
        Open,
    }

    public enum ArchitecturalExpectation
    {
        Iconic = 0,
        CampusStandard,
        LowCost,
    }

    public enum PerformanceExpectation
    {
        LEEDPlatinumEquivalent = 0,
        LEEDGoldEquivalent,
        LEEDSilverEquivalent,
    }

    public enum LocationFactor
    {
        NationalAverage = 0,
        LosAngeles,
        TBD
    }

    public enum ProjectDeliveryStrategyFactor
    {
        DesignBidBuild = 0,
        CMARRegionalTier2,
        CMARRegionalTier1,
        CMARNational
    }

    public enum Plumbing
    {
        Typical = 0,
        Flexible,
    }

    public enum HVAC
    {
        Air = 0,
        Hydronic,
        Hybrid,
    }

    public enum BuildingPlant
    {
        Local = 0,
        Remote
    }
    public enum FireProtection
    {
        Wet = 0,
        WetPreAction,
        Dry,
    }

    public enum Electrical
    {
        Typical = 0,
        Comprehensive,
    }

    public enum EscalateTo
    {
        EstimateDate = 0,
        ConstructionStartDate,
        ConstructionMidpoint,
        ConstructionDuration,
    }

    public enum FacilityRemediation
    {
        Simple = 0,
        Complex,
    }

    public enum FacilityDemolition
    {
        Simple = 0,
        Complex,
    }
    public enum SitePreparation
    {
        Greenfield = 0,
        Brownfield,
    }
    public enum SiteDevelopment 
    {
        Simple = 0,
        Moderate,
        Extensive,
    }
    public enum SiteInfrastructureServices
    {
        Proximate = 0,
        Remote,      
    }
    public enum SiteServices 
    {
        SteamAndCondensate = 0,
        HotWaterSupply,
        ChilledWaterSupply
    }
    public enum SiteInfrastructureCivil
    {
        Proximate = 0,
        Remote,        
    }
    public enum SiteCivil 
    {
        WaterOrFire = 0,
        SanitarySewer,
        StormSewer,
        Gas
    }

}
