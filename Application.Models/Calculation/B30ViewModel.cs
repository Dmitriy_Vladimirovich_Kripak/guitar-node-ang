﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class B30ViewModel
    {
        public int GrossSquareFeet { get; set; }
        public int NumberOfLevels { get; set; }
        public int  BuildingFootprint { get; set; }
        public BuildingType BuildingType { get; set; }
        public double RoofTerraceRatio { get; set; }
        public double RoofGlazingRatio { get; set; }
        public double HorizontalArticulationFactor { get; set; }
        public ArchitecturalExpectation ArchitecturalExpectation { get; set; }
        public PerformanceExpectation PerformanceExpectation { get; set; }
        public LocationFactor LocationFactor { get; set; }
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }

    }
}
