﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class ProjectViewModel
    {
        public BuildingType BuildingType { get; set; }
        //A10, A20, A40, A90, B10, B20, B30
        public int NumberOfLevels { get; set; }
        //A20, A90
        public int NumberOfBelowGradeLevels { get; set; }

        public int FloorPlateWidth { get; set; }
        //B20
        public int NumberOfAboveGradeLevels { get; set; }
        //A20, A40, A90, B10, B20, B30, C10, F10BC, F20BC, F30BC, 
        public double GrossSquareFeet { get; set; }
        //G10SD,G20SD, G60SD , G30SU, G40SU,  G50SU
        public double GrossSiteArea { get; set; }
        //G10SD,G20SD, G60SD, G30SU, G40SU,  G50SU
        public double BuildingFootprint { get; set; }
        public double DevelopedSiteArea { get; set; }
        /* Main building settings */
        //A10, A90
        public SoilCondition SoilCondition { get; set; }
        //A20, B20
        public double CladdingRatio { get; set; }
        //A20
        public double FFHeights { get; set; }
        //A90
        public double BasementFFHeights { get; set; }
        //A20
        public SiteConstraint SiteConstraint { get; set; }
        //B20
        public double GlazingRatio { get; set; }
        //B10, B30
        public double HorizontalArticulationFactor { get; set; }
        //B20
        public double VerticalArticulationFactor { get; set; }
        //D20, D20Fitout
        public Plumbing Plumbing { get; set; }
        //D30, D30Fitout, D80, D80Fitout
        public HVAC HVAC { get; set; }
        //D30, D30Fitout, D80, D80Fitout, G30SU
        public BuildingPlant BuildingPlant { get; set; }
        //D40, D40Fitout
        public FireProtection FireProtection { get; set; }
        //D50, D50Fitout, D60, D60Fitout, D70, D70Fitout
        public Electrical Electrical { get; set; }
        //A10, A20, A40, A90, B10, B20, B30, C10, C10Fitout, C20, C20Fitout, D10, D20,  D20Fitout, D30, D30Fitout, D40, D40Fitout
        //D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout, E10, E10Fitout,E20, E20Fitout, F10BC, F20BC
        //F30BC,G10SD, G20SD, G60SD , G30SU, G40SU,  G50SU
        public LocationFactor LocationFactor { get; set; }
        //A10, A20, A40, A90, B10, B20, B30, C10, C10Fitout,C20, C20Fitout, D10, D20,  D20Fitout, D30, D30Fitout, D40, D40Fitout
        //D50, D50Fitout, D60, D60Fitout,D70, D70Fitout, D80, D80Fitout, E10, E10Fitout, E20, E20Fitout, F10BC, F20BC
        //F30BC,Z10BC, Z930BC, Z940BC,G10SD, G20SD, G60SD, Z10SD, Z70SD, Z930SD, Z940SD, G30SU, G40SU,  G50SU, Z10SU
        //Z70SU, Z930SU, Z940SU
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }

        /* Core+Shell and Fitout settings */
        //B10
        public string FloorConstructionType { get; set; }
        //B10
        public string RoofConstructionType { get; set; }
        //B20, B30, C10, C10Fitout, C20, C20Fitout, D20, D20Fitout, D30, D30Fitout, D40, D40Fitout, D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout
        //E10, E10Fitout, E20,E20Fitout
        public ArchitecturalExpectation ArchitecturalExpectation { get; set; }
        //B20, B30, D20, D20Fitout, D30, D30Fitout, D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout
        public  PerformanceExpectation PerformanceExpectation { get; set; }
        //B30
        public double RoofTerraceRatio { get; set; }
        //B30
        public double RoofGlazingRatio { get; set; }
        //C10
        public double BuildingEfficiency { get; set; }
        //C10, C10Fitout, D20, D20Fitout, D30Fitout, D40, D40Fitout, D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout 
        //E10, E10Fitout, E20, E20Fitout
        public bool AssignableProgram1IsUsing { get; set; }
        public bool AssignableProgram2IsUsing { get; set; }
        public bool  AssignableProgram3IsUsin { get; set; }
        public bool AssignableProgram4IsUsing { get; set; }
        public bool AssignableProgram5IsUsing { get; set; }
        public bool AssignableProgram6IsUsing { get; set; }
        public bool AssignableProgram7IsUsing { get; set; }
        public bool AssignableProgram8IsUsing { get; set; }

        public double AssignableProgram1  { get; set; }
        public double AssignableProgram2  { get; set; }
        public double AssignableProgram3  { get; set; }
        public double AssignableProgram4  { get; set; }
        public double AssignableProgram5  { get; set; }
        public double AssignableProgram6  { get; set; }
        public double AssignableProgram7  { get; set; }
        public double AssignableProgram8  { get; set; }
        public double AssignableProgram1ASF { get; set; }
        public double AssignableProgram2ASF { get; set; }
        public double AssignableProgram3ASF { get; set; }
        public double AssignableProgram4ASF { get; set; }
        public double AssignableProgram5ASF { get; set; }
        public double AssignableProgram6ASF { get; set; }
        public double AssignableProgram7ASF { get; set; }
        public double AssignableProgram8ASF { get; set; }


        //C10, C10Fitout, D20, D20Fitout, D30Fitout, D40, D40Fitout, D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout
        //E10, E10Fitout, E20, E20Fitout
        public double GrossUpProgram1 { get; set; }
        public double GrossUpProgram2 { get; set; }
        public double GrossUpProgram3 { get; set; }
        public double GrossUpProgram4 { get; set; }
        public double GrossUpProgram1ASF { get; set; }
        public double GrossUpProgram2ASF { get; set; }
        public double GrossUpProgram3ASF { get; set; }
        public double GrossUpProgram4ASF { get; set; }
       
        public string status { get; set; }

        /* Taxes Escalations Fees settings */
        //Z910BC,Z910SD, Z70SU
        public double ContingenciesDesign { get; set; }
        //Z920BC , Z920SD, Z920SU
        public EscalateTo EscalateTo { get; set; }
        //Z920BC , Z920SD , Z920SU        
        public DateTime EstimateDate { get; set; }
       
            //Z920BC , Z920SD , Z920SU
        public string    EstimateDateStr { get; set; }
        //Z920BC , Z920SD, Z920SU
        public DateTime ConstructionStartDate { get; set; }
        //Z920BC , Z920SD, Z920SU
        public string ConstructionStartDateStr { get; set; }
        //Z920BC , Z920SD, Z920SU
        public int DurationInMonths { get; set; }

        //Contingencies (Escalation) 
        public double Z920Percent { get; set; }
        //Contingencies (Construction)
        public double Z930Percent { get; set; }

        /* Building Constraction settings */
        //F10BC
        public int SpecialConstructionAllowance { get; set; }
        //F20BC, F30BC
        public int AreaOfFacilityToBeRemediated { get; set; }
        public int AreaOfFacilityToBeDemolished { get; set; }
        //F20BC, F30BC
        public FacilityRemediation  FacilityRemediation { get; set; }
        public FacilityDemolition FacilityDemolition { get; set; }

        /* Site Development settings */
        //G10SD
        public SitePreparation  SitePreparation { get; set; }
        //G20SD
        public SiteDevelopment  SiteDevelopment { get; set; }


        /* Site Utilities settings */
        //G30SU, G40SU, G50SU
        public SiteInfrastructureServices SiteInfrastructureServices { get; set; }
        //G30SU
        public SiteInfrastructureCivil SiteInfrastructureCivil { get; set; }
        //G30SU
        public bool SiteServicesHotWater { get; set; }
        //G30SU
        public bool SiteServicesSteamCondensate { get; set; }
        //G30SU
        public bool SiteServicesChilledWaterSupply { get; set; }
        //G30SU
        public bool SiteCivilWaterFire { get; set; }
        //G30SU
        public bool SiteCivilSanitarySewer { get; set; }
        //G30SU
        public bool SiteCivilStormSewer { get; set; }
        //G30SU
        public bool SiteCivilGas { get; set; }
        //G40SU, G50SU
        public bool SiteInfrastructureElectrical { get; set; }

        /* Project Costs settings */
        public double DesignFee { get; set; }
        public double DesignFeeASF { get; set; }
        public double FFE { get; set; }
        public double FfeASF { get; set; }
        public double Technology { get; set; }
        public double TechnologyASF { get; set; }
        public double Contingency { get; set; }
        public double ContingencyASF { get; set; }
        public double Misc { get; set; }
        public double MiscASF { get; set; }

    }
}
