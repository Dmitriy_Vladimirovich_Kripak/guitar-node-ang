﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class B10ViewModel
    {
        public int GrossSquareFeet { get; set; }
        public int NumberOfLevels { get; set; }
        public BuildingType BuildingType { get; set; }
        public string FloorConstructionType { get; set; } // different enums for each building type, unfortunatly enum unsuported incheritence
        public string RoofConstructionType { get; set; } // different enums for each building type, unfortunatly enum unsuported incheritence

        public double HorizontalArticulationFactor { get; set; }
        public LocationFactor LocationFactor { get; set; }
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }

    }
}
