﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class Z930FitoutViewModel
    {
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }
        public BuildingType BuildingType { get; set; }
        public double A10F30SumOfFitouts { get; set; }
        public double Z910Fitout { get; set; }
        public double Z920Fitout { get; set; }
    }
}
