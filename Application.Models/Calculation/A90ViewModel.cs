﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class A90ViewModel
    {
        public int GrossSquareFeet { get; set; }
        public int NumberOfLevels { get; set; }
        public int NumberOfBelowGradeLevels { get; set; }
        public int BuildingFootprint { get; set; }
        public double BasementFFHeights { get; set; }
        public SoilCondition SoilCondition { get; set; }
        public LocationFactor LocationFactor { get; set; }
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }
        public double CladdingRatio { get; set; }
    }
}
