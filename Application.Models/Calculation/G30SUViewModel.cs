﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class G30SUViewModel
    {
        public int GrossSiteArea { get; set; }
        public int BuildingFootprint { get; set; }
        public int DevelopedSiteArea { get; set; }
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }
        public LocationFactor LocationFactor { get; set; }
        public BuildingPlant BuildingPlant { get; set; }
        public SiteInfrastructureServices SiteInfrastructureServices { get; set; }
        public SiteInfrastructureCivil SiteInfrastructureCivil { get; set; }
        public bool SiteServicesHotWater { get; set; }
        public bool SiteServicesSteamCondensate { get; set; }
        public bool SiteServicesChilledWaterSupply { get; set; }      
        public bool SiteCivilWaterFire { get; set; }
        public bool SiteCivilSanitarySewer { get; set; }
        public bool SiteCivilStormSewer { get; set; }
        public bool SiteCivilGas { get; set; }


    }

}
