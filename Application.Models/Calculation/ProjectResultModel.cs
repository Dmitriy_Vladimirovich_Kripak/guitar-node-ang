﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class ProjectResultModel
    {
        public string status { get; set; }
        public string StrDataDate { get; set; }
        public DateTime DataDate { get; set; }
        public double A10 { get; set; }
        public double A20 { get; set; }
        public double A40 { get; set; }
        public double A60 { get; set; }
        public double A90 { get; set; }
        public double B10 { get; set; }
        public double B20 { get; set; }
        public double B30 { get; set; }
        public double C10 { get; set; }
        public double C10Fitout { get; set; }
        public double C20 { get; set; }
        public double C20Fitout { get; set; }
        public double D10 { get; set; }
        public double D20 { get; set; }
        public double D20Fitout { get; set; }
        public double D30 { get; set; }
        public double D30Fitout { get; set; }
        public double D40 { get; set; }
        public double D40Fitout { get; set; }
        public double D50 { get; set; }
        public double D50Fitout { get; set; }
        public double D60 { get; set; }
        public double D60Fitout { get; set; }
        public double D70 { get; set; }
        public double D70Fitout { get; set; }
        public double D80 { get; set; }
        public double D80Fitout { get; set; }
        public double E10 { get; set; }
        public double E10Fitout { get; set; }
        public double E20 { get; set; }
        public double E20Fitout { get; set; }
        public double F10BC { get; set; }
        public double F20BC { get; set; }
        public double F30BC { get; set; }

        public double Z910BC { get; set; }      
        public double Z920BC { get; set; }       
        public double Z930BC { get; set; }      
        public double Z10BC { get; set; }   
        public double Z70BC { get; set; }
        public double Z940BC { get; set; }

        public double Z910Fitout { get; set; }
        public double Z920Fitout { get; set; }
        public double Z930Fitout { get; set; }
        public double Z10Fitout { get; set; }
        public double Z70Fitout { get; set; }
        public double Z940Fitout { get; set; }

        public double Z10Percent { get; set; }
        public double Z70Percent { get; set; }
        public double Z910Percent { get; set; }
        public double Z920Percent { get; set; }
        public double Z930Percent { get; set; }
        public double Z940Percent { get; set; }

        public double G10SD { get; set; }
        public double G20SD { get; set; }
        public double G60SD { get; set; }   
        public double Z910SD { get; set; }       
        public double Z920SD { get; set; }       
        public double Z930SD { get; set; }        
        public double Z10SD { get; set; }      
        public double Z70SD { get; set; }
        public double Z940SD { get; set; }
        public double G30SU { get; set; }
        public double G40SU { get; set; }
        public double G50SU { get; set; }
        public double Z910SU { get; set; }   
        public double Z920SU { get; set; }     
        public double Z930SU { get; set; }      
        public double Z10SU { get; set; }
        public double Z70SU { get; set; }
        public double Z940SU { get; set; }
        public double A10F30SumOfBC { get; set; }
        public double A10F30SumOfFitouts { get; set; }
        public double A10F30Sum { get; set; }
        public double G10G60Sum { get; set; }
        public double G30G50SUSum { get; set; }
      
        public double GrossSiteArea { get; set; }
        public double GrossSquareFeet { get; set; }
        public double DevelopedSiteArea { get; set; }
        public double DesignFee { get; set; }
        public double FFE { get; set; }      
        public double Technology { get; set; }        
        public double Contingency { get; set; }    
        public double Misc { get; set; }


        public double A_Substructure { get; set; }
        public double B_Shell { get; set; }
        public double C_Interiors { get; set; }
        public double D_Services { get; set; }
        public double E_EquipmentFurnishings { get; set; }
        public double F_ConstructionDemolition { get; set; }
        public double SubtotalDirectCost_building { get; set; }
        public double ContractorMarkups_building { get; set; }
        public double Contingencies_building { get; set; }
        public double TotalBuildingConstruction { get; set; }

        public double SiteDevelopment_site { get; set; }
        public double SiteUtilities_site { get; set; }
        public double SubtotalDirectCost_site { get; set; }
        public double ContractorMarkups_site { get; set; }
        public double Contingencies_site { get; set; }
        public double TotalSiteConstruction_site { get; set; }

        public double TotalConstructionCosts { get; set; }

        public double SoftCosts { get; set; }
        public double TotalProjectCosts { get; set; }

        public double SiteUtilitiesSum { get; set; }
        public double SiteDevelopmentSum { get; set; }
        public double BuildingConstuctionSum { get; set; }
    }   
}     