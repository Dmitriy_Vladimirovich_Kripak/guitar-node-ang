﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class Z920FitoutViewModel : BaseZ920ViewModel
    {
        public double A10F30SumOfFitouts { get; set; }
        public double Z910Fitout { get; set; }
    }
}
