﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class Z70SDViewModel
    {
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }
        public BuildingType BuildingType { get; set; }       
        public double G10G60Sum { get; set; }
        public double Z910SD { get; set; }
        public double Z920SD { get; set; }
        public double Z930SD { get; set; }
        public double Z10SD { get; set; }

    }

}
