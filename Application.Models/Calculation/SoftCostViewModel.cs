﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class SoftCostViewModel
    {
        public double TotalConstructionCosts { get; set; }
        public double DesignFee { get; set; }
        public double FFE { get; set; }
        public double Technology { get; set; }
        public double Contingency { get; set; }
        public double Misc { get; set; }


    }

}
