﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class Z70BCViewModel
    {
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }
        public BuildingType BuildingType { get; set; }
        public double A10F30SumOfBC { get; set; }
        public double Z910BC { get; set; }
        public double Z920BC { get; set; }
        public double Z930BC { get; set; }
        public double Z10BC { get; set; }
    }

}
