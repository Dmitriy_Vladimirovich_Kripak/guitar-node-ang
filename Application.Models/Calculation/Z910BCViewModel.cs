﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class Z910BCViewModel
    {
        public double ContingenciesDesign { get; set; }
        public double A10F30SumOfBC { get; set; }
    }

}
