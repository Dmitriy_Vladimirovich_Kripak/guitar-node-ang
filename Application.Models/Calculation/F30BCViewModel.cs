﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class F30BCViewModel
    {
        public int GrossSquareFeet { get; set; }
        public int AreaOfFacilityToBeDemolished { get; set; }      
        public FacilityDemolition FacilityDemolition  { get; set; }
        public LocationFactor LocationFactor { get; set; }
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }
    }

}
