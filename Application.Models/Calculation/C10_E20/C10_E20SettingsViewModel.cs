﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation.C10_E20
{
    public class C10_E20SettingsViewModel
    {
        public bool AssignableProgram1IsUsing { get; set; }
        public bool AssignableProgram2IsUsing { get; set; }
        public bool AssignableProgram3IsUsing { get; set; }
        public bool AssignableProgram4IsUsing { get; set; }
        public bool AssignableProgram5IsUsing { get; set; }
        public bool AssignableProgram6IsUsing { get; set; }
        public bool AssignableProgram7IsUsing { get; set; }
        public bool AssignableProgram8IsUsing { get; set; }

        public string AssignableProgram1Caption { get; set; }
        public string AssignableProgram2Caption { get; set; }
        public string AssignableProgram3Caption { get; set; }
        public string AssignableProgram4Caption { get; set; }
        public string AssignableProgram5Caption { get; set; }
        public string AssignableProgram6Caption { get; set; }
        public string AssignableProgram7Caption { get; set; }
        public string AssignableProgram8Caption { get; set; }

        public string GrossUpProgram1Caption { get; set; }
        public string GrossUpProgram2Caption { get; set; }
        public string GrossUpProgram3Caption { get; set; }
        public string GrossUpProgram4Caption { get; set; }
    }
}
