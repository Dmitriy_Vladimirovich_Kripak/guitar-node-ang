﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation.C10_E20
{
    public class BaseC10_E20ViewModel
    {
        public int GrossSquareFeet { get; set; }
        public BuildingType BuildingType { get; set; }

        public double BuildingEfficiency { get; set; }

        public double AssignableProgram1 { get; set; }
        public double AssignableProgram2 { get; set; }
        public double AssignableProgram3 { get; set; }
        public double AssignableProgram4 { get; set; }
        public double AssignableProgram5 { get; set; }
        public double AssignableProgram6 { get; set; }
        public double AssignableProgram7 { get; set; }
        public double AssignableProgram8 { get; set; }

        public double GrossUpProgram1 { get; set; }
        public double GrossUpProgram2 { get; set; }
        public double GrossUpProgram3 { get; set; }
        public double GrossUpProgram4 { get; set; }
    }
}
