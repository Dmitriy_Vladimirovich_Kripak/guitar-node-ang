﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation.C10_E20
{
    public class D30ViewModel: BaseC10_E20ViewModel
    {
        public LocationFactor LocationFactor { get; set; }
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }

        public ArchitecturalExpectation ArchitecturalExpectation { get; set; }
        public PerformanceExpectation PerformanceExpectation { get; set; }
        public HVAC HVAC { get; set; }
        public BuildingPlant BuildingPlant { get; set; }
    }
}
