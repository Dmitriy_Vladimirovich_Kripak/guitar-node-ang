﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class Z920BCViewModel: BaseZ920ViewModel
    {
        public double A10F30SumOfBC { get; set; }
        public double Z910BC { get; set; }
    }

}
