﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Calculation
{
    public class G40SUViewModel
    {
        public int GrossSiteArea { get; set; }
        public int BuildingFootprint { get; set; }
        public int DevelopedSiteArea { get; set; }
        public ProjectDeliveryStrategyFactor ProjectDeliveryStrategyFactor { get; set; }
        public LocationFactor LocationFactor { get; set; }
        public SiteInfrastructureServices SiteInfrastructureServices { get; set; }      
        public bool SiteInfrastructureElectrical { get; set; }
       
    }

}
