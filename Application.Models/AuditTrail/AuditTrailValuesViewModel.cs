﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.AuditTrail
{
    public class AuditTrailValuesViewModel
    {
        public int Id { get; set; }
        public int AuditTrailEntityId { get; set; }
        public string PropertyName { get; set; }
        public string Action { get; set; }
        public string EntityType { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}
