﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.AuditTrail
{
    public class AuditTrailViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; }

        public string EntityName { get; set; }
        public string EntityType { get; set; }
        public string Action { get; set; }

        public IList<AuditTrailValuesViewModel> AuditTrailValues { get; set; }

    }
}
