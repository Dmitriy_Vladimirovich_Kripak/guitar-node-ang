﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.AuditTrail
{
    public class PaginationAuditTrailsViewModel
    {
        public int TotalCount { get; set; }
        public IList<AuditTrailViewModel> AuditTrails { get; set; }
    }
}
