﻿using Application.Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.DTO
{
    public class FileInfoDTO
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public byte[] File { get; set; }       

        public string Date { get; set; }

        public string Label { get; set; }

        public ProjectModel Project { get; set; }
    }
}
