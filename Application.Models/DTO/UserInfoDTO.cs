﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.DTO
{
    public class UserInfoDTO
    {
        public string Id { get; set; }

        public string Username { get; set; }
        
        public string Email { get; set; }

        public string[] Roles { get; set; }

    }
}
