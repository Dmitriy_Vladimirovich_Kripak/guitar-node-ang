﻿using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Manage
{
    public class ComplexProjectValue
    {
        public ProjectModel Project { get; set; }

        public ProjectViewModel ProjectValue { get; set; }

        public ProjectResultModel CalcResult { get; set; }

    }
}
