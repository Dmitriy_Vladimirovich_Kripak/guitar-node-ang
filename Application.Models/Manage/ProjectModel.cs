﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Manage
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Institution { get; set; }
        public string CostModel { get; set; }
        public string CampusPrecint { get; set; }
        public List<ProjectValuesModel> ProjectValues { get; set; }
    }
}
