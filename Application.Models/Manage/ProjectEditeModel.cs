﻿using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Manage
{
    public class ProjectEditeModel
    {
        public bool IsUserProject { get; set; } 
        public ProjectViewModel ProjectValue { get; set; }
        public ProjectModel Project { get; set; }

    }
}
