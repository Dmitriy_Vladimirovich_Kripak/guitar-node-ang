﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Manage
{
    public class ApplicationResultObject
    {
        public string Status { get; set; }
        public object Data { get; set; }
        public Exception Error { get; set; }
       
    }
}
