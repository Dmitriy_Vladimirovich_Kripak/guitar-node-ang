﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Manage
{
    public class PaginationProjectViewModel
    {
        public int TotalCount { get; set; }

        public IList<ProjectModel> Projects { get; set; }

        public PaginationProjectViewModel()
        {
            Projects = new List<ProjectModel>();
        }
    }
}
