﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Models.Manage
{
    public class ProjectValuesModel
    {
        public int ProjectId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }
    }
}
