﻿using Application.Models.DTO;
using Application.Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLLInterfaces.BusinessServicesInterfaces
{
    public interface ICampusInfo
    {
        CampusInfoDTO Update(CampusInfoDTO campusInfo);

        CampusInfoDTO Delete(CampusInfoDTO campusInfo);

        CampusInfoDTO Get(int campusId);

        CampusInfoDTO Get();
    }
}
