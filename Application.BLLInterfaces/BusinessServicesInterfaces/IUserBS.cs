﻿using Application.Entities;
using Application.Models;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using Application.Models.DTO;
using Application.Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLLInterfaces.BusinessServicesInterfaces
{
    public interface IUserBS
    {
        List<UserView> GetAllUsers();

        UserView GetUserById(string id);

        List<UserView> ChangeUserBlockStatus(UserView model);

        bool IsUserBlock(ApplicationUser model);

        string GetUserRole(string userName);

        UserView UpdateUserInfo(UserView user);

        ApplicationUser GetUserByName(string userName);

        UserInfoDTO GetUserInfo(string userName);
    }
}
