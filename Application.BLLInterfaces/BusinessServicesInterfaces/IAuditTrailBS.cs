﻿using Application.Entities;
using Application.Entities.AuditTrailSupportEntities;
using Application.Models;
using Application.Models.AuditTrail;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLLInterfaces.BusinessServicesInterfaces
{
    public interface IAuditTrailBS
    {

        AuditTrail FormAuditData(IList<DbEntityEntry> added, IList<CustomDbEntityEntry> modified, IList<CustomDbEntityEntry> deleted, ApplicationUser user);
        /// <summary>
        /// workaround: 
        /// Issue: IdentityDbContext<ApplicationUser> doesn't invoke SaveChanges method when we add new User
        /// We invoke this method manually in AccountController.Register after success User's registration
        /// </summary>
        void ProcessUser(ApplicationUser user, bool adding);

        IList<AuditTrailViewModel> GetAuditTrailInfo();
        PaginationAuditTrailsViewModel GetAuditTrailInfoForPaging(PaginationRequest paging);

    }
}
