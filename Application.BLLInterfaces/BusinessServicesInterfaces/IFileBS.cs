﻿using Application.Models.DTO;
using Application.Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLLInterfaces.BusinessServicesInterfaces
{
    public interface IFileBS
    {
        FileInfoDTO GetById(int fileId);

        List<FileInfoDTO> GetAll(bool showAll, string userName);

        FileInfoDTO SaveFile(ProjectModel file, string fileName);

        bool DeleteFile(int fileInfoId);
    }
}
