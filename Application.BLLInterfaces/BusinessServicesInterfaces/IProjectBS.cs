﻿using Application.Entities;
using Application.Models;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using Application.Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLLInterfaces.BusinessServicesInterfaces
{
    public interface IProjectBS
    {
        List<ProjectModel> GetAllProject(string userName);
        Project GetProjectByUserId(string userId);
        Project GetProjectById(int projectId, string currentUserName);
        ComplexProjectValue GetProjectValues(ProjectModel model);
        ComplexProjectValue AddProject(ComplexProjectValue model);
        PaginationProjectViewModel GetPagedListProject(PaginationRequest paginationModel, string userName);
        bool DeleteProject(int projectId);
        string SaveAsProject(ComplexProjectValue model);
        byte[] GetPDFByProject(ProjectResultModel resultModel,ProjectModel projectModel);
        bool IsProjectExist(ProjectModel model);
         
    }
}
