﻿using Application.Models;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLLInterfaces.BusinessServicesInterfaces
{
    public interface ICalculationProjectBS
    {        
        ProjectResultModel CalcProject(ProjectViewModel model);
    }
}
