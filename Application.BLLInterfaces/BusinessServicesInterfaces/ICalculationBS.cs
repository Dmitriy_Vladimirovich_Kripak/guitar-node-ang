﻿using Application.Models;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BLLInterfaces.BusinessServicesInterfaces
{
    public interface ICalculationBS
    {
        void ConfigureCalculationBS();

        BaselineVariablesViewModel GetBaselineVariables(CalculationType calculationType);
        C10_E20SettingsViewModel GetC10_E20ViewSettings(BuildingType buildingType);
        string UpdateVariable(BaselineVariableViewModel model);
        string CalcA10(A10ViewModel model);
        string CalcA20(A20ViewModel model);
        string CalcA40(A40ViewModel model);
        string CalcA60(A60ViewModel model);
        string CalcA90(A90ViewModel model);
        string CalcB10(B10ViewModel model);
        string CalcB20(B20ViewModel model);
        string CalcB30(B30ViewModel model);
        string CalcD10(D10ViewModel model);
        string CalcC10(C10ViewModel model);
        string CalcC10Fitout(C10FitoutViewModel model);
        string CalcC20(C20ViewModel model);
        string CalcC20Fitout(C20FitoutViewModel model);
        string CalcD20(D20ViewModel model);
        string CalcD20Fitout(D20FitoutViewModel model);
        string CalcD30(D30ViewModel model);
        string CalcD30Fitout(D30FitoutViewModel model);
        string CalcD40(D40ViewModel model);
        string CalcD40Fitout(D40FitoutViewModel model);
        string CalcD50(D50ViewModel model);
        string CalcD50Fitout(D50FitoutViewModel model);
        string CalcD60(D60ViewModel model);
        string CalcD60Fitout(D60FitoutViewModel model);
        string CalcD70(D70ViewModel model);
        string CalcD70Fitout(D70FitoutViewModel model);
        string CalcD80(D80ViewModel model);
        string CalcD80Fitout(D80FitoutViewModel model);
        string CalcE10(E10ViewModel model);
        string CalcE10Fitout(E10FitoutViewModel model);
        string CalcE20(E20ViewModel model);
        string CalcE20Fitout(E20FitoutViewModel model);        
        string CalcF10BC(F10BCViewModel model);
        string CalcF20BC(F20BCViewModel model);
        string CalcF30BC(F30BCViewModel model);
        string CalcZ10BC(Z10BCViewModel model);
        string CalcZ70BC(Z70BCViewModel model);
        string CalcZ910BC(Z910BCViewModel model);
        string CalcZ920BC(Z920BCViewModel model);
        string CalcZ930BC(Z930BCViewModel model);
        string CalcZ940BC(Z940BCViewModel model);
        string CalcG10SD(G10SDViewModel model);
        string CalcG20SD(G20SDViewModel model);
        string CalcG60SD(G60SDViewModel model);
        string CalcZ10SD(Z10SDViewModel model);
        string CalcZ70SD(Z70SDViewModel model);
        string CalcZ910SD(Z910SDViewModel model);
        string CalcZ920SD(Z920SDViewModel model);
        string CalcZ930SD(Z930SDViewModel model);
        string CalcZ940SD(Z940SDViewModel model);
        string CalcG30SU(G30SUViewModel model);
        string CalcG40SU(G40SUViewModel model);
        string CalcG50SU(G50SUViewModel model);
        string CalcZ10SU(Z10SUViewModel model);
        string CalcZ70SU(Z70SUViewModel model);
        string CalcZ910SU(Z910SUViewModel model);
        string CalcZ920SU(Z920SUViewModel model);
        string CalcZ930SU(Z930SUViewModel model);
        string CalcZ940SU(Z940SUViewModel model);
        string CalcSoftCost(SoftCostViewModel model);
        string CalcTotalProjectCost(TotalProjectCostViewModel model);
    }
}
