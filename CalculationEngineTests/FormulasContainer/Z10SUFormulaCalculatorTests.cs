﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z10SUFormulaCalculatorTests
    {


        private static Z10SUFormulaCalculator _z10formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC, "0.1");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC, "0.085");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC, "0.115");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC, "0.1");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC, "0.085");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC, "0.115");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC, "0.085");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC, "0.07");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC, "0.09");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC, "0.075");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC, "0.095");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC, "0.085");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC, "0.085");


            localStorage.Update(values);

            _z10formulacalculator = new Z10SUFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z10SUDefaultValues()
        {
            Z10SUViewModel model = new Z10SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910SU = 3.03;
            model.Z920SU = 4.45;
            model.Z930SU = 0.98;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 3.37);
        }
        [TestMethod]
        public void Z10SU_Dining_CMARRegionalTier2()
        {
            Z10SUViewModel model = new Z10SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.Dining;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910SU = 3.03;
            model.Z920SU = 4.45;
            model.Z930SU = 0.98;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 2.53);
        }
        [TestMethod]
        public void Z10SU_Z920_Z910()
        {
            Z10SUViewModel model = new Z10SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910SU = 4.23;
            model.Z920SU = 3.23;
            model.Z930SU = 0.98;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 3.37);
        }

    }
}
