﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class G50SUFormulaCalculatorTests
    {

        private static G50SUFormulaCalculator _G50SUFormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.SiteInfrastructureServicesProximate, "300");
            values.Add(BaselineVariableNames.SiteInfrastructureServicesRemote, "1200");
            values.Add(BaselineVariableNames.SiteInfrastructureCommunications, "325");
            values.Add(BaselineVariableNames.SiteCommunications, "1.0");




            localStorage.Update(values);

            _G50SUFormulaCalculator = new G50SUFormulaCalculator(localStorage);
        }

        [TestMethod]
        public void G50SUDefaultValues()
        {
            G50SUViewModel model = new G50SUViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.SiteInfrastructureElectrical = true;
            

            var result = _G50SUFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 3.24);
        }
        [TestMethod]
        public void G50SU_TBD_CMARRegionalTier2_Electrical()
        {
            G50SUViewModel model = new G50SUViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARNational;
            model.SiteInfrastructureElectrical = false;
         

            var result = _G50SUFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 1.03);
        }
        [TestMethod]
        public void G50SU_NationalAverage()
        {
            G50SUViewModel model = new G50SUViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.SiteInfrastructureElectrical = true;


            var result = _G50SUFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2.95);
        }



    }
}
