﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z10SDFormulaCalculatorTests
    {


        private static Z10SDFormulaCalculator _z10formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC, "0.1");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC, "0.085");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC, "0.115");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC, "0.1");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC, "0.085");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC, "0.115");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC, "0.085");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC, "0.07");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC, "0.09");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC, "0.075");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC, "0.095");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC, "0.085");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC, "0.085");


            localStorage.Update(values);

            _z10formulacalculator = new Z10SDFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z10SDDefaultValues()
        {
            Z10SDViewModel model = new Z10SDViewModel();
            model.G10G60Sum = 34.87;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910SD = 4.18;
            model.Z920SD = 6.15;
            model.Z930SD = 1.36;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 4.66);
        }
        [TestMethod]
        public void Z10SD_CMARRegionalTier2()
        {
            Z10SDViewModel model = new Z10SDViewModel();
            model.G10G60Sum = 34.87;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910SD = 4.18;
            model.Z920SD = 6.15;
            model.Z930SD = 1.36;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 3.96);
        }
        [TestMethod]
        public void Z10SD_Dining_CMARRegionalTier2_Z910()
        {
            Z10SDViewModel model = new Z10SDViewModel();
            model.G10G60Sum = 34.87;
            model.BuildingType = BuildingType.Dining;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910SD = 4.18;
            model.Z920SD = 6.15;
            model.Z930SD = 1.36;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 3.49);
        }

    }
}
