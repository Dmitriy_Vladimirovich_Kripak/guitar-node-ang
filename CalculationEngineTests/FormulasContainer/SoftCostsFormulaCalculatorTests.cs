﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class SoftCostFormulaCalculatorTests
    {


        private static SoftCostFormulaCalculator _SoftCostformulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();           


            localStorage.Update(values);

            _SoftCostformulacalculator = new SoftCostFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void SoftCostDefaultValues()
        {
            SoftCostViewModel model = new SoftCostViewModel();
            model.DesignFee = 0.1;
            model.FFE = 0.05;
            model.Contingency = 0.07;
            model.Technology = 0.03;
            model.Misc = 0.1;
            model.TotalConstructionCosts = 657.43;
            var result = _SoftCostformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 230.10);
        }
        [TestMethod]
        public void SoftCost_FFE_Misc()
        {
            SoftCostViewModel model = new SoftCostViewModel();
            model.DesignFee = 0.1;
            model.FFE = 0.08;
            model.Contingency = 0.07;
            model.Technology = 0.03;
            model.Misc = 0.05;
            model.TotalConstructionCosts = 657.43;
            var result = _SoftCostformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 216.95);
        }
        [TestMethod]
        public void SoftCost_DesignFee_FFE()
        {
            SoftCostViewModel model = new SoftCostViewModel();
            model.DesignFee = 0.15;
            model.FFE = 0.03;
            model.Contingency = 0.14;
            model.Technology = 0.03;
            model.Misc = 0.05;
            model.TotalConstructionCosts = 657.43;
            var result = _SoftCostformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 262.97);
        }

    }
}
