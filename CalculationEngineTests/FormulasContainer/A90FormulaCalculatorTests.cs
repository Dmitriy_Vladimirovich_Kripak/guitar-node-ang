﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class A90FormulaCalculatorTests
    {
        private static A90FormulaCalculator _a90FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.EarthworkExcavationHaulAwayCYSoil, "30");
            values.Add(BaselineVariableNames.EarthworkExcavationHaulAwayCYRippableRock, "35");
            values.Add(BaselineVariableNames.EarthworkExcavationHaulAwayCYSolidRock, "70");

            localStorage.Update(values);

            _a90FormulaCalculator = new A90FormulaCalculator(localStorage);
        }

        [TestMethod]
        public void A90SoilCondition()
        {
            A90ViewModel model = new A90ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.NumberOfBelowGradeLevels = 1;
            model.BasementFFHeights = 20;
            model.SoilCondition = SoilCondition.RippableRock;

            var result = _a90FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 5.19);

        }

        [TestMethod]
        public void A90NumberOfBelowGradeLevels()
        {
            A90ViewModel model = new A90ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.NumberOfBelowGradeLevels = 2;
            model.BasementFFHeights = 20;
            model.SoilCondition = SoilCondition.RippableRock;

            var result = _a90FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 10.37);

            model.NumberOfLevels = 10;
            model.NumberOfBelowGradeLevels = 3;
            result = _a90FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 7.78);

        }

        [TestMethod]
        public void A90BasementFFHeights()
        {
            A90ViewModel model = new A90ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.NumberOfBelowGradeLevels = 1;
            model.BasementFFHeights = 30;
            model.SoilCondition = SoilCondition.RippableRock;

            var result = _a90FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 7.78);

            model.BasementFFHeights = 25;

            result = _a90FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 6.48);
        }


    }
}
