﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z940SUFormulaCalculatorTests
    {


        private static Z940SUFormulaCalculator _Z940formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ940BC, "0.04");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ940BC, "0.04");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ940BC, "0.04");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ940BC, "0.03");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ940BC, "0.03");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ940BC, "0.03");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ940BC, "0.03");
            


            localStorage.Update(values);

            _Z940formulacalculator = new Z940SUFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z940SUDefaultValues()
        {
            Z940SUViewModel model = new Z940SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910SU = 3.03;
            model.Z920SU = 4.45;
            model.Z930SU = 0.98;
            model.Z10SU = 3.37;
            model.Z70SU = 0.74;

            var result = _Z940formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 1.13);
        }
        [TestMethod]
        public void Z940SU_ParkingGarageBelowGrade_CMARRegionalTier2()
        {
            Z940SUViewModel model = new Z940SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.ParkingGarageBelowGrade;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910SU = 3.03;
            model.Z920SU = 4.45;
            model.Z930SU = 0.98;
            model.Z10SU = 3.37;
            model.Z70SU = 0.74;

            var result = _Z940formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.76);
        }
        [TestMethod]
        public void Z940SU_LaboratoryBuilding_DesignBidBuild_Z910()
        {
            Z940SUViewModel model = new Z940SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.LaboratoryBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.DesignBidBuild;
            model.Z910SU = 3.03;
            model.Z920SU = 4.45;
            model.Z930SU = 0.98;
            model.Z10SU = 3.37;
            model.Z70SU = 0.74;

            var result = _Z940formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 1.13);
        }

    }
}
