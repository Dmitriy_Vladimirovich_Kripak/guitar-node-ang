﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class B10FormulaCalculatorTests
    {

        private static B10FormulaCalculator _b10FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.NumberOfLevelsFactor, "0.95");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingDeepGSF, "20");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingShallowGSF, "10");
            values.Add(BaselineVariableNames.LaboratoryBuildingDeepGSF, "20");
            values.Add(BaselineVariableNames.LaboratoryBuildingShallowGSF, "10");
            values.Add(BaselineVariableNames.StudentHousingDeepGSF, "20");
            values.Add(BaselineVariableNames.StudentHousingShallowGSF, "10");
            values.Add(BaselineVariableNames.DiningDeepGSF, "20");
            values.Add(BaselineVariableNames.DiningShallowGSF, "10");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeDeepGSF, "20");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeShallowGSF, "10");
            values.Add(BaselineVariableNames.ParkingGarageAboveGradeDeepGSF, "20");
            values.Add(BaselineVariableNames.ParkingGarageAboveGradeShallowGSF, "10");

            values.Add(BaselineVariableNames.FloorConstructionAcademicClassroomConcreteFlatSlabSF, "50");
            values.Add(BaselineVariableNames.FloorConstructionAcademicClassroomConcreteSlabAndBeamSF, "55");
            values.Add(BaselineVariableNames.FloorConstructionAcademicClassroomSteelBracedFrameSF, "35");
            values.Add(BaselineVariableNames.FloorConstructionAcademicClassroomSteelMomentFrameSF, "40");
            values.Add(BaselineVariableNames.FloorConstructionLaboratoryConcreteFlatSlabSF, "60");
            values.Add(BaselineVariableNames.FloorConstructionLaboratoryConcreteSlabAndBeamSF, "65");
            values.Add(BaselineVariableNames.FloorConstructionLaboratorySteelBracedFrameSF, "55");
            values.Add(BaselineVariableNames.FloorConstructionLaboratorySteelMomentFrameSF, "60");
            values.Add(BaselineVariableNames.FloorConstructionStudentHousingConcreteFlatSlabSF, "40");
            values.Add(BaselineVariableNames.FloorConstructionStudentHousingConcreteSlabAndBeamSF, "45");
            values.Add(BaselineVariableNames.FloorConstructionStudentHousingSteelBracedFrameSF, "35");
            values.Add(BaselineVariableNames.FloorConstructionStudentHousingSteelMomentFrameSF, "40");
            values.Add(BaselineVariableNames.FloorConstructionStudentHousingWoodFrameSF, "30");
            values.Add(BaselineVariableNames.FloorConstructionDiningConcreteFlatSlabSF, "50");
            values.Add(BaselineVariableNames.FloorConstructionDiningConcreteSlabAndBeamSF, "55");
            values.Add(BaselineVariableNames.FloorConstructionDiningSteelBracedFrameSF, "45");
            values.Add(BaselineVariableNames.FloorConstructionDiningSteelMomentFrameSF, "50");
            values.Add(BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteFlatSlabSF, "35");
            values.Add(BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF, "40");
            values.Add(BaselineVariableNames.FloorConstructionParkingGarageBelowGradePrecastSF, "30");
            values.Add(BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteFlatSlabSF, "35");
            values.Add(BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF, "40");
            values.Add(BaselineVariableNames.FloorConstructionParkingGarageAboveGradePrecastSF, "30");

            values.Add(BaselineVariableNames.RoofConstructionAcademicClassroomConcreteFlatSlabSF, "45");
            values.Add(BaselineVariableNames.RoofConstructionAcademicClassroomConcreteSlabAndBeamSF, "40");
            values.Add(BaselineVariableNames.RoofConstructionAcademicClassroomSteelBracedFrameSF, "30");
            values.Add(BaselineVariableNames.RoofConstructionAcademicClassroomSteelMomentFrameSF, "35");
            values.Add(BaselineVariableNames.RoofConstructionLaboratoryConcreteFlatSlabSF, "55");
            values.Add(BaselineVariableNames.RoofConstructionLaboratoryConcreteSlabAndBeamSF, "60");
            values.Add(BaselineVariableNames.RoofConstructionLaboratorySteelBracedFrameSF, "50");
            values.Add(BaselineVariableNames.RoofConstructionLaboratorySteelMomentFrameSF, "55");
            values.Add(BaselineVariableNames.RoofConstructionStudentHousingConcreteFlatSlabSF, "35");
            values.Add(BaselineVariableNames.RoofConstructionStudentHousingConcreteSlabAndBeamSF, "40");
            values.Add(BaselineVariableNames.RoofConstructionStudentHousingSteelBracedFrameSF, "30");
            values.Add(BaselineVariableNames.RoofConstructionStudentHousingSteelMomentFrameSF, "35");
            values.Add(BaselineVariableNames.RoofConstructionStudentHousingWoodFrameSF, "25");
            values.Add(BaselineVariableNames.RoofConstructionDiningConcreteFlatSlabSF, "45");
            values.Add(BaselineVariableNames.RoofConstructionDiningConcreteSlabAndBeamSF, "50");
            values.Add(BaselineVariableNames.RoofConstructionDiningSteelBracedFrameSF, "40");
            values.Add(BaselineVariableNames.RoofConstructionDiningSteelMomentFrameSF, "45");
            values.Add(BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteFlatSlabSF, "50");
            values.Add(BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF, "55");
            values.Add(BaselineVariableNames.RoofConstructionParkingGarageBelowGradePrecastSF, "40");
            values.Add(BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteFlatSlabSF, "35");
            values.Add(BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF, "40");
            values.Add(BaselineVariableNames.RoofConstructionParkingGarageAboveGradePrecastSF, "30");

            values.Add(BaselineVariableNames.StairAcademicClassroomEgressFLT, "40000");
            values.Add(BaselineVariableNames.StairAcademicClassroomOrnamentalFLT, "120000");
            values.Add(BaselineVariableNames.StairLaboratoryEgressFLT, "40000");
            values.Add(BaselineVariableNames.StairLaboratoryOrnamentalFLT, "120000");
            values.Add(BaselineVariableNames.StairStudentHousingEgressFLT, "40000");
            values.Add(BaselineVariableNames.StairStudentHousingOrnamentalFLT, "120000");
            values.Add(BaselineVariableNames.StairDiningEgressFLT, "40000");
            values.Add(BaselineVariableNames.StairDiningOrnamentalFLT, "120000");
            values.Add(BaselineVariableNames.StairParkingGarageBelowGradeEgressFLT, "40000");
            values.Add(BaselineVariableNames.StairParkingGarageBelowGradeOrnamentalFLT, "120000");
            values.Add(BaselineVariableNames.StairParkingGarageAboveGradeEgressFLT, "40000");
            values.Add(BaselineVariableNames.StairParkingGarageAboveGradeOrnamentalFLT, "120000");

            values.Add(BaselineVariableNames.FlightAcademicClassroomEgressRatio, "0.00007");
            values.Add(BaselineVariableNames.FlightAcademicClassroomOrnamentalRatio, "0.00003");
            values.Add(BaselineVariableNames.FlightLaboratoryEgressRatio, "0.00007");
            values.Add(BaselineVariableNames.FlightLaboratoryOrnamentalRatio, "0.00003");
            values.Add(BaselineVariableNames.FlightStudentHousingEgressRatio, "0.00007");
            values.Add(BaselineVariableNames.FlightStudentHousingOrnamentalRatio, "0.00003");
            values.Add(BaselineVariableNames.FlightDiningEgressRatio, "0.00007");
            values.Add(BaselineVariableNames.FlightDiningOrnamentalRatio, "0.00003");
            values.Add(BaselineVariableNames.FlightParkingGarageBelowGradeEgressRatio, "0.00007");
            values.Add(BaselineVariableNames.FlightParkingGarageBelowGradeOrnamentalRatio, "0.00003");
            values.Add(BaselineVariableNames.FlightParkingGarageAboveGradeEgressRatio, "0.00007");
            values.Add(BaselineVariableNames.FlightParkingGarageAboveGradeOrnamentalRatio, "0.00003");

            localStorage.Update(values);

            _b10FormulaCalculator = new B10FormulaCalculator(localStorage);
        }

        [TestMethod]
        public void B10RoofConstructionType()
        {
            B10ViewModel model = new B10ViewModel();

            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfLevels = 5;
            model.GrossSquareFeet = 100000;
            model.HorizontalArticulationFactor = 1.2;
            model.FloorConstructionType = "ConcreteFlatSlab";
            model.RoofConstructionType = "ConcreteFlatSlab";
            var result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 57.2);

            model.RoofConstructionType = "SteelMomentFrame";
            result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 54.8);

            model.RoofConstructionType = "ConcreteSlabAndBeam";
            result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 56);
        }

        [TestMethod]
        public void B10HorizontalArticulationFactor()
        {
            B10ViewModel model = new B10ViewModel();

            model.BuildingType = BuildingType.StudentHousing;
            model.NumberOfLevels = 5;
            model.GrossSquareFeet = 100000;
            model.HorizontalArticulationFactor = 2;
            model.FloorConstructionType = "WoodFrame";
            model.RoofConstructionType = "ConcreteFlatSlab";
            var result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 44.4);

            model.HorizontalArticulationFactor = 1.4;
            result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 40.2);

            model.HorizontalArticulationFactor = 1;
            result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 37.4);
        }

        [TestMethod]
        public void B10FloorConstructionType()
        {
            B10ViewModel model = new B10ViewModel();

            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfLevels = 5;
            model.GrossSquareFeet = 100000;
            model.HorizontalArticulationFactor = 1.2;
            model.FloorConstructionType = "ConcreteFlatSlab";
            model.RoofConstructionType = "ConcreteFlatSlab";
            var result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 57.2);

            model.FloorConstructionType = "SteelMomentFrame";
            result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 49.2);

            model.FloorConstructionType = "SteelBracedFrame";
            result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 45.2);
        }

        [TestMethod]
        public void B10NumberOfLevels()
        {
            B10ViewModel model = new B10ViewModel();

            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfLevels = 10;
            model.GrossSquareFeet = 100000;
            model.HorizontalArticulationFactor = 1.2;
            model.FloorConstructionType = "ConcreteFlatSlab";
            model.RoofConstructionType = "ConcreteFlatSlab";
            var result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 56.8);

            model.NumberOfLevels = 20;
            result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 56.6);

            model.NumberOfLevels = 1;
            result = _b10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 60.4);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void B10WrongFloorConstructionType()
        {
            try
            {
                B10ViewModel model = new B10ViewModel();

                model.BuildingType = BuildingType.ParkingGarageAboveGrade;
                model.NumberOfLevels = 5;
                model.GrossSquareFeet = 100000;
                model.HorizontalArticulationFactor = 1.2;
                model.FloorConstructionType = "SteelMomentFrame";
                model.RoofConstructionType = "Precast";
                var result = _b10FormulaCalculator.CalculateFormula(model);
               
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Floor Construction Type:SteelMomentFrame for Building Type: ParkingGarageAboveGrade, possible values are: ConcreteFlatSlab, ConcreteSlabAndBeam, Precast");
                throw;
            }
        }
        
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void B10WrongRoofConstructionType()
        {
            try
            {
                B10ViewModel model = new B10ViewModel();

                model.BuildingType = BuildingType.Dining;
                model.NumberOfLevels = 5;
                model.GrossSquareFeet = 100000;
                model.HorizontalArticulationFactor = 1.2;
                model.FloorConstructionType = "SteelMomentFrame";
                model.RoofConstructionType = "Precast";
                var result = _b10FormulaCalculator.CalculateFormula(model);

            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Roof Construction Type:Precast for Building Type: Dining, possible values are: ConcreteFlatSlab, ConcreteSlabAndBeam, SteelBracedFrame, SteelMomentFrame");
                throw;
            }
        }

    }
}
