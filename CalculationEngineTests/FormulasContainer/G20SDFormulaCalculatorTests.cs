﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class G20SDSDFormulaCalculatorTests
    {

        private static G20SDFormulaCalculator _G20SDFormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.SiteDevelopmentExtensive, "40.0");
            values.Add(BaselineVariableNames.SiteDevelopmentModerate, "25.0");
            values.Add(BaselineVariableNames.SiteDevelopmentSimple, "15.0");
        



            localStorage.Update(values);

            _G20SDFormulaCalculator = new G20SDFormulaCalculator(localStorage);
        }

        [TestMethod]
        public void G20SDSDDefaultValues()
        {
            G20SDViewModel model = new G20SDViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.SiteDevelopment = SiteDevelopment.Moderate;

            var result = _G20SDFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 27.50);
        }
        [TestMethod]
        public void G20SDSD_CMARRegionalTier2()
        {
            G20SDViewModel model = new G20SDViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.SiteDevelopment = SiteDevelopment.Moderate;

            var result = _G20SDFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 26.95);
        }
        [TestMethod]
        public void G20SDSD_CMARRegionalTier2_Simple()
        {
            G20SDViewModel model = new G20SDViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.SiteDevelopment = SiteDevelopment.Simple;

            var result = _G20SDFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 16.17);
        }



    }
}
