﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z70SDFormulaCalculatorTests
    {


        private static Z70SDFormulaCalculator _Z70formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC, "0.025");
            


            localStorage.Update(values);

            _Z70formulacalculator = new Z70SDFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z70SDDefaultValues()
        {
            Z70SDViewModel model = new Z70SDViewModel();
            model.G10G60Sum = 34.87;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910SD = 4.18;
            model.Z920SD = 6.15;
            model.Z930SD = 1.36;
            model.Z10SD = 4.66;

            var result = _Z70formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 1.02);
        }
        [TestMethod]
        public void Z70SD_Dining_CMARNational()
        {
            Z70SDViewModel model = new Z70SDViewModel();
            model.G10G60Sum = 34.87;
            model.BuildingType = BuildingType.Dining;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARNational;
            model.Z910SD = 4.18;
            model.Z920SD = 6.15;
            model.Z930SD = 1.36;
            model.Z10SD = 4.66;


            var result = _Z70formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 1.28);
        }
        [TestMethod]
        public void Z70SD_LaboratoryBuilding_CMARRegionalTier2()
        {
            Z70SDViewModel model = new Z70SDViewModel();
            model.G10G60Sum = 34.87;
            model.BuildingType = BuildingType.LaboratoryBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910SD = 4.18;
            model.Z920SD = 6.15;
            model.Z930SD = 1.36;
            model.Z10SD = 4.66;

            var result = _Z70formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.77);
        }

    }
}
