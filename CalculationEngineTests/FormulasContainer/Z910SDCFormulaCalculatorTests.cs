﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z910SDFormulaCalculatorTests
    {


        private static Z910SDFormulaCalculator _Z910formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();           


            localStorage.Update(values);

            _Z910formulacalculator = new Z910SDFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z910SDDefaultValues()
        {
            Z910SDViewModel model = new Z910SDViewModel();
            model.G10G60Sum = 34.87;
            model.ContingenciesDesign = 0.12;

            var result = _Z910formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 4.18);
        }
        [TestMethod]
        public void Z910SD_A10F30Sum_ContingenciesDesign()
        {
            Z910SDViewModel model = new Z910SDViewModel();
            model.G10G60Sum = 34.87;
            model.ContingenciesDesign = 0.2;
            var result = _Z910formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 6.97);
        }       

    }
}
