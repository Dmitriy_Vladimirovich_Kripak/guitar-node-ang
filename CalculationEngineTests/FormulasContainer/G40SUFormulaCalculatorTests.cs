﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class G40SUFormulaCalculatorTests
    {

        private static G40SUFormulaCalculator _G40SUFormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.SiteInfrastructureServicesProximate, "300");
            values.Add(BaselineVariableNames.SiteInfrastructureServicesRemote, "1200");
            values.Add(BaselineVariableNames.SiteInfrastructureElectrical, "750");
            values.Add(BaselineVariableNames.SiteLighting, "3.5");




            localStorage.Update(values);

            _G40SUFormulaCalculator = new G40SUFormulaCalculator(localStorage);
        }

        [TestMethod]
        public void G40SUDefaultValues()
        {
            G40SUViewModel model = new G40SUViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.SiteInfrastructureElectrical = true;
            

            var result = _G40SUFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 8.8);
        }
        [TestMethod]
        public void G40SU_TBD_CMARRegionalTier2_Electrical()
        {
            G40SUViewModel model = new G40SUViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.SiteInfrastructureElectrical = false;
         

            var result = _G40SUFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 3.43);
        }
        [TestMethod]
        public void G40SU_Electrical()
        {
            G40SUViewModel model = new G40SUViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.SiteInfrastructureElectrical = false;


            var result = _G40SUFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 3.85);
        }



    }
}
