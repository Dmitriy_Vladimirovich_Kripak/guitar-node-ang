﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z940BCFormulaCalculatorTests
    {


        private static Z940BCFormulaCalculator _Z940formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ940BC, "0.04");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ940BC, "0.04");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ940BC, "0.04");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ940BC, "0.03");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ940BC, "0.03");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ940BC, "0.03");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ940BC, "0.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ940BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ940BC, "0.03");
            


            localStorage.Update(values);

            _Z940formulacalculator = new Z940BCFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z940BCDefaultValues()
        {
            Z940BCViewModel model = new Z940BCViewModel();
            model.A10F30SumOfBC = 395.96;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910BC = 47.52;
            model.Z920BC = 69.86;
            model.Z930BC = 15.4;
            model.Z10BC = 52.87;
            model.Z70BC = 11.63;

            var result = _Z940formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 17.8);
        }
        [TestMethod]
        public void Z940BC_ParkingGarageBelowGrade_CMARRegionalTier2()
        {
            Z940BCViewModel model = new Z940BCViewModel();
            model.A10F30SumOfBC = 395.96;
            model.BuildingType = BuildingType.ParkingGarageBelowGrade;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910BC = 47.52;
            model.Z920BC = 69.86;
            model.Z930BC = 15.4;
            model.Z10BC = 52.87;
            model.Z70BC = 11.63;

            var result = _Z940formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 11.86);
        }
        [TestMethod]
        public void Z940BC_LaboratoryBuilding_DesignBidBuild_Z910()
        {
            Z940BCViewModel model = new Z940BCViewModel();
            model.A10F30SumOfBC = 395.96;
            model.BuildingType = BuildingType.LaboratoryBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.DesignBidBuild;
            model.Z910BC = 39.5;
            model.Z920BC = 69.86;
            model.Z930BC = 15.4;
            model.Z10BC = 52.87;
            model.Z70BC = 11.63;

            var result = _Z940formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 17.56);
        }

    }
}
