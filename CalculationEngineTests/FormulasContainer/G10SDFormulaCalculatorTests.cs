﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class G10SDFormulaCalculatorTests
    {

        private static G10SDFormulaCalculator _G10SDFormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.SitePreparationGreenfield, "1.5");
            values.Add(BaselineVariableNames.SitePreparationBrownfield, "2.0");
            values.Add(BaselineVariableNames.SiteDemolitionGreenfield, "1.5");
            values.Add(BaselineVariableNames.SiteDemolitionBrownfield, "3.0");



            localStorage.Update(values);

            _G10SDFormulaCalculator = new G10SDFormulaCalculator(localStorage);
        }

        [TestMethod]
        public void G10SDDefaultValues()
        {
            G10SDViewModel model = new G10SDViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.SitePreparation = SitePreparation.Greenfield;

            var result = _G10SDFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 4.62);
        }
        [TestMethod]
        public void G10SD_CMARRegionalTier2_Brownfield()
        {
            G10SDViewModel model = new G10SDViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.SitePreparation = SitePreparation.Brownfield;

            var result = _G10SDFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 9.06);
        }



    }
}
