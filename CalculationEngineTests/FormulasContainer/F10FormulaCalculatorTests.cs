﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class F10FormulaCalculatorTests
    {

        private static F10BCFormulaCalculator _F10FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");

            localStorage.Update(values);

            _F10FormulaCalculator = new F10BCFormulaCalculator(localStorage);
        }

        [TestMethod]
        public void F10DefaultValues()
        {
            F10BCViewModel model = new F10BCViewModel();
            model.GrossSquareFeet = 100000;
            model.SpecialConstructionAllowance = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            
            var result = _F10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 1.1);
        }

        [TestMethod]
        public void F10_DesignBidBuild_TBD()
        {
            F10BCViewModel model = new F10BCViewModel();
            model.GrossSquareFeet = 100000;
            model.SpecialConstructionAllowance = 100000;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.DesignBidBuild;

            var result = _F10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 0.97);
        }
        

    }
}
