﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class A40FormulaCalculatorTests
    {
        private static A40FormulaCalculator _a40FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.AcademicClassroomBuildingSlabSF, "10");
            values.Add(BaselineVariableNames.LaboratoryBuildingSlabSF, "15");
            values.Add(BaselineVariableNames.StudentHousingSlabSF, "8");
            values.Add(BaselineVariableNames.DiningSlabSF, "12");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeSlabSF, "12");
            values.Add(BaselineVariableNames.ParkingGarageAboveGradeSlabSF, "10");

            localStorage.Update(values);

            _a40FormulaCalculator = new A40FormulaCalculator(localStorage);
        }

        [TestMethod]
        public void A40BuildingType()
        {
            A40ViewModel model = new A40ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;

            var result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2);

            model.BuildingType = BuildingType.LaboratoryBuilding;

            result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 3);

            model.BuildingType = BuildingType.StudentHousing;

            result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 1.6);

            model.BuildingType = BuildingType.Dining;

            result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2.4);

            model.BuildingType = BuildingType.ParkingGarageBelowGrade;

            result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2.4);

            model.BuildingType = BuildingType.ParkingGarageAboveGrade;

            result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2);            
        }

        [TestMethod]
        public void A40NumberOfLevels()
        {
            A40ViewModel model = new A40ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;

            var result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2);

            model.NumberOfLevels = 6;
            model.BuildingType = BuildingType.Dining;

            result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2);

            model.NumberOfLevels = 10;
            model.BuildingType = BuildingType.ParkingGarageAboveGrade;

            result = _a40FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 1);

        }

    }
}
