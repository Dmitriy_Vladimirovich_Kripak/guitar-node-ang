﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class G60SDSDFormulaCalculatorTests
    {

        private static G60SDFormulaCalculator _G60SDFormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.MiscellaneousSiteConstruction, "2.5");
           
        



            localStorage.Update(values);

            _G60SDFormulaCalculator = new G60SDFormulaCalculator(localStorage);
        }

        [TestMethod]
        public void G60SDSDDefaultValues()
        {
            G60SDViewModel model = new G60SDViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            

            var result = _G60SDFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2.75);
        }
        [TestMethod]
        public void G60SDSD_CMARRegionalTier2()
        {
            G60SDViewModel model = new G60SDViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
        

            var result = _G60SDFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2.7);
        }
        [TestMethod]
        public void G60SDSD_CMARNational_NationalAverage()
        {
            G60SDViewModel model = new G60SDViewModel();
            model.GrossSiteArea = 70000;
            model.BuildingFootprint = 20000;
            model.DevelopedSiteArea = model.GrossSiteArea - model.BuildingFootprint;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARNational;
            

            var result = _G60SDFormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2.58);
        }



    }
}
