﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z70SUFormulaCalculatorTests
    {


        private static Z70SUFormulaCalculator _Z70formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC, "0.025");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC, "0.01");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC, "0.02");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC, "0.015");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC, "0.025");
            


            localStorage.Update(values);

            _Z70formulacalculator = new Z70SUFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z70SUDefaultValues()
        {
            Z70SUViewModel model = new Z70SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910SU = 3.03;
            model.Z920SU = 4.45;
            model.Z930SU = 0.98;
            model.Z10SU = 3.37;

            var result = _Z70formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.74);
        }
        [TestMethod]
        public void Z70SU_ParkingGarageAboveGrade_Z920()
        {
            Z70SUViewModel model = new Z70SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.ParkingGarageAboveGrade;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910SU = 3.03;
            model.Z920SU = 3.87;
            model.Z930SU = 0.98;
            model.Z10SU = 3.37;


            var result = _Z70formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.73);
        }
        [TestMethod]
        public void Z70SU_LaboratoryBuilding_CMARRegionalTier2()
        {
            Z70SUViewModel model = new Z70SUViewModel();
            model.G30G50SUSum = 25.25;
            model.BuildingType = BuildingType.LaboratoryBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910SU = 3.03;
            model.Z920SU = 4.45;
            model.Z930SU = 0.98;
            model.Z10SU = 3.37;

            var result = _Z70formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.56);
        }

    }
}
