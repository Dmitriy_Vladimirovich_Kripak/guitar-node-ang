﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class B30FormulaCalculatorTests
    {
        private static B30FormulaCalculator _b30FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicB30, "1.5");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardB30, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostB30, "0.8");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB30, "1.5");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentB30, "1.15");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentB30, "1");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingSolidRoofSF, "17.5");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingGlazedRoofSF, "175");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingTerraceRoofSF, "50");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingSoffitRoofSF, "45");
            values.Add(BaselineVariableNames.LaboratoryBuildingSolidRoofSF, "20");
            values.Add(BaselineVariableNames.LaboratoryBuildingGlazedRoofSF, "175");
            values.Add(BaselineVariableNames.LaboratoryBuildingTerraceRoofSF, "50");
            values.Add(BaselineVariableNames.LaboratoryBuildingSoffitRoofSF, "50");
            values.Add(BaselineVariableNames.StudentHousingSolidRoofSF, "15");
            values.Add(BaselineVariableNames.StudentHousingGlazedRoofSF, "125");
            values.Add(BaselineVariableNames.StudentHousingTerraceRoofSF, "35");
            values.Add(BaselineVariableNames.StudentHousingSoffitRoofSF, "30");
            values.Add(BaselineVariableNames.DiningSolidRoofSF, "20");
            values.Add(BaselineVariableNames.DiningGlazedRoofSF, "150");
            values.Add(BaselineVariableNames.DiningTerraceRoofSF, "50");
            values.Add(BaselineVariableNames.DiningSoffitRoofSF, "45");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeSolidRoofSF, "35");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeGlazedRoofSF, "175");

            localStorage.Update(values);

            _b30FormulaCalculator = new B30FormulaCalculator(localStorage);
        }

        [TestMethod]
        public void B30DefaultValues()
        {
            B30ViewModel model = new B30ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.RoofTerraceRatio = 0.1;
            model.RoofGlazingRatio = 0.05;
            model.HorizontalArticulationFactor = 1.2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 9.7);
        }

        [TestMethod]
        public void B30NumberOfLevels()
        {
            B30ViewModel model = new B30ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 15;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.RoofTerraceRatio = 0.1;
            model.RoofGlazingRatio = 0.05;
            model.HorizontalArticulationFactor = 1.2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 3.23);
        }

        [TestMethod]
        public void B30RoofTerraceRatio()
        {
            B30ViewModel model = new B30ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.RoofTerraceRatio = 0.3;
            model.RoofGlazingRatio = 0.05;
            model.HorizontalArticulationFactor = 1.2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 11.49);
        }

        [TestMethod]
        public void B30RoofGlazingRatio()
        {
            B30ViewModel model = new B30ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.RoofTerraceRatio = 0.1;
            model.RoofGlazingRatio = 0.1;
            model.HorizontalArticulationFactor = 1.2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 11.87);
        }

        [TestMethod]
        public void B30HorizontalArticulationFactor()
        {
            B30ViewModel model = new B30ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.RoofTerraceRatio = 0.1;
            model.RoofGlazingRatio = 0.05;
            model.HorizontalArticulationFactor = 1.9;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 20.61);
        }

        [TestMethod]
        public void B30ArchitecturalExpectation()
        {
            B30ViewModel model = new B30ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.RoofTerraceRatio = 0.1;
            model.RoofGlazingRatio = 0.05;
            model.HorizontalArticulationFactor = 1.2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.LowCost;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 8.12);
        }

        [TestMethod]
        public void B30PerformanceExpectation()
        {
            B30ViewModel model = new B30ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.RoofTerraceRatio = 0.1;
            model.RoofGlazingRatio = 0.05;
            model.HorizontalArticulationFactor = 1.2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDPlatinumEquivalent;

            var result = _b30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 12.1);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void B30WrongHorizontalArticulationFactor()
        {
            try
            { 
                B30ViewModel model = new B30ViewModel();

                model.GrossSquareFeet = 100000;
                model.NumberOfLevels = 5;
                model.BuildingType = BuildingType.AcademicClassroomBuilding;
                model.RoofTerraceRatio = 0.1;
                model.RoofGlazingRatio = 0.05;
                model.HorizontalArticulationFactor = 2.2;
                model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
                model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

                var result = _b30FormulaCalculator.CalculateFormula(model);
                Assert.AreEqual(result, 9.7);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "The Horizontal Articulation Factor must be from 1 to 2, actual result is: 2.2");
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void B30WrongRoofTerraceRatio()
        {
            try
            {
                B30ViewModel model = new B30ViewModel();

                model.GrossSquareFeet = 100000;
                model.NumberOfLevels = 5;
                model.BuildingType = BuildingType.AcademicClassroomBuilding;
                model.RoofTerraceRatio = 1.1;
                model.RoofGlazingRatio = 0.05;
                model.HorizontalArticulationFactor = 1.2;
                model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
                model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

                var result = _b30FormulaCalculator.CalculateFormula(model);
                Assert.AreEqual(result, 9.7);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "The Roof Terrace Ratio must be from 0 to 1, actual result is: 1.1");
                throw;
            }
        }
    }
}
