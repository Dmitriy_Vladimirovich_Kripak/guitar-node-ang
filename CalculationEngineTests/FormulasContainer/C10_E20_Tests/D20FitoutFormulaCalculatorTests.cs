﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Application.Models.Calculation.C10_E20;
using CalculationEngine.FormulasContainer.C10_E20;
using CalculationEngine;
using System.Collections.Generic;
using Application.Models.Calculation;
using Application.Common;

namespace CalculationEngineTests.FormulasContainer.C10_E20_Tests
{
    [TestClass]
    public class D20FitoutFormulaCalculatorTests
    {
        private static D20FitoutFormulaCalculator _D20Fitoutformulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardD20Fitout, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostD20Fitout, "0.8");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicD20Fitout, "1.5");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentD20Fitout, "1.15");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD20Fitout, "1.3");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentD20Fitout, "1");            
            values.Add(BaselineVariableNames.PlumbingTypical, "1");
            values.Add(BaselineVariableNames.PlumbingFlexible, "1.25");

            values.Add("AcademicClassroomBuildingAssignableProgram1IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram2IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram3IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram4IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram5IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram6IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram7IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram8IsUsing", "False");

            values.Add("AcademicClassroomBuildingAssignableProgram1Caption", "Teaching");
            values.Add("AcademicClassroomBuildingAssignableProgram2Caption", "Enclosed Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram3Caption", "Open Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram4Caption", "Kitchenette");
            values.Add("AcademicClassroomBuildingAssignableProgram5Caption", "Shell Space");
            values.Add("AcademicClassroomBuildingAssignableProgram6Caption", "AssignableProgram6");
            values.Add("AcademicClassroomBuildingAssignableProgram7Caption", "AssignableProgram7");
            values.Add("AcademicClassroomBuildingAssignableProgram8Caption", "AssignableProgram8");

            values.Add("AcademicClassroomBuildingGrossUpProgram4IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram1IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram2IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram3IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram4IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram1IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram2IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram4IsUsing", "True");
            values.Add("DiningGrossUpProgram1IsUsing", "True");
            values.Add("DiningGrossUpProgram2IsUsing", "True");
            values.Add("DiningGrossUpProgram3IsUsing", "True");
            values.Add("DiningGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram4IsUsing", "True");

            values.Add("D20Fitout_AcademicClassroomBuildingProgram1Assignable$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram2Assignable$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram3Assignable$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram4Assignable$/ASF", "30");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram5Assignable$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram6Assignable$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram7Assignable$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram8Assignable$/ASF", "0");

            values.Add("D20Fitout_AcademicClassroomBuildingProgram1GrossUp$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram2GrossUp$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram3GrossUp$/ASF", "0");
            values.Add("D20Fitout_AcademicClassroomBuildingProgram4GrossUp$/ASF", "0");





            localStorage.Update(values);

            _D20Fitoutformulacalculator = new D20FitoutFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void D20FitoutDefaultValues()
        {
            D20FitoutViewModel model = new D20FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;            
            model.Plumbing = Plumbing.Typical;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D20Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.76);
        }

        [TestMethod]
        public void D20Fitout_NationalAverage_CMARRegionalTier2_LowCost()
        {
            D20FitoutViewModel model = new D20FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.LowCost;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.Plumbing = Plumbing.Typical;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D20Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.54);
        }

        [TestMethod]
        public void D20Fitout_TBD_CMARNational_Iconic()
        {
            D20FitoutViewModel model = new D20FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARNational;
            model.ArchitecturalExpectation = ArchitecturalExpectation.Iconic;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.Plumbing = Plumbing.Typical;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D20Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 1.07);
        }

        [TestMethod]
        public void D20_BuildingEfficiency76_GrossUp28()
        {
            D20FitoutViewModel model = new D20FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.Plumbing = Plumbing.Typical;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.716;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0.096;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.004;

            var result = _D20Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.76);
        }
    }
}
