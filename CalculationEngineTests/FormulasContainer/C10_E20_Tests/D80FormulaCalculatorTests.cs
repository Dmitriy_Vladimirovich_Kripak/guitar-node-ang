﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Application.Models.Calculation.C10_E20;
using CalculationEngine.FormulasContainer.C10_E20;
using CalculationEngine;
using System.Collections.Generic;
using Application.Models.Calculation;
using Application.Common;

namespace CalculationEngineTests.FormulasContainer.C10_E20_Tests
{
    [TestClass]
    public class D80FormulaCalculatorTests
    {
        private static D80FormulaCalculator _D80formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardD80, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostD80, "0.8");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicD80, "1.5");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentD80, "1.15");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD80, "1.3");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentD80, "1");
            values.Add(BaselineVariableNames.HVACAir, "1.0");
            values.Add(BaselineVariableNames.HVACHydronic, "1.1");
            values.Add(BaselineVariableNames.HVACHybrid, "1.1");
            values.Add(BaselineVariableNames.BuildingPlantLocal, "1.15");
            values.Add(BaselineVariableNames.BuildingPlantRemote, "1.0");

            values.Add("AcademicClassroomBuildingAssignableProgram1IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram2IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram3IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram4IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram5IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram6IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram7IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram8IsUsing", "False");

            values.Add("AcademicClassroomBuildingAssignableProgram1Caption", "Teaching");
            values.Add("AcademicClassroomBuildingAssignableProgram2Caption", "Enclosed Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram3Caption", "Open Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram4Caption", "Kitchenette");
            values.Add("AcademicClassroomBuildingAssignableProgram5Caption", "Shell Space");
            values.Add("AcademicClassroomBuildingAssignableProgram6Caption", "AssignableProgram6");
            values.Add("AcademicClassroomBuildingAssignableProgram7Caption", "AssignableProgram7");
            values.Add("AcademicClassroomBuildingAssignableProgram8Caption", "AssignableProgram8");

            values.Add("AcademicClassroomBuildingGrossUpProgram4IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram1IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram2IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram3IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram4IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram1IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram2IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram4IsUsing", "True");
            values.Add("DiningGrossUpProgram1IsUsing", "True");
            values.Add("DiningGrossUpProgram2IsUsing", "True");
            values.Add("DiningGrossUpProgram3IsUsing", "True");
            values.Add("DiningGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram4IsUsing", "True");

            values.Add("D80_AcademicClassroomBuildingProgram1Assignable$/ASF", "2.5");
            values.Add("D80_AcademicClassroomBuildingProgram2Assignable$/ASF", "2.5");
            values.Add("D80_AcademicClassroomBuildingProgram3Assignable$/ASF", "1.5");
            values.Add("D80_AcademicClassroomBuildingProgram4Assignable$/ASF", "3");
            values.Add("D80_AcademicClassroomBuildingProgram5Assignable$/ASF", "1.5");
            values.Add("D80_AcademicClassroomBuildingProgram6Assignable$/ASF", "0");
            values.Add("D80_AcademicClassroomBuildingProgram7Assignable$/ASF", "0");
            values.Add("D80_AcademicClassroomBuildingProgram8Assignable$/ASF", "0");

            values.Add("D80_AcademicClassroomBuildingProgram1GrossUp$/ASF", "7.5");
            values.Add("D80_AcademicClassroomBuildingProgram2GrossUp$/ASF", "1.5");
            values.Add("D80_AcademicClassroomBuildingProgram3GrossUp$/ASF", "5");
            values.Add("D80_AcademicClassroomBuildingProgram4GrossUp$/ASF", "1.5");





            localStorage.Update(values);

            _D80formulacalculator = new D80FormulaCalculator(localStorage);

        }

        [TestMethod]
        public void D80DefaultValues()
        {
            D80ViewModel model = new D80ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.HVAC = HVAC.Air;
            model.BuildingPlant = BuildingPlant.Remote;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D80formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 3.26);
        }

        [TestMethod]
        public void D80_NationalAverage_DesignBidBuild_LEEDSilverEquivalent()
        {
            D80ViewModel model = new D80ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.DesignBidBuild;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDSilverEquivalent;
            model.HVAC = HVAC.Air;
            model.BuildingPlant = BuildingPlant.Remote;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D80formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 2.5);
        }
        [TestMethod]
        public void D80_TBD_DesignBidBuild_LowCost_LEEDSilverEquivalent()
        {
            D80ViewModel model = new D80ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.DesignBidBuild;
            model.ArchitecturalExpectation = ArchitecturalExpectation.LowCost;
            model.PerformanceExpectation = PerformanceExpectation.LEEDSilverEquivalent;
            model.HVAC = HVAC.Air;
            model.BuildingPlant = BuildingPlant.Remote;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D80formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 2);
        }
        [TestMethod]
        public void D80_Efficiency71_GrossUp28()
        {
            D80ViewModel model = new D80ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.HVAC = HVAC.Air;
            model.BuildingPlant = BuildingPlant.Remote;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.716;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0.096;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.004;

            var result = _D80formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 3.26);
        }
    }
}
