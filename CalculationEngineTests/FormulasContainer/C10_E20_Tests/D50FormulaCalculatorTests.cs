﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine.FormulasContainer.C10_E20;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using Application.Models.Calculation.C10_E20;
using Application.Models.Calculation;

namespace CalculationEngineTests.FormulasContainer.C10_E20_Tests
{
    [TestClass]
    public class D50FormulaCalculatorTests
    {
        private static D50FormulaCalculator _D50formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardD50, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostD50, "0.8");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicD50, "1.5");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentD50, "1.15");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD50, "1.3");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentD50, "1");
            values.Add(BaselineVariableNames.ElectricalTypical, "1.0");
            values.Add(BaselineVariableNames.ElectricalComprehensive, "1.25");           
           

            values.Add("AcademicClassroomBuildingAssignableProgram1IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram2IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram3IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram4IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram5IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram6IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram7IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram8IsUsing", "False");

            values.Add("AcademicClassroomBuildingAssignableProgram1Caption", "Teaching");
            values.Add("AcademicClassroomBuildingAssignableProgram2Caption", "Enclosed Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram3Caption", "Open Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram4Caption", "Kitchenette");
            values.Add("AcademicClassroomBuildingAssignableProgram5Caption", "Shell Space");
            values.Add("AcademicClassroomBuildingAssignableProgram6Caption", "AssignableProgram6");
            values.Add("AcademicClassroomBuildingAssignableProgram7Caption", "AssignableProgram7");
            values.Add("AcademicClassroomBuildingAssignableProgram8Caption", "AssignableProgram8");

            values.Add("AcademicClassroomBuildingGrossUpProgram4IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram1IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram2IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram3IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram4IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram1IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram2IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram4IsUsing", "True");
            values.Add("DiningGrossUpProgram1IsUsing", "True");
            values.Add("DiningGrossUpProgram2IsUsing", "True");
            values.Add("DiningGrossUpProgram3IsUsing", "True");
            values.Add("DiningGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram4IsUsing", "True");

            values.Add("D50_AcademicClassroomBuildingProgram1Assignable$/ASF", "10");
            values.Add("D50_AcademicClassroomBuildingProgram2Assignable$/ASF", "7");
            values.Add("D50_AcademicClassroomBuildingProgram3Assignable$/ASF", "7");
            values.Add("D50_AcademicClassroomBuildingProgram4Assignable$/ASF", "10");
            values.Add("D50_AcademicClassroomBuildingProgram5Assignable$/ASF", "7");
            values.Add("D50_AcademicClassroomBuildingProgram6Assignable$/ASF", "0");
            values.Add("D50_AcademicClassroomBuildingProgram7Assignable$/ASF", "0");
            values.Add("D50_AcademicClassroomBuildingProgram8Assignable$/ASF", "0");

            values.Add("D50_AcademicClassroomBuildingProgram1GrossUp$/ASF", "50");
            values.Add("D50_AcademicClassroomBuildingProgram2GrossUp$/ASF", "5");
            values.Add("D50_AcademicClassroomBuildingProgram3GrossUp$/ASF", "15");
            values.Add("D50_AcademicClassroomBuildingProgram4GrossUp$/ASF", "2.5");





            localStorage.Update(values);

            _D50formulacalculator = new D50FormulaCalculator(localStorage);

        }

        [TestMethod]
        public void D50DefaultValues()
        {
            D50ViewModel model = new D50ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Typical;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D50formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result,12.02);
        }

        [TestMethod]
        public void D50_NationalAverage_CMARRegionalTier2()
        {
            D50ViewModel model = new D50ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Typical;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D50formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 10.71);
        }

        [TestMethod]
        public void D50_CMARRegionalTier2_LowCost_Comprehensive_LEEDPlatinumEquivalent()
        {
            D50ViewModel model = new D50ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.LowCost;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Comprehensive;
            model.PerformanceExpectation = PerformanceExpectation.LEEDPlatinumEquivalent;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D50formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 13.31);
        }

        [TestMethod]
        public void D50_BuildingEfficiency65_GrossUp34()
        {
            D50ViewModel model = new D50ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Typical;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            model.BuildingEfficiency = 0.657;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0.037;

            model.GrossUpProgram1 = 0.051;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.042;

            var result = _D50formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 13.49);
        }
    }
}
