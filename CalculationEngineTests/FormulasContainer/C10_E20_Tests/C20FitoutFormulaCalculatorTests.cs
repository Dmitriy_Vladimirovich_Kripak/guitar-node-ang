﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using Application.Models.Calculation;
using CalculationEngine.FormulasContainer.C10_E20;
using Application.Models.Calculation.C10_E20;

namespace CalculationEngineTests.FormulasContainer.C20_E20_Tests
{
    [TestClass]
    public class C20FitoutFormulaCalculatorTests
    {
        private static C20FitoutFormulaCalculator _C20Fitoutformulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardC20Fitout, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostC20Fitout, "0.8");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicC20Fitout, "1.5");



            values.Add("AcademicClassroomBuildingAssignableProgram1IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram2IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram3IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram4IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram5IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram6IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram7IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram8IsUsing", "False");

            values.Add("AcademicClassroomBuildingAssignableProgram1Caption", "Teaching");
            values.Add("AcademicClassroomBuildingAssignableProgram2Caption", "Enclosed Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram3Caption", "Open Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram4Caption", "Kitchenette");
            values.Add("AcademicClassroomBuildingAssignableProgram5Caption", "Shell Space");
            values.Add("AcademicClassroomBuildingAssignableProgram6Caption", "AssignableProgram6");
            values.Add("AcademicClassroomBuildingAssignableProgram7Caption", "AssignableProgram7");
            values.Add("AcademicClassroomBuildingAssignableProgram8Caption", "AssignableProgram8");

            values.Add("AcademicClassroomBuildingGrossUpProgram4IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram1IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram2IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram3IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram4IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram1IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram2IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram4IsUsing", "True");
            values.Add("DiningGrossUpProgram1IsUsing", "True");
            values.Add("DiningGrossUpProgram2IsUsing", "True");
            values.Add("DiningGrossUpProgram3IsUsing", "True");
            values.Add("DiningGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram4IsUsing", "True");

            values.Add("C20Fitout_AcademicClassroomBuildingProgram1Assignable$/ASF", "30");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram2Assignable$/ASF", "20");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram3Assignable$/ASF", "15");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram4Assignable$/ASF", "20");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram5Assignable$/ASF", "2.5");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram6Assignable$/ASF", "0");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram7Assignable$/ASF", "0");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram8Assignable$/ASF", "0");

            values.Add("C20Fitout_AcademicClassroomBuildingProgram1GrossUp$/ASF", "0");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram2GrossUp$/ASF", "15");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram3GrossUp$/ASF", "0");
            values.Add("C20Fitout_AcademicClassroomBuildingProgram4GrossUp$/ASF", "0");





            localStorage.Update(values);

            _C20Fitoutformulacalculator = new C20FitoutFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void C20FitoutDefaultValues()
        {
            C20FitoutViewModel model = new C20FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _C20Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 18.26);
        }

        [TestMethod]
        public void C20Fitout_NationalAverage_Iconic()
        {
            C20FitoutViewModel model = new C20FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.Iconic;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _C20Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 24.9);
        }

        [TestMethod]
        public void C20Fitout_Efficiency71_GrossUp28()
        {
            C20FitoutViewModel model = new C20FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.712;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0.092;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0;

            var result = _C20Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 18.51);
        }



    }
}
