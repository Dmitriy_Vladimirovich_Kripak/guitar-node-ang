﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine.FormulasContainer.C10_E20;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using Application.Models.Calculation.C10_E20;
using Application.Models.Calculation;

namespace CalculationEngineTests.FormulasContainer.C10_E20_Tests
{
    [TestClass]
    public class D60FitoutFitoutFormulaCalculatorTests
    {
        private static D60FitoutFormulaCalculator _D60Fitoutformulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardD60Fitout, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostD60Fitout, "0.8");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicD60Fitout, "1.5");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentD60Fitout, "1.15");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD60Fitout, "1.3");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentD60Fitout, "1");
            values.Add(BaselineVariableNames.ElectricalTypical, "1.0");
            values.Add(BaselineVariableNames.ElectricalComprehensive, "1.25");


            values.Add("AcademicClassroomBuildingAssignableProgram1IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram2IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram3IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram4IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram5IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram6IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram7IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram8IsUsing", "False");

            values.Add("AcademicClassroomBuildingAssignableProgram1Caption", "Teaching");
            values.Add("AcademicClassroomBuildingAssignableProgram2Caption", "Enclosed Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram3Caption", "Open Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram4Caption", "Kitchenette");
            values.Add("AcademicClassroomBuildingAssignableProgram5Caption", "Shell Space");
            values.Add("AcademicClassroomBuildingAssignableProgram6Caption", "AssignableProgram6");
            values.Add("AcademicClassroomBuildingAssignableProgram7Caption", "AssignableProgram7");
            values.Add("AcademicClassroomBuildingAssignableProgram8Caption", "AssignableProgram8");

            values.Add("AcademicClassroomBuildingGrossUpProgram4IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram1IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram2IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram3IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram4IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram1IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram2IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram4IsUsing", "True");
            values.Add("DiningGrossUpProgram1IsUsing", "True");
            values.Add("DiningGrossUpProgram2IsUsing", "True");
            values.Add("DiningGrossUpProgram3IsUsing", "True");
            values.Add("DiningGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram4IsUsing", "True");

            values.Add("D60Fitout_AcademicClassroomBuildingProgram1Assignable$/ASF", "10");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram2Assignable$/ASF", "12.5");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram3Assignable$/ASF", "10");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram4Assignable$/ASF", "10");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram5Assignable$/ASF", "1.5");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram6Assignable$/ASF", "0");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram7Assignable$/ASF", "0");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram8Assignable$/ASF", "0");

            values.Add("D60Fitout_AcademicClassroomBuildingProgram1GrossUp$/ASF", "0");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram2GrossUp$/ASF", "7.5");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram3GrossUp$/ASF", "0");
            values.Add("D60Fitout_AcademicClassroomBuildingProgram4GrossUp$/ASF", "0");





            localStorage.Update(values);

            _D60Fitoutformulacalculator = new D60FitoutFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void D60FitoutDefaultValues()
        {
            D60FitoutViewModel model = new D60FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Typical;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D60Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 9.55);
        }

        [TestMethod]
        public void D60Fitout_NationalAverage_Comprehensive()
        {
            D60FitoutViewModel model = new D60FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Comprehensive;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D60Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 10.85);
        }

        [TestMethod]
        public void D60Fitout_TBD_Iconic_Comprehensive()
        {
            D60FitoutViewModel model = new D60FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.Iconic;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Comprehensive;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D60Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 16.28);
        }
        [TestMethod]
        public void D60Fitout_TBD_DesignBidBuild_Iconic_Comprehensive()
        {
            D60FitoutViewModel model = new D60FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.DesignBidBuild;
            model.ArchitecturalExpectation = ArchitecturalExpectation.Iconic;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Comprehensive;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _D60Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 15.79);
        }

        [TestMethod]
        public void D60Fitout_Efficiency72_GrossUp27()
        {
            D60FitoutViewModel model = new D60FitoutViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.Electrical = Electrical.Typical;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.BuildingEfficiency = 0.725;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0.105;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.114;
            model.GrossUpProgram4 = 0.001;

            var result = _D60Fitoutformulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 9.75);
        }
    }
}
