﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Application.Models.Calculation.C10_E20;
using CalculationEngine.FormulasContainer.C10_E20;
using CalculationEngine;
using System.Collections.Generic;
using Application.Models.Calculation;
using Application.Common;

namespace CalculationEngineTests.FormulasContainer.C10_E20_Tests
{
    [TestClass]
    public class D30FormulaCalculatorTests
    {
        private static D30FormulaCalculator _d30formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardD30, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostD30, "0.8");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicD30, "1.0");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentD30, "1.15");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD30, "1.3");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentD30, "1");
            values.Add(BaselineVariableNames.HVACAir, "1.0");
            values.Add(BaselineVariableNames.HVACHydronic, "1.1");
            values.Add(BaselineVariableNames.HVACHybrid, "1.1");
            values.Add(BaselineVariableNames.BuildingPlantLocal, "1.15");
            values.Add(BaselineVariableNames.BuildingPlantRemote, "1.0");

            values.Add("AcademicClassroomBuildingAssignableProgram1IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram2IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram3IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram4IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram5IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram6IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram7IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram8IsUsing", "False");

            values.Add("AcademicClassroomBuildingAssignableProgram1Caption", "Teaching");
            values.Add("AcademicClassroomBuildingAssignableProgram2Caption", "Enclosed Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram3Caption", "Open Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram4Caption", "Kitchenette");
            values.Add("AcademicClassroomBuildingAssignableProgram5Caption", "Shell Space");
            values.Add("AcademicClassroomBuildingAssignableProgram6Caption", "AssignableProgram6");
            values.Add("AcademicClassroomBuildingAssignableProgram7Caption", "AssignableProgram7");
            values.Add("AcademicClassroomBuildingAssignableProgram8Caption", "AssignableProgram8");

            values.Add("AcademicClassroomBuildingGrossUpProgram4IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram1IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram2IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram3IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram4IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram1IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram2IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram4IsUsing", "True");
            values.Add("DiningGrossUpProgram1IsUsing", "True");
            values.Add("DiningGrossUpProgram2IsUsing", "True");
            values.Add("DiningGrossUpProgram3IsUsing", "True");
            values.Add("DiningGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram4IsUsing", "True");

            values.Add("D30_AcademicClassroomBuildingProgram1Assignable$/ASF", "35");
            values.Add("D30_AcademicClassroomBuildingProgram2Assignable$/ASF", "30");
            values.Add("D30_AcademicClassroomBuildingProgram3Assignable$/ASF", "25");
            values.Add("D30_AcademicClassroomBuildingProgram4Assignable$/ASF", "30");
            values.Add("D30_AcademicClassroomBuildingProgram5Assignable$/ASF", "5");
            values.Add("D30_AcademicClassroomBuildingProgram6Assignable$/ASF", "0");
            values.Add("D30_AcademicClassroomBuildingProgram7Assignable$/ASF", "0");
            values.Add("D30_AcademicClassroomBuildingProgram8Assignable$/ASF", "0");

            values.Add("D30_AcademicClassroomBuildingProgram1GrossUp$/ASF", "30");
            values.Add("D30_AcademicClassroomBuildingProgram2GrossUp$/ASF", "15");
            values.Add("D30_AcademicClassroomBuildingProgram3GrossUp$/ASF", "15");
            values.Add("D30_AcademicClassroomBuildingProgram4GrossUp$/ASF", "5");
    




            localStorage.Update(values);

            _d30formulacalculator = new D30FormulaCalculator(localStorage);

        }

        [TestMethod]
        public void D30DefaultValues()
        {
            D30ViewModel model = new D30ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
            model.HVAC = HVAC.Air;
            model.BuildingPlant = BuildingPlant.Remote;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;
                                       
            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _d30formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 30.99);
        }
        [TestMethod]
        public void D30_SilverEquivalent_Tier2()
        {
            D30ViewModel model = new D30ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDSilverEquivalent;
            model.HVAC = HVAC.Air;
            model.BuildingPlant = BuildingPlant.Remote;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _d30formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 26.41);
        }
        [TestMethod]
        public void D30_NationalAverage_LowCost_Hydronic()
        {
            D30ViewModel model = new D30ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.LowCost;
            model.PerformanceExpectation = PerformanceExpectation.LEEDSilverEquivalent;
            model.HVAC = HVAC.Hydronic;
            model.BuildingPlant = BuildingPlant.Remote;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _d30formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 21.13);
        }
        [TestMethod]
        public void D30_TBD_Iconic_LEEDPlatinumEquivalent_Local_GrossSquare5000_Efficiency69_9_GrossUp_30_1()
        {
            D30ViewModel model = new D30ViewModel();

            model.GrossSquareFeet = 5000;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.Iconic;
            model.PerformanceExpectation = PerformanceExpectation.LEEDPlatinumEquivalent;
            model.HVAC = HVAC.Air;
            model.BuildingPlant = BuildingPlant.Local;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;

            model.BuildingEfficiency = 0.699;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.021;

            var result = _d30formulacalculator.CalculateFormula(model);

             Assert.AreEqual(result, 36.04); //??? site
        }
    }
}
