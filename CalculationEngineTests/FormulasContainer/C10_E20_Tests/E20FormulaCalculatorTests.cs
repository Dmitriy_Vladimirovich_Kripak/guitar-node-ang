﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using Application.Models.Calculation;
using CalculationEngine.FormulasContainer.C10_E20;
using Application.Models.Calculation.C10_E20;

namespace CalculationEngineTests.FormulasContainer.C10_E20_Tests
{
    [TestClass]
    public class E20FormulaCalculatorTests
    {
        private static E20FormulaCalculator _E20formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardE20, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostE20, "0.8");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicE20, "1.5");


            values.Add("AcademicClassroomBuildingAssignableProgram1IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram2IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram3IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram4IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram5IsUsing", "True");
            values.Add("AcademicClassroomBuildingAssignableProgram6IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram7IsUsing", "False");
            values.Add("AcademicClassroomBuildingAssignableProgram8IsUsing", "False");

            values.Add("AcademicClassroomBuildingAssignableProgram1Caption", "Teaching");
            values.Add("AcademicClassroomBuildingAssignableProgram2Caption", "Enclosed Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram3Caption", "Open Office / Conference");
            values.Add("AcademicClassroomBuildingAssignableProgram4Caption", "Kitchenette");
            values.Add("AcademicClassroomBuildingAssignableProgram5Caption", "Shell Space");
            values.Add("AcademicClassroomBuildingAssignableProgram6Caption", "AssignableProgram6");
            values.Add("AcademicClassroomBuildingAssignableProgram7Caption", "AssignableProgram7");
            values.Add("AcademicClassroomBuildingAssignableProgram8Caption", "AssignableProgram8");

            values.Add("AcademicClassroomBuildingGrossUpProgram4IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram1IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram2IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram3IsUsing", "True");
            values.Add("LaboratoryBuildingGrossUpProgram4IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram1IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram2IsUsing", "True");
            values.Add("StudentHousingGrossUpProgram4IsUsing", "True");
            values.Add("DiningGrossUpProgram1IsUsing", "True");
            values.Add("DiningGrossUpProgram2IsUsing", "True");
            values.Add("DiningGrossUpProgram3IsUsing", "True");
            values.Add("DiningGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageBelowGradeGrossUpProgram4IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram1IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram2IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram3IsUsing", "True");
            values.Add("ParkingGarageAboveGradeGrossUpProgram4IsUsing", "True");

            values.Add("E20_AcademicClassroomBuildingProgram1Assignable$/ASF", "0.5");
            values.Add("E20_AcademicClassroomBuildingProgram2Assignable$/ASF", "0.5");
            values.Add("E20_AcademicClassroomBuildingProgram3Assignable$/ASF", "0.5");
            values.Add("E20_AcademicClassroomBuildingProgram4Assignable$/ASF", "0.5");
            values.Add("E20_AcademicClassroomBuildingProgram5Assignable$/ASF", "0.5");
            values.Add("E20_AcademicClassroomBuildingProgram6Assignable$/ASF", "0");
            values.Add("E20_AcademicClassroomBuildingProgram7Assignable$/ASF", "0");
            values.Add("E20_AcademicClassroomBuildingProgram8Assignable$/ASF", "0");

            values.Add("E20_AcademicClassroomBuildingProgram1GrossUp$/ASF", "0.5");
            values.Add("E20_AcademicClassroomBuildingProgram2GrossUp$/ASF", "0.5");
            values.Add("E20_AcademicClassroomBuildingProgram3GrossUp$/ASF", "0.5");
            values.Add("E20_AcademicClassroomBuildingProgram4GrossUp$/ASF", "0.5");





            localStorage.Update(values);

            _E20formulacalculator = new E20FormulaCalculator(localStorage);

        }

        [TestMethod]
        public void E20DefaultValues()
        {
            E20ViewModel model = new E20ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _E20formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.55);
        }
        [TestMethod]
        public void E20_TBD_CMARRegionalTier2()
        {
            E20ViewModel model = new E20ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.TBD;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _E20formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.49);
        }

        [TestMethod]
        public void E20_CMARRegionalTier2_Iconic()
        {
            E20ViewModel model = new E20ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.ArchitecturalExpectation = ArchitecturalExpectation.Iconic;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.62;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.10;

            var result = _E20formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.81);
        }
        [TestMethod]
        public void E20_Efficiency70_GrossUp29()
        {
            E20ViewModel model = new E20ViewModel();

            model.GrossSquareFeet = 100000;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.BuildingEfficiency = 0.708;
            model.AssignableProgram1 = 0.3;
            model.AssignableProgram2 = 0.15;
            model.AssignableProgram3 = 0.15;
            model.AssignableProgram4 = 0.02;
            model.AssignableProgram5 = 0.088;

            model.GrossUpProgram1 = 0.03;
            model.GrossUpProgram2 = 0.13;
            model.GrossUpProgram3 = 0.12;
            model.GrossUpProgram4 = 0.012;

            var result = _E20formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 0.55);
        }

    }
}
