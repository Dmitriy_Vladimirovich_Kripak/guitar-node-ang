﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class A20FormulaCalculatorTests
    {
        private static A20FormulaCalculator _a20FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.BasementWallsSF, "50");
            values.Add(BaselineVariableNames.RetentionSF, "65");
            values.Add(BaselineVariableNames.EarthworkExcavationBackfillCY, "40");
            localStorage.Update(values);

            _a20FormulaCalculator = new A20FormulaCalculator(localStorage);
        }

        [TestMethod]
        public void A20SiteConstraint()
        {
            A20ViewModel model = new A20ViewModel();

            model.CladdingRatio = 0.6;
            model.FFHeights = 20;
            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.NumberOfBelowGradeLevels = 1;
            model.SiteConstraint = SiteConstraint.Constrained;

            var result = _a20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 14.69);

            model.SiteConstraint = SiteConstraint.Open;
            result = _a20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 7.78);
        }

        [TestMethod]
        public void A20CladdingRatio()
        {
            A20ViewModel model = new A20ViewModel();

            model.CladdingRatio = 1;
            model.FFHeights = 20;
            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.NumberOfBelowGradeLevels = 1;
            model.SiteConstraint = SiteConstraint.Constrained;

            var result = _a20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 24.48);

        }

        [TestMethod]
        public void A20FFHeights()
        {
            A20ViewModel model = new A20ViewModel();

            model.CladdingRatio = 1;
            model.FFHeights = 27;
            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.NumberOfBelowGradeLevels = 1;
            model.SiteConstraint = SiteConstraint.Open;

            var result = _a20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 14);

        }

        [TestMethod]
        public void A20NumberOfBelowGradeLevels()
        {
            A20ViewModel model = new A20ViewModel();

            model.CladdingRatio = 1;
            model.FFHeights = 20;
            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.NumberOfBelowGradeLevels = 2;
            model.SiteConstraint = SiteConstraint.Constrained;

            var result = _a20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 48.96);

        }


        [TestMethod]
        public void A20NumberOfLevels()
        {
            A20ViewModel model = new A20ViewModel();

            model.CladdingRatio = 1;
            model.FFHeights = 20;
            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 10;
            model.NumberOfBelowGradeLevels = 1;
            model.SiteConstraint = SiteConstraint.Constrained;

            var result = _a20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 12.98);

        }
    }
}
