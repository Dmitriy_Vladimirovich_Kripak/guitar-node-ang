﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class A10FormulaCalculatorTests
    {
       
        private static A10FormulaCalculator _a10FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.NumberOfLevelsFactor, "0.95");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingDeepGSF, "20");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingShallowGSF, "10");
            values.Add(BaselineVariableNames.LaboratoryBuildingDeepGSF, "20");
            values.Add(BaselineVariableNames.LaboratoryBuildingShallowGSF, "10");
            values.Add(BaselineVariableNames.StudentHousingDeepGSF, "20");
            values.Add(BaselineVariableNames.StudentHousingShallowGSF, "10");
            values.Add(BaselineVariableNames.DiningDeepGSF, "20");
            values.Add(BaselineVariableNames.DiningShallowGSF, "10");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeDeepGSF, "20");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeShallowGSF, "10");
            values.Add(BaselineVariableNames.ParkingGarageAboveGradeDeepGSF, "20");
            values.Add(BaselineVariableNames.ParkingGarageAboveGradeShallowGSF, "10");
            localStorage.Update(values);

            _a10FormulaCalculator = new A10FormulaCalculator(localStorage);
        }

        [TestMethod]
        public void A10SoilConditionBuildingType()
        {
            A10ViewModel model = new A10ViewModel();

            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfLevels = 5;
            model.SoilCondition = SoilCondition.RippableRock;

            var result = _a10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 15.0);

            model.BuildingType = BuildingType.StudentHousing;
            model.SoilCondition = SoilCondition.SolidRock;

            result = _a10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 10.0);


            model.BuildingType = BuildingType.LaboratoryBuilding;
            model.SoilCondition = SoilCondition.Soil;

            result = _a10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 20.0);
        }

        [TestMethod]
        public void A10NumberOfLevels()
        {
            A10ViewModel model = new A10ViewModel();

            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfLevels = 5;
            model.SoilCondition = SoilCondition.SolidRock;

            var result = _a10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 10.0);

            model.NumberOfLevels = 15;//5 + 10 = added 10%
            result = _a10FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 11.0);
        }
    }
}
