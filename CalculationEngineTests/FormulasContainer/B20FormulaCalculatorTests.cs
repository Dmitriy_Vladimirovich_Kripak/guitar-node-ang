﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class B20FormulaCalculatorTests
    {
        private static B20FormulaCalculator _b20FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicB20, "1.5");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardB20, "1.0");
            values.Add(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostB20, "0.8");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB20, "1.5");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentB20, "1.15");
            values.Add(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentB20, "1");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingSolidWallSF, "80");
            values.Add(BaselineVariableNames.AcademicClassroomBuildingGlazedWallSF, "100");
            values.Add(BaselineVariableNames.LaboratoryBuildingSolidWallSF, "80");
            values.Add(BaselineVariableNames.LaboratoryBuildingGlazedWallSF, "100");
            values.Add(BaselineVariableNames.StudentHousingSolidWallSF, "60");
            values.Add(BaselineVariableNames.StudentHousingGlazedWallSF, "80");
            values.Add(BaselineVariableNames.DiningSolidWallSF, "70");
            values.Add(BaselineVariableNames.DiningGlazedWallSF, "90");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeSolidWallSF, "80");
            values.Add(BaselineVariableNames.ParkingGarageBelowGradeGlazedWallSF, "100");
            values.Add(BaselineVariableNames.ParkingGarageAboveGradeSolidWallSF, "80");
            values.Add(BaselineVariableNames.ParkingGarageAboveGradeGlazedWallSF, "100");
            localStorage.Update(values);

            _b20FormulaCalculator = new B20FormulaCalculator(localStorage);
        }

        [TestMethod]
        public void B20DefaultValues()
        {
            B20ViewModel model = new B20ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfAboveGradeLevels = 4;
            model.CladdingRatio = 0.6;
            model.GlazingRatio = 0.3;
            model.VerticalArticulationFactor = 1.15;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 56.87);
        }

        [TestMethod]
        public void B20GlazingRatio()
        {
            B20ViewModel model = new B20ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfAboveGradeLevels = 4;
            model.CladdingRatio = 0.6;
            model.GlazingRatio = 0.4;
            model.VerticalArticulationFactor = 1.15;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 58.19);
        }

        [TestMethod]
        public void B20CladdingRatio()
        {
            B20ViewModel model = new B20ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfAboveGradeLevels = 4;
            model.CladdingRatio = 0.8;
            model.GlazingRatio = 0.3;
            model.VerticalArticulationFactor = 1.15;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 75.82);
        }


        [TestMethod]
        public void B20BuildingType()
        {
            B20ViewModel model = new B20ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.Dining;
            model.NumberOfAboveGradeLevels = 4;
            model.CladdingRatio = 0.6;
            model.GlazingRatio = 0.3;
            model.VerticalArticulationFactor = 1.15;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 50.25);
        }

        [TestMethod]
        public void B20NumberOfLevels()
        {
            B20ViewModel model = new B20ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 25;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfAboveGradeLevels = 25;
            model.CladdingRatio = 0.6;
            model.GlazingRatio = 0.3;
            model.VerticalArticulationFactor = 1.15;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 68.24);
        }

        [TestMethod]
        public void B20VerticalArticulationFactor()
        {
            B20ViewModel model = new B20ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfAboveGradeLevels = 4;
            model.CladdingRatio = 0.6;
            model.GlazingRatio = 0.3;
            model.VerticalArticulationFactor = 1.5;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 74.18);
        }

        [TestMethod]
        public void B20ArchitecturalExpectation()
        {
            B20ViewModel model = new B20ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfAboveGradeLevels = 4;
            model.CladdingRatio = 0.6;
            model.GlazingRatio = 0.3;
            model.VerticalArticulationFactor = 1.15;
            model.ArchitecturalExpectation = ArchitecturalExpectation.Iconic;
            model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

            var result = _b20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 85.3);
        }

        [TestMethod]
        public void B20PerformanceExpectation()
        {
            B20ViewModel model = new B20ViewModel();

            model.GrossSquareFeet = 100000;
            model.NumberOfLevels = 5;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.NumberOfAboveGradeLevels = 4;
            model.CladdingRatio = 0.6;
            model.GlazingRatio = 0.3;
            model.VerticalArticulationFactor = 1.15;
            model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
            model.PerformanceExpectation = PerformanceExpectation.LEEDSilverEquivalent;

            var result = _b20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 49.45);
        }


        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void B20WrongVerticalArticulationFactor()
        {
            try
            {
                B20ViewModel model = new B20ViewModel();

                model.GrossSquareFeet = 100000;
                model.NumberOfLevels = 5;
                model.BuildingType = BuildingType.AcademicClassroomBuilding;
                model.NumberOfAboveGradeLevels = 4;
                model.CladdingRatio = 0.6;
                model.GlazingRatio = 0.3;
                model.VerticalArticulationFactor = 0.15;
                model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
                model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

                var result = _b20FormulaCalculator.CalculateFormula(model);
                Assert.AreEqual(result, 9.7);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "The Vertical Articulation Factor must be from 0 to 1, actual result is: 0.15");
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void B20WrongCladdingRatio()
        {
            try
            {
                B20ViewModel model = new B20ViewModel();

                model.GrossSquareFeet = 100000;
                model.NumberOfLevels = 5;
                model.BuildingType = BuildingType.AcademicClassroomBuilding;
                model.NumberOfAboveGradeLevels = 4;
                model.CladdingRatio = 3.6;
                model.GlazingRatio = 0.3;
                model.VerticalArticulationFactor = 1.15;
                model.ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
                model.PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;

                var result = _b20FormulaCalculator.CalculateFormula(model);
                Assert.AreEqual(result, 9.7);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "The Cladding Ratio must be from 0 to 2, actual result is: 3.6");
                throw;
            }
        }
    }
}
