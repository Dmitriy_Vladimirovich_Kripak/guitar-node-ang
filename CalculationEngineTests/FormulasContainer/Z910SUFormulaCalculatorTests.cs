﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z910SUFormulaCalculatorTests
    {


        private static Z910SUFormulaCalculator _Z910formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();           


            localStorage.Update(values);

            _Z910formulacalculator = new Z910SUFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z910SUDefaultValues()
        {
            Z910SUViewModel model = new Z910SUViewModel();
            model.G30G50SUSum = 25.25;
            model.ContingenciesDesign = 0.12;

            var result = _Z910formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 3.03);
        }
        [TestMethod]
        public void Z910SU_ContingenciesDesign()
        {
            Z910SUViewModel model = new Z910SUViewModel();
            model.G30G50SUSum = 25.25;
            model.ContingenciesDesign = 0.34;
            var result = _Z910formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 8.59);
        }       

    }
}
