﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z10BCFormulaCalculatorTests
    {


        private static Z10BCFormulaCalculator _z10formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC, "0.1");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC, "0.085");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC, "0.115");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC, "0.1");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC, "0.085");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC, "0.115");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC, "0.085");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC, "0.07");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC, "0.09");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC, "0.075");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC, "0.095");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC, "0.085");

            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC, "0.06");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC, "0.08");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC, "0.065");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC, "0.085");


            localStorage.Update(values);

            _z10formulacalculator = new Z10BCFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z10BCDefaultValues()
        {
            Z10BCViewModel model = new Z10BCViewModel();
            model.A10F30SumOfBC = 395.96;
            model.BuildingType = BuildingType.AcademicClassroomBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            model.Z910BC = 47.52;
            model.Z920BC = 69.86;
            model.Z930BC = 15.40;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 52.87);
        }
        [TestMethod]
        public void Z10BC_Dining_CMARRegionalTier2()
        {
            Z10BCViewModel model = new Z10BCViewModel();
            model.A10F30SumOfBC = 395.96;
            model.BuildingType = BuildingType.Dining;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910BC = 47.52;
            model.Z920BC = 69.86;
            model.Z930BC = 15.40;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 39.66);
        }
        [TestMethod]
        public void Z10BC_LaboratoryBuilding_CMARRegionalTier2_Z910()
        {
            Z10BCViewModel model = new Z10BCViewModel();
            model.A10F30SumOfBC = 395.96;
            model.BuildingType = BuildingType.LaboratoryBuilding;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;
            model.Z910BC = 47.52;
            model.Z920BC = 57.86;
            model.Z930BC = 15.40;



            var result = _z10formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 43.92);
        }

    }
}
