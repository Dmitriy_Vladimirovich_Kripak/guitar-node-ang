﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class Z910BCFormulaCalculatorTests
    {


        private static Z910BCFormulaCalculator _Z910formulacalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();           


            localStorage.Update(values);

            _Z910formulacalculator = new Z910BCFormulaCalculator(localStorage);

        }

        [TestMethod]
        public void Z910BCDefaultValues()
        {
            Z910BCViewModel model = new Z910BCViewModel();
            model.A10F30SumOfBC = 395.96;
            model.ContingenciesDesign = 0.12;

            var result = _Z910formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 47.52);
        }
        [TestMethod]
        public void Z910BC_A10F30Sum_ContingenciesDesign()
        {
            Z910BCViewModel model = new Z910BCViewModel();
            model.A10F30SumOfBC = 381.3;
            model.ContingenciesDesign = 0.14;
            var result = _Z910formulacalculator.CalculateFormula(model);

            Assert.AreEqual(result, 53.38);
        }       

    }
}
