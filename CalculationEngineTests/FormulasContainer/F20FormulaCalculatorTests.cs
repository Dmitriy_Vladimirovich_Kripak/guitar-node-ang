﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class F20FormulaCalculatorTests
    {

        private static F20BCFormulaCalculator _F20FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.FacilityRemediationRate, "2.5");
            values.Add(BaselineVariableNames.FacilityRemediationSimple, "1.0");
            values.Add(BaselineVariableNames.FacilityRemediationComplex, "1.5");           

            localStorage.Update(values);

            _F20FormulaCalculator = new F20BCFormulaCalculator(localStorage);
        }

        [TestMethod]
        public void F20DefaultValues()
        {
            F20BCViewModel model = new F20BCViewModel();
            model.GrossSquareFeet = 100000;
            model.AreaOfFacilityToBeRemediated = 60000;
            model.FacilityRemediation = FacilityRemediation.Simple;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            
            var result = _F20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 1.65);
        }

        [TestMethod]
        public void F20_NationalAveraged_Complex()
        {
            F20BCViewModel model = new F20BCViewModel();
            model.GrossSquareFeet = 100000;
            model.AreaOfFacilityToBeRemediated = 60000;
            model.FacilityRemediation = FacilityRemediation.Complex;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;

            var result = _F20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 2.25);
        }

        [TestMethod]
        public void F20_NationalAveraged_CMARRegionalTier2_AreaOfFacilityToBeRemediated()
        {
            F20BCViewModel model = new F20BCViewModel();
            model.GrossSquareFeet = 100000;
            model.AreaOfFacilityToBeRemediated = 55000;
            model.FacilityRemediation = FacilityRemediation.Simple;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;

            var result = _F20FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 1.35);
        }


    }
}
