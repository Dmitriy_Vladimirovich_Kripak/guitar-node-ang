﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;
using Application.Common;
using CalculationEngine.FormulasContainer;
using Application.Models.Calculation;

namespace CalculationEngineTests
{
    [TestClass]
    public class F30FormulaCalculatorTests
    {

        private static F30BCFormulaCalculator _F30FormulaCalculator;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();
            var values = new Dictionary<string, string>();
            values.Add(BaselineVariableNames.LocationFactorNationalAverage, "1.0");
            values.Add(BaselineVariableNames.LocationFactorLosAngeles, "1.1");
            values.Add(BaselineVariableNames.LocationFactorTBD, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1, "1.0");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2, "0.98");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational, "1.03");
            values.Add(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild, "0.97");
            values.Add(BaselineVariableNames.FacilityDemolitionRate, "12");
            values.Add(BaselineVariableNames.FacilityDemolitionSimple, "1.0");
            values.Add(BaselineVariableNames.FacilityDemolitionComplex, "1.5");           

            localStorage.Update(values);

            _F30FormulaCalculator = new F30BCFormulaCalculator(localStorage);
        }

        [TestMethod]
        public void F30DefaultValues()
        {
            F30BCViewModel model = new F30BCViewModel();
            model.GrossSquareFeet = 100000;
            model.AreaOfFacilityToBeDemolished = 60000;
            model.FacilityDemolition = FacilityDemolition.Simple;
            model.LocationFactor = LocationFactor.LosAngeles;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
            
            var result = _F30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 7.92);
        }

        [TestMethod]
        public void F30_NationalAveraged_Complex()
        {
            F30BCViewModel model = new F30BCViewModel();
            model.GrossSquareFeet = 100000;
            model.AreaOfFacilityToBeDemolished = 60000;
            model.FacilityDemolition = FacilityDemolition.Complex;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;

            var result = _F30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 10.8);
        }

        [TestMethod]
        public void F30_AreaOfFacilityToBeDemolished_Complex_NationalAveraged_CMARRegionalTier2()
        {
            F30BCViewModel model = new F30BCViewModel();
            model.GrossSquareFeet = 100000;
            model.AreaOfFacilityToBeDemolished = 55000;
            model.FacilityDemolition = FacilityDemolition.Complex;
            model.LocationFactor = LocationFactor.NationalAverage;
            model.ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier2;

            var result = _F30FormulaCalculator.CalculateFormula(model);
            Assert.AreEqual(result, 9.7);
        }


    }
}
