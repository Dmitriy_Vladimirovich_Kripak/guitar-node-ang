﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;

namespace CalculationEngineTests
{
    [TestClass]
    public class VariablesLocalStorageTests
    {
        private static IReadLocalStorage _localStorage;
        private static IUpdateLocalStorage _localStorageUpdate;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();

            _localStorage = localStorage;
            _localStorageUpdate = localStorage;

        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void StorageHasNoSuchVariable()
        {
            try
            {
                var values = new Dictionary<string, string>();
                values.Add("SolidWall$/SF", "0.2");
                _localStorageUpdate.Update(values);

                _localStorage.GetValueAsInt("UnexistedVariableName");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Baseline variables doesn't contain variable: 'UnexistedVariableName'");
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetWithWrongVariableName()
        {
            try
            {
                var values = new Dictionary<string, string>();
                values.Add("SolidWall$/SF", "0.2");
                _localStorageUpdate.Update(values);

                _localStorage.GetValueAsInt("  ");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Wrong name of variable: '  '");
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetDoubleAsWrongStr()
        {
            try
            {
                var values = new Dictionary<string, string>();
                values.Add("CladdingRatio", "WrongStr");
                _localStorageUpdate.Update(values);
                _localStorage.GetValueAsDouble("CladdingRatio");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Variable: 'CladdingRatio' has value: 'WrongStr' which is not a float");
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetIntAsDouble()
        {
            try
            {
                var values = new Dictionary<string, string>();
                values.Add("SolidWall$/SF", "0.2");
                _localStorageUpdate.Update(values);
                _localStorage.GetValueAsInt("SolidWall$/SF");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Variable: 'SolidWall$/SF' has value: '0.2' which is not an integer");
                throw;
            }
        }

        [TestMethod]
        public void GetCorrectIntsAndDoubles()
        {
            var values = new Dictionary<string, string>();
            values.Add("SolidWall$/SF", "17.2");
            values.Add("ParkingGarage(AboveGrade)", "22");
            values.Add("GlazedWall$/SF", "19");
            _localStorageUpdate.Update(values);
            Assert.AreEqual(_localStorage.GetValueAsInt("ParkingGarage(AboveGrade)"), 22);
            Assert.AreEqual(_localStorage.GetValueAsDouble("ParkingGarage(AboveGrade)"), 22);
            Assert.AreEqual(_localStorage.GetValueAsDouble("SolidWall$/SF"), 17.2);
        }

        [TestMethod]
        public void UpdateValues()
        {
            var values = new Dictionary<string, string>();
            values.Add("ParkingGarage(AboveGrade)", "22");
            values.Add("GlazedWall$/SF", "19");
            _localStorageUpdate.Update(values);

            _localStorageUpdate.Update(new Tuple<string, string>("ParkingGarage(AboveGrade)", "21"));
            Assert.AreEqual(_localStorage.GetValueAsInt("ParkingGarage(AboveGrade)"), 21);

            var newValues = new Dictionary<string, string>();
            newValues.Add("SolidWall$/SF", "22.1");
            newValues.Add("GlazedWall$/SF", "10");
            _localStorageUpdate.Update(newValues);
            Assert.AreEqual(_localStorage.GetValueAsInt("ParkingGarage(AboveGrade)"), 21);
            Assert.AreEqual(_localStorage.GetValueAsDouble("SolidWall$/SF"), 22.1);
            Assert.AreEqual(_localStorage.GetValueAsInt("GlazedWall$/SF"), 10);
        }
    }
}
