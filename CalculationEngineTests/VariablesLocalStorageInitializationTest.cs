﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationEngine;
using System.Collections.Generic;

namespace CalculationEngineTests
{
    [TestClass]
    public class VariablesLocalStorageInitializationTest
    {
        private static IReadLocalStorage _localStorage;

        [ClassInitialize]
        public static void InitializBeforeAllTests(TestContext testContext)
        {
            var localStorage = new VariablesLocalStorage();

            _localStorage = localStorage;

        }
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void StorageNotInitialized()
        {
            try
            {
                _localStorage.GetValueAsInt("NumberOfBelowGradeLevels");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Baseline variables are not initialized!");
                throw;
            }
        }
    }
}
