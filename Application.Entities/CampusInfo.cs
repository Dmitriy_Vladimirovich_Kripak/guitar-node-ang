﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class CampusInfo
    {
        public int Id { get; set; }

        public string InstitutionName { get; set; }

        public string InstitutionType { get; set; } 

        public string Location { get; set; }

        public string ArchitechName { get; set; }

        public string CampusProject { get; set; }        
    }
}
