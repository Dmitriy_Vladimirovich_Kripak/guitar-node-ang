﻿using Application.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class ComplexAudit
    {
        public AuditTrail AuditTrail { get; set; }
        public List<AuditTrailEntity> AuditTrailEntities { get; set; }
        public List<AuditTrailValues> AuditTrailValues { get; set; }
        public List<AuditTrail> AuditTrails { get; set; }
    }
}
