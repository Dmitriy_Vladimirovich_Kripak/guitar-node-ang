﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class UserBlock
    {      
        public int  Id { get; set; }    

        public string UserId { get; set; }
        
        //public virtual ApplicationUser User { get; set; }
    }
}
