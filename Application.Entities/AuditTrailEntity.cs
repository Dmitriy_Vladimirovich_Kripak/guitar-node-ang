﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class AuditTrailEntity
    {
        public int Id { get; set; }
        public Nullable<int> AuditTrailEntityParentId { get; set; }      
        public virtual AuditTrailEntity Parent { get; set; }
        public int AuditTrailId { get; set; }
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string EntityType { get; set; }
        public string Action { get; set; }
        public AuditTrail AuditTrails { get; set; }
        public virtual ICollection<AuditTrailValues> AuditTrailValues { get; set; }
    }
}
