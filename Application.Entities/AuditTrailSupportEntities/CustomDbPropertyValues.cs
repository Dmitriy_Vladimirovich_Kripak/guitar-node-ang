﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities.AuditTrailSupportEntities
{
    public class CustomDbPropertyValues
    {
        Dictionary<string, string> _propertyValues = new Dictionary<string, string>();
        public CustomDbPropertyValues(DbPropertyValues values)
        {
            PropertyNames = values.PropertyNames;
            values.PropertyNames.All(p =>
                {
                    _propertyValues.Add(p, values[p] == null ? string.Empty : values[p].ToString());
                    return true;
                }
            );
        }

        public IEnumerable<string> PropertyNames { get; set; }
        public string this[string propertyName]
        {
            get
            {
                string retValue;
                return _propertyValues.TryGetValue(propertyName, out retValue) ? retValue : string.Empty;
            }
        }
    }
}
