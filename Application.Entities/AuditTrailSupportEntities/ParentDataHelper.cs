﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities.AuditTrailSupportEntities
{
    public class ParentDataHelper
    {
        public static void FillParentData(CustomDbEntityEntry customDbEntityEntry, DbEntityEntry dbEntityEntry)
        {
            //the only ProjectValues has parent now
            if (customDbEntityEntry.EntityType == typeof(ProjectValues))
            {
                FillProjectParentData(customDbEntityEntry, dbEntityEntry);
            }
            if (customDbEntityEntry.EntityType == typeof(UserBlock))
            {
                FillUserBlockParentData(customDbEntityEntry, dbEntityEntry);
            }
        }

        private static void FillProjectParentData(CustomDbEntityEntry customDbEntityEntry, DbEntityEntry dbEntityEntry)
        {
            var value = (ProjectValues)dbEntityEntry.Entity;
            customDbEntityEntry.ParentName = value.Project.Name;
            customDbEntityEntry.ParentId = value.Project.Id;
        }
        private static void FillUserBlockParentData(CustomDbEntityEntry customDbEntityEntry, DbEntityEntry dbEntityEntry)
        {
            var value = (UserBlock)dbEntityEntry.Entity;
            //if (value != null && value.User != null)
            //    customDbEntityEntry.ParentName = value.User.Email;
        }
    }
}
