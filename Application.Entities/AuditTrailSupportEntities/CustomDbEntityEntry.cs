﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities.AuditTrailSupportEntities
{
    public class CustomDbEntityEntry
    {
        public CustomDbEntityEntry(DbEntityEntry dbEntityEntry)
        {
            EntityType = ObjectContext.GetObjectType(dbEntityEntry.Entity.GetType());
            OriginalValues = new CustomDbPropertyValues(dbEntityEntry.OriginalValues);
            if(dbEntityEntry.State != System.Data.Entity.EntityState.Deleted)
                CurrentValues = new CustomDbPropertyValues(dbEntityEntry.CurrentValues);

            ParentDataHelper.FillParentData(this, dbEntityEntry);
        }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public Type EntityType { get; set; }
        public CustomDbPropertyValues OriginalValues { get; set; }
        public CustomDbPropertyValues CurrentValues { get; set; }
    }
}
