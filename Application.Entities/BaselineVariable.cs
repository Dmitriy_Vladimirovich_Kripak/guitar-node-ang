﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class BaselineVariable
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public virtual ICollection<CalculationScenario> CalculationScenarios { get; set; }
    }
}
