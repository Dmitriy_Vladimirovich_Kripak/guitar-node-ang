﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string UserId { get; set; }       
        public string Institution { get; set; }
        public string CampusPrecint { get; set; }
        public ApplicationUser User { get; set; }       
        public virtual ICollection<ProjectValues> ProjectValues { get; set; }
    }
}
