﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class FileInfo
    {
        public int Id { get; set; }

        public byte[] File { get; set; }

        public string UserId { get; set; }

        public int ProjectId { get; set; }

        public string Label { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual ApplicationUser User { get; set; }

    }
}
