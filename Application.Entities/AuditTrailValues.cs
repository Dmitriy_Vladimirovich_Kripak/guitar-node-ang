﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class AuditTrailValues
    {
        public int Id { get; set; }
        public int AuditTrailEntityId { get; set; }        
        public string PropertyName { get; set; }
        public string EntityType { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public AuditTrailEntity AuditTrailEntity { get; set; }
    }
}
