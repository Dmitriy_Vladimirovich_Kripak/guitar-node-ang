﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Entities
{
    public class ProjectValues
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }     
        public  Project Project { get; set; }

    }
}
