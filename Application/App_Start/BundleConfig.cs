﻿using System.Web;
using System.Web.Optimization;

namespace Application
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jsfiles").Include(
                      "~/Scripts/bootstrap.min.js",                      
                      "~/Scripts/script.js",
                      "~/Scripts/jquery.flexslider.js",
                      "~/Scripts/pie-chart.js",   
                      "~/Scripts/rangeslider.js",
                      "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/jquery.flexslider.js",
                      "~/Scripts/easyResponsiveTabs.js",                      
                       "~/Scripts/gauge.min.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/site.css",                      
                      "~/Content/style.css",
                      "~/Content/flexslider.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/rangeslider.css",
                      "~/Content/animate.css",
                      "~/Content/easy-responsive-tabs.css"));
        }
    }
}
