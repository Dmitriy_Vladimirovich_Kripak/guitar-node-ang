﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Api;
using Microsoft.Owin;
using Owin;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Application.DAL;
using CalculationEngine;
using Application.Api.Controllers;
using Application.BLL.Common;
using System.Web;
using Application.Common.Identity;
using System.Data.Entity;
using Application.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using Application.BLL.BusinessServices;

[assembly: OwinStartupAttribute(typeof(Application.Startup))]
namespace Application
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            var config = ConfigureContainer(app);

            ConfigureCalculationBS(config.DependencyResolver);

            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            WebApiConfig.Register(config);

        }

        private static void ConfigureCalculationBS(System.Web.Http.Dependencies.IDependencyResolver resolver)
        {
            var calculationBS = DependencyResolver.Current.GetService<ICalculationBS>();
            calculationBS.ConfigureCalculationBS();
        }
        

        private static HttpConfiguration ConfigureContainer(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var builder = new ContainerBuilder();           

            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(typeof(CalculationController).Assembly);
          
            builder.RegisterType<ApplicationDbContext>().As<IApplicationDbContext>();
            builder.RegisterType<DbContextFactory>().As<IDbContextFactory>();
            
            builder.RegisterType<CalculationBS>().As<ICalculationBS>();
            builder.RegisterType<CalculationProjectBS>().As<ICalculationProjectBS>().InstancePerRequest();
            builder.RegisterType<UserBS>().As<IUserBS>().InstancePerRequest();
            builder.RegisterType<ProjectBS>().As<IProjectBS>().InstancePerRequest();
            builder.RegisterType<AuditTrailBS>().As<IAuditTrailBS>().InstancePerRequest();
            builder.RegisterType<FormulasCalculatorContainer>().As<IFormulasContainer> ().SingleInstance();
            builder.RegisterType<CampusInfoBS>().As<ICampusInfo>().InstancePerRequest();
            builder.RegisterType<FileBS>().As<IFileBS>().InstancePerRequest();

            builder.RegisterType<ProjectModelsMapper>().As<ProjectModelsMapper>().SingleInstance();
           
            var localStorage = new VariablesLocalStorage();
            builder.Register<VariablesLocalStorage>(v => localStorage).As<IReadLocalStorage>();
            builder.Register<VariablesLocalStorage>(v => localStorage).As<IUpdateLocalStorage>();
           
    
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);
            app.UseAutofacMvc();

            return config;
        }
    }
}
