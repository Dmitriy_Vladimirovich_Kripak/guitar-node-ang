var opts = {
  lines: 0, // The number of lines to draw
  angle: 0.00, // The length of each line
  lineWidth: 0.44, // The line thickness
  
 
  pointer: {
    length: 0.5, // The radius of the inner circle
    strokeWidth: 0.060, // The rotation offset
    color: 'red' // Fill color
  },
  
  limitMax: false,   // If true, the pointer will not go past the end of the gauge
  colorStart: 'blue',   // Colors
  colorStop: '#8FC0DA',    // just experiment with them
  strokeColor: '#E0E0E0',   // to see which ones work best for you
  generateGradient: true
};
var target = document.getElementById('foo'); // your canvas element
var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!

var target2 = document.getElementById('foo2'); 
var gauge2 = new Gauge(target2).setOptions(opts); // create sexy gauge!

var target3 = document.getElementById('foo3'); 
var gauge3 = new Gauge(target3).setOptions(opts); // create sexy gauge!

var target4 = document.getElementById('foo4'); 
var gauge4 = new Gauge(target4).setOptions(opts); // create sexy gauge!

var target5 = document.getElementById('foo5'); 
var gauge5 = new Gauge(target5).setOptions(opts); // create sexy gauge!

var target6 = document.getElementById('foo6'); 
var gauge6 = new Gauge(target6).setOptions(opts); // create sexy gauge!


gauge.maxValue = 100; // set max gauge value
gauge.animationSpeed = 32; // set animation speed (32 is default value)

gauge2.maxValue = 200; // set max gauge value
gauge2.animationSpeed = 32; // set animation speed (32 is default value)

gauge3.maxValue = 200; // set max gauge value
gauge3.animationSpeed = 32; // set animation speed (32 is default value)

gauge4.maxValue = 200; // set max gauge value
gauge4.animationSpeed = 32; // set animation speed (32 is default value)

gauge5.maxValue = 200; // set max gauge value
gauge5.animationSpeed = 32; // set animation speed (32 is default value)

gauge6.maxValue = 200; // set max gauge value
gauge6.animationSpeed = 32; // set animation speed (32 is default value)