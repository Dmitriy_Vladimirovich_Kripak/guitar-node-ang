﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Application.Entities;
using Application.DAL;

namespace Application.Common.Identity
{
    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        private ApplicationRoleManager _roleManager;

        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            var signInManager = new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
            signInManager._roleManager = context.Get<ApplicationRoleManager>();
            return signInManager;
        }

        public void InitializeDefaultAdminUser()
        {
            const string name = "admin@proteacorp.com";
            const string password = "1";
            const string roleName = "Admin";

            //Create Role Admin if it does not exist
            var role = _roleManager.FindByNameAsync(roleName).Result;

            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = _roleManager.CreateAsync(role).Result;
            }

            var user = UserManager.FindByNameAsync(name).Result;

            if (user == null)
            {
                user = new ApplicationUser { UserName = name, Email = name };
                var result = UserManager.CreateAsync(user, password).Result;
                result = UserManager.SetLockoutEnabledAsync(user.Id, false).Result;
            }

            // Add user admin to Role Admin if not already added
            var rolesForUser = UserManager.GetRolesAsync(user.Id).Result;

            if (!rolesForUser.Contains(role.Name))
            {
                var result = UserManager.AddToRoleAsync(user.Id, role.Name).Result;
            }
        }

    }
}
