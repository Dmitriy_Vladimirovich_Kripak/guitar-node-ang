﻿import { Component, OnInit } from '@angular/core';
import { CalculationType,TotalProjectCostViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'totalProjectCalulation',
    templateUrl: './TotalProjectCostcalulation.component.html'
})


export class TotalProjectCostCalulationComponent {
    result: any;   
    model: TotalProjectCostViewModel = new TotalProjectCostViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.TotalProjectCost).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcTotalProjectCost(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate Total Project Cost Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate Total Project Cost Formula");
        });
    }

}
