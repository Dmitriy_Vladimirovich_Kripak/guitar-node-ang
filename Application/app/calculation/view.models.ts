﻿import { Dictionary } from '../common/dictionary.type'

export class BaselineVarialbleViewModel {
    Name: string = "";
    Value: string = "";
}

export class A10ViewModel {
    SoilCondition: SoilCondition = SoilCondition.Soil;
    NumberOfLevels: number = 5;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}

export class A20ViewModel {
    GrossSquareFeet: number = 100000;
    NumberOfLevels: number = 5;
    NumberOfBelowGradeLevels: number = 1;
    NumberOfAboveGradeLevels: number = 4;
    CladdingRatio: number = 0.6;
    FFHeights: number = 16;
    FloorPlateWidth: number = 75;
    BuildingFootprint: number = 20000;
    BasementFFHeights: number = 20;
    SiteConstraint: SiteConstraint = SiteConstraint.Constrained;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;

}

export class A40ViewModel {
    GrossSquareFeet: number = 100000;
    NumberOfLevels: number = 5;
    NumberOfBelowGradeLevels: number = 1;
    NumberOfAboveGradeLevels: number = 4;
    BuildingFootprint: number = 20000;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}
export class A60ViewModel {
    GrossSquareFeet: number = 100000;
    NumberOfLevels: number = 5;
    NumberOfBelowGradeLevels: number = 1;
    NumberOfAboveGradeLevels: number = 4;
    CladdingRatio: number = 0.6;
    FFHeights: number = 16;    
    FloorPlateWidth: number = 75;
    BuildingFootprint: number = 20000;
    BasementFFHeights: number = 20; 
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}
export class A90ViewModel {
    GrossSquareFeet: number = 100000;
    NumberOfLevels: number = 5;
    NumberOfBelowGradeLevels: number = 1;
    NumberOfAboveGradeLevels: number = 4;
    CladdingRatio: number = 0.6;
    FFHeights: number = 16;
    FloorPlateWidth: number = 75;
    BuildingFootprint: number = 20000;
    BasementFFHeights: number = 20;
    SoilCondition: SoilCondition = SoilCondition.RippableRock;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}

export class B10ViewModel {
    GrossSquareFeet: number = 100000;
    NumberOfLevels: number = 5;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    HorizontalArticulationFactor: number = 1.2;
    FloorConstructionType: string = "ConcreteFlatSlab";
    RoofConstructionType: string = "ConcreteFlatSlab";
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}

export class B20ViewModel {
    GrossSquareFeet: number = 100000;
    NumberOfLevels: number = 5;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    NumberOfAboveGradeLevels: number = 4;
    NumberOfBelowGradeLevels: number = 1;
    FFHeights: number = 16;
    FloorPlateWidth: number = 75;
    BuildingFootprint: number = 20000;
    BasementFFHeights: number = 20;
    CladdingRatio: number = 0.6;
    GlazingRatio: number = 0.3;
    VerticalArticulationFactor: number = 1.15;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}
export class B30ViewModel {
    GrossSquareFeet: number = 100000;
    NumberOfLevels: number = 5; 
    NumberOfAboveGradeLevels: number = 4;
    NumberOfBelowGradeLevels: number = 1;
    BuildingFootprint: number = 20000;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    RoofTerraceRatio: number = 0.1;
    RoofGlazingRatio: number = 0.05;
    HorizontalArticulationFactor: number = 1.2;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}

export class C10_E20SettingsViewModel {
    AssignableProgram1IsUsing: boolean = false;
    AssignableProgram2IsUsing: boolean = false;
    AssignableProgram3IsUsing: boolean = false;
    AssignableProgram4IsUsing: boolean = false;
    AssignableProgram5IsUsing: boolean = false;
    AssignableProgram6IsUsing: boolean = false;
    AssignableProgram7IsUsing: boolean = false;
    AssignableProgram8IsUsing: boolean = false;

    AssignableProgram1Caption: string = "";
    AssignableProgram2Caption: string = "";
    AssignableProgram3Caption: string = "";
    AssignableProgram4Caption: string = "";
    AssignableProgram5Caption: string = "";
    AssignableProgram6Caption: string = "";
    AssignableProgram7Caption: string = "";
    AssignableProgram8Caption: string = "";

    GrossUpProgram1Caption: string = "";
    GrossUpProgram2Caption: string = "";
    GrossUpProgram3Caption: string = "";
    GrossUpProgram4Caption: string = "";
}
export class BaseC10_E20ViewModel {
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;

    GrossSquareFeet: number = 0;
    BuildingEfficiency: number = 0;

    AssignableProgram1: number = 0;
    AssignableProgram2: number = 0;
    AssignableProgram3: number = 0;
    AssignableProgram4: number = 0;
    AssignableProgram5: number = 0;
    AssignableProgram6: number = 0;
    AssignableProgram7: number = 0;
    AssignableProgram8: number = 0;
    AssignableProgram1ASF: number = 0;
    AssignableProgram2ASF: number = 0;
    AssignableProgram3ASF: number = 0;
    AssignableProgram4ASF: number = 0;
    AssignableProgram5ASF: number = 0;
    AssignableProgram6ASF: number = 0;
    AssignableProgram7ASF: number = 0;
    AssignableProgram8ASF: number = 0;

    GrossUpProgram1: number = 0;
    GrossUpProgram2: number = 0;
    GrossUpProgram3: number = 0;
    GrossUpProgram4: number = 0;
    GrossUpProgram1ASF: number = 0;
    GrossUpProgram2ASF: number = 0;
    GrossUpProgram3ASF: number = 0;
    GrossUpProgram4ASF: number = 0;

    status: string = "";
}

export class C10ViewModel extends BaseC10_E20ViewModel {
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class C10FitoutViewModel extends BaseC10_E20ViewModel {
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class C20ViewModel extends BaseC10_E20ViewModel {
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;

    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class C20FitoutViewModel extends BaseC10_E20ViewModel {
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;

    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D10ViewModel {
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}

export class D20ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    Plumbing: Plumbing = Plumbing.Typical;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D20FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    Plumbing: Plumbing = Plumbing.Typical;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D30ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    HVAC: HVAC = HVAC.Air;
    BuildingPlant: BuildingPlant = BuildingPlant.Remote;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D30FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    HVAC: HVAC = HVAC.Air;
    BuildingPlant: BuildingPlant = BuildingPlant.Remote;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D40ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    FireProtection: FireProtection = FireProtection.Wet;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D40FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    FireProtection: FireProtection = FireProtection.Wet;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D50ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    Electrical: Electrical = Electrical.Typical;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D50FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    Electrical: Electrical = Electrical.Typical;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D60ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    Electrical: Electrical = Electrical.Typical;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D60FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    Electrical: Electrical = Electrical.Typical;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D70ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    Electrical: Electrical = Electrical.Typical;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D70FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    Electrical: Electrical = Electrical.Typical;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D80ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    HVAC: HVAC = HVAC.Air;
    BuildingPlant: BuildingPlant = BuildingPlant.Remote;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class D80FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    HVAC: HVAC = HVAC.Air;
    BuildingPlant: BuildingPlant = BuildingPlant.Remote;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class E10ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class E10FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class E20ViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class E20FitoutViewModel extends BaseC10_E20ViewModel {
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    constructor() {
        super();
        this.GrossSquareFeet = 100000; this.BuildingEfficiency = 0.62;
        this.AssignableProgram1 = 0.30; this.AssignableProgram2 = 0.15; this.AssignableProgram3 = 0.15; this.AssignableProgram4 = 0.02;
        this.GrossUpProgram1 = 0.03; this.GrossUpProgram2 = 0.13; this.GrossUpProgram3 = 0.12; this.GrossUpProgram4 = 0.10;
    }
}

export class F10BCViewModel {
    GrossSquareFeet: number = 100000;
    SpecialConstructionAllowance: number = 100000;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}

export class F20BCViewModel {
    GrossSquareFeet: number = 100000;
    AreaOfFacilityToBeRemediated: number = 60000;
    FacilityRemediation: FacilityRemediation = FacilityRemediation.Simple;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}

export class F30BCViewModel {
    GrossSquareFeet: number = 100000;
    AreaOfFacilityToBeDemolished: number = 60000;
    FacilityDemolition: FacilityDemolition = FacilityDemolition.Simple;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}

export class Z10BCViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    A10F30Sum: number = 395.96;
    Z910BC: number = 47.52;
    Z920BC: number = 69.86;
    Z930BC: number = 15.40;
}

export class Z70BCViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    A10F30Sum: number = 395.96;
    Z910BC: number = 47.52;
    Z920BC: number = 69.86;
    Z930BC: number = 15.04;
    Z10BC: number = 52.87;
}

export class Z910BCViewModel {
    ContingenciesDesign: number = 0.12;
    A10F30Sum: number = 395.96;
}

export class Z920BCViewModel {
    EscalateTo: EscalateTo = EscalateTo.ConstructionMidpoint;
    EstimateDate: Object = { date: { year: 2017, day: 1, month: 1, } };  //Date = new Date(2017, 0, 1);
    EstimateDateStr: string = '';
    ConstructionStartDate: Object = { date: { year: 2019, day: 1, month: 1, } };//Date = new Date(2019, 0, 1);
    ConstructionStartDateStr: string = '';
    DurationInMonths: number = 24;
    A10F30Sum: number = 395.96;
    Z910BC: number = 47.52;
}
export class Z930BCViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    A10F30Sum: number = 395.96;
    Z910BC: number = 47.52;
    Z920BC: number = 69.86;
}

export class Z940BCViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    A10F30Sum: number = 395.96;
    Z910BC: number = 47.52;
    Z920BC: number = 69.86;
    Z930BC: number = 15.4;
    Z10BC: number = 52.87;
    Z70BC: number = 11.63;
}
export class G10SDViewModel {
    GrossSiteArea: number = 70000;
    BuildingFootprint: number = 20000;
    SitePreparation: SitePreparation = SitePreparation.Greenfield;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}
export class G20SDViewModel {
    GrossSiteArea: number = 70000;
    BuildingFootprint: number = 20000;
    SiteDevelopment: SiteDevelopment = SiteDevelopment.Moderate;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}
export class G60SDViewModel {
    GrossSiteArea: number = 70000;
    BuildingFootprint: number = 20000;  
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
}
export class Z10SDViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    G10G60Sum: number = 34.87;
    Z910SD: number = 4.18;
    Z920SD: number = 6.15;
    Z930SD: number = 1.36;
}
export class Z70SDViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    G10G60Sum: number = 34.87;
    Z910SD: number = 4.18;
    Z920SD: number = 6.15;
    Z930SD: number = 1.36;
    Z10SD: number = 4.66;
}
export class Z910SDViewModel {
    ContingenciesDesign: number = 0.12;
    G10G60Sum: number = 34.87;
}
export class Z920SDViewModel {
    EscalateTo: EscalateTo = EscalateTo.ConstructionMidpoint;
    EstimateDate: Object = { date: { year: 2017, day: 1, month: 1, } };  //Date = new Date(2017, 0, 1);
    EstimateDateStr: string = '';
    ConstructionStartDate: Object = { date: { year: 2019, day: 1, month: 1, } };//Date = new Date(2019, 0, 1);
    ConstructionStartDateStr: string = '';
    DurationInMonths: number = 24;
    G10G60Sum: number = 34.87;
    Z910SD: number = 4.18;
}
export class Z930SDViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    G10G60Sum: number = 34.87;
    Z910SD: number = 4.18;
    Z920SD: number = 6.15;
}
export class Z940SDViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    G10G60Sum: number = 34.87;
    Z910SD: number = 4.18;
    Z920SD: number = 6.15;
    Z930SD: number = 1.36;
    Z10SD: number = 4.66;
    Z70SD: number = 1.02;
}
export class G30SUViewModel {
    GrossSiteArea: number = 70000;
    BuildingFootprint: number = 20000;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    SiteInfrastructureServices: SiteInfrastructureServices = SiteInfrastructureServices.Proximate;
    BuildingPlant: BuildingPlant = BuildingPlant.Remote;
    SiteInfrastructureCivil: SiteInfrastructureCivil = SiteInfrastructureCivil.Proximate; 
    SiteServicesHotWater: boolean = false;
    SiteServicesSteamCondensate: boolean = false;
    SiteServicesChilledWaterSupply: boolean = false;
    SiteCivilWaterFire: boolean = false;
    SiteCivilSanitarySewer: boolean = false;
    SiteCivilStormSewer: boolean = false;
    SiteCivilGas: boolean = false;
}
export class G40SUViewModel {
    GrossSiteArea: number = 70000;
    BuildingFootprint: number = 20000;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    SiteInfrastructureServices: SiteInfrastructureServices = SiteInfrastructureServices.Proximate;  
    SiteInfrastructureElectrical: boolean = true; 
}
export class G50SUViewModel {
    GrossSiteArea: number = 70000;
    BuildingFootprint: number = 20000;
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    SiteInfrastructureServices: SiteInfrastructureServices = SiteInfrastructureServices.Proximate;  
    SiteInfrastructureElectrical: boolean = true;
}

export class Z10SUViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    G30G50SUSum: number = 25.25;
    Z910SU: number = 3.03;
    Z920SU: number = 4.45;
    Z930SU: number = 0.98;
}
export class Z70SUViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    G30G50SUSum: number = 25.25;
    Z910SU: number = 3.03;
    Z920SU: number = 4.45;
    Z930SU: number = 0.98;
    Z10SU: number = 3.37;
}
export class Z910SUViewModel {
    ContingenciesDesign: number = 0.12;
    G30G50SUSum: number = 25.25;
}
export class Z920SUViewModel {
    EscalateTo: EscalateTo = EscalateTo.ConstructionMidpoint;
    EstimateDate: Object = { date: { year: 2017, day: 1, month: 1, } };  //Date = new Date(2017, 0, 1);
    EstimateDateStr: string = '';
    ConstructionStartDate: Object = { date: { year: 2019, day: 1, month: 1, } };//Date = new Date(2019, 0, 1);
    ConstructionStartDateStr: string = '';
    DurationInMonths: number = 24;
    G30G50SUSum: number = 25.25;
    Z910SU: number = 3.03;
}
export class Z930SUViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    G10G50SUSum: number = 25.25;
    Z910SU: number = 3.03;
    Z920SU: number = 4.45;
}
export class Z940SUViewModel {
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;   
    G30G50SUSum: number = 25.25;
    Z910SU: number = 3.03;
    Z920SU: number = 4.45;
    Z930SU: number = 0.98;
    Z10SU: number = 3.37;
    Z70SU: number = 0.74;
}
export class SoftCostViewModel {   
    DesignFee: number = 0.1;
    FFE: number = 0.05;
    Technology: number = 0.03;
    Contingency: number = 0.07;
    Misc: number = 0.1;
    TotalConstructionCosts: number = 657.43;
}
export class TotalProjectCostViewModel {   
    TotalConstructionCosts: number = 657.43;
    SoftCosts: number = 230.10;
}
export enum CalculationType {
    All = 0,
    A10,
    A20,
    A40,
    A60,
    A90,
    B10,
    B20,
    B30,
    D10,
    C10,
    C10Fitout,
    C20,
    C20Fitout,
    D20,
    D20Fitout,
    D30,
    D30Fitout,
    D40,
    D40Fitout,
    D50,
    D50Fitout,
    D60,
    D60Fitout,
    D70,
    D70Fitout,
    D80,
    D80Fitout,
    E10,
    E10Fitout,
    E20,
    E20Fitout,
    F10BC,
    F20BC,
    F30BC,
    Z10BC,
    Z70BC,
    Z910BC,
    Z920BC,
    Z930BC,
    Z940BC,
    G10SD,
    G20SD,
    G60SD,
    Z10SD,
    Z70SD,
    Z910SD,
    Z920SD,
    Z930SD,
    Z940SD,
    G30SU,
    G40SU,
    G50SU,
    Z10SU,
    Z70SU,
    Z910SU,
    Z920SU,
    Z930SU,
    Z940SU,
    SoftCost,
    TotalProjectCost,
}

export enum BuildingType {
    AcademicClassroomBuilding = 0,
    LaboratoryBuilding,
    StudentHousing,
    Dining,
    ParkingGarageBelowGrade,
    ParkingGarageAboveGrade,
}

export enum SoilCondition {
    Soil = 0,
    RippableRock,
    SolidRock,
}

export enum SiteConstraint {
    Constrained = 0,
    Open,
}

export class FloorRoofConstractionPossibleValues {
    FloorPossibleTypes: Dictionary<string[]>;
    RoofPossibleTypes: Dictionary<string[]>;

    constructor() {
        this.FloorPossibleTypes = new Dictionary<string[]>();
        this.FloorPossibleTypes.Add(BuildingType[BuildingType.AcademicClassroomBuilding], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame"]);
        this.FloorPossibleTypes.Add(BuildingType[BuildingType.LaboratoryBuilding], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame"]);
        this.FloorPossibleTypes.Add(BuildingType[BuildingType.StudentHousing], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", "WoodFrame"]);
        this.FloorPossibleTypes.Add(BuildingType[BuildingType.Dining], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame"]);
        this.FloorPossibleTypes.Add(BuildingType[BuildingType.ParkingGarageBelowGrade], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "Precast"]);
        this.FloorPossibleTypes.Add(BuildingType[BuildingType.ParkingGarageAboveGrade], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "Precast"]);

        this.RoofPossibleTypes = new Dictionary<string[]>();
        this.RoofPossibleTypes.Add(BuildingType[BuildingType.AcademicClassroomBuilding], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame"]);
        this.RoofPossibleTypes.Add(BuildingType[BuildingType.LaboratoryBuilding], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame"]);
        this.RoofPossibleTypes.Add(BuildingType[BuildingType.StudentHousing], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", "WoodFrame"]);
        this.RoofPossibleTypes.Add(BuildingType[BuildingType.Dining], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame"]);
        this.RoofPossibleTypes.Add(BuildingType[BuildingType.ParkingGarageBelowGrade], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "Precast"]);
        this.RoofPossibleTypes.Add(BuildingType[BuildingType.ParkingGarageAboveGrade], ["ConcreteFlatSlab", "ConcreteSlabAndBeam", "Precast"]);
    }
}
export enum ArchitecturalExpectation {
    Iconic = 0,
    CampusStandard,
    LowCost,
}

export enum PerformanceExpectation {
    LEEDPlatinumEquivalent = 0,
    LEEDGoldEquivalent,
    LEEDSilverEquivalent,
}

export enum LocationFactor {
    NationalAverage = 0,
    LosAngeles,
    TBD
}

export enum ProjectDeliveryStrategyFactor {
    DesignBidBuild = 0,
    CMARRegionalTier2,
    CMARRegionalTier1,
    CMARNational
}

export enum Plumbing {
    Typical = 0,
    Flexible,
}

export enum HVAC {
    Air = 0,
    Hydronic,
    Hybrid,
}

export enum BuildingPlant {
    Local = 0,
    Remote
}
export enum FireProtection {
    Wet = 0,
    WetPreAction,
    Dry,
}

export enum Electrical {
    Typical = 0,
    Comprehensive,
}

export enum EscalateTo {
    EstimateDate = 0,
    ConstructionStartDate,
    ConstructionMidpoint,
    ConstructionDuration,
}
export enum FacilityRemediation {
    Simple = 0,
    Complex,
}
export enum FacilityDemolition {
    Simple = 0,
    Complex,
}
export enum SitePreparation {
    Greenfield = 0,
    Brownfield,
}
export enum SiteDevelopment  {
    Simple  = 0,
    Moderate,
    Extensive,
}
export enum SiteInfrastructureServices {
    Proximate = 0,
    Remote,   
}
export enum SiteServices {
    SteamAndCondensate = 0,
    HotWaterSupply,
    ChilledWaterSupply
}
export enum SiteInfrastructureCivil {
    Proximate = 0,
    Remote,
}
export enum SiteCivil {
    WaterOrFire = 0,
    SanitarySewer,
    StormSewer,
    Gas
}