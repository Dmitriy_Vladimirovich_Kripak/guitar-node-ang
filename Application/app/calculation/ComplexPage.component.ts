﻿import { Component, OnInit, ChangeDetectorRef, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder, } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import {
    CalculationType, ProjectDeliveryStrategyFactor, LocationFactor, BuildingType, SoilCondition, SiteConstraint, ArchitecturalExpectation,
    PerformanceExpectation, Plumbing, HVAC, BuildingPlant, FireProtection, Electrical, EscalateTo, FacilityRemediation, FacilityDemolition, SitePreparation,
    SiteDevelopment, SiteInfrastructureServices, SiteServices, SiteInfrastructureCivil, SiteCivil, C10_E20SettingsViewModel, FloorRoofConstractionPossibleValues
} from './view.models';
import { ProjectViewModel } from './project.model';
import { ACalculationService } from './a-calulation.service';
import { EnumHelperService } from '../common/enum-helper.service';
import { ProjectViewResultModel } from './project-view-result.model';
import { NotificationStatusService } from '../services/notification-status.service';
import { IMyDpOptions, IMyDateModel, IMyInputFieldChanged, IMyCalendarViewChanged, IMyMarkedDate, IMyDate, IMyDefaultMonth } from 'mydatepicker';

@Component({
    selector: 'complex-page',    
    templateUrl: './ComplexPage.component.html',      
}) 

export class ComplexPageComponent {

    @Input() set currentModel(newModel: ProjectViewModel) {
        if (newModel)
        {
            this.model = newModel;
            this.resultView = new ProjectViewResultModel();
        }          
    }

    @Input() set IsUserProject(isUserProject: boolean) {
        this.isBlockUserInputs = isUserProject;
    }

    @Input() projectName: string;

    isBlockUserInputs: boolean;
    model: ProjectViewModel = new ProjectViewModel();
    resultView: ProjectViewResultModel = new ProjectViewResultModel();
    floorRoofConstractionPossibleValues: FloorRoofConstractionPossibleValues = new FloorRoofConstractionPossibleValues();
    result: any;
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    buildingTypes: any[] = EnumHelperService.getValues(BuildingType); 
    soilConditions: any[] = EnumHelperService.getValues(SoilCondition); 
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation); 
    performanceExpectations: any[] = EnumHelperService.getValues(PerformanceExpectation); 
    siteConstraints: any[] = EnumHelperService.getValues(SiteConstraint); 
    plumbings: any[] = EnumHelperService.getValues(Plumbing); 
    HVACs: any[] = EnumHelperService.getValues(HVAC); 
    buildingPlants: any[] = EnumHelperService.getValues(BuildingPlant); 
    fireProtections: any[] = EnumHelperService.getValues(FireProtection); 
    electricals: any[] = EnumHelperService.getValues(Electrical); 
    escalateToValues: any[] = EnumHelperService.getValues(EscalateTo);
    facilityRemediation: any[] = EnumHelperService.getValues(FacilityRemediation);
    facilityDemolition: any[] = EnumHelperService.getValues(FacilityDemolition);
    sitePreparation: any[] = EnumHelperService.getValues(SitePreparation);
    siteDevelopment: any[] = EnumHelperService.getValues(SiteDevelopment);
    siteInfrastructureServices: any[] = EnumHelperService.getValues(SiteInfrastructureServices);
    siteServices: any[] = EnumHelperService.getValues(SiteServices);
    siteInfrastructureCivil: any[] = EnumHelperService.getValues(SiteInfrastructureCivil);
    siteCivil: any[] = EnumHelperService.getValues(SiteCivil);
    floorConstructionTypes: any[] = this.floorRoofConstractionPossibleValues.FloorPossibleTypes.Item(BuildingType[this.model.BuildingType]);
    roofConstructionTypes: any[] = this.floorRoofConstractionPossibleValues.RoofPossibleTypes.Item(BuildingType[this.model.BuildingType]);
    isResult: boolean = false;


    @Output() resultViewEmit: EventEmitter<any> = new EventEmitter<any>();
    

    /*------------------------------COREL+SHELL variables--------------------------------*/
    viewSettings: C10_E20SettingsViewModel = new C10_E20SettingsViewModel();
    assignableProgramsStatus: string = '';
    grossUpProgramsStatus: string = '';
    totalAssignableProgramsASF: number = 0;
    totalGrossUpProgramsASF: number = 0;

    stepSize: number = 0.001;
    buildingEfficiencyASF: number;
    minBuildingEfficiency: number = 0.5;
    maxBuildingEfficiency: number = 0.75;
    shellSpaceIndex: number = 0;   
    isInit: boolean = true;
    status: string = '';

    /*------------------------------END COREL+SHELL variables--------------------------------*/

    /*------------------------------- Taxes Escalations Fees variables---------------------------------------*/

    yourModelDate: Date = new Date();  

    /*------------------------------------END Taxes Escalations Fees variables----------------------------------*/

    /*------------------------------- Soft Costs variables---------------------------------------*/
    minDesignFee: number = 0.05;
    maxDesignFee: number = 0.15;
    minFFE: number = 0.03
    maxFFE: number = 0.08;
    minTechnology: number = 0.03;
    maxTechnology: number = 0.07;
    minContingency: number = 0.04;
    maxContingency: number = 0.2;
    minMisc: number = 0;
    maxMisc: number = 0.15;
    minContingenciesDesign: number = 0;
    maxContingenciesDesign: number = 1;
    /*------------------------------- END Soft Costs variables---------------------------------------*/

    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private _changeDetectionRef: ChangeDetectorRef, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.isBlockUserInputs = true;
    }

    /*-------------------------------DatePicker-------------------------*/

    private myDatePickerOptions: IMyDpOptions = {
        // other options...
        dateFormat: 'mm.dd.yyyy',
    };

    onEstimateDateChanged(event: IMyDateModel): void {
        this.model.EstimateDateStr = event.formatted.replace(/\./g, "/");        
    }
    onConstructionStartDateChanged(event: IMyDateModel): void {
        this.model.ConstructionStartDateStr = event.formatted.replace(/\./g,"/");
    }

     /*-------------------------------END DatePicker-------------------------*/
                      
    mychange(val) {
        console.log(val); // updated value
    }
    calc(personForm: FormGroup) {
           
        if (!personForm.valid)
            return;
        this.service.calcProject(this.model).subscribe((response) => {
            this.resultView = response.Data;
            this.isResult = true;     
            if (response.Status == "Ok")
                this.notificationStatusService.displayOkMessage("Project was calculated", "Success");
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate All Formula");  
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate All Formula"); 
            });
        this.resultViewEmit.emit(this.model);
    }
   
    goTop() {
        window.scrollTo(0, 0);
    }

    ngOnInit() {
        this.onBuildingTypeChange(this.model.BuildingType);
        this.resultViewEmit.emit(this.model);
    }
  
    /*------------------------------COREL+SHELL METHODS --------------------------------*/

    public options = {
        position: ["bottom", "right"],
        timeOut: 0,
        lastOnBottom: true,
    }; 
   
    notificationAboutCalculate()
    {
        this.notificationStatusService.displayOkMessage('For update formula You should click "Calculate" button ', "Warning");       
        this.resultViewEmit.emit(this.model);
    }

    onBuildingTypeChange(newValue: number) {
        this.model.BuildingType = newValue;       
        this.updateBuildingEfficiencyASF();
        this.service.getC10E20ViewSettings(this.model.BuildingType).subscribe((res) => {
            if (res.Status == "Ok") {
                this.viewSettings = res.Data;
                this.isInit = false;
                this.prepareAssignablyProgramValues();
                this.updateUIAfterChanges();
                this._changeDetectionRef.detectChanges();
            }
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get View Settings");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get View Settings");
        });
        
        /*------------------Possible Values By BuildingType-------------------------*/

        this.floorConstructionTypes = this.floorRoofConstractionPossibleValues.FloorPossibleTypes.Item(BuildingType[this.model.BuildingType]);
        this.roofConstructionTypes = this.floorRoofConstractionPossibleValues.RoofPossibleTypes.Item(BuildingType[this.model.BuildingType]);

        this.model.FloorConstructionType = this.floorConstructionTypes[0];
        this.model.RoofConstructionType = this.roofConstructionTypes[0];

        /*------------------End Possible Values By BuildingType---------------------*/       
    }

    assignableProgramASFValueUpdated(newValue: number, index: number) {
        if (newValue < 0)
            newValue = - newValue;
        if (this.isInit) return;
        this.model['AssignableProgram' + index.toString() + 'ASF'] = newValue;
        if (index != this.shellSpaceIndex) {
            //  shellSpace program = BuildingEfficiency - all the others programs
            //  one program is shellSpace, so all programs (1 +..+ 8) - shellSpace program = all programs except shellSpace program
            this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] = this.buildingEfficiencyASF -
                (this.model.AssignableProgram1ASF + this.model.AssignableProgram2ASF + this.model.AssignableProgram3ASF + this.model.AssignableProgram4ASF
                    + this.model.AssignableProgram5ASF + this.model.AssignableProgram6ASF + this.model.AssignableProgram7ASF + this.model.AssignableProgram8ASF
                    - this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF']
                );
            if (this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] < 0) {
                this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] = 0;
            }
        }
        for (var i = 1; i <= 8; i++) {
            this.model['AssignableProgram' + i.toString()] = parseFloat((this.model['AssignableProgram' + i.toString() + 'ASF'] / this.model.GrossSquareFeet).toFixed(3));
        } 

        this.updateUIAfterChanges();
        this._changeDetectionRef.detectChanges();
    }

    grossUpProgramASFValueUpdated(newValue: number, index: number) {
        if (this.isInit) return;

        this.model['GrossUpProgram' + index.toString() + 'ASF'] = newValue;
        if (index != 4) {
            let restBuildingEfficiencyASF = this.model.GrossSquareFeet - this.buildingEfficiencyASF;
            this.model.GrossUpProgram4ASF = restBuildingEfficiencyASF - (this.model.GrossUpProgram1ASF + this.model.GrossUpProgram2ASF + this.model.GrossUpProgram3ASF);
            if (this.model.GrossUpProgram4ASF < 0) {
                this.model.GrossUpProgram4ASF = 0;
            }
        }
        for (var i = 1; i <= 4; i++) {
            this.model['GrossUpProgram' + i.toString()] = parseFloat((this.model['GrossUpProgram' + i.toString() + 'ASF'] / this.model.GrossSquareFeet).toFixed(3));
        }

        this.updateUIAfterChanges();
        this._changeDetectionRef.detectChanges();
    }

    updateBuildingEfficiency() {
        this.model.BuildingEfficiency = parseFloat((this.buildingEfficiencyASF / this.model.GrossSquareFeet).toFixed(3));
    }

    updateBuildingEfficiencyASF() {
        this.buildingEfficiencyASF = Math.round((this.model.BuildingEfficiency * this.model.GrossSquareFeet));
    }

    updateUIAfterChanges() {
        this.updateBuildingEfficiency();
        this.calculateAllPrograms();
        this.updateAssignableStatus();
        this.updateGrossUpStatus();
    }

    calculateAllPrograms() {
        this.totalAssignableProgramsASF = 0;
        for (var i = 1; i <= 8; i++) {
            this.totalAssignableProgramsASF = this.totalAssignableProgramsASF + this.model['AssignableProgram' + i.toString() + 'ASF'];
        }
        this.totalGrossUpProgramsASF = 0;
        for (var i = 1; i <= 4; i++) {
            this.totalGrossUpProgramsASF = this.totalGrossUpProgramsASF + this.model['GrossUpProgram' + i.toString() + 'ASF'];
        }
    }

    prepareAssignablyProgramValues() {
        for (var i = 1; i <= 8; i++) {
            this.model['AssignableProgram' + i.toString() + 'ASF'] = Math.round((this.model['AssignableProgram' + i.toString()] * this.model.GrossSquareFeet));
        }
        for (var i = 1; i <= 4; i++) {
            this.model['GrossUpProgram' + i.toString() + 'ASF'] = Math.round((this.model['GrossUpProgram' + i.toString()] * this.model.GrossSquareFeet));
        }

        for (var i = 1; i <= 8; i++) {
            if (this.isUsingByIndex(i)) {
                //last visible index is index of shellSpace program
                this.shellSpaceIndex = i;
            }
            else {// set invisible programs to 0
                this.model['AssignableProgram' + i.toString()] = 0;
            }

        }
    }

    isUsingByIndex(index: number) {
        return this.viewSettings['AssignableProgram' + index.toString() + 'IsUsing'];
    }

    updateAssignableStatus() {
        this.assignableProgramsStatus = '';
        if (this.totalAssignableProgramsASF < (this.minBuildingEfficiency * this.model.GrossSquareFeet))
            this.assignableProgramsStatus = 'Sum of all assignable programs should be equal or more than ' + ((Math.round(this.minBuildingEfficiency * 10000)) / 100).toString() + ' %'
        if (this.totalAssignableProgramsASF > this.buildingEfficiencyASF)
            this.assignableProgramsStatus = 'Sum of all assignable programs should be equal or less than ' + ((Math.round(this.model.BuildingEfficiency * 10000)) / 100).toString() + ' %'

    }

    updateGrossUpStatus() {
        this.grossUpProgramsStatus = '';
        if (this.totalGrossUpProgramsASF < ((1 - this.maxBuildingEfficiency) * this.model.GrossSquareFeet))
            this.grossUpProgramsStatus = 'Sum of all Gross Up programs should be equal or more than ' + ((Math.round((1 - this.maxBuildingEfficiency) * 10000)) / 100).toString() + ' %'
        if (this.totalGrossUpProgramsASF > (this.model.GrossSquareFeet - this.buildingEfficiencyASF))
            this.grossUpProgramsStatus = 'Sum of all Gross Up programs should be equal or less than ' + ((Math.round((1 - this.model.BuildingEfficiency) * 10000)) / 100).toString() + ' %'
    }  
    /*------------------------------END COREL+SHELL METHODS --------------------------------*/

    /*------------------------------------Building Constraction/Site Development/Site Utilities settings----------------------------------------*/
    changeValueCheckBox(element: HTMLInputElement) {
        this.model.SiteInfrastructureElectrical = element.checked ? true : false;
    }

    /*------------------------------- END Building Constraction/Site Development/Site Utilities settings -------------------------------------*/

    numberOfLevelsUpdated(newValue: any) {
        this.model.NumberOfLevels = newValue.numberOfLevels;
        this.model.NumberOfBelowGradeLevels = newValue.numberOfBelowGradeLevels;
        this.model.NumberOfAboveGradeLevels = newValue.numberOfAboveGradeLevels;
        this.model.BuildingFootprint = newValue.buildingFootprint;
        this.model.GrossSquareFeet = newValue.grossSquareFeet;

        this.recalCladdingRatio();
    }

    claddingRatioUpdated(newValue: any) {
        this.model.CladdingRatio = newValue.claddingRatio;
        this.model.FFHeights = newValue.fFHeights;
        this.model.FloorPlateWidth = newValue.floorPlateWidth;
        this.model.BasementFFHeights = newValue.basementFFHeights;
    }    

    recalCladdingRatio() {
        if (this.model.FloorPlateWidth > 0 && this.model.FFHeights > 0 && this.model.GrossSquareFeet > 0 && this.model.NumberOfLevels > 0 && this.model.NumberOfBelowGradeLevels > 0 && this.model.NumberOfAboveGradeLevels && this.model.FFHeights) {
            this.model.CladdingRatio = ((((this.model.GrossSquareFeet / this.model.NumberOfLevels) / this.model.FloorPlateWidth + this.model.FloorPlateWidth) * 2 * this.model.BasementFFHeights * this.model.NumberOfBelowGradeLevels * 1.15) +
                ((this.model.GrossSquareFeet / this.model.NumberOfLevels) / this.model.FloorPlateWidth + this.model.FloorPlateWidth) * 2 * this.model.NumberOfAboveGradeLevels * this.model.FFHeights * 1.15) / this.model.GrossSquareFeet;
            this.model.CladdingRatio = parseFloat(this.model.CladdingRatio.toFixed(3));
        }
    }
    
  

    
}
