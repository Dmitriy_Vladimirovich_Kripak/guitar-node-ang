﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, PerformanceExpectation, Electrical, LocationFactor, ProjectDeliveryStrategyFactor, D60FitoutViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'd60FitoutCalulation',
    templateUrl: './d60fitout.calulation.component.html'
})


export class D60FitoutCalulationComponent {
    result: any;
    model: D60FitoutViewModel = new D60FitoutViewModel();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    performanceExpectations: any[] = EnumHelperService.getValues(PerformanceExpectation);
    electricals: any[] = EnumHelperService.getValues(Electrical);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.D60Fitout).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcD60Fitout(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate D60Fitout formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate D60Fitout formula");
        });
    }

}
