﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, LocationFactor, ProjectDeliveryStrategyFactor, C10FitoutViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'c10FitoutCalulation',
    templateUrl: './c10fitout.calulation.component.html'
})


export class C10FitoutCalulationComponent {
    result: any;
    model: C10FitoutViewModel = new C10FitoutViewModel();
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.C10Fitout).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcC10Fitout(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate C10Fitout formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate C10Fitout formula");
        });
    }

}
