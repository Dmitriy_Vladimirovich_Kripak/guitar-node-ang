﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, FireProtection, LocationFactor, ProjectDeliveryStrategyFactor, D40FitoutViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'd40FitoutCalulation',
    templateUrl: './d40fitout.calulation.component.html'
})


export class D40FitoutCalulationComponent {
    result: any;
    model: D40FitoutViewModel = new D40FitoutViewModel();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    fireProtections: any[] = EnumHelperService.getValues(FireProtection);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.D40Fitout).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcD40Fitout(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate D40Fitout formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate D40Fitout formula");
        });
    }

}
