﻿import { Component, EventEmitter, Input, Output, AfterViewChecked, ChangeDetectorRef } from '@angular/core';


@Component({
    templateUrl: './program-range.component.html',
    selector: 'program-range',
    styleUrls: ['./program-range.component.css']
})

export class ProgramRangeComponent implements AfterViewChecked {

    private rangeSliderPlugin: any;

    @Input() caption: string;
    @Input('Id') set setId(newValue) {
        this.Id = newValue;
        if (!this.Id) {
            this.initialRangeComponent();
        }
    }
    @Input('maxValue') set setMaxValue(value) {
        this.maxValue = value;
        var selector = '#' + this.Id;
        var $element = $(selector);
        $element.rangeslider('update', true);
        //Workaround - the max value do not updates in the rangeslider.js plugin
        if (this.rangeSliderPlugin) {
            this.rangeSliderPlugin.max = value;
        }       
    }

    @Input('programASFValue') set setProgramASFValue(value) {
        this.programASFValue = value;
        this.calculatePercentValue();
        this.cdRef.detectChanges();
    }

    @Input('programPercentValue') set setProgramPercentValue(value) {
        this.programPercentValue = value;
        this.cdRef.detectChanges();
    }

    @Input() gsfValue: number;
    @Input() className: string;

    private Id: any;
    private maxValue: number;
    programPercentValue: number;

    private programASFValue: number;
    stepSize: number = 0.001;

    //Variable wich allow to determine end of rangeslider,
    //and set the maxValue of programASFValue. 
    endScrollFormulasPage: number = 16;
    endScrollProjectsPage: number = 7;

    @Output() programASFValueUpdated: EventEmitter<number> = new EventEmitter<number>();

    constructor(private cdRef: ChangeDetectorRef) {

    }
    ngOnInit() {
        this.calculatePercentValue();
    }

    ngAfterViewChecked() {
        this.initialRangeComponent();
    }

    calculatePercentValue() {
        if (this.programASFValue < 0)
            this.programASFValue = - this.programASFValue;

        this.programPercentValue = parseFloat((this.programASFValue / this.gsfValue).toFixed(3));

        //This block did not allow to display the incorrect value %
        $('#' + this.Id).val(this.programPercentValue).change();
    }

    updatePercentValue(newValue: number) {
        if (newValue < 0) {
            newValue = - newValue;
            this.programASFValue = newValue;
        }

        this.programASFValue = Math.round(this.programPercentValue * this.gsfValue);
        this.programASFValueUpdated.emit(this.programASFValue);
    }

    updateASFValue(newValue: number) {
        this.calculatePercentValue();
        if (newValue < 0) {
            newValue = - newValue;
            this.programASFValue = newValue;
        }
        this.programASFValueUpdated.emit(this.programASFValue);
        this.cdRef.detectChanges();
    }

    ngOnChanges(changes: any) {
        //
        //if (this.maxValue < this.programPercentValue) {
        //    changes.setMaxValue.currentValue = this.programPercentValue;
        //}
        this.updateASFValue(this.programASFValue);
    }

    initialRangeComponent() {
        let that = this;
        if (this.Id) {
            var $document = $(document);
            var selector = '#' + this.Id;
            var $element = $(selector);

            // For ie8 support
            var textContent = ('textContent' in document) ? 'textContent' : 'innerText';

            // Basic rangeslider initialization
            $element.rangeslider({

                // Deactivate the feature detection
                polyfill: false,
                rangeClass: 'rangeslider',
                update: true,

                // Callback function
                onInit: function () {
                    that.rangeSliderPlugin = this;
                },

                // Callback function
                onSlide: function (position, value) {
                    that.programPercentValue = value;
                    if (!that.cdRef['destroyed']) {
                        that.cdRef.detectChanges();
                    }
                },

                // Callback function
                onSlideEnd: function (position, value) {
                    that.programPercentValue = value;
                    that.cdRef.detectChanges();
                    that.updatePercentValue(value);
                }
            });
        }
    }
}
