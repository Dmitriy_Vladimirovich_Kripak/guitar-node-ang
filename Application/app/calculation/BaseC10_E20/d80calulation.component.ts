﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, PerformanceExpectation, HVAC, BuildingPlant, LocationFactor, ProjectDeliveryStrategyFactor, D80ViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'd80Calulation',
    templateUrl: './d80calulation.component.html'
})


export class D80CalulationComponent {
    result: any;
    model: D80ViewModel = new D80ViewModel();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    performanceExpectations: any[] = EnumHelperService.getValues(PerformanceExpectation);
    HVACs: any[] = EnumHelperService.getValues(HVAC);
    buildingPlants: any[] = EnumHelperService.getValues(BuildingPlant);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.D80).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcD80(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate D80 formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate D80 formula");
        });
    }

}
