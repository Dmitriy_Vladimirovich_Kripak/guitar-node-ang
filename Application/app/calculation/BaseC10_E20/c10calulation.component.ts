﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, LocationFactor, ProjectDeliveryStrategyFactor, C10ViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'c10Calulation',
    templateUrl: './c10calulation.component.html'
})


export class C10CalulationComponent {
    result: any;
    model: C10ViewModel = new C10ViewModel();
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.C10).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcC10(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate C10 formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate C10 formula");
        });
    }

}
