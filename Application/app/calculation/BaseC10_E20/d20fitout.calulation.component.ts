﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, PerformanceExpectation, Plumbing, LocationFactor, ProjectDeliveryStrategyFactor, D20FitoutViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'd20FitoutCalulation',
    templateUrl: './d20fitout.calulation.component.html'
})


export class D20FitoutCalulationComponent {
    result: any;
    model: D20FitoutViewModel = new D20FitoutViewModel();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    performanceExpectations: any[] = EnumHelperService.getValues(PerformanceExpectation);
    plumbings: any[] = EnumHelperService.getValues(Plumbing);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.D20Fitout).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcD20Fitout(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate D20Fitout formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate D20Fitout formula");
        });
    }

}
