﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, FireProtection, LocationFactor, ProjectDeliveryStrategyFactor, D40ViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'd40Calulation',
    templateUrl: './d40calulation.component.html'
})


export class D40CalulationComponent {
    result: any;
    model: D40ViewModel = new D40ViewModel();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    fireProtections: any[] = EnumHelperService.getValues(FireProtection);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.D40).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcD40(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate D40 formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate D40 formula");
        });
    }

}
