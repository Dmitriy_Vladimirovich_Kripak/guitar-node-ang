﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, LocationFactor, ProjectDeliveryStrategyFactor, E10FitoutViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'e10FitoutCalulation',
    templateUrl: './e10fitout.calulation.component.html'
})


export class E10FitoutCalulationComponent {
    result: any;
    model: E10FitoutViewModel = new E10FitoutViewModel();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.E10Fitout).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcE10Fitout(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate E10Fitout formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate E10Fitout formula");
        });
    }

}
