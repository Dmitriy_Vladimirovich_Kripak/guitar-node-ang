﻿import { Component, Input, Output, ChangeDetectorRef, AfterViewInit, EventEmitter } from '@angular/core';
import { BuildingType, BaseC10_E20ViewModel, C10_E20SettingsViewModel} from '../view.models'
import { EnumHelperService } from '../../common/enum-helper.service'
import { ACalculationService } from '../a-calulation.service'
import { NotificationStatusService } from '../../services/notification-status.service'
import { FormGroup } from '@angular/forms';
import { ProjectViewModel } from '../../calculation/project.model';
import { ProjectModel } from 'manage/project/project.model';

@Component({
    templateUrl: './base-c10-e20-calulation.component.html',
    selector: 'base-c10-e20-calulation',
})


export class BaseC10E20CalulationComponent {
    project: ProjectModel = new ProjectModel(); 
    isResult: boolean;
    resultView: any;
    @Input() model: BaseC10_E20ViewModel;
    viewSettings: C10_E20SettingsViewModel = new C10_E20SettingsViewModel();
    projectViewModel: ProjectViewModel = new ProjectViewModel();

    assignableProgramsStatus: string = '';
    grossUpProgramsStatus: string = '';
    totalAssignableProgramsASF: number = 0;
    totalGrossUpProgramsASF: number = 0;

    stepSize: number = 0.001;
    buildingEfficiencyASF: number;
    minBuildingEfficiency: number = 0.5;
    maxBuildingEfficiency: number = 0.75;
    shellSpaceIndex: number = 0;
    buildingTypes: any[] = EnumHelperService.getValues(BuildingType);
    isInit: boolean = true;
    @Output() resultViewEmit: EventEmitter<any> = new EventEmitter<any>();

    constructor(private service: ACalculationService,
                private enumHelperService: EnumHelperService,
                private _changeDetectionRef: ChangeDetectorRef,
                private notificationStatusService: NotificationStatusService) {
    }

    ngOnInit() {
        this.onBuildingTypeChange(this.model.BuildingType);
    }    

    setRangeValue(property,value){
        this.model[property] = value;
    }

    onBuildingTypeChange(newValue: number) {
        this.model.BuildingType = newValue;
        this.updateBuildingEfficiencyASF();
        this.service.getC10E20ViewSettings(this.model.BuildingType).subscribe((res) => {
            if (res.Status == "Ok")
            {
                this.viewSettings = res.Data;
                this.isInit = false;
                this.prepareAssignablyProgramValues();
                this.updateUIAfterChanges();
                this._changeDetectionRef.detectChanges();
            }
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get View Settings");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get View Settings");
        });
    }

    assignableProgramASFValueUpdated(newValue: number, index: number)
    {
        if (this.isInit) return;
        this.model['AssignableProgram' + index.toString() + 'ASF'] = newValue;
        if (index != this.shellSpaceIndex)
        {
            //  shellSpace program = BuildingEfficiency - all the others programs
            //  one program is shellSpace, so all programs (1 +..+ 8) - shellSpace program = all programs except shellSpace program
            this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] = this.buildingEfficiencyASF -
                (this.model.AssignableProgram1ASF + this.model.AssignableProgram2ASF + this.model.AssignableProgram3ASF + this.model.AssignableProgram4ASF
                + this.model.AssignableProgram5ASF + this.model.AssignableProgram6ASF + this.model.AssignableProgram7ASF + this.model.AssignableProgram8ASF
                - this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF']
                ); 
            if (this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] < 0) {
                this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] = 0;
            }
        }
        for (var i = 1; i <= 8; i++) {
            this.model['AssignableProgram' + i.toString()] = parseFloat((this.model['AssignableProgram' + i.toString() + 'ASF'] / this.model.GrossSquareFeet).toFixed(3));
        }


        this.updateUIAfterChanges();
        this._changeDetectionRef.detectChanges();
    }
    grossUpProgramASFValueUpdated(newValue: number, index: number) {
        if (this.isInit) return;

        this.model['GrossUpProgram' + index.toString() + 'ASF'] = newValue;
        if (index != 4)
        {
            let restBuildingEfficiencyASF = this.model.GrossSquareFeet - this.buildingEfficiencyASF;
            this.model.GrossUpProgram4ASF = restBuildingEfficiencyASF - (this.model.GrossUpProgram1ASF + this.model.GrossUpProgram2ASF + this.model.GrossUpProgram3ASF);
            if (this.model.GrossUpProgram4ASF < 0) {
                this.model.GrossUpProgram4ASF = 0;
            }
        }
        for (var i = 1; i <= 4; i++) {
            this.model['GrossUpProgram' + i.toString()] = parseFloat((this.model['GrossUpProgram' + i.toString() + 'ASF'] / this.model.GrossSquareFeet).toFixed(3));
        }

        this.updateUIAfterChanges();
        this._changeDetectionRef.detectChanges();
    }

    updateBuildingEfficiency()
    {
        this.model.BuildingEfficiency = parseFloat((this.buildingEfficiencyASF / this.model.GrossSquareFeet).toFixed(3));
    }
    updateBuildingEfficiencyASF()
    {
        this.buildingEfficiencyASF = Math.round((this.model.BuildingEfficiency * this.model.GrossSquareFeet));
    }
    updateUIAfterChanges()
    {
        this.updateBuildingEfficiency();
        this.calculateAllPrograms();
        this.updateAssignableStatus();
        this.updateGrossUpStatus();
    }

    calculateAllPrograms()
    {
        this.totalAssignableProgramsASF = 0;
        for (var i = 1; i <= 8; i++) {
            this.totalAssignableProgramsASF = this.totalAssignableProgramsASF + this.model['AssignableProgram' + i.toString() +'ASF'];
        }
        this.totalGrossUpProgramsASF = 0;
        for (var i = 1; i <= 4; i++) {
            this.totalGrossUpProgramsASF = this.totalGrossUpProgramsASF + this.model['GrossUpProgram' + i.toString() + 'ASF'];
        }
    }

    prepareAssignablyProgramValues()
    {
        for (var i = 1; i <= 8; i++) {
            this.model['AssignableProgram' + i.toString() + 'ASF'] = Math.round((this.model['AssignableProgram' + i.toString()] * this.model.GrossSquareFeet));
        }
        for (var i = 1; i <= 4; i++) {
            this.model['GrossUpProgram' + i.toString() + 'ASF'] = Math.round((this.model['GrossUpProgram' + i.toString()] * this.model.GrossSquareFeet));
        }

        for (var i = 1; i <= 8; i++) {
            if (this.isUsingByIndex(i)) {
                //last visible index is index of shellSpace program
                this.shellSpaceIndex = i;
            }
            else {// set invisible programs to 0
                this.model['AssignableProgram' + i.toString()] = 0;
            }

        }
    }
    isUsingByIndex(index: number)
    {
        return this.viewSettings['AssignableProgram' + index.toString() + 'IsUsing'];
    }

    updateAssignableStatus() {
        this.assignableProgramsStatus = '';
        if (this.totalAssignableProgramsASF < (this.minBuildingEfficiency * this.model.GrossSquareFeet) )
            this.assignableProgramsStatus = 'Sum of all assignable programs should be equal or more than ' + ((Math.round(this.minBuildingEfficiency * 10000))/100).toString() + ' %'
        if (this.totalAssignableProgramsASF > this.buildingEfficiencyASF)
            this.assignableProgramsStatus = 'Sum of all assignable programs should be equal or less than ' + ((Math.round(this.model.BuildingEfficiency * 10000)) / 100).toString() + ' %'

    }
    updateGrossUpStatus() {
        this.grossUpProgramsStatus = '';
        if (this.totalGrossUpProgramsASF < ((1 - this.maxBuildingEfficiency) * this.model.GrossSquareFeet))
            this.grossUpProgramsStatus = 'Sum of all Gross Up programs should be equal or more than ' + ((Math.round((1 - this.maxBuildingEfficiency) * 10000)) / 100).toString() + ' %'
        if (this.totalGrossUpProgramsASF > (this.model.GrossSquareFeet - this.buildingEfficiencyASF))
            this.grossUpProgramsStatus = 'Sum of all Gross Up programs should be equal or less than ' + ((Math.round((1 - this.model.BuildingEfficiency) * 10000)) / 100).toString() + ' %'
    }

    changedRangeValue(newValue: any, propertyName: string, form) {
        if (propertyName === "BuildingEfficiency") {
            this.updateUIAfterChanges();
        }
        this.model[propertyName] = newValue;
    }
    changedASFValue(newValue) {
        if (newValue.value) {
            this.buildingEfficiencyASF = newValue.valuASF;
            this.model.BuildingEfficiency = newValue.value;
        }
        else {
            this.buildingEfficiencyASF = newValue;
        }

        this.updateUIAfterChanges();
        this._changeDetectionRef.detectChanges();
    }


    notificationAboutCalculate() {
        this.notificationStatusService.displayOkMessage('For update formula You should click "Calculate" button ', "Warning");
        this.resultViewEmit.emit(this.model);
    }
}
