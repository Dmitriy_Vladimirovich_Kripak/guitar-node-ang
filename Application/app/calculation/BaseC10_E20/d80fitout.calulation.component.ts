﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, PerformanceExpectation, HVAC, BuildingPlant, LocationFactor, ProjectDeliveryStrategyFactor, D80FitoutViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'd80FitoutCalulation',
    templateUrl: './d80fitout.calulation.component.html'
})


export class D80FitoutCalulationComponent {
    result: any;
    model: D80FitoutViewModel = new D80FitoutViewModel();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    performanceExpectations: any[] = EnumHelperService.getValues(PerformanceExpectation);
    HVACs: any[] = EnumHelperService.getValues(HVAC);
    buildingPlants: any[] = EnumHelperService.getValues(BuildingPlant);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.D80Fitout).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcD80Fitout(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate D80Fitout formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate D80Fitout formula");
        });
    }

}
