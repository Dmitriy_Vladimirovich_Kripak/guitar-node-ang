﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ArchitecturalExpectation, PerformanceExpectation, HVAC, BuildingPlant, LocationFactor, ProjectDeliveryStrategyFactor, D30ViewModel } from '../view.models'
import { ACalculationService } from '../a-calulation.service'
import { EnumHelperService } from '../../common/enum-helper.service'
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({
    selector: 'd30Calulation',
    templateUrl: './d30calulation.component.html'
})


export class D30CalulationComponent {
    result: any;
    model: D30ViewModel = new D30ViewModel();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    performanceExpectations: any[] = EnumHelperService.getValues(PerformanceExpectation);
    HVACs: any[] = EnumHelperService.getValues(HVAC);
    buildingPlants: any[] = EnumHelperService.getValues(BuildingPlant);
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.D30).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcD30(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.model.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate D30 formula");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate D30 formula");
        });
    }

}
