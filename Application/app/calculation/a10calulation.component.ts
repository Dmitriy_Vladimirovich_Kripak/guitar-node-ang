﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, SoilCondition, BuildingType, LocationFactor, ProjectDeliveryStrategyFactor, A10ViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector:'a10Calulation',
    templateUrl: './a10calulation.component.html'
})


export class A10CalulationComponent {
    result: any;
    soilConditions: any[] = EnumHelperService.getValues(SoilCondition);
    buildingTypes: any[] = EnumHelperService.getValues(BuildingType);
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    model: A10ViewModel = new A10ViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};

        this.service.getBaselineVariables(CalculationType.A10).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
            });
    }

    calc() {
        this.service.calcA10(this.model).subscribe((response) => {
            if(response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate A10 formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate A10 formula");
        });
    }

}
