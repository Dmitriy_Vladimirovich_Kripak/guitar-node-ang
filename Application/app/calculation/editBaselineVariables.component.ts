﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, BaselineVarialbleViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'editBaselineVariables',
    templateUrl: './editBaselineVariables.component.html'
})


export class EditBaselineVariablesComponent {
    result: any;
    calculationTypes: any[] = EnumHelperService.getValues(CalculationType);

    calculationType: CalculationType = CalculationType.A10;

    constructor(private service: ACalculationService, private notificationStatusService: NotificationStatusService) {
        this.onCalculationTypeChange(this.calculationType);
    }

    update(variable: any) {
        variable.status = "Updating..."
        var model = new BaselineVarialbleViewModel();
        model.Name = variable.Name;
        model.Value = variable.Value;
        this.service.updateVariable(model).subscribe((response) => {
            if (response.Status == "Ok")
            {
                variable.status = response.Data;
                this.notificationStatusService.displayOkMessage("Baseline variable was updated", "Success");
            }    
               
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Update Basevariable");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Update Basevariable");
        });
    }

    onCalculationTypeChange(newValue: number) {
        this.result = {};
        this.calculationType = newValue;
        this.service.getBaselineVariables(this.calculationType).subscribe((res) => {            
            if (res.Status == "Ok")
                this.result = res.Data;     
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");  
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

}
