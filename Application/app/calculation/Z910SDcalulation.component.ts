﻿import { Component, OnInit } from '@angular/core';
import { CalculationType,Z910SDViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'z910SDCalulation',
    templateUrl: './Z910SDcalulation.component.html'
})


export class Z910SDCalulationComponent {
    result: any;   
    model: Z910SDViewModel = new Z910SDViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.Z910SD).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcZ910SD(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate Z910 SD Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate Z910 SD Formula");
        });
    }

}
