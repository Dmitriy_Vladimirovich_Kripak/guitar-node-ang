﻿import { Component, OnInit } from '@angular/core';
import { CalculationType,Z910BCViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'z910BCCalulation',
    templateUrl: './Z910BCcalulation.component.html'
})


export class Z910BCCalulationComponent {
    result: any;   
    model: Z910BCViewModel = new Z910BCViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.Z910BC).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcZ910BC(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate Z910 BC Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate Z910 BC Formula");
        });
    }

}
