﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, BuildingType, ArchitecturalExpectation, PerformanceExpectation, LocationFactor, ProjectDeliveryStrategyFactor, B20ViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'

@Component({
    selector: 'b20Calulation',
    templateUrl: './b20calulation.component.html'
})

export class B20CalulationComponent {
    result: any;
    buildingTypes: any[] = EnumHelperService.getValues(BuildingType);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    performanceExpectations: any[] = EnumHelperService.getValues(PerformanceExpectation);
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    model: B20ViewModel = new B20ViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.B20).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcB20(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate B20 formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate B20 formula");
        });
    }

    numberOfLevelsUpdated(newValue: any) {
        this.model.NumberOfLevels = newValue.numberOfLevels;
        this.model.NumberOfBelowGradeLevels = newValue.numberOfBelowGradeLevels;
        this.model.NumberOfAboveGradeLevels = newValue.numberOfAboveGradeLevels;
        this.model.BuildingFootprint = newValue.buildingFootprint;
        this.model.GrossSquareFeet = newValue.grossSquareFeet;
    }
    claddingRatioUpdated(newValue: any) {
        this.model.CladdingRatio = newValue.claddingRatio;
        this.model.FFHeights = newValue.fFHeights;
        this.model.FloorPlateWidth = newValue.floorPlateWidth;
        this.model.BasementFFHeights = newValue.basementFFHeights;
    }    
}