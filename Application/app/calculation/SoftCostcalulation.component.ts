﻿import { Component, OnInit } from '@angular/core';
import { CalculationType,SoftCostViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'softCostCalulation',
    templateUrl: './SoftCostcalulation.component.html'
})


export class SoftCostCalulationComponent {
    result: any;   
    model: SoftCostViewModel = new SoftCostViewModel();
    status: string = '';

    stepSize: number = 0.01;
    minDesignFee: number = 0.05;
    maxDesignFee: number = 0.15; 
    minFFE: number = 0.03
    maxFFE: number = 0.08;
    minTechnology: number = 0.03;
    maxTechnology: number = 0.07;
    minContingency: number = 0.04;
    maxContingency: number = 0.2;
    minMisc: number = 0;
    maxMisc: number = 0.15;
    

    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.SoftCost).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcSoftCost(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate Soft Cost Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate Soft Cost Formula");
        });
    }

}
