﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ProjectDeliveryStrategyFactor, LocationFactor, SiteInfrastructureServices,G50SUViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'g50SUCalulation',
    templateUrl: './G50SUcalulation.component.html'
})


export class G50SUCalulationComponent {
    result: any;
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);    
    siteInfrastructureServices: any[] = EnumHelperService.getValues(SiteInfrastructureServices);
    model: G50SUViewModel = new G50SUViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.G50SU).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcG50SU(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate G50 SU Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate G50 SU Formula");
        });
    }
    changeValueCheckBox(element: HTMLInputElement) {
        this.model.SiteInfrastructureElectrical = element.checked ? true : false ;
    }

}
