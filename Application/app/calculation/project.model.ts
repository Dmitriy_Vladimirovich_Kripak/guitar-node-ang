﻿import { Dictionary } from '../common/dictionary.type'
import {
    CalculationType, ProjectDeliveryStrategyFactor, LocationFactor, BuildingType, SoilCondition, SiteConstraint, ArchitecturalExpectation,
    PerformanceExpectation, Plumbing, HVAC, BuildingPlant, FireProtection, Electrical, EscalateTo, FacilityRemediation, FacilityDemolition, SitePreparation,
    SiteDevelopment, SiteInfrastructureServices, SiteServices, SiteInfrastructureCivil, SiteCivil
} from './view.models'

interface CustomDateFormate {
    date: Object,
    formatted: string
}
export class ProjectViewModel {

    /* Basic Area settings */
    //A10, A40, B10, B20, B30, C10, D10, Z10BC, Z930BC, Z940BC,Z10SD, Z70SD, Z930SD , Z940SD, Z10SU, Z70SU, Z930SU , Z940SU
    BuildingType: BuildingType = BuildingType.AcademicClassroomBuilding;
    //A10, A20, A40, A90, B10, B20, B30
    NumberOfLevels: number = 5;
    //A20, A90
    NumberOfBelowGradeLevels: number = 1;
    //B20
    NumberOfAboveGradeLevels: number = 4;
    //A20, A40, A90, B10, B20, B30, C10, F10BC, F20BC, F30BC, 
    GrossSquareFeet: number = 100000;
    //G10SD,G20SD, G60SD , G30SU, G40SU,  G50SU
    GrossSiteArea: number = 70000;
    //G10SD,G20SD, G60SD, G30SU, G40SU,  G50SU
    BuildingFootprint: number = 20000;

    /* Main building settings */
    //A10, A90
    SoilCondition: SoilCondition = SoilCondition.RippableRock;

    FloorPlateWidth: number = 100;
    //A20, B20
    CladdingRatio: number = 0.6;
    //A20
    FFHeights: number = 20;
    //A90
    BasementFFHeights: number = 20;
    //A20
    SiteConstraint: SiteConstraint = SiteConstraint.Open;
    //B20
    GlazingRatio: number = 0.3;
    //B10, B30
    HorizontalArticulationFactor: number = 1.2;
    //B20
    VerticalArticulationFactor: number = 1.15;
    //D20, D20Fitout
    Plumbing: Plumbing = Plumbing.Typical;
    //D30, D30Fitout, D80, D80Fitout
    HVAC: HVAC = HVAC.Air;
    //D30, D30Fitout, D80, D80Fitout, G30SU
    BuildingPlant: BuildingPlant = BuildingPlant.Remote;
    //D40, D40Fitout
    FireProtection: FireProtection = FireProtection.Wet;
    //D50, D50Fitout, D60, D60Fitout, D70, D70Fitout
    Electrical: Electrical = Electrical.Typical;
    //A10, A20, A40, A90, B10, B20, B30, C10, C10Fitout, C20, C20Fitout, D10, D20,  D20Fitout, D30, D30Fitout, D40, D40Fitout
    //D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout, E10, E10Fitout,E20, E20Fitout, F10BC, F20BC
    //F30BC,G10SD, G20SD, G60SD , G30SU, G40SU,  G50SU
    LocationFactor: LocationFactor = LocationFactor.LosAngeles;
    //A10, A20, A40, A90, B10, B20, B30, C10, C10Fitout,C20, C20Fitout, D10, D20,  D20Fitout, D30, D30Fitout, D40, D40Fitout
    //D50, D50Fitout, D60, D60Fitout,D70, D70Fitout, D80, D80Fitout, E10, E10Fitout, E20, E20Fitout, F10BC, F20BC
    //F30BC,Z10BC, Z930BC, Z940BC,G10SD, G20SD, G60SD, Z10SD, Z70SD, Z930SD, Z940SD, G30SU, G40SU,  G50SU, Z10SU
    //Z70SU, Z930SU, Z940SU
    ProjectDeliveryStrategyFactor: ProjectDeliveryStrategyFactor = ProjectDeliveryStrategyFactor.CMARRegionalTier1;

    /* Core+Shell and Fitout settings */
    //B10
    FloorConstructionType: string = "ConcreteFlatSlab";
    //B10
    RoofConstructionType: string = "ConcreteFlatSlab";
    //B20, B30, C10, C10Fitout, C20, C20Fitout, D20, D20Fitout, D30, D30Fitout, D40, D40Fitout, D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout
    //E10, E10Fitout, E20,E20Fitout
    ArchitecturalExpectation: ArchitecturalExpectation = ArchitecturalExpectation.CampusStandard;
    //B20, B30, D20, D20Fitout, D30, D30Fitout, D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout
    PerformanceExpectation: PerformanceExpectation = PerformanceExpectation.LEEDGoldEquivalent;
    //B30
    RoofTerraceRatio: number = 0.1;
    //B30
    RoofGlazingRatio: number = 0.05;
    //C10
    BuildingEfficiency: number = 0.62;
    //C10, C10Fitout, D20, D20Fitout, D30Fitout, D40, D40Fitout, D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout 
    //E10, E10Fitout, E20, E20Fitout
    AssignableProgram1IsUsing: boolean = false;
    AssignableProgram2IsUsing: boolean = false;
    AssignableProgram3IsUsing: boolean = false;
    AssignableProgram4IsUsing: boolean = false;
    AssignableProgram5IsUsing: boolean = false;
    AssignableProgram6IsUsing: boolean = false;
    AssignableProgram7IsUsing: boolean = false;
    AssignableProgram8IsUsing: boolean = false;

    AssignableProgram1: number = 0.30;
    AssignableProgram2: number = 0.15;
    AssignableProgram3: number = 0.15;
    AssignableProgram4: number = 0.02;
    AssignableProgram5: number = 0;
    AssignableProgram6: number = 0;
    AssignableProgram7: number = 0;
    AssignableProgram8: number = 0;
    AssignableProgram1ASF: number;
    AssignableProgram2ASF: number;
    AssignableProgram3ASF: number;
    AssignableProgram4ASF: number;
    AssignableProgram5ASF: number;
    AssignableProgram6ASF: number;
    AssignableProgram7ASF: number;
    AssignableProgram8ASF: number;




    //C10, C10Fitout, D20, D20Fitout, D30Fitout, D40, D40Fitout, D50, D50Fitout, D60, D60Fitout, D70, D70Fitout, D80, D80Fitout
    //E10, E10Fitout, E20, E20Fitout
    GrossUpProgram1: number = 0.03;
    GrossUpProgram2: number = 0.13;
    GrossUpProgram3: number = 0.12;
    GrossUpProgram4: number = 0.10;
    GrossUpProgram1ASF: number = 0;
    GrossUpProgram2ASF: number = 0;
    GrossUpProgram3ASF: number = 0;
    GrossUpProgram4ASF: number = 0;



    status: string = "";

    /* Taxes Escalations Fees settings */
    //Z910BC,Z910SD, Z70SU
    ContingenciesDesign: number = 0.12;
    ContingenciesDesignASF: number;
    //Z920BC , Z920SD, Z920SU

    //Contingencies (Escalation) 
    Z920Percent: number = 0.1575;
    //Contingencies (Construction)
    Z930Percent: number = 0.03;


    EscalateTo: EscalateTo = EscalateTo.ConstructionMidpoint;
    //Z920BC , Z920SD , Z920SU
    EstimateDate: Object /*= { date: { year: 2018, day: 1, month: 1, } };*/;
    //Z920BC , Z920SD , Z920SU
    EstimateDateStr: string = '';

    DataDate: Object ;

    StrDataDate: string = '';
    //Z920BC , Z920SD, Z920SU
    ConstructionStartDate: Object;
    //Z920BC , Z920SD, Z920SU
    ConstructionStartDateStr: string ;
    //Z920BC , Z920SD, Z920SU
    DurationInMonths: number = 24;


    /* Building Constraction settings */
    //F10BC
    SpecialConstructionAllowance: number = 100000;
    //F20BC, F30BC
    AreaOfFacilityToBeRemediated: number = 60000;
    //F30BC
    AreaOfFacilityToBeDemolished: number = 60000;
    //F20BC, F30BC
    FacilityRemediation: FacilityRemediation = FacilityRemediation.Simple;

    FacilityDemolition: FacilityDemolition = FacilityDemolition.Simple;

    /* Site Development settings */
    //G10SD
    SitePreparation: SitePreparation = SitePreparation.Greenfield;
    //G20SD
    SiteDevelopment: SiteDevelopment = SiteDevelopment.Moderate;


    /* Site Utilities settings */
    //G30SU, G40SU, G50SU
    SiteInfrastructureServices: SiteInfrastructureServices = SiteInfrastructureServices.Proximate;
    //G30SU
    SiteInfrastructureCivil: SiteInfrastructureCivil = SiteInfrastructureCivil.Proximate;
    //G30SU
    SiteServicesHotWater: boolean = true;
    //G30SU
    SiteServicesSteamCondensate: boolean = true;
    //G30SU
    SiteServicesChilledWaterSupply: boolean = true;
    //G30SU
    SiteCivilWaterFire: boolean = true;
    //G30SU
    SiteCivilSanitarySewer: boolean = true;
    //G30SU
    SiteCivilStormSewer: boolean = true;
    //G30SU
    SiteCivilGas: boolean = true;
    //G40SU, G50SU
    SiteInfrastructureElectrical: boolean = true;

    /* Project Costs settings */
    //SoftCost
    DesignFee: number = 0.1;
    DesignFeeASF: number;
    //SoftCost
    FFE: number = 0.05;
    FfeASF: number;
    //SoftCost
    Technology: number = 0.03;
    TechnologyASF: number;
    //SoftCost
    Contingency: number = 0.07;
    ContingencyASF: number;
    //SoftCost
    Misc: number = 0.1;
    MiscASF: number;
  
}
