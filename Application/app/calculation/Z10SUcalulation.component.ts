﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ProjectDeliveryStrategyFactor, BuildingType ,Z10SUViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'z10SUCalulation',
    templateUrl: './Z10SUcalulation.component.html'
})


export class Z10SUCalulationComponent {
    result: any;   
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    buildingTypes: any[] = EnumHelperService.getValues(BuildingType);
    model: Z10SUViewModel = new Z10SUViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.Z10SU).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcZ10SU(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate Z10 SU Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate Z10 SU Formula");
        });
    }

}
