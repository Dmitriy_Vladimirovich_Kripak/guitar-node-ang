﻿import { Component, OnInit } from '@angular/core';
import { CalculationType,Z910SUViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'z910SUCalulation',
    templateUrl: './Z910SUcalulation.component.html'
})


export class Z910SUCalulationComponent {
    result: any;   
    model: Z910SUViewModel = new Z910SUViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.Z910SU).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcZ910SU(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate Z910 SU Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate Z910 SU Formula");
        });
    }

}
