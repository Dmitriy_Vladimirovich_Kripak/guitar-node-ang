﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, BuildingType, FloorRoofConstractionPossibleValues, LocationFactor, ProjectDeliveryStrategyFactor, B10ViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'

@Component({
    selector: 'b10Calulation',
    templateUrl: './b10calulation.component.html'
})


export class B10CalulationComponent {
    result: any;
    model: B10ViewModel = new B10ViewModel();
    buildingTypes: any[] = EnumHelperService.getValues(BuildingType);
    floorRoofConstractionPossibleValues: FloorRoofConstractionPossibleValues = new FloorRoofConstractionPossibleValues();
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    floorConstructionTypes: any[] = this.floorRoofConstractionPossibleValues.FloorPossibleTypes.Item(BuildingType[this.model.BuildingType]);
    roofConstructionTypes: any[] = this.floorRoofConstractionPossibleValues.RoofPossibleTypes.Item(BuildingType[this.model.BuildingType]);

    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.B10).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });

    }

    onBuildingTypeChange(newValue : number) {
        this.model.BuildingType = newValue;

        this.floorConstructionTypes = this.floorRoofConstractionPossibleValues.FloorPossibleTypes.Item(BuildingType[this.model.BuildingType]);
        this.roofConstructionTypes = this.floorRoofConstractionPossibleValues.RoofPossibleTypes.Item(BuildingType[this.model.BuildingType]);

        this.model.FloorConstructionType = this.floorConstructionTypes[0];
        this.model.RoofConstructionType = this.roofConstructionTypes[0];
    }
    

    calc() {
        this.service.calcB10(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate B10 formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate B10 formula");
        });
    }

}
