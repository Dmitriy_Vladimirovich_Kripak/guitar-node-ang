﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, BuildingType, D10ViewModel, LocationFactor, ProjectDeliveryStrategyFactor} from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'd10Calulation',
    templateUrl: './d10calulation.component.html'
})


export class D10CalulationComponent {
    result: any;
    model: D10ViewModel = new D10ViewModel();
    buildingTypes: any[] = EnumHelperService.getValues(BuildingType);
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.D10).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcD10(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate D10 Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate D10 Formula");
        });
    }

}
