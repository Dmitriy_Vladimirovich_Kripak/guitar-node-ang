﻿import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { CommonAppModule } from '../common/common.module';
import { A10CalulationComponent } from './a10calulation.component'
import { A20CalulationComponent } from './a20calulation.component'
import { A40CalulationComponent } from './a40calulation.component'
import { A60CalulationComponent } from './a60calulation.component'
import { A90CalulationComponent } from './a90calulation.component'
import { B10CalulationComponent } from './b10calulation.component'
import { B20CalulationComponent } from './b20calulation.component'
import { B30CalulationComponent } from './b30calulation.component'
import { BaseC10E20CalulationComponent } from './BaseC10_E20/base-c10-e20-calulation.component'
import { ProgramRangeComponent } from './BaseC10_E20/program-range.component'
import { SoftCostSliderComponent } from '../components/common/soft-cost-slider/soft-cost-slider.component'
import { D10CalulationComponent } from './d10calulation.component'
import { C10CalulationComponent } from './BaseC10_E20/c10calulation.component'
import { C10FitoutCalulationComponent } from './BaseC10_E20/c10fitout.calulation.component'
import { C20CalulationComponent } from './BaseC10_E20/c20calulation.component'
import { C20FitoutCalulationComponent } from './BaseC10_E20/c20fitout.calulation.component'
import { D20CalulationComponent } from './BaseC10_E20/d20calulation.component'
import { D20FitoutCalulationComponent } from './BaseC10_E20/d20fitout.calulation.component'
import { D30CalulationComponent } from './BaseC10_E20/d30calulation.component'
import { D30FitoutCalulationComponent } from './BaseC10_E20/d30fitout.calulation.component'
import { D40CalulationComponent } from './BaseC10_E20/d40calulation.component'
import { D40FitoutCalulationComponent } from './BaseC10_E20/d40fitout.calulation.component'
import { D50CalulationComponent } from './BaseC10_E20/d50calulation.component'
import { D50FitoutCalulationComponent } from './BaseC10_E20/d50fitout.calulation.component'
import { D60CalulationComponent } from './BaseC10_E20/d60calulation.component'
import { D60FitoutCalulationComponent } from './BaseC10_E20/d60fitout.calulation.component'
import { D70CalulationComponent } from './BaseC10_E20/d70calulation.component'
import { D70FitoutCalulationComponent } from './BaseC10_E20/d70fitout.calulation.component'
import { D80CalulationComponent } from './BaseC10_E20/d80calulation.component'
import { D80FitoutCalulationComponent } from './BaseC10_E20/d80fitout.calulation.component'
import { E10CalulationComponent } from './BaseC10_E20/e10calulation.component'
import { E10FitoutCalulationComponent } from './BaseC10_E20/e10fitout.calulation.component'
import { E20CalulationComponent } from './BaseC10_E20/e20calulation.component'
import { E20FitoutCalulationComponent } from './BaseC10_E20/e20fitout.calulation.component'


import { F10BCCalulationComponent } from './f10BCcalulation.component'
import { F20BCCalulationComponent } from './f20BCcalulation.component'
import { F30BCCalulationComponent } from './f30BCcalulation.component'
import { Z10BCCalulationComponent } from './z10BCcalulation.component'
import { Z70BCCalulationComponent } from './z70BCcalulation.component'
import { Z910BCCalulationComponent } from './z910BCcalulation.component'
import { Z920BCCalulationComponent } from './z920BCcalulation.component'
import { Z930BCCalulationComponent } from './z930BCcalulation.component'
import { Z940BCCalulationComponent } from './z940BCcalulation.component'

import { G10SDCalulationComponent } from './g10SDcalulation.component'
import { G20SDCalulationComponent } from './g20SDcalulation.component'
import { G60SDCalulationComponent } from './g60SDcalulation.component'
import { Z10SDCalulationComponent } from './z10SDcalulation.component'
import { Z70SDCalulationComponent } from './z70SDcalulation.component'
import { Z910SDCalulationComponent } from './z910SDcalulation.component'
import { Z920SDCalulationComponent } from './z920SDcalulation.component'
import { Z930SDCalulationComponent } from './z930SDcalulation.component'
import { Z940SDCalulationComponent } from './z940SDcalulation.component'
import { G30SUCalulationComponent } from './g30SUcalulation.component'
import { G40SUCalulationComponent } from './g40SUcalulation.component'
import { G50SUCalulationComponent } from './g50SUcalulation.component'
import { Z10SUCalulationComponent } from './z10SUcalulation.component'
import { Z70SUCalulationComponent } from './z70SUcalulation.component'
import { Z910SUCalulationComponent } from './z910SUcalulation.component'
import { Z920SUCalulationComponent } from './z920SUcalulation.component'
import { Z930SUCalulationComponent } from './z930SUcalulation.component'
import { Z940SUCalulationComponent } from './z940SUcalulation.component'

import { SoftCostCalulationComponent } from './softCostcalulation.component';
import { TotalProjectCostCalulationComponent } from './totalProjectCostcalulation.component';
import { ComplexPageComponent } from './complexPage.component';

import { EditBaselineVariablesComponent } from './editBaselineVariables.component';
import { ACalculationService } from './a-calulation.service';
import { UserManageService } from '../services/user-manage.service';
import { ProjectService } from '../services/project.service ';
import { NotificationStatusService } from '../services/notification-status.service';
import { PaginationHelperService } from '../services/pagination-helper.service';
import { AuditTrailService } from '../services/audit-trail.service';
import { BsModalService } from 'ngx-bootstrap/modal';

import { BuildingLevelsComponent } from './common/building-levels.component';
import { CladdingRatioComponent } from './common/cladding-ratio.component';
import { AllUserComponent } from '../manage/users/all-user.component';
import { UserComponent } from '../manage/users/user.component';
import { CalculationComponent } from '../components/common/calculation.component';
import { ProjectComponent } from '../manage/project/project.component';
import { AllProjectsComponent } from '../manage/project/all-projects.component';
import { ViewProjectComponent } from '../manage/project/view-project.component';   
import { CustomDialogComponent } from 'components/common/dialog.component';
import { AllErrorComponent } from '../components/common/all-errors.component';
import { TreeGridComponent } from '../components/common/tree-error-grid.component';
import { AuditTrailComponent } from '../components/audit/audit-trail.component';
import { ClickableAuditTrailComponent } from "../components/audit/clickable.audit-trail.component";
import { AuditTrailViewComponent } from "../components/audit/audit-trail-view.component";

import { DatepickerModule } from 'angular2-material-datepicker';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyDatePickerModule } from 'mydatepicker';
import { DataTableModule } from "angular2-datatable";
import { DataFilterPipe } from '../services/data-filter.pipe';
import { NgxPaginationModule } from 'ngx-pagination';  
import { AgGridModule } from "ag-grid-angular/main";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RouterModule } from '@angular/router';
import { StorageService } from 'services/storage-service';
import { RangeComponent } from 'components/common/range.component';
import { InformationModule } from 'modules/information.module';

@NgModule({
    imports: [
        CommonModule, HttpModule, FormsModule, CommonAppModule, DatepickerModule, SimpleNotificationsModule.forRoot(), BrowserAnimationsModule, MyDatePickerModule, DataTableModule, NgxPaginationModule,
        AgGridModule.withComponents(
            [ClickableAuditTrailComponent]
        ), ModalModule.forRoot()
    ],
    declarations: [
        EditBaselineVariablesComponent
        , A10CalulationComponent, A20CalulationComponent, A40CalulationComponent, A60CalulationComponent, A90CalulationComponent
        , B10CalulationComponent, B20CalulationComponent, B30CalulationComponent, D10CalulationComponent
        , BaseC10E20CalulationComponent, ProgramRangeComponent, SoftCostSliderComponent

        , C10CalulationComponent, C20CalulationComponent, C10FitoutCalulationComponent, C20FitoutCalulationComponent
        , D20CalulationComponent, D20FitoutCalulationComponent, D30CalulationComponent, D30FitoutCalulationComponent
        , D40CalulationComponent, D40FitoutCalulationComponent, D50CalulationComponent, D50FitoutCalulationComponent
        , D60CalulationComponent, D60FitoutCalulationComponent, D70CalulationComponent, D70FitoutCalulationComponent
        , D80CalulationComponent, D80FitoutCalulationComponent, E10CalulationComponent, E10FitoutCalulationComponent
        , E20CalulationComponent, E20FitoutCalulationComponent

        , F10BCCalulationComponent, F20BCCalulationComponent, F30BCCalulationComponent, Z10BCCalulationComponent, Z70BCCalulationComponent
        , Z910BCCalulationComponent, Z920BCCalulationComponent, Z930BCCalulationComponent, Z940BCCalulationComponent

        , G10SDCalulationComponent, G20SDCalulationComponent, G60SDCalulationComponent, Z10SDCalulationComponent, Z70SDCalulationComponent
        , Z910SDCalulationComponent, Z920SDCalulationComponent, Z930SDCalulationComponent, Z940SDCalulationComponent

        , G30SUCalulationComponent , G40SUCalulationComponent, G50SUCalulationComponent, Z10SUCalulationComponent, Z70SUCalulationComponent, Z910SUCalulationComponent, Z920SUCalulationComponent
        , Z930SUCalulationComponent, Z940SUCalulationComponent

        , SoftCostCalulationComponent, TotalProjectCostCalulationComponent, ComplexPageComponent,

        DataFilterPipe, CustomDialogComponent,
         
        BuildingLevelsComponent, CladdingRatioComponent, AllUserComponent, UserComponent, CalculationComponent, ProjectComponent, AllProjectsComponent, ViewProjectComponent, AllErrorComponent,
        TreeGridComponent, AuditTrailComponent, AuditTrailViewComponent

    ],
    providers: [
        ACalculationService, UserManageService, ProjectService, NotificationStatusService, AuditTrailService, PaginationHelperService, StorageService
    ],
    exports:[
        BuildingLevelsComponent, CladdingRatioComponent, ProgramRangeComponent, SoftCostSliderComponent
    ],
    entryComponents: [CustomDialogComponent],
   
    
})
export class CalculationModule { }