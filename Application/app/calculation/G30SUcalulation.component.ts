﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ProjectDeliveryStrategyFactor, LocationFactor, SiteInfrastructureServices, BuildingPlant, SiteInfrastructureCivil, G30SUViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'g30SUCalulation',
    templateUrl: './G30SUcalulation.component.html'
})


export class G30SUCalulationComponent {
    result: any;
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);    
    siteInfrastructureServices: any[] = EnumHelperService.getValues(SiteInfrastructureServices);
    buildingPlant: any[] = EnumHelperService.getValues(BuildingPlant);
    siteInfrastructureCivil: any[] = EnumHelperService.getValues(SiteInfrastructureCivil);
    model: G30SUViewModel = new G30SUViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.G30SU).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {        
        this.service.calcG30SU(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate G30 SU Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate G30 SU Formula");
        });
    }
   

}
