﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ProjectDeliveryStrategyFactor, LocationFactor,G60SDViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service';
@Component({
    selector: 'g60SDCalulation',
    templateUrl: './G60SDcalulation.component.html'
})


export class G60SDCalulationComponent {
    result: any;
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);      
    model: G60SDViewModel = new G60SDViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.G60SD).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcG60SD(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate G60 SD Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate G60 SD Formula");
        });
    }

}
