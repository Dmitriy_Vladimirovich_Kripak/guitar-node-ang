﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {
    CalculationType, BuildingType, BaselineVarialbleViewModel, C10_E20SettingsViewModel
    , A10ViewModel, A20ViewModel, A40ViewModel, A60ViewModel, A90ViewModel
    , B10ViewModel, B20ViewModel, B30ViewModel, D10ViewModel
    , C10ViewModel, C20ViewModel, C10FitoutViewModel, C20FitoutViewModel

    , D20ViewModel, D20FitoutViewModel, D30ViewModel, D30FitoutViewModel
    , D40ViewModel, D40FitoutViewModel, D50ViewModel, D50FitoutViewModel
    , D60ViewModel, D60FitoutViewModel, D70ViewModel, D70FitoutViewModel
    , D80ViewModel, D80FitoutViewModel, E10ViewModel, E10FitoutViewModel
    , E20ViewModel, E20FitoutViewModel

    , F10BCViewModel, F20BCViewModel, F30BCViewModel, Z10BCViewModel, Z70BCViewModel, Z910BCViewModel, Z920BCViewModel
    , Z930BCViewModel, Z940BCViewModel

    , G10SDViewModel, G20SDViewModel, G60SDViewModel, Z10SDViewModel, Z70SDViewModel, Z910SDViewModel, Z920SDViewModel
    , Z930SDViewModel, Z940SDViewModel

    , G30SUViewModel, G40SUViewModel, G50SUViewModel, Z10SUViewModel, Z70SUViewModel, Z910SUViewModel, Z920SUViewModel, Z930SUViewModel
    , Z940SUViewModel

    , SoftCostViewModel, TotalProjectCostViewModel,
} from './view.models'


import { ProjectViewModel } from './project.model'

let headers = new Headers({ 'Content-Type': 'application/json' });
let options = new RequestOptions({ headers: headers });

@Injectable()
export class ACalculationService {
    constructor(private http: Http) { }

    getBaselineVariables(calculationType: CalculationType): Observable<any> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('CalculationType', CalculationType[calculationType]);
        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        return this.http.get('/api/Calculation/getBaselineVariables', requestOptions)
            .map(this.extractData).catch(this.handleError);
    };

    updateVariable(model: BaselineVarialbleViewModel): Observable<any> {
        return this.http.post('/api/Calculation/updateVariable', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    getC10E20ViewSettings(buildingType: BuildingType): Observable<any> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('BuildingType', BuildingType[buildingType]);
        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        return this.http.get('/api/Calculation/getC10E20ViewSettings', requestOptions)
            .map(this.extractData).catch(this.handleError);
    };

    calcA10(model: A10ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcA10', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcA20(model: A20ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcA20', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcA40(model: A40ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcA40', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcA60(model: A60ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcA60', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcA90(model: A90ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcA90', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcB10(model: B10ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcB10', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcB20(model: B20ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcB20', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcB30(model: B30ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcB30', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    
    calcD10(model: D10ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD10', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcC10(model: C10ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcC10', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcC10Fitout(model: C10FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcC10Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcC20(model: C20ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcC20', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcC20Fitout(model: C20FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcC20Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD20(model: D20ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD20', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD20Fitout(model: D20FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD20Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD30(model: D30ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD30', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD30Fitout(model: D30FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD30Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD40(model: D40ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD40', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD40Fitout(model: D40FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD40Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD50(model: D50ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD50', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD50Fitout(model: D50FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD50Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD60(model: D60ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD60', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD60Fitout(model: D60FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD60Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD70(model: D70ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD70', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD70Fitout(model: D70FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD70Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD80(model: D80ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD80', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcD80Fitout(model: D80FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcD80Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcE10(model: E10ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcE10', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcE10Fitout(model: E10FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcE10Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcE20(model: E20ViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcE20', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcE20Fitout(model: E20FitoutViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcE20Fitout', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

   

    calcF10BC(model: F10BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcF10BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcF20BC(model: F20BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcF20BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcF30BC(model: F30BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcF30BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcZ10BC(model: Z10BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ10BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcZ70BC(model: Z70BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ70BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcZ910BC(model: Z910BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ910BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcZ920BC(model: Z920BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ920BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }

    calcZ930BC(model: Z930BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ930BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ940BC(model: Z940BCViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ940BC', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcG10SD(model: G10SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcG10SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcG20SD(model: G20SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcG20SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcG60SD(model: G60SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcG60SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ10SD(model: Z10SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ10SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ70SD(model: Z70SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ70SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ910SD(model: Z910SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ910SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ920SD(model: Z920SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ920SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ930SD(model: Z930SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ930SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ940SD(model: Z940SDViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ940SD', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcG30SU(model: G30SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcG30SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcG40SU(model: G40SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcG40SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcG50SU(model: G50SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcG50SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ10SU(model: Z10SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ10SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ70SU(model: Z70SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ70SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ910SU(model: Z910SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ910SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ920SU(model: Z920SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ920SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ930SU(model: Z930SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ930SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcZ940SU(model: Z940SUViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcZ940SU', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcSoftCost(model: SoftCostViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcSoftCost', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcTotalProjectCost(model: TotalProjectCostViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcTotalProjectCost', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    calcProject(model: ProjectViewModel): Observable<any> {
        return this.http.post('/api/Calculation/calcProject', model, options)
            .map(this.extractDataPost).catch(this.handleError);
    }
    
    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }
    private extractDataPost(res: Response) {
        let body = res.json();
        return body || {};  
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error.status == 0 && error.ok)
            return Observable.throw("Could not reach the server");
        else if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }      
        return Observable.throw(errMsg);
    }
}