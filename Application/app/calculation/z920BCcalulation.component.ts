﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, EscalateTo, Z920BCViewModel } from './view.models';
import { ACalculationService } from './a-calulation.service';
import { EnumHelperService } from '../common/enum-helper.service';
//import '../common/date-extention';
import { NotificationStatusService } from '../services/notification-status.service';
import { IMyDpOptions, IMyDateModel, IMyInputFieldChanged, IMyCalendarViewChanged, IMyMarkedDate, IMyDate, IMyDefaultMonth } from 'mydatepicker';

@Component({
    selector: 'z920BCCalulation',
    templateUrl: './z920BCcalulation.component.html'
})

export class Z920BCCalulationComponent {
    result: any;
    escalateToValues: any[] = EnumHelperService.getValues(EscalateTo);
    model: Z920BCViewModel = new Z920BCViewModel();
    status: string = '';
    yourModelDate: Date = new Date();   

    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.Z920BC).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {       
            //this.model.ConstructionStartDateStr = Date.prototype.ToServerDateStr(this.model.ConstructionStartDate);
            //this.model.EstimateDateStr = Date.prototype.ToServerDateStr(this.model.EstimateDate); 
            this.service.calcZ920BC(this.model).subscribe((response) => {
                if (response.Status == "Ok")
                    this.status = response.Data;
                else
                    this.notificationStatusService.displayErrorMessage(response.Error, "Calculate Z920 BC Formula");   
            }, (error) => {
                this.notificationStatusService.displayErrorMessage(error, "Calculate Z920 BC Formula");
            });      
    }

    /*-------------------------------DatePicker-------------------------*/
    private myDatePickerOptions: IMyDpOptions = {
        // other options...
        dateFormat: 'mm.dd.yyyy',
    };

    onEstimateDateChanged(event: IMyDateModel): void {
        this.model.EstimateDateStr = event.formatted.replace(/\./g, "/");
    }
    onConstructionStartDateChanged(event: IMyDateModel): void {
        this.model.ConstructionStartDateStr = event.formatted.replace(/\./g, "/");
    } 
    /*-------------------------------END DatePicker-------------------------*/

}