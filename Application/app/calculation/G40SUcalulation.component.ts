﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, ProjectDeliveryStrategyFactor, LocationFactor, SiteInfrastructureServices,G40SUViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'g40SUCalulation',
    templateUrl: './G40SUcalulation.component.html'
})


export class G40SUCalulationComponent {
    result: any;
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);    
    siteInfrastructureServices: any[] = EnumHelperService.getValues(SiteInfrastructureServices);
    model: G40SUViewModel = new G40SUViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.G40SU).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcG40SU(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate G40 SU Formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate G40 SU Formula");
        });
    }
    changeValueCheckBox(element: HTMLInputElement) {
        this.model.SiteInfrastructureElectrical = element.checked ? true : false;
    }

}
