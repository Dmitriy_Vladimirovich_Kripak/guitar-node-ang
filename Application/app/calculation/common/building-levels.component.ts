﻿import { Component, EventEmitter, Input, Output, ViewChild, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';

interface NumberLevels {
    numberOfLevels: number,
    numberOfBelowGradeLevels: number,
    numberOfAboveGradeLevels: number,
    buildingFootprint: number,
    grossSquareFeet: number,
    isValid: boolean,
    errMsg: string
}

@Component({
    templateUrl: './building-levels.component.html',
    selector: 'building-levels'
})


export class BuildingLevelsComponent {
    @Input() numberOfLevels?: number;
    @Input() numberOfBelowGradeLevels?: number;
    @Input() numberOfAboveGradeLevels?: number;
    @Input() buildingFootprint?: number;
    @Input() grossSquareFeet?: number;

    @Output() numberOfLevelsUpdated: EventEmitter<any> = new EventEmitter<any>();
    private out: NumberLevels;

    numberOfLevelsChanged() {
        this.numberOfAboveGradeLevels = this.numberOfLevels - this.numberOfBelowGradeLevels;
        this.sendNotificationToParentModel();
    }

    numberOfBelowGradeLevelsChanged(levelsForm: FormGroup) {
        if (this.isFormValid(levelsForm) !== "") {
            this.sendNotificationToParentModel(false, this.isFormValid(levelsForm));
        }
        else {
            if (this.numberOfLevels > 0) {
                this.numberOfAboveGradeLevels = this.numberOfLevels - this.numberOfBelowGradeLevels;
            }
            else if (this.numberOfAboveGradeLevels > 0) {
                this.numberOfLevels = this.numberOfBelowGradeLevels + this.numberOfAboveGradeLevels;
            }
            this.sendNotificationToParentModel();
        }
    }

    numberOfAboveGradeLevelsChanged(levelsForm: FormGroup) {
        if (this.isFormValid(levelsForm) !== "") {
            this.sendNotificationToParentModel(false, this.isFormValid(levelsForm));
        }
        else {
            if (this.numberOfBelowGradeLevels > 0) {
                if (this.numberOfLevels > 0) {
                    this.numberOfBelowGradeLevels = this.numberOfLevels - this.numberOfAboveGradeLevels;
                }
            }
            else {
                this.numberOfLevels = this.numberOfAboveGradeLevels;
            }
            this.sendNotificationToParentModel();
        }
    }
    buildingFootprintChange(levelsForm: FormGroup) {
        if (this.isFormValid(levelsForm) !== "") {
            this.sendNotificationToParentModel(false, this.isFormValid(levelsForm));
        }
        else {
            if (this.grossSquareFeet > 0 && this.numberOfLevels > 0) {
                this.buildingFootprint = this.grossSquareFeet / this.numberOfLevels;
            }
            this.numberOfLevelsChanged();
            this.sendNotificationToParentModel();
        }
    }

    sendNotificationToParentModel(_isValid: boolean = true, _errMsg: string = "") {
        this.out = {
            numberOfLevels: this.numberOfLevels,
            numberOfBelowGradeLevels: this.numberOfBelowGradeLevels,
            numberOfAboveGradeLevels: this.numberOfAboveGradeLevels,
            buildingFootprint: this.buildingFootprint,
            grossSquareFeet: this.grossSquareFeet,
            isValid: _isValid,
            errMsg: _errMsg
        }
        this.numberOfLevelsUpdated.emit(this.out);
    }

    parseDouble(event: any) {
        if (event) {
            event.currentTarget.value[0] === '.' ?
                event.currentTarget.value = "0" + event.currentTarget.value :
                "";
        }
    }

    isFormValid(levelsForm: FormGroup): string {
        if (levelsForm && !levelsForm.valid) {
            for (var control in levelsForm.controls) {
                if (levelsForm.controls[control].invalid)
                    return "Incorrect value of " + control;
            }
        }
        return "";
    }
}
