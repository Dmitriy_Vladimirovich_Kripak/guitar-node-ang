﻿import { Component, EventEmitter, Input, Output } from '@angular/core';

interface CladdingRatio {
    floorPlateWidth: number,
    fFHeights: number,
    basementFFHeights: number,
    claddingRatio: number
}

@Component({
    templateUrl: './cladding-ratio.component.html',
    selector: 'cladding-ratio'
})


export class CladdingRatioComponent { 
    @Input() gFS?: number;
    @Input() numberOfLevels?: number; 
    @Input() numberOfBelowGradeLevels?: number;
    @Input() numberOfAboveGradeLevels?: number;
    @Input() floorPlateWidth?: number;
    @Input() fFHeights?: number;
    @Input() basementFFHeights?: number;  
    @Input() claddingRatio?: number;

    @Output() claddingRatioUpdated: EventEmitter<any> = new EventEmitter<any>();
    private out: CladdingRatio;
    minFloorPlate: number = 60;
    maxFloorPlate: number = 100;
    isCladdingEntry: boolean = true;

    floorPlateChange() {      
        this.changeClddingRatio();
    }
    fFHeightsChange() {
        this.changeClddingRatio();
    }
    basementFFHeightsChange() {
        this.changeClddingRatio();
    }
    claddingRatioChange() {    
        this.sendNotificationToParentModel();
    }
    changeClddingRatio() {
        if (this.floorPlateWidth > 0 && this.fFHeights > 0 && this.gFS > 0 && this.numberOfLevels > 0 && this.numberOfBelowGradeLevels > 0 && this.numberOfAboveGradeLevels && this.basementFFHeights) {
            this.claddingRatio = ((((this.gFS / this.numberOfLevels) / this.floorPlateWidth + this.floorPlateWidth) * 2 * this.basementFFHeights * this.numberOfBelowGradeLevels * 1.15) +
                ((this.gFS / this.numberOfLevels) / this.floorPlateWidth + this.floorPlateWidth) * 2 * this.numberOfAboveGradeLevels * this.fFHeights * 1.15) / this.gFS;
            this.claddingRatio = parseFloat(this.claddingRatio.toFixed(3));
        }       
        this.sendNotificationToParentModel();
    }
    sendNotificationToParentModel() {
        this.out = {
            floorPlateWidth: this.floorPlateWidth,
            fFHeights: this.fFHeights,
            basementFFHeights: this.basementFFHeights,
            claddingRatio: this.claddingRatio
        }
        this.claddingRatioUpdated.emit(this.out);

    }

    parseDouble(event: any) {
        if (event) {
            event.currentTarget.value[0] === '.' ?
                event.currentTarget.value = "0" + event.currentTarget.value :
                "";
        }
    }
   
}
