﻿import { Component, OnInit } from '@angular/core';
import { CalculationType, BuildingType, ArchitecturalExpectation, PerformanceExpectation, LocationFactor, ProjectDeliveryStrategyFactor, B30ViewModel } from './view.models'
import { ACalculationService } from './a-calulation.service'
import { EnumHelperService } from '../common/enum-helper.service'
import { NotificationStatusService } from '../services/notification-status.service'
@Component({
    selector: 'b30Calulation',
    templateUrl: './b30calulation.component.html'
})


export class B30CalulationComponent {
    result: any;
    buildingTypes: any[] = EnumHelperService.getValues(BuildingType);
    architecturalExpectations: any[] = EnumHelperService.getValues(ArchitecturalExpectation);
    performanceExpectations: any[] = EnumHelperService.getValues(PerformanceExpectation);
    locationFactors: any[] = EnumHelperService.getValues(LocationFactor);
    projectDeliveryStrategyFactors: any[] = EnumHelperService.getValues(ProjectDeliveryStrategyFactor);
    model: B30ViewModel = new B30ViewModel();
    status: string = '';
    constructor(private service: ACalculationService, private enumHelperService: EnumHelperService, private notificationStatusService: NotificationStatusService) {
        this.result = {};
        this.service.getBaselineVariables(CalculationType.B30).subscribe((res) => {
            if (res.Status == "Ok")
                this.result = res.Data;
            else
                this.notificationStatusService.displayErrorMessage(res.Error, "Get Basevariable");
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Get Basevariable");
        });
    }

    calc() {
        this.service.calcB30(this.model).subscribe((response) => {
            if (response.Status == "Ok")
                this.status = response.Data;
            else
                this.notificationStatusService.displayErrorMessage(response.Error, "Calculate B30 formula");   
        }, (error) => {
            this.notificationStatusService.displayErrorMessage(error, "Calculate B30 formula");
        });
    }
    numberOfLevelsUpdated(newValue: any) {
        this.model.NumberOfLevels = newValue.numberOfLevels;
        this.model.NumberOfBelowGradeLevels = newValue.numberOfBelowGradeLevels;
        this.model.NumberOfAboveGradeLevels = newValue.numberOfAboveGradeLevels;
        this.model.BuildingFootprint = newValue.buildingFootprint;
        this.model.GrossSquareFeet = newValue.grossSquareFeet;
    }
    

}
