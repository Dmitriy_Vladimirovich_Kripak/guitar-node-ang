import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampusHomeComponent } from 'components/campus/campus-home/campus-home.component';
import { CampusInformationComponent } from 'components/campus/campus-information/campus-information.component';
import { CampusProfileComponent } from 'components/campus/campus-profile/campus-profile.component';
import { CampusStandardsComponent } from 'components/campus/campus-standards/campus-standards.component';
import { ProjectComplexComponent } from '../components/project-complex/project-complex.component';
import { GuageComponent } from 'manage/project/gauge-component/gauge-component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CalculationModule } from 'calculation/calculation.module';
import { RouterModule } from '@angular/router';
import { LoginService } from '../services/login-service';
import { CampusInfoService } from 'services/campus-info.service';
import { FileComponent } from 'components/file/file.component';
import { StorageService } from 'services/storage-service';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FileService } from 'services/file-service';
import { ListFileInfoComponent } from '../components/file/file-list.component';
import { FAQComponent } from 'components/support/faq.component';
import { DialogModalService } from 'services/dialog-modal.service';
import { FileSaveDialogComponent } from 'components/common/file-save-dialog.component';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { SettingComponent } from 'components/setting/setting.component';
import { RangeComponent } from 'components/common/range.component';
import { CommonAppModule } from 'common/common.module';
import { MyDatePickerModule } from 'mydatepicker';

@NgModule({
  imports: [
    CommonModule,HttpModule, FormsModule, SimpleNotificationsModule, ModalModule.forRoot(), BootstrapModalModule.forRoot({ container: document.body }),
    CalculationModule, PdfViewerModule, CommonAppModule, MyDatePickerModule
  ],
    declarations: [CampusHomeComponent, CampusInformationComponent, CampusProfileComponent, CampusStandardsComponent, ProjectComplexComponent,
                 GuageComponent, FileComponent, ListFileInfoComponent, FAQComponent,FileSaveDialogComponent, SettingComponent],
  providers: [
      LoginService,CampusInfoService, StorageService, FileService, DialogModalService
  ],
  entryComponents: [FileSaveDialogComponent]

})
export class InformationModule { }
