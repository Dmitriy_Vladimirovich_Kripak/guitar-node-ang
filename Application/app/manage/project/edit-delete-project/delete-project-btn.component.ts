﻿import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { Router } from '@angular/router';

import { ProjectService } from '../../../services/project.service ';
import { NotificationStatusService } from '../../../services/notification-status.service';

@Component({
    selector: 'delete-project',
    template: `   
              <a style="cursor: pointer">
                <img src="/Images/delete.png">
              </a>
              `
})

export class DeleteProjectBtnComponent implements ICellRendererAngularComp {
    private params: any;
    public cell: any;
    refresh: any;

    @Output() onClicked = new EventEmitter<boolean>();

    constructor(private parentRouter: Router, private projectService: ProjectService,
        private notificationService: NotificationStatusService) { }

    agInit(params: any): void {
        this.params = params;
        this.cell = { row: params.value, col: params.colDef.headerName };
    }
}

