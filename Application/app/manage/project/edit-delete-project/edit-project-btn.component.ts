﻿import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { Router } from '@angular/router';

import { ProjectService } from '../../../services/project.service ';

@Component({
    selector: 'edit-project',
    template: `   
              <a (click)="click()" style="cursor: pointer">
                <img src="/Images/edit.png">
              </a>
              `
})

export class EditProjectBtnComponent implements ICellRendererAngularComp {
    private params: any;
    public cell: any;
    refresh: any;

    @Output() onClicked = new EventEmitter<boolean>();

    constructor(private parentRouter: Router, private projectService: ProjectService) {
    }

    agInit(params: any): void {
        this.params = params;
        this.cell = { row: params.value, col: params.colDef.headerName };
    }

    click(): void {
        this.onClicked.emit(this.cell);
        this.projectService.data = this.cell.row;
        this.parentRouter.navigate(['project-edit/' + this.cell.row]);
    }
}

