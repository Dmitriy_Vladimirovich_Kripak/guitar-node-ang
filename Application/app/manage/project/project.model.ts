﻿import { Dictionary } from 'common/dictionary.type'

export class ProjectModel {
    public Name: string;
    public Id: number;
    public Date: string;
    public UserId: string;
    public UserName: string;
    public Institution: string;
    public CostModel: string;
    public CampusPrecint:string;
}
