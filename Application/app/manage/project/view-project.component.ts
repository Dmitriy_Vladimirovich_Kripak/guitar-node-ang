import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectService } from 'services/project.service '
import { EditeProjectModel } from './edite-project.model'
import { ProjectComplexValueModel } from './projectComplexValue.model'
import { ProjectViewModel } from 'calculation/project.model';
import { NotificationStatusService } from 'services/notification-status.service'
import { ProjectModel } from './project.model'; 
import { CustomDialogComponent } from 'components/common/dialog.component';
import { FileSaver, saveAs } from 'file-saver';

import 'rxjs/Rx';
import { Subscription } from 'rxjs/Rx';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
    selector: 'view-project',
    templateUrl: './view-project.component.html',

})

export class ViewProjectComponent {

    bsModalRef: BsModalRef;
    private subscription: Subscription;

    @Input() userId: any;
    @Input() projectId: any;
    @Input() projectName: string;
    institution: string;
    costModel: string;
    isProjectHidden: boolean;
    project: ProjectModel;
    projectValue: ProjectViewModel;
    projectComplexValue: ProjectComplexValueModel;
    model: EditeProjectModel = new EditeProjectModel();

    public constructor(private projectService: ProjectService, private parentRouter: Router, private notificatopnService: NotificationStatusService, private bsModalService: BsModalService,
                        private modalService: BsModalService) {
        this.model.isUserProject = false;
        this.model.projectValue = null;
        this.projectComplexValue = new ProjectComplexValueModel();
        this.project = new ProjectModel();
        this.projectValue = new ProjectViewModel();
    }

    ngOnInit(): void {
        this.isProjectHidden = true;
    }

    ngOnChanges() {
        if (this.projectId)
            this.projectService.getProjectById(this.projectId, this.userId).subscribe(result => {
                let data = result.Data;
                this.model.isUserProject = data.IsUserProject;
                this.model.projectValue = data.ProjectValue;
                this.projectValue = data.ProjectValue;
                this.project = data.Project;
                this.isProjectHidden = false;

            }, (error: any) => {
                this.notificatopnService.displayErrorMessage(error, "Get Project Details");
            });
    }

    public openModalWithComponent() {

        this.bsModalRef = this.modalService.show(CustomDialogComponent);
        this.bsModalRef.content.title = 'Save project as';

        this.subscription = this.modalService.onHide.subscribe(results => {

            if (this.bsModalRef.content.isSave && this.bsModalRef.content.projectName) {
                this.projectComplexValue.project = new ProjectModel();
                this.projectComplexValue.project.Name = this.bsModalRef.content.projectName;
                this.projectComplexValue.projectValue = this.projectValue;
                this.projectComplexValue.project.Institution = this.project.Institution;
                this.projectService.createProject(this.projectComplexValue).subscribe((response: any) => {
                    if (response.Status == "Ok")
                        this.notificatopnService.displayOkMessage("Project was added", "Success");
                    else
                        this.notificatopnService.displayErrorMessage(response.Error, "Save as Project");
                });
            }

            this.subscription.unsubscribe();
        }
        );  
    }

    resultViewEmit(userInputs: any) {
        this.projectValue = userInputs;
    }

    updateProject() {
        this.project.Id = this.projectId;
        this.projectComplexValue.project = this.project;
        this.projectComplexValue.projectValue = this.projectValue;
        this.projectService.updateProject(this.projectComplexValue).subscribe((response: any) => {
            if (response.Status == "Ok")
                this.notificatopnService.displayOkMessage("Project was updated", "Success");
            else
                this.notificatopnService.displayErrorMessage(response.Error, "Update Project");
        }, (error: any) => {
            this.notificatopnService.displayErrorMessage(error, "Update Project");
        });
    } 

    getPDF() {
        this.project.Id = this.projectId;
        this.project.CostModel = this.costModel;
        this.projectComplexValue.project = this.project;
        this.projectComplexValue.projectValue = this.projectValue;
    }
}



