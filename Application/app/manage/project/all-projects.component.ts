/// <reference path="../../components/common/tree-error-grid.component.ts" />
import { Component, ChangeDetectorRef } from '@angular/core';
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectService } from '../../services/project.service '
import { ProjectModel } from './project.model'
import { NotificationStatusService } from '../../services/notification-status.service'

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/theme-fresh.css';
import 'ag-grid/dist/styles/theme-bootstrap.css';
import { PaginationHelperService } from '../../services/pagination-helper.service';
import { TreeGridComponent } from 'components/common/tree-error-grid.component';

import { Grid, GridOptions } from "ag-grid/main";
import { IDatasource } from "ag-grid/main";
import { EditProjectBtnComponent } from "../project/edit-delete-project/edit-project-btn.component";
import { DeleteProjectBtnComponent } from 'manage/project/edit-delete-project/delete-project-btn.component';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
    templateUrl: './all-projects.component.html',
    styleUrls: ['./all-projects.component.css']
})

export class AllProjectsComponent {
    public pageNumber: number;
    gridOptions: GridOptions;
    dataSource: IDatasource;
    public filterQuery = "";
    public rowsOnPage = 10;
    public sortBy = "Name";
    public sortOrder = "asc";
    projectList: Array<ProjectModel> = new Array<ProjectModel>();
    viewProjectId: number;
    viewUserId: number;
    projectName: string;
    hideProjectTable: boolean;
    noShowProjectDetails: boolean;
    isNotAdmin: boolean;
    public constructor(private projectService: ProjectService,
        private route: Router,
        private notificatopnService: NotificationStatusService,
        private cdRef: ChangeDetectorRef, ) {

        var that = this;

        this.gridOptions = <GridOptions>{
            datasource: dataSource,
            columnDefs: this.createColumnDefs(),
            enableServerSideFilter: true,
            animateRows: true,
            enableColResize: true,
            rowSelection: 'single',
            rowDeselection: true,
            rowModelType: 'infinite',
            pagination: true,
            paginationPageSize: 10,
            cacheOverflowSize: 2,
            maxConcurrentDatasourceRequests: 2,
            infiniteInitialRowCount: 1,
            maxBlocksInCache: 1,
            cacheBlockSize: 10,
            rowHeight: 40,
            headerHeight: 30,
            suppressHorizontalScroll: true,
            getRowNodeId: function (item) {
                return item.id;
            },
            onGridReady: function (params) {
                params.api.sizeColumnsToFit();
            }
        };

        var dataSource = {
            getRows: function (params: any) {
                let pagination = {
                    PageIndex: params.startRow,
                    PageSize: params.endRow - params.startRow,
                    Filters: Array<any>()
                };
                if (params.sortModel.length > 0) {
                    (<any>pagination).Sort = {
                        SortBy: params.sortModel[0].colId,
                        SortDirection: params.sortModel[0].sort == 'asc' ? 0 : 1
                    }
                }
                else {
                    (<any>pagination).Sort = {
                        SortBy: "Date",
                        SortDirection: 1
                    }
                }

                for (let filter in params.filterModel) {
                    if (filter == "Date") {
                        pagination.Filters.push({
                            ColumnName: filter,
                            Type: params.filterModel[filter].type,
                            FilterValue: params.filterModel[filter].dateFrom,
                            FilterType: params.filterModel[filter].filterType,
                            FilterValueTo: params.filterModel[filter].dateTo
                        });
                    }
                    else {
                        pagination.Filters.push({
                            ColumnName: filter,
                            Type: params.filterModel[filter].type,
                            FilterValue: params.filterModel[filter].filter,
                            FilterType: params.filterModel[filter].filterType,
                            FilterValueTo: params.filterModel[filter].filterTo
                        });
                    }
                }

                that.projectService.getPagedProjectList(pagination).subscribe((response) => {
                    if (response.Data) {
                        that.projectService.allGridState = response;
                        that.projectService.paginationOld = pagination;
                        that.projectList = response.Data.Projects;
                        params.successCallback(that.projectList, response.Data.TotalCount);
                    }
                    else {
                        this.hideProjectTable = true;
                    }

                }, (error: any) => {
                    this.notificatopnService.displayErrorMessage(error, "Get Projects");
                });
            }
        };

        this.gridOptions.onGridReady = (event: any) => {
            this.gridOptions.api.setDatasource(dataSource);
        }

        this.gridOptions.onCellClicked = (event: any) => {
            if (event.colDef.headerName == "Delete") {
                that.projectService.delteProject(event.data.Id).subscribe((response) => {
                    if (response) {
                        var projDeleted = this.projectList.find(p => p.Id == event.data.Id);
                        var index = this.projectList.indexOf(projDeleted, 0);
                        this.projectList.splice(index, 1);
                        this.gridOptions.onGridReady();
                    }
                })
            }
        }

    }
    ngOnInit(): void {
        this.noShowProjectDetails = true;
    }

    public toInt(num: string) {
        return +num;
    }

    CreateProject() {
        this.route.navigateByUrl('/createProject');
    }

    viewProject(projectId: number, userId: any, projectName: string) {
        var route = '/project-edit/' + projectId;
        this.route.navigate([route]);
    }

    deleteProject(projectId: number, index: number) {
        this.projectService.delteProject(projectId).subscribe(response => {
            if (response.Data) {
                this.projectList.slice(index, 1);
                this.cdRef.detectChanges();
            }
        })
    }

    private createColumnDefs() {
        return [
            { headerName: "Project name", field: 'Name', filter: 'text', width: 596 },
            {
                headerName: "Edit",
                field: "Id",
                suppressFilter: true,
                width: 60,
                cellRendererFramework: EditProjectBtnComponent,
                cellClassRules: {
                    'text-center': function (params: any) { return true }
                }
            },
            {
                headerName: "Delete",
                field: "Id",
                suppressFilter: true,
                width: 60,
                cellRendererFramework: DeleteProjectBtnComponent,
                cellClassRules: {
                    'text-center': function (params: any) { return true }
                }
            }
        ];
    }
}
