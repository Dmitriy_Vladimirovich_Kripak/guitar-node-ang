﻿import { Dictionary } from 'common/dictionary.type'
import { ProjectModel } from './project.model'
import { ProjectViewModel } from 'calculation/project.model';
import { ProjectViewResultModel } from 'calculation/project-view-result.model';
export class ProjectComplexValueModel {
    
    project: ProjectModel;
    projectValue: ProjectViewModel;
    calcResult:ProjectViewResultModel;

}
