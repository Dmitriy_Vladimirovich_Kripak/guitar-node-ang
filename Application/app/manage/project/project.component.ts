import { Component } from '@angular/core';
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectService } from '../../services/project.service '
import { ProjectModel } from './project.model'
import { ProjectComplexValueModel } from './projectComplexValue.model'
import { ProjectViewModel } from 'calculation/project.model';
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({  
    templateUrl: './project.component.html',
   
})

export class ProjectComponent { 

    project: ProjectModel;   
    projectValue: ProjectViewModel;
    projectComplexValue: ProjectComplexValueModel;
    showComplexPage: boolean = true;
    showProjectForm: boolean = false; 
    showProjectNameError: boolean = true;
    public options = {
        position: ["bottom", "right"],
        timeOut: 0,
        lastOnBottom: true,
    };
    public constructor(private projectService: ProjectService, private parentRouter: Router, private notificatopnService: NotificationStatusService) {
        this.project = new ProjectModel();
        this.projectValue = new ProjectViewModel();
    }    

    saveProjectFrom() {
        if (this.project.Name == null)
        {
            this.showProjectNameError = false;
            return;
        }
        else {
            this.showProjectNameError = true;
            this.projectComplexValue = new ProjectComplexValueModel();
            this.projectComplexValue.project = this.project;
            this.showComplexPage = false;
            this.showProjectForm = true;
        }
     
    }
    saveProject() {       
        this.projectComplexValue.projectValue = this.projectValue;
        this.projectService.createProject(this.projectComplexValue).subscribe((response: any) => {
            if (response.Status == "Ok")
                this.notificatopnService.displayOkMessage("Project was added", "Success");
            else
                this.notificatopnService.displayErrorMessage(response.Error, "Save Project");        
        }, (error: any) => {
            this.notificatopnService.displayErrorMessage(error, "Save Project");  
            }
        );
        this.parentRouter.navigate(['/allProject']);      
    }
    resultViewEmit(userInputs: any) {
        this.projectValue = userInputs;
    }   
   
}
