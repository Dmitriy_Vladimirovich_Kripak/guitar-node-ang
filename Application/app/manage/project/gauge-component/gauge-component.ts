import { Component, AfterViewInit, ViewChild, ElementRef, Input } from "@angular/core";
declare var Gauge : any;
@Component({      
    selector:"guage-component",
    templateUrl: './gauge-component.html'
})

export class GuageComponent implements  AfterViewInit{ 
    @Input() id:any;
    @Input() set setValue(newValue:any){
        if(this.gauge)
        {
            this.gauge.set(newValue)
        }
        this.value = newValue;
    }
    opts:any; 
    gauge:any;
    value:any;
    constructor(){
        this.opts = {
            lines: 0, // The number of lines to draw
            angle: 0.00, // The length of each line
            lineWidth: 0.44, // The line thickness
            
           
            pointer: {
              length: 0.5, // The radius of the inner circle
              strokeWidth: 0.060, // The rotation offset
              color: 'red' // Fill color
            },
            
            limitMax: false,   // If true, the pointer will not go past the end of the gauge
            colorStart: 'blue',   // Colors
            colorStop: '#8FC0DA',    // just experiment with them
            strokeColor: '#E0E0E0',   // to see which ones work best for you
            generateGradient: true,
            maxValue: 100,
            animationSpeed: 32
          };

    }

    ngAfterViewInit(){
        var target = document.getElementById('foo-'+this.id);
        this.gauge= new Gauge(target).setOptions(this.opts); // create sexy gauge!   
        this.gauge.animationSpeed = 32; // set animation speed (32 is default value)
        this.gauge.set(this.value);
    }
}