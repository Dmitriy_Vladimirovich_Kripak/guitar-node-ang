﻿import { Dictionary } from 'common/dictionary.type'
import { ProjectViewModel } from 'calculation/project.model';

export class EditeProjectModel {
    public isUserProject: boolean;
    public projectValue: ProjectViewModel;

}
