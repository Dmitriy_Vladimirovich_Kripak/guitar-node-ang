﻿import { Dictionary } from 'common/dictionary.type';

export class AuditTrail {
    public UserName: string;
    public Date: Date;
    public Action: string;
    public EntityName: string;
    public AuditTrailValues: Array<AuditTrailValues>;
}

export class AuditTrailValues {
    public Id: number;
    public AuditTrailEntityId: number;
    public PropertyName: string;
    public Action: string;
    public EntityName: string;
    public OldValue: string;
    public NewValue: string;
}
