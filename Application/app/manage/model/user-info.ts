export class UserInfo {

    public Id:string;

    public Username:string;
    
    public Email:string;

    public Roles:Array<string> = new Array();
    
}