﻿import { Dictionary } from 'common/dictionary.type'

export class UserModel {
    public Name: string;
    public Id: string;
    public Email: string;
    public Status: boolean;
    public Password: string;
    public ConfirmPassword: string;
}
