﻿import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UserManageService } from '../../services/user-manage.service'
import { UserModel } from './user.model'
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationStatusService } from '../../services/notification-status.service'
import { LoginService } from 'services/login-service';
@Component({
    selector: 'user-details',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})

export class UserComponent {
    userId: string;
    sub: any;
    status: string;
    actionType: string;
    errorMessage: string;
    label: string;
    IsAdded: boolean;
    model: UserModel = new UserModel();

    public constructor(private userManageService: UserManageService,
        private activeRoute: ActivatedRoute,
        private route: Router,
        private notificationService: NotificationStatusService,
        private loginService: LoginService
    ) {
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.userId = params['id'];
            if (this.userId && this.userId != "add") {
                this.label = "Update User"
                this.IsAdded = false;
                this.userManageService.getUserById(this.userId).subscribe(repsonse => {
                    this.model = repsonse.Data;

                })
            }
            else {
                this.label = "Add User"
                this.IsAdded = true;
            }
        });
    }

    update() {
        this.userManageService.updateUserInfo(this.model).subscribe((res) => {
            if (res.Data) {
                if (res.Data.Id) {
                    this.notificationService.displayOkMessage("User was added", "Success");
                    this.returnToList();
                }
                else {
                    if (res.Status == "Ok")
                        this.notificationService.displayOkMessage("User was processed", "Success");
                    else
                        this.notificationService.displayErrorMessage(res.Error, "Update User");
                }
            }

            else {
                this.notificationService.displayErrorMessage(res.Error, "Update User");
            }
        }, (error: any) => {
                this.notificationService.displayErrorMessage(error, "Update User");
            });
    }

    returnToList() {
        this.route.navigate(['/allUser']);
    }



}
