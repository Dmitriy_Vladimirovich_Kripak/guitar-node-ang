import { Component } from '@angular/core';
import { UserManageService } from '../../services/user-manage.service'
import { UserModel } from './user.model'
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router } from '@angular/router';
import { NotificationStatusService } from '../../services/notification-status.service'
@Component({  
    templateUrl: './all-user.component.html'
})

export class AllUserComponent {
  
    public filterQuery = "";
    public rowsOnPage = 10;
    public sortBy = "Name";
    public sortOrder = "asc";   
    showUserId: string;
    notShowUserBlock: boolean = true;
    usersList: Array<UserModel> = new Array<UserModel>();

    public constructor(private userManageService: UserManageService, private router: Router, private notificationService: NotificationStatusService) {
        userManageService.getAllUser().subscribe((response) => { 
           if (response.Status == "Ok") 
               this.usersList = response.Data;  
           else
               this.notificationService.displayErrorMessage(response.Error, "Get All Users");   
        }, (error: any) => {
            this.notificationService.displayErrorMessage(error, "Get All Users");
        });
    }

    ngOnInit(): void {

    }
    public viewUser(Id: string) {
        this.showUserId = Id;
        this.notShowUserBlock = false;
    }
    changeBlockStatus(element: HTMLInputElement, Id: string) {
        let user = this.usersList.find(x => x.Id == Id);
        user.Status = element.checked;
        this.userManageService.changeUserBlockStatus(user).subscribe((response: any) => {
            if (response.Status == "Ok")
                this.usersList = response.Data;  
            else
                this.notificationService.displayErrorMessage(response.Error, "Change User Block Status");            
        }, (error: any) => {
            this.notificationService.displayErrorMessage(error, "Change User Block Status");
        });
    }

    GoToAddUser(){
        this.router.navigate(['/user/add']);        
    }

    GoToEditUser(id){
        var route = '/user/edit/'+id;
        this.router.navigate([route]);    
    }

}
