import { Component, OnInit, forwardRef } from '@angular/core';
import { UserManageService } from 'services/user-manage.service'
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationStatusService } from 'services/notification-status.service'
import { ProjectService } from 'services/project.service '
import { ErrorModel } from 'common/error.model'
import { AllErrorComponent } from 'components/common/all-errors.component'
import { Subject } from 'rxjs/Subject';
import { UserInfo } from 'manage/model/user-info';
import { LoginService } from 'services/login-service';
import { StorageService } from 'services/storage-service';
import { CampusInfoService } from 'services/campus-info.service';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    providers: [NotificationStatusService],
})
export class AppComponent {
    isAdmin: boolean = false;
    isLoad: boolean = false;
    public options = {
        position: ["bottom", "right"],
        timeOut: 0,
        lastOnBottom: true,
    };
    errorList: Array<any> = new Array();

    userInfo: UserInfo = new UserInfo();

    constructor(
        private userManageService: UserManageService,
        private notificatopnService: NotificationsService,
        private notificationStatusService: NotificationStatusService,
        private projectService: ProjectService,
        private loginService: LoginService,
        private storageService: StorageService,
        private campusService: CampusInfoService
    ) {
        this.notificationStatusService.NotificationOkResult.subscribe((messageInfo) => {
            this.displayNotification(messageInfo.message, messageInfo.type);
        });
        this.notificationStatusService.NotificationErrorResult.subscribe((messageInfo) => {
            this.errorList.push(messageInfo);
            this.displayNotification(messageInfo.message, "Error");
        });
        this.notificationStatusService.GetErrorList.subscribe(() => {
            this.notificationStatusService.sendErrorList(this.errorList);
        });

    }
    ngOnInit() {
        this.userManageService.getUserInfo().subscribe((res) => {
            this.userInfo = res.Data;
            this.storageService.setUser(this.userInfo);
            if (this.userInfo.Roles.indexOf('Admin') >= 0) {
                this.loginService.setIsAdmin(true);
            }
            if (this.storageService.campusInfo) {
                this.isLoad = true;
            }
        });
        this.campusService.getAllCampusInfo().subscribe(resposne => {
            this.storageService.campusInfo = resposne.Data;
            if (this.storageService.getUser()) {
                this.isLoad = true;
            }
        })
    }

    displayNotification(msg: string, type: string) {
        this.notificatopnService.remove();
        if (type == "Success")
            this.notificatopnService.success(
                'New message',
                msg,
                {
                    timeOut: 2200,
                    showProgressBar: true,
                    pauseOnHover: false,
                    clickToClose: false,
                    maxLength: 50
                }
            )
        else if (type == "Error")
            this.notificatopnService.error(
                'New message',
                msg,
                {
                    timeOut: 3500,
                    showProgressBar: true,
                    pauseOnHover: false,
                    clickToClose: false,
                    maxLength: 200
                }
            )
        else if (type == "Warning")
            this.notificatopnService.warn(
                'New message',
                msg,
                {
                    timeOut: 1200,
                    showProgressBar: true,
                    pauseOnHover: false,
                    clickToClose: false,
                    maxLength: 200
                }
            )
    }

}
