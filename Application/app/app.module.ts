import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import * as $ from 'jquery';

import { AppComponent } from './app.component';
import { CalculationModule } from './calculation/calculation.module';
import { CommonAppModule } from './common/common.module';
import { AppRoutingModule } from './app-routing.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { DataTableModule } from "angular2-datatable";
import { NgxPaginationModule } from 'ngx-pagination';
import { NotificationStatusService } from 'services/notification-status.service'; 

import { ClickableModule } from "./components/audit/clickable.module";
import { NavMenuComponent } from 'components/navmenu/navmenu.component';
import { HomeComponent } from 'components/home/home.component';
import { FooterComponent } from 'components/footer/footer.component';
import { InformationModule } from 'modules/information.module';
import { LoginService } from 'services/login-service';
import { StorageService } from 'services/storage-service';
import { EditProjectBtnComponent } from "./manage/project/edit-delete-project/edit-project-btn.component";
import { DeleteProjectBtnComponent } from "./manage/project/edit-delete-project/delete-project-btn.component";


@NgModule({
    imports: [BrowserModule, CommonAppModule, CalculationModule, AppRoutingModule, DataTableModule, NgxPaginationModule,
        SimpleNotificationsModule, ClickableModule, InformationModule ],
    entryComponents: [EditProjectBtnComponent, DeleteProjectBtnComponent],
    declarations: [AppComponent, NavMenuComponent, HomeComponent, FooterComponent, EditProjectBtnComponent, DeleteProjectBtnComponent],
    providers: [
       LoginService, StorageService
    ],
    bootstrap: [AppComponent,]
})
export class AppModule { }
