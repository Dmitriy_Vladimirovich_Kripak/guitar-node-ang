﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ErrorModel, Exception } from 'common/error.model'
import 'rxjs/Rx';


@Injectable()
export class NotificationStatusService {    
    
    NotificationOkResult: Subject<any> = new Subject();
    NotificationErrorResult: Subject<any> = new Subject();
    GetErrorList: Subject<any> = new Subject();
    SendErrorList: Subject<any> = new Subject();
    displayOkMessage(msg: any, type: string) {      
        let messageInfo = {
            message: msg,
            type: type
        };
        this.NotificationOkResult.next(messageInfo);
    }    
    displayErrorMessage(exeption: any, action: string) {        
        this.NotificationErrorResult.next(this.getError(exeption, action));       
    }
    getErrorList() {
        this.GetErrorList.next();
    }
    sendErrorList(list: any)
    {
        this.SendErrorList.next(list);
    }
    private getError(ex: any, action: string)
    {
        let error: ErrorModel = new ErrorModel();
        error.date = new Date().toDateString();
        error.action = action;      
        if (ex.Message) {
            error.message = ex.Message;
            error.inners = new Array<Exception>();
            if (ex.InnerException)
            {
                let currEx = ex.InnerException;
                do {
                    let newInner = new Exception();
                    newInner.message = currEx.Message;
                    newInner.stackTrace = currEx.StackTrace;
                    error.inners.push(newInner);
                    currEx = currEx.InnerException;
                } while (currEx)
            }
            
        }       
        else
        {
            error.message = ex;
        }

        return error
    }   
}
