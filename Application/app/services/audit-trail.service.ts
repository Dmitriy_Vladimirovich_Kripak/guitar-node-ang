﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams, ResponseContentType  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { NotificationStatusService } from './notification-status.service'
let headers = new Headers({ 'Content-Type': 'application/json' });
let options = new RequestOptions({ headers: headers });  

import { AuditTrailValues } from 'manage/model/audit.model'

@Injectable()
export class AuditTrailService {
    public constructor(private http: Http, private notificationStatusService: NotificationStatusService) { }

    public data: Array<AuditTrailValues>;
    public allGridState: any;
    public paginationOld: any;
    public doNotreload = false; 

    invokeEvent: Subject<any> = new Subject();
    componentMethodCalled$ = this.invokeEvent.asObservable();

    getAuditTrailInfo() {
        return this.http.get('/api/Audit/getAuditTrailInfo', options)
            .map(this.extractData).catch(this.handleError);
    }         

    getAuditTrailInfoListForPaging(paginationModel: any): Observable<any> {
        return this.http.post('/api/Audit/getAuditTrailInfoListForPaging', paginationModel, options)
            .map(this.extractData).catch(this.handleError);
    };  

    //getAuditTrailById(id: number): Observable<any> {
    //    return this.http.get('/api/Audit/getAuditTrailById/' + id, options)
    //        .map(this.extractData).catch(this.handleError);
    //};

    private extractDataPDF(res: Response) {
        let body = res.json();        
        return body || {};
    }
    private extractData(res: Response ) {
        let body = res.json();     
        return body || {};
    }
    private extractDataPost(res: Response) {
        let body = res.json();
        return body || {}; 
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);

        return Observable.throw(errMsg);
    }
    private pdfHandleError(error: Response | any)
    {
        return Observable.throw(error.statusText);
    }
    
    private displayCustomMessage(message: string, type: string) {
        this.invokeEvent.next();
    }
    
}
