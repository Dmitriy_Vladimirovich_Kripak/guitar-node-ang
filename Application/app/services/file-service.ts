import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams, ResponseContentType  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { FileSaver } from 'file-saver';
import { NotificationStatusService } from './notification-status.service'
import { ProjectModel } from 'manage/project/project.model';
let headers = new Headers({ 'Content-Type': 'application/json' });
let options = new RequestOptions({ headers: headers });

@Injectable()
export class FileService {
    public constructor(private http: Http, private notificationStatusService: NotificationStatusService) { }

    invokeEvent: Subject<any> = new Subject();
    componentMethodCalled$ = this.invokeEvent.asObservable();   

    getFileInfos(showAll:boolean) {
        let url = '/api/File/getAllFileInfo/' + showAll;
        return this.http.get(url, options)
            .map(this.extractData).catch(this.handleError);
    }

    getPDF(model?: any,fileInfoId?:number): any {
        let headersPDF = new Headers({ 'Content-Type': 'application/json' });
        let optionsPDF = new RequestOptions({ headers: headersPDF });
        optionsPDF.responseType = ResponseContentType.Blob;
        return this.http.post('/api/File/getPDF', model, optionsPDF)
            .map(this.extractDataPDF).catch(this.pdfHandleError);
    }

    savePDF(model:ProjectModel,fileName:string){
        return this.http.post('/api/File/saveFilePDF/' + fileName, model, options)
            .map(response => this.extractData(response)).catch(this.handleError);
    }

    getFileInfo(id:number){
        let url = '/api/File/getFileInfo/'+id;   

        return this.http.get(url, options)
            .map(this.extractData).catch(this.handleError);
    }
    getFileById(id:number){
        let url = '/api/File/getFile/'+id;   

        let headersPDF = new Headers({ 'Content-Type': 'application/json' });
        let optionsPDF = new RequestOptions({ headers: headersPDF });
        optionsPDF.responseType = ResponseContentType.Blob;
        return this.http.get(url , optionsPDF)
            .map(this.extractDataPDF).catch(this.pdfHandleError);       
    }

    deleteFileInfo(id:number){
        return this.http.delete('/api/File/deleteFileInfo/'+id, options)
            .map(response => this.extractData(response)).catch(this.handleError);
    }

    

    getErrorMessage(exeption: any)
    {
        if (exeption)
            return;
    }
   
    private extractDataPDF(res: Response) {
        return res;
    }
    private extractData(res: Response ) {
        let body = res.json();     
        return body || {};
    }
    private extractDataPost(res: Response) {
        let body = res.json();
        return body || {}; 
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);

        return Observable.throw(errMsg);
    }
    private pdfHandleError(error: Response | any)
    {
        return Observable.throw(error.statusText);
    }
    
    private displayCustomMessage(message: string, type: string) {
        this.invokeEvent.next();
    }
    
}
