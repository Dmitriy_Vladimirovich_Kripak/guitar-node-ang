import { Injectable } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { ProjectModel } from 'manage/project/project.model';
import { FileSaveDialogComponent } from 'components/common/file-save-dialog.component';
import { CustomDialogComponent } from 'components/common/dialog.component';


@Injectable()
export class DialogModalService {
    options: any;  
    constructor(private dialogService: DialogService) { }

    showFileNameModal(project:ProjectModel): any {        
        return this.dialogService.addDialog(FileSaveDialogComponent,{project:project});       
    };    
    
    showProjectNameModal(): any {        
        return this.dialogService.addDialog(CustomDialogComponent);       
    };  
}