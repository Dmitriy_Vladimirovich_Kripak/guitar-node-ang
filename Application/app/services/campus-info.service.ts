import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams, ResponseContentType  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { FileSaver } from 'file-saver';
import { NotificationStatusService } from './notification-status.service'
import { CampusInfoModel } from 'models/campusInfo.model';
let headers = new Headers({ 'Content-Type': 'application/json' });
let options = new RequestOptions({ headers: headers });

@Injectable()
export class CampusInfoService {
  static StorageService: any;
  static Storage: any;
    public constructor(private http: Http, private notificationStatusService: NotificationStatusService) { }

    invokeEvent: Subject<any> = new Subject();
    componentMethodCalled$ = this.invokeEvent.asObservable();

   

    getAllCampusInfo() {
        return this.http.get('/api/CampusInfo', options)
            .map(this.extractData).catch(this.handleError);
    } 

    updateCampusInfo(campusInfo:CampusInfoModel) {      
        return this.http.put('/api/CampusInfo/', campusInfo, options )
            .map(this.extractData).catch(this.handleError);
    } 
    

    private extractDataPDF(res: Response) {
        let body = res.json();        
        return body || {};
    }
    private extractData(res: Response ) {
        let body = res.json();     
        return body || {};
    }
    private extractDataPost(res: Response) {
        let body = res.json();
        return body || {}; 
    }

    private handleError(error: Response | any) {      
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);

        return Observable.throw(errMsg);
    }
    private pdfHandleError(error: Response | any)
    {
        return Observable.throw(error.statusText);
    }
    
    private displayCustomMessage(message: string, type: string) {
        this.invokeEvent.next();
    }
    
}
