﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams, ResponseContentType  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { FileSaver } from 'file-saver';
import { NotificationStatusService } from './notification-status.service'
import { ProjectModel } from 'manage/project/project.model';
let headers = new Headers({ 'Content-Type': 'application/json' });
let options = new RequestOptions({ headers: headers });

@Injectable()
export class ProjectService {
    public constructor(private http: Http, private notificationStatusService: NotificationStatusService) { }

    public allGridState: any;
    public paginationOld: any;
    public data: any; 

    invokeEvent: Subject<any> = new Subject();
    componentMethodCalled$ = this.invokeEvent.asObservable();   

    getAllProjects() {
        return this.http.get('/api/Project/getAllProjects', options)
            .map(this.extractData).catch(this.handleError);
    }  

    getPagedProjectList(model: any) {
        return this.http.post('/api/Project/getPagedProject', model, options)
            .map(this.extractData).catch(this.handleError);
    }
   
    createProject(model:any)
    {
        return this.http.post('/api/Project/createProject/', model, options)
            .map(response => this.extractData(response)).catch(this.handleError);
    }
    updateProject(model: any) {
        return this.http.post('/api/Project/createProject/', model, options)
            .map(this.extractData).catch(this.handleError);
    }

    delteProject(projectId: number) {
        return this.http.delete('/api/Project/delete/'+ projectId, options)
            .map(this.extractData).catch(this.handleError);
    }

    getProjectById(projectId: number, userId: string)
    {
        let model = {
            projectId: projectId,
            userId: userId
        }
        return this.http.post('/api/Project/getProject', model , options)
            .map(this.extractData).catch(this.handleError);
    }  
    
    getErrorMessage(exeption: any)
    {
        if (exeption)
            return;
    }
   
    private extractDataPDF(res: Response) {
        return res;
    }
    private extractData(res: Response ) {
        let body = res.json();     
        return body || {};
    }
    private extractDataPost(res: Response) {
        let body = res.json();
        return body || {}; 
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);

        return Observable.throw(errMsg);
    }
    private pdfHandleError(error: Response | any)
    {
        return Observable.throw(error.statusText);
    }
    
    private displayCustomMessage(message: string, type: string) {
        this.invokeEvent.next();
    }
    
}
