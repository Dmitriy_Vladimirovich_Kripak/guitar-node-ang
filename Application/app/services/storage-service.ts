import { Injectable } from '@angular/core';
import { ProjectModel } from 'manage/project/project.model';
import { UserInfo } from 'manage/model/user-info';
import { CampusInfoModel } from 'models/campusInfo.model';

@Injectable()
export class StorageService {

    private user:UserInfo;
    private isUserSet:boolean = false;

    public currentPorjectForGenerate:ProjectModel;    
    public campusInfo:CampusInfoModel;
   

    public constructor() { }  

    public setUser(newUser:UserInfo)
    {
        if(this.isUserSet)
            return;
        this.isUserSet = true;
        this.user = newUser;        
    }

    public getUser(){
        return this.user;
    }

}