import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class LoginService {
    private admin:boolean;

    public constructor() { }

    setIsAdmin(data:boolean) {
        this.admin = data;
    }

    isAdmin() {
        return this.admin;
    }
    
    
}
