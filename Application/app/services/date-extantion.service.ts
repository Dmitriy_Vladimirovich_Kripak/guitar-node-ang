﻿import { Injectable } from '@angular/core';

declare global {
    interface Date {
        ServerToDateStr(date: Date): string;
    }
}

Date.prototype.ServerToDateStr = function (date: Date) {    
    return [(this.date.getMonth() + 1),
        this.date.getDate(),
        this.date.getFullYear()].join('/');  
};
