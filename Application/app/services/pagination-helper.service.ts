﻿import { Injectable } from '@angular/core'; 

@Injectable()

export class PaginationHelperService {
    isEquivalent(a: any, b: any): boolean {
        if (a == undefined || b == undefined)
            return false;

        else if (a.PageIndex == b.PageIndex && a.PageSize == b.PageSize )
            //&& this.comparePaginationFilters(a.Filters, b.Filters)   TODO implement deep compare method
            //&& this.comparePaginationSortDirection(a.Sort, b.Sort))
            //return true;
        //else
            return true;  
    }

    private comparePaginationFilters(a: Array<Sort>, b: Array<Sort>): boolean {
        if (a == undefined && b != undefined
            || a != undefined && b == undefined) {
            return false;
        }
        else if (a.length == b.length) {
            return true;
        }
        else
            return false;
    }

    private comparePaginationSortDirection(a: any, b: any): boolean {
        if (a == undefined && b == undefined) {
            return true;
        }
        else if (a.SortDirection != undefined && a.SortBy != undefined && b == undefined) {
            return false;
        }
        else if (a.SortDirection == b.SortDirection
            && a.SortBy == b.SortBy) {
            return true;
        }
        else
            return false;
    }
          
}

interface Pagination {
    PageIndex: number;
    PageSize: number;
    TotalCount: number;
    Sort: Array<Sort>;
    Filters: Array<any>;
}

export class Sort {
    public ColumnName: string;
    public Type: string;
    public FilterValue: string;
    public FilterType: string;
    public FilterValueTo: string;

}

