﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

let headers = new Headers({ 'Content-Type': 'application/json' });
let options = new RequestOptions({ headers: headers });

@Injectable()
export class UserManageService {
    public constructor(private http: Http) { }

    getAllUser() {
        return this.http.get('/api/User/getAllUsers', options)
            .map(this.extractData).catch(this.handleError);
    }
    
    getUserById(id: string): Observable<any> {
        return this.http.get('/api/User/' + id)
            .map(this.extractData).catch(this.handleError);
    };

    changeUserBlockStatus(model: any): Observable<any> {    
        return this.http.post('/api/User/changeUserBlockStatus/', model, options)
            .map(this.extractData).catch(this.handleError);
    };
    getUserRole() {
        return this.http.get('/api/User/UserRole/')
            .map(this.extractData).catch(this.handleError);
    }
    getUserInfo() {
        return this.http.get('/api/User/UserInfo/')
            .map(this.extractData).catch(this.handleError);
    }
    updateUserInfo(model: any): Observable<any> {
        return this.http.post('/api/User/updateUser/', model, options)
            .map(this.extractData).catch(this.handleError);
    };

    private extractData(res: Response) {       
        let body = res.json();
        return body || {};
    }
    private extractDataPost(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.Message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
    
}
