﻿/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
    id: string;
}
interface JQuery {
    pieChart(options?:any):JQuery;
    flexslider(options?:any):JQuery;
    tooltip(options?:any):JQuery;
    datepicker(options?:any):JQuery;
    rangeslider(options?:any):JQuery;
    rangeslider(options?:any,options1?:any):JQuery;
    getContext(options?:any):JQuery;
    left(options?:any):JQuery;
    easyResponsiveTabs(options?:any):JQuery
}    
