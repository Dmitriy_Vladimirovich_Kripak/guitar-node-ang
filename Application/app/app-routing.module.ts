﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EditBaselineVariablesComponent } from './calculation/editBaselineVariables.component'
import { A10CalulationComponent } from './calculation/a10calulation.component'
import { A20CalulationComponent } from './calculation/a20calulation.component'
import { A40CalulationComponent } from './calculation/a40calulation.component'
import { A60CalulationComponent } from './calculation/a60calulation.component'
import { A90CalulationComponent } from './calculation/a90calulation.component'
import { B10CalulationComponent } from './calculation/b10calulation.component'
import { B20CalulationComponent } from './calculation/b20calulation.component'
import { B30CalulationComponent } from './calculation/b30calulation.component'
import { D10CalulationComponent } from './calculation/d10calulation.component'
import { C10CalulationComponent } from './calculation/BaseC10_E20/c10calulation.component'
import { C10FitoutCalulationComponent } from './calculation/BaseC10_E20/c10fitout.calulation.component'
import { C20CalulationComponent } from './calculation/BaseC10_E20/c20calulation.component'
import { C20FitoutCalulationComponent } from './calculation/BaseC10_E20/c20fitout.calulation.component'
import { D20CalulationComponent } from './calculation/BaseC10_E20/d20calulation.component'
import { D20FitoutCalulationComponent } from './calculation/BaseC10_E20/d20fitout.calulation.component'
import { D30CalulationComponent } from './calculation/BaseC10_E20/d30calulation.component'
import { D30FitoutCalulationComponent } from './calculation/BaseC10_E20/d30fitout.calulation.component'
import { D40CalulationComponent } from './calculation/BaseC10_E20/d40calulation.component'
import { D40FitoutCalulationComponent } from './calculation/BaseC10_E20/d40fitout.calulation.component'
import { D50CalulationComponent } from './calculation/BaseC10_E20/d50calulation.component'
import { D50FitoutCalulationComponent } from './calculation/BaseC10_E20/d50fitout.calulation.component'
import { D60CalulationComponent } from './calculation/BaseC10_E20/d60calulation.component'
import { D60FitoutCalulationComponent } from './calculation/BaseC10_E20/d60fitout.calulation.component'
import { D70CalulationComponent } from './calculation/BaseC10_E20/d70calulation.component'
import { D70FitoutCalulationComponent } from './calculation/BaseC10_E20/d70fitout.calulation.component'
import { D80CalulationComponent } from './calculation/BaseC10_E20/d80calulation.component'
import { D80FitoutCalulationComponent } from './calculation/BaseC10_E20/d80fitout.calulation.component'
import { E10CalulationComponent } from './calculation/BaseC10_E20/e10calulation.component'
import { E10FitoutCalulationComponent } from './calculation/BaseC10_E20/e10fitout.calulation.component'
import { E20CalulationComponent } from './calculation/BaseC10_E20/e20calulation.component'
import { E20FitoutCalulationComponent } from './calculation/BaseC10_E20/e20fitout.calulation.component'
import { F10BCCalulationComponent } from './calculation/f10BCcalulation.component'
import { F20BCCalulationComponent } from './calculation/f20BCcalulation.component'
import { F30BCCalulationComponent } from './calculation/f30BCcalulation.component'
import { Z10BCCalulationComponent } from './calculation/z10BCcalulation.component'
import { Z70BCCalulationComponent } from './calculation/z70BCcalulation.component'
import { Z910BCCalulationComponent } from './calculation/z910BCcalulation.component'
import { Z920BCCalulationComponent } from './calculation/z920BCcalulation.component'
import { Z930BCCalulationComponent } from './calculation/z930BCcalulation.component'
import { Z940BCCalulationComponent } from './calculation/z940BCcalulation.component'
import { G10SDCalulationComponent } from './calculation/g10SDcalulation.component'
import { G20SDCalulationComponent } from './calculation/g20SDcalulation.component'
import { G60SDCalulationComponent } from './calculation/g60SDcalulation.component'
import { Z10SDCalulationComponent } from './calculation/z10SDcalulation.component'
import { Z70SDCalulationComponent } from './calculation/z70SDcalulation.component'
import { Z910SDCalulationComponent } from './calculation/z910SDcalulation.component'
import { Z920SDCalulationComponent } from './calculation/z920SDcalulation.component'
import { Z930SDCalulationComponent } from './calculation/z930SDcalulation.component'
import { Z940SDCalulationComponent } from './calculation/z940SDcalulation.component'
import { G30SUCalulationComponent } from './calculation/g30SUcalulation.component'
import { G40SUCalulationComponent } from './calculation/g40SUcalulation.component'
import { G50SUCalulationComponent } from './calculation/g50SUcalulation.component'
import { Z10SUCalulationComponent } from './calculation/z10SUcalulation.component'
import { Z70SUCalulationComponent } from './calculation/z70SUcalulation.component'
import { Z910SUCalulationComponent } from './calculation/z910SUcalulation.component'
import { Z920SUCalulationComponent } from './calculation/z920SUcalulation.component'
import { Z930SUCalulationComponent } from './calculation/z930SUcalulation.component'
import { Z940SUCalulationComponent } from './calculation/z940SUcalulation.component'
import { SoftCostCalulationComponent } from './calculation/softCostcalulation.component'
import { TotalProjectCostCalulationComponent } from './calculation/totalProjectCostcalulation.component'
import { ComplexPageComponent } from './calculation/complexPage.component'
import { AllUserComponent } from './manage/users/all-user.component'
import { AllProjectsComponent } from './manage/project/all-projects.component'
import { UserComponent } from './manage/users/user.component'
import { CalculationComponent } from './components/common/calculation.component'
import { ProjectComponent } from './manage/project/project.component'
import { AllErrorComponent } from './components/common/all-errors.component'
import { AuditTrailComponent } from './components/audit/audit-trail.component' 
import { AuditTrailViewComponent } from './components/audit/audit-trail-view.component'
import { HomeComponent } from 'components/home/home.component';
import { CampusHomeComponent } from 'components/campus/campus-home/campus-home.component';
import { CampusProfileComponent } from 'components/campus/campus-profile/campus-profile.component';
import { CampusStandardsComponent } from 'components/campus/campus-standards/campus-standards.component';
import { CampusInformationComponent } from 'components/campus/campus-information/campus-information.component';
import { ProjectComplexComponent } from './components/project-complex/project-complex.component';
import { FileComponent } from 'components/file/file.component';
import { ListFileInfoComponent } from 'components/file/file-list.component';
import { FAQComponent } from 'components/support/faq.component';
import { SettingComponent } from 'components/setting/setting.component';

const appRoutes: Routes = [
    {
        path: 'a10Calculation',
        component: A10CalulationComponent,
    },
    {
        path: 'a20Calculation',
        component: A20CalulationComponent,
    },
    {
        path: 'a40Calculation',
        component: A40CalulationComponent,
    },
    {
        path: 'a60Calculation',
        component: A60CalulationComponent,
    },
    {
        path: 'a90Calculation',
        component: A90CalulationComponent,
    },
    {
        path: 'b10Calculation',
        component: B10CalulationComponent,
    },
    {
        path: 'b20Calculation',
        component: B20CalulationComponent,
    },
    {
        path: 'b30Calculation',
        component: B30CalulationComponent,
    },
    {
        path: 'd10Calculation',
        component: D10CalulationComponent,
    },
    {
        path: 'c10Calculation',
        component: C10CalulationComponent,
    },
    {
        path: 'c10FitoutCalculation',
        component: C10FitoutCalulationComponent,
    },
    {
        path: 'c20Calculation',
        component: C20CalulationComponent,
    },
    {
        path: 'c20FitoutCalculation',
        component: C20FitoutCalulationComponent,
    },
    {
        path: 'd20Calculation',
        component: D20CalulationComponent,
    },
    {
        path: 'd20FitoutCalculation',
        component: D20FitoutCalulationComponent,
    },
    {
        path: 'd30Calculation',
        component: D30CalulationComponent,
    },
    {
        path: 'd30FitoutCalculation',
        component: D30FitoutCalulationComponent,
    },
    {
        path: 'd40Calculation',
        component: D40CalulationComponent,
    },
    {
        path: 'd40FitoutCalculation',
        component: D40FitoutCalulationComponent,
    },
    {
        path: 'd50Calculation',
        component: D50CalulationComponent,
    },
    {
        path: 'd50FitoutCalculation',
        component: D50FitoutCalulationComponent,
    },
    {
        path: 'd60Calculation',
        component: D60CalulationComponent,
    },
    {
        path: 'd60FitoutCalculation',
        component: D60FitoutCalulationComponent,
    },
    {
        path: 'd70Calculation',
        component: D70CalulationComponent,
    },
    {
        path: 'd70FitoutCalculation',
        component: D70FitoutCalulationComponent,
    },
    {
        path: 'd80Calculation',
        component: D80CalulationComponent,
    },
    {
        path: 'd80FitoutCalculation',
        component: D80FitoutCalulationComponent,
    },
    {
        path: 'e10Calculation',
        component: E10CalulationComponent,
    },
    {
        path: 'e10FitoutCalculation',
        component: E10FitoutCalulationComponent,
    },
    {
        path: 'e20Calculation',
        component: E20CalulationComponent,
    },
    {
        path: 'e20FitoutCalculation',
        component: E20FitoutCalulationComponent,
    },
    {
        path: 'f10BCCalculation',
        component: F10BCCalulationComponent,
    },
    {
        path: 'f20BCCalculation',
        component: F20BCCalulationComponent,
    },
    {
        path: 'f30BCCalculation',
        component: F30BCCalulationComponent,
    },
    {
        path: 'z10BCCalculation',
        component: Z10BCCalulationComponent,
    },
    {
        path: 'z70BCCalculation',
        component: Z70BCCalulationComponent,
    },
    {
        path: 'z910BCCalculation',
        component: Z910BCCalulationComponent,
    },
    {
        path: 'z920BCCalculation',
        component: Z920BCCalulationComponent,
    },
    {
        path: 'z930BCCalculation',
        component: Z930BCCalulationComponent,
    },
    {
        path: 'z940BCCalculation',
        component: Z940BCCalulationComponent,
    },
    {
        path: 'g10SDCalculation',
        component: G10SDCalulationComponent,
    },
    {
        path: 'g20SDCalculation',
        component: G20SDCalulationComponent,
    },
    {
        path: 'g60SDCalculation',
        component: G60SDCalulationComponent,
    },
    {
        path: 'z10SDCalculation',
        component: Z10SDCalulationComponent,
    },
    {
        path: 'z70SDCalculation',
        component: Z70SDCalulationComponent,
    },
    {
        path: 'z910SDCalculation',
        component: Z910SDCalulationComponent,
    },
    {
        path: 'z920SDCalculation',
        component: Z920SDCalulationComponent,
    },
    {
        path: 'z930SDCalculation',
        component: Z930SDCalulationComponent,
    },
    {
        path: 'z940SDCalculation',
        component: Z940SDCalulationComponent,
    },
    {
        path: 'g30SUCalculation',
        component: G30SUCalulationComponent,
    },
    {
        path: 'g40SUCalculation',
        component: G40SUCalulationComponent,
    },
    {
        path: 'g50SUCalculation',
        component: G50SUCalulationComponent,
    },
    {
        path: 'z10SUCalculation',
        component: Z10SUCalulationComponent,
    },
    {
        path: 'z70SUCalculation',
        component: Z70SUCalulationComponent,
    },
    {
        path: 'z910SUCalculation',
        component: Z910SUCalulationComponent,
    },
    {
        path: 'z920SUCalculation',
        component: Z920SUCalulationComponent,
    },
    {
        path: 'z930SUCalculation',
        component: Z930SUCalulationComponent,
    },
    {
        path: 'z940SUCalculation',
        component: Z940SUCalulationComponent,
    },
    {
        path: 'softCostCalculation',
        component: SoftCostCalulationComponent,
    }
    ,
    {
        path: 'totalProjectCostCalculation',
        component: TotalProjectCostCalulationComponent,
    },    
    {
        path: 'edit-baseline-variables',
        component: EditBaselineVariablesComponent,
    },
    {
        path: 'complexPage',
        component: ComplexPageComponent,  
    },
    {
        path: 'allUser',
        component: AllUserComponent,   
    },
    {
        path: 'formulas-calculation',
        component: CalculationComponent, 
    },
    {
        path: 'allProject',
        component: AllProjectsComponent,  
    },
    {
        path: 'user/:id',
        component: UserComponent,
    },
    {
        path: 'project-create',
        component: ProjectComplexComponent,  
    },
    {
        path: 'project-edit/:id',
        component: ProjectComplexComponent,  
    },
    {
        path: 'errorList',
        component: AllErrorComponent,   
    },
    {
        path: 'auditTrail',
        component: AuditTrailComponent,   
    },
    {
        path: 'auditTrailValues',
        component: AuditTrailViewComponent,
    },
    {
        path: 'home',
        component: HomeComponent,
    },
    {  path: 'campusHomePage', component: CampusHomeComponent },
    {  path: 'campusProfile',component: CampusProfileComponent  },  
    {  path: 'campusInformation',component: CampusInformationComponent  }, 
    {  path: 'campusStandards',component: CampusStandardsComponent  }, 
    {  path: 'user/add',component: UserComponent  }, 
    {  path: 'user/edit/:id',component: UserComponent  },
    {  path: 'published-reports',component: ListFileInfoComponent  },
    {  path: 'report-detail',component: FileComponent  },
    {  path: 'report-detail/:id',component: FileComponent  },
    {  path: 'faq',component: FAQComponent  },
    {  path: 'setting',component: SettingComponent  },
    { path: '', redirectTo: 'campusHomePage', pathMatch: 'full' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }