﻿import { Dictionary } from 'common/dictionary.type'

export class ErrorModel {
    public date: any;
    public action: string;   
    public message: string;
    public inners: Array<Exception>;
}

export class Exception {
    public message: string;
    public stackTrace: string;
}
