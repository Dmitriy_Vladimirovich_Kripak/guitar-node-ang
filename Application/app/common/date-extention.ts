﻿import { Injectable } from '@angular/core';

declare global {
    interface Date {
        ToServerDateStr(date: Date): string;
        ToServerObjectDateStr(dateFromClient: Object): string;
    }
}
Date.prototype.ToServerDateStr = function (dateFromClient: Date): string {
    //format M/d/yyyy
    return [(dateFromClient.getMonth() + 1),
        dateFromClient.getDate(),
        dateFromClient.getFullYear()].join('/');
};


