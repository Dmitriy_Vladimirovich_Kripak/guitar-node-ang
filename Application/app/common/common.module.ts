﻿import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { EnumHelperService } from './enum-helper.service'
import { RangeComponent } from 'components/common/range.component';
//import { DateHelperService } from './date-extention.service'

@NgModule({
    imports: [
        CommonModule, HttpModule, FormsModule
    ],
    declarations: [RangeComponent],
    exports:[
        RangeComponent
    ],
    providers: [
        EnumHelperService, //DateHelperService
    ],
})
export class CommonAppModule { }