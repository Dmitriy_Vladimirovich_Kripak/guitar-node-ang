﻿import { Injectable } from '@angular/core';

@Injectable()
export class EnumHelperService {
    static getValues(e: any, addSpaces = false): KeyValuePair<number>[] {
        let options = Object.keys(e);
        let values = options.slice(options.length / 2);
        let result: KeyValuePair<number>[] = [];

        for (let val of values) {
            result.push({ name: val, value: e[val] });
        }

        if (addSpaces) {
            result = this.addSpaces(result);
        }

        return result;
    }

    /*Group of methods that adds spaces to the drop-down items*/

    static addSpaces(inputArray): any[] {
        var result = "";
        if (inputArray) {
            for (let inpPosition = 0; inpPosition < inputArray.length; inpPosition++) {

                var temp = inputArray[inpPosition].name.split('');

                for (let namePosition = 0; namePosition < inputArray[inpPosition].name.length; namePosition++) {

                    if (temp[namePosition] == temp[namePosition].toUpperCase()
                        && namePosition != 0 && temp[namePosition - 1] != ' ') {

                        if (!this.isKnownUppercaseAbbreviations(inputArray[inpPosition].name)) {
                            temp.splice(namePosition, 0, ' ');
                        }
                        else {

                            if (inputArray[inpPosition].name.includes('CMAR')) {
                                temp = this.addSpacesToKnownUppercaseAbbreviations(inputArray[inpPosition].name, 'CMAR');
                                break;
                            }
                            else if (inputArray[inpPosition].name.includes('LEED')) {
                                temp = this.addSpacesToKnownUppercaseAbbreviations(inputArray[inpPosition].name, 'LEED');
                                break;
                            }
                        }
                    }
                }

                for (let tempPosition = 0; tempPosition < temp.length; tempPosition++) {
                    result += temp[tempPosition];
                }

                inputArray[inpPosition].name = result.toString();
                result = "";
            }
        }
        return inputArray;
    }

    static addSpacesToKnownUppercaseAbbreviations(inputString, abbr) {
        var splitted = inputString.split(abbr),
            temp = splitted[1].split(''),
            result = '';

        for (let i = 0; i < splitted[1].length; i++) {
            if (temp[i] == temp[i].toUpperCase()
                && i != 0 && temp[i - 1] != ' ') {
                temp.splice(i, 0, ' ');
            }
        }

        for (let tempPosition = 0; tempPosition < temp.length; tempPosition++) {
            result += temp[tempPosition];
        }

        return abbr + " " + result;
    }

    static addSpacesToBuildingType(inputArray): any[] {
        var result = "";
        if (inputArray) {
            for (let inpPosition = 0; inpPosition < inputArray.length; inpPosition++) {

                var temp = inputArray[inpPosition].split('');

                for (let namePosition = 0; namePosition < inputArray[inpPosition].length; namePosition++) {
                    if (temp[namePosition] == temp[namePosition].toUpperCase()
                        && namePosition != 0 && temp[namePosition - 1] != ' ') {
                        temp.splice(namePosition, 0, ' ');
                    }
                }

                for (let tempPosition = 0; tempPosition < temp.length; tempPosition++) {
                    result += temp[tempPosition];
                }

                inputArray[inpPosition] = result.toString();
                result = "";
            }
        }
        return inputArray;
    }

    static isKnownUppercaseAbbreviations(inputString) {

        let knownUppercaseAbbreviations = ['CMAR', 'TBD', 'LEED'];

        for (var i = 0; i < knownUppercaseAbbreviations.length; i++) {
            if (inputString.includes(knownUppercaseAbbreviations[i])) {
                return true;
            }
        }
        return false;
    }

    /*End of group of methods*/
}

interface KeyValuePair<T> {
    name: string;
    value: T;
}
