﻿import { NgModule } from '@angular/core';

import { ClickableAuditTrailComponent } from "./clickable.audit-trail.component"; 

@NgModule({
    imports: [],
    declarations: [
        ClickableAuditTrailComponent,
    ],
    exports: [
        ClickableAuditTrailComponent,
    ]
})
export class ClickableModule {
}
