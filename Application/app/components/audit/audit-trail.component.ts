import { Component, EventEmitter, Input, Output, ElementRef, AfterViewInit } from '@angular/core';
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router } from '@angular/router';
import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/theme-fresh.css';
import 'ag-grid/dist/styles/theme-bootstrap.css';

import { NotificationStatusService } from '../../services/notification-status.service';
import { PaginationHelperService } from '../../services/pagination-helper.service';
import { AuditTrailService } from '../../services/audit-trail.service';
import { AuditTrailValues, AuditTrail } from 'manage/model/audit.model';
import { TreeGridComponent } from '../common/tree-error-grid.component';
import { Grid, GridOptions } from "ag-grid/main";
import { IDatasource } from "ag-grid/main";
import { ClickableAuditTrailComponent } from "./clickable.audit-trail.component";

@Component({
    moduleId: module.id,
    selector: 'audit-trail',
    templateUrl: './audit-trail.component.html'
})

export class AuditTrailComponent {

    public gridOptions: GridOptions;
    public dataSource: IDatasource; 
    public pageNumber: number = 5;

    public constructor(
        private parentRouter: Router,
        private notificationStatusService: NotificationStatusService,
        private auditTrailService: AuditTrailService,
        private paginationHelperService: PaginationHelperService
    ) {

        var that = this;

        this.gridOptions = <GridOptions>{
            datasource: dataSource,
            columnDefs: this.createColumnDefs(),
            enableServerSideSorting: true,
            enableServerSideFilter: true,
            animateRows: true,
            enableColResize: true,
            rowSelection: 'single',
            rowDeselection: true,
            rowModelType: 'infinite',
            pagination: true,
            paginationPageSize: 10,
            cacheOverflowSize: 2,
            maxConcurrentDatasourceRequests: 2,
            infiniteInitialRowCount: 1,
            maxBlocksInCache: 1,
            cacheBlockSize: 10,
            rowHeight: 40,
            headerHeight: 30,
            suppressHorizontalScroll: true,
            getRowNodeId: function (item) {
                return item.id;
            },
            onGridReady: function (params) {
                params.api.sizeColumnsToFit();
            }
        };

        var dataSource = {
            getRows: function (params: any) {
                let pagination = {
                    PageIndex: params.startRow,
                    PageSize: params.endRow - params.startRow,
                    Filters: Array<any>()
                };
                if (params.sortModel.length > 0) {
                    (<any>pagination).Sort = {
                        SortBy: params.sortModel[0].colId,
                        SortDirection: params.sortModel[0].sort == 'asc' ? 0 : 1
                    }
                }
                else {
                    (<any>pagination).Sort = {
                        SortBy: "Date",
                        SortDirection: 1
                    }
                }

                for (let filter in params.filterModel) {
                    if (filter == "Date") {
                        pagination.Filters.push({
                            ColumnName: filter,
                            Type: params.filterModel[filter].type,
                            FilterValue: params.filterModel[filter].dateFrom,
                            FilterType: params.filterModel[filter].filterType,
                            FilterValueTo: params.filterModel[filter].dateTo
                        });
                    }
                    else {
                        pagination.Filters.push({
                            ColumnName: filter,
                            Type: params.filterModel[filter].type,
                            FilterValue: params.filterModel[filter].filter,
                            FilterType: params.filterModel[filter].filterType,
                            FilterValueTo: params.filterModel[filter].filterTo
                        });
                    }
                }

                if (that.auditTrailService.doNotreload == false) {
                    that.auditTrailService.getAuditTrailInfoListForPaging(pagination).subscribe((response) => {
                        that.auditTrailService.allGridState = response;
                        that.auditTrailService.paginationOld = pagination;
                        params.successCallback(response.Data.AuditTrails as Array<AuditTrail>, response.Data.TotalCount);
                    });
                } 
                else {
                    that.auditTrailService.doNotreload = false; 
                    params.successCallback(that.auditTrailService.allGridState.Data.AuditTrails as Array<AuditTrail>, that.auditTrailService.allGridState.Data.TotalCount);
                    that.onPageByNumber(that.auditTrailService.paginationOld.PageIndex / that.auditTrailService.paginationOld.PageSize);
                }
            }
        };

        this.gridOptions.onGridReady = (event: any) => {
            this.gridOptions.api.setDatasource(dataSource);
        }
    }

    ngOnInit(): void {

    }

    private createColumnDefs() {
        return [
            {
                headerName: "Date",
                field: 'Date',
                filter: 'date'
            },
            { headerName: "User Name", field: 'UserName', filter: "text" },
            { headerName: "Action", field: 'Action', filter: "text" },
            { headerName: "Entity Name", field: 'EntityName', filter: "text" },
            { headerName: "Entity Type", field: 'EntityType', filter: "text" },
            {
                headerName: "Values",
                field: "AuditTrailValues",
                suppressFilter: true,
                width: 80,
                cellRendererFramework: ClickableAuditTrailComponent,
                cellClassRules: {
                    'text-center': function (params: any) { return true }
                }
            }
        ];
    }

    ngAfterViewInit() {
        this.gridOptions.api.sizeColumnsToFit();
    }

    onFilterChanged(value) {
        this.gridOptions.api.setQuickFilter(value);
    }

    onPageByNumber(pageNumber: number) {
        this.gridOptions.api.paginationGoToPage(Math.floor(pageNumber));
    }
}