﻿import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { Router } from '@angular/router';

import { AuditTrailService } from '../../services/audit-trail.service';
                                                                          
@Component({
    selector: 'clickable-audit-trail',
    template: `   
              <a (click)="click()">
                <span class="glyphicon glyphicon-edit gi-1-5x" aria-hidden="true"></span>
              </a>
              `
})

export class ClickableAuditTrailComponent implements ICellRendererAngularComp {
    private params: any;
    public cell: any;
    refresh: any;

    @Output() onClicked = new EventEmitter<boolean>();

    constructor(private parentRouter: Router, private auditTrailService: AuditTrailService) {
    }

    agInit(params: any): void {
        this.params = params;
        this.cell = { row: params.value, col: params.colDef.headerName };
    }

    click(): void {

        this.onClicked.emit(this.cell);
        this.auditTrailService.doNotreload = true; 
        this.auditTrailService.data = this.cell.row;
        this.parentRouter.navigate(['/auditTrailValues']);
    }
}

