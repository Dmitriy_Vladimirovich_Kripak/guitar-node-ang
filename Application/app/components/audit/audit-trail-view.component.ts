﻿import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { AuditTrailValues, AuditTrail } from '../../manage/model/audit.model';
import { AuditTrailService } from '../../services/audit-trail.service'; 

@Component({
    selector: 'audit-trail-view',
    templateUrl: './audit-trail-view.component.html'
})

export class AuditTrailViewComponent {
    @Input() auditTrailValues: Array<AuditTrailValues>; 

    constructor(
        private auditTrailService: AuditTrailService,
        private location: Location,
        private route: Router
    ) {
        
        this.auditTrailValues = auditTrailService.data;
    }

    private ngOnInit() {   
       
    } 

    goBack() {
        this.route.navigate(['/auditTrail']);
        //this.location.back();
    }
}