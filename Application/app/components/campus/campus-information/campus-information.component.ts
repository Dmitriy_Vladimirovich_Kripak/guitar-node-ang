import { Component, OnInit } from '@angular/core';
import { CampusInfoService } from '../../../services/campus-info.service';
import { CampusInfoModel } from 'models/campusInfo.model';
import { AppComponent } from '../../../app.component';
import { LoginService } from '../../../services/login-service';
import { NotificationStatusService } from 'services/notification-status.service';


@Component({
    selector: 'campus-information',
    templateUrl: './campus-information.component.html',
    styleUrls: ['./campus-info.component.css']
})
export class CampusInformationComponent {

    isEdit: boolean;
    isAdmin: boolean;
    campusInfo: CampusInfoModel;
    constructor(private campusServer: CampusInfoService,
        private loginService: LoginService,
        private notificationStatusService: NotificationStatusService,
    ) {
        this.isAdmin = loginService.isAdmin();
    }

    ngOnInit() {
        this.campusInfo = new CampusInfoModel();
        this.campusServer.getAllCampusInfo().subscribe(response => {
            this.campusInfo = response.Data;
        })
    }

    ngAfterViewInit() {
        $('.flexslider').flexslider({
            animation: "fade",
        });

        function toggleChevron(e) {
            $(e.target)
                .prev('.panel-heading')
                .find("i.indicator")
                .toggleClass('fa fa-minus-circle fa fa-plus-circle');
        }
        $('#accordion').on('hidden.bs.collapse', toggleChevron);
        $('#accordion').on('shown.bs.collapse', toggleChevron);
    }

    editCampus() {
        this.campusServer.updateCampusInfo(this.campusInfo).subscribe(response => {
            this.campusInfo = response.Data;
            if (this.campusInfo.Id && response.Status == "Ok") {
                this.notificationStatusService.displayOkMessage("Campus Info was updated", "Success")
                this.isEdit = false;
            }
            else {
                this.notificationStatusService.displayErrorMessage(response.Error, "Campus Info was not updated")
            }
        })
    }
    return() {
        this.isEdit = false;
    }
    edit() {
        this.isEdit = true;
    }
}
