import { Component, OnInit } from '@angular/core'; 
import { Router, ActivatedRoute } from '@angular/router';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ProjectModel } from 'manage/project/project.model';
    
export interface DialogModel{
    project:ProjectModel;
}

@Component({
    selector: 'file-save-dialog',
    template: `
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" 
            (onHide)="handler('onHide', $event)">
                <h4 class="modal-title pull-left">{{title}}</h4>
                <button type="button" class="close pull-right" aria-label="Close" (click)="closeModel()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>File Name</label>
                <input *ngIf="placeHolderModel || fileName " type="text" [(ngModel)]="fileName"  placeholder="{{placeHolderModel}}" class="form-control" />
            </div>
        
            <div class="modal-footer">
                <button type="button" class="btn btn-success" (click)="confirm()">Save</button>
                <button type="button" class="btn btn-danger" (click)="closeModel()">Close</button>
            </div>
        </div>
    </div>
  `      
})

export class FileSaveDialogComponent extends DialogComponent<DialogModel, string> implements DialogModel,OnInit{
    project: ProjectModel;
    fileName:string;
    placeHolderModel:string;
    constructor(dialogService: DialogService) {
        super(dialogService); 
     }

    ngOnInit(){
        this.placeHolderModel = this.project.Name + ' - ' + this.getDate();
    }

    confirm() {
        if(!this.fileName)
        {
            this.fileName = this.project.Name + ' - ' + this.getDate();
        }
        this.result = this.fileName;
        this.close();
    }     

    closeModel(){
        this.result = null;
        this.close();
    }

    getDate(){
        let ddStr;
        let mmStr;      
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            ddStr = '0'+dd
        } 

        if(mm<10) {
            mmStr = '0'+mm
        } 

        return  mmStr + '/' + ddStr + '/' + yyyy;     
    }

}
