import { Component } from '@angular/core';
import { UserManageService } from '../../services/user-manage.service'
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router, ActivatedRoute } from '@angular/router';
@Component({  
    templateUrl: './calculation.component.html'
})

export class CalculationComponent {  
    indexComponent: number;

    public constructor(private userManageService: UserManageService,
                       private parentRouter: Router, private route: ActivatedRoute) {      
        
    }

    ngOnInit(){
        this.indexComponent = 3;
    }
    Show(index: number)
    {
        this.indexComponent = index;
    }
    goToBaseLine(){
        this.parentRouter.navigate(['/edit-baseline-variables']);
    }
}
