import { Component, Input, ChangeDetectorRef, AfterViewInit, Output, EventEmitter, AfterViewChecked } from '@angular/core';
import { UserManageService } from '../../services/user-manage.service'
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'range-component',
    templateUrl: './range.component.html'
})

export class RangeComponent implements AfterViewChecked {

    @Input() step: number;
    @Input() max: number;
    @Input() min: number;
    @Input('label') set initialLabel(newValue) {
        this.label = newValue;
        if (!this.Id) {
            this.initialRangeComponent();
        }
        this.Id = this.label.replace(" ", "");
    }
    @Input() model: any;
    @Input() isASF: boolean = false;
    @Input() programASFValue: number;
    @Input() className: string;
    @Input() GFA: number;

    private label: string;
    private Id: string;
    private asfValue: number;
    @Output() rangeChanged: EventEmitter<any> = new EventEmitter<any>();
    @Output() updateAFS: EventEmitter<any> = new EventEmitter<any>();

    constructor(private cdRef: ChangeDetectorRef,
    ) {
    }

    ngOnInit() {
    }


    ngAfterViewChecked() {
        this.initialRangeComponent();
    }

    updateProgramASFValue(newValue: any) {
        if (newValue.value) {
            this.programASFValue = newValue.valuASF;

            this.cdRef.detectChanges();
            this.updateAFS.emit(newValue);
        }
        else {
            var item = {
                value: newValue / this.GFA,
                valuASF: newValue
            }

            //This block did not allow to display the incorrect value %
            $('#' + this.Id).val(item.value).change();
            this.programASFValue = newValue;

            this.cdRef.detectChanges();
            this.updateAFS.emit(item);
        }

    }
    updateASF(value) {
        var item = {
            value: value,
            valuASF: Math.round((value * this.GFA))
        }
        this.updateProgramASFValue(item);
    }

    initialRangeComponent() {
        let that = this;
        if (this.Id) {
            var $document = $(document);
            var selector = '#' + that.Id;
            var $element = $(selector);
            // For ie8 support
            var textContent = ('textContent' in document) ? 'textContent' : 'innerText';

            // Basic rangeslider initialization
            $element.rangeslider({

                // Deactivate the feature detection
                polyfill: false,
                rangeClass: 'rangeslider',

                // Callback function
                onInit: function () { },

                // Callback function
                onSlide: function (position, value) {
                    that.model = value;
                    that.cdRef.detectChanges();
                },

                // Callback function
                onSlideEnd: function (position, value) {
                    that.model = value;
                    that.cdRef.detectChanges();
                    if (that.isASF) {
                        that.updateASF(value);
                    }
                    else
                        that.rangeChanged.emit(value);
                }

            });


        }

    }


}
