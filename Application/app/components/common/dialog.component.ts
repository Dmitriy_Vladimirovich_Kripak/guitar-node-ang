import { Component } from '@angular/core'; 
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
    
@Component({
    selector: 'custom-dialog',
    template: `
    <div class="modal-dialog" style="z-index : 9999">
        <div class="modal-content">
            <div class="modal-header" 
            (onHide)="handler('onHide', $event)">
                <h4 class="modal-title pull-left">{{title}}</h4>
                <button type="button" class="close pull-right" aria-label="Close" (click)="closeModel()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>Project Name</label>
                <input type="text" [(ngModel)]="projectName"  placeholder="Entry project name" class="form-control" />  
            </div>
        
            <div class="modal-footer">
                <button type="button" class="btn btn-success" (click)="confirm()">Save</button>
                <button type="button" class="btn btn-danger" (click)="closeModel()">Close</button>
            </div>
        </div>
    </div>   
  `      
})

export class CustomDialogComponent extends DialogComponent<null, string> {
    isSave: false;
    public title: string;
    projectName: string;
    public list: any[] = [];
    constructor(dialogService: DialogService) {
        super(dialogService);
    }

    confirm() {
        this.result = this.projectName;
        this.close();
    } 

    closeModel(){
        this.close();
    }

}
