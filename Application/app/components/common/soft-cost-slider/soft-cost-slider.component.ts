﻿import { Component, EventEmitter, Input, Output, AfterViewChecked, ChangeDetectorRef } from '@angular/core';

@Component({
    templateUrl: './soft-cost-slider.component.html',
    selector: 'soft-cost-slider',
    styleUrls: ['./soft-cost-slider.component.css']
})

export class SoftCostSliderComponent implements AfterViewChecked {
    @Input() caption: string;
    @Input('Id') set setId(newValue) {
        this.Id = newValue;
        if (!this.Id) {
            this.initialRangeComponent();
        }
    }
    @Input('maxValue') set setMaxValue(value) {
        this.maxValue = value;
        var selector = '#' + this.Id;
        var $element = $(selector);
        $element.rangeslider('update', true);
    }

    @Input('programASFValue') set setProgramASFValue(value) {
        this.programASFValue = value;
        this.calculatePercentValue();
        this.cdRef.detectChanges();
    }

    @Input('programPercentValue') set setProgramPercentValue(value) {
        this.programPercentValue = value;
        this.cdRef.detectChanges();
    }

    @Input() gsfValue: number;
    @Input() className: string;

    private Id: any;
    private maxValue: number;
    programPercentValue: number;
    private isInit: false;
    private programASFValue: number;
    stepSize: number = 0.001;
    endOfScroll: boolean = false;

    //Variable wich allow to determine end of rangeslider,
    //and set the maxValue of programASFValue. 
    endScrollFormulasPage: number = 16;
    endScrollProjectsPage: number = 7;

    @Output() programASFValueUpdated: EventEmitter<number> = new EventEmitter<number>();

    constructor(private cdRef: ChangeDetectorRef) {

    }
    ngOnInit() {
        this.calculatePercentValue();
    }

    ngAfterViewChecked() {
        this.initialRangeComponent();
    }

    calculatePercentValue() {
        if (this.programASFValue < 0)
            this.programASFValue = - this.programASFValue;
        this.programPercentValue = parseFloat((this.programASFValue / this.gsfValue).toFixed(3));

        //This block did not allow to display the incorrect value %
        this.programPercentValue <= this.maxValue ?
            $('#' + this.Id).val(this.programPercentValue).change() :
            "";
    }

    updatePercentValue(newValue: number) {
        if (newValue < 0) {
            newValue = - newValue;
            this.programASFValue = newValue;
        }
        this.programASFValue = +(this.programPercentValue * this.gsfValue).toFixed(1);
        this.programASFValueUpdated.emit(this.programASFValue);
    }

    updateASFValue(newValue: number) {
        this.calculatePercentValue();
        if (newValue < 0) {
            newValue = - newValue;
            this.programASFValue = newValue;
        }
        this.programASFValueUpdated.emit(this.programASFValue);
        this.cdRef.detectChanges();
    }

    ngOnChanges(changes: any) {
        this.updateASFValue(this.programASFValue);
    }

    parseDouble(event: any) {
        if (event) {
            event.currentTarget.value[0] === '.' ?
                event.currentTarget.value = "0" + event.currentTarget.value :
                "";
        }
    }

    initialRangeComponent() {
        let that = this;
        if (this.Id) {
            var $document = $(document);
            var selector = '#' + this.Id;
            var $element = $(selector);

            // For ie8 support
            var textContent = ('textContent' in document) ? 'textContent' : 'innerText';

            // Basic rangeslider initialization
            $element.rangeslider({

                // Deactivate the feature detection
                polyfill: false,
                rangeClass: 'rangeslider',
                update: true,

                // Callback function
                onInit: function () { },

                // Callback function
                onSlide: function (position, value) {
                    that.programPercentValue = value;

                    if (!that.cdRef['destroyed']) {
                        that.cdRef.detectChanges();
                    }
                },

                // Callback function
                onSlideEnd: function (position, value) {
                    //Take the last slider which is in the section "Program Parameters" 
                    var slidersOnPage = $(".rangeslider--horizontal");
                    var sliderWidth = slidersOnPage[slidersOnPage.length - 1].clientWidth;

                    if (sliderWidth - position == that.endScrollFormulasPage ||
                        sliderWidth - position == that.endScrollProjectsPage) {
                        that.endOfScroll = true;
                        //that.programPercentValue = that.maxValue;
                        that.cdRef.detectChanges();
                        //that.updatePercentValue(that.maxValue);
                        that.updatePercentValue(that.programPercentValue);
                    }
                    else {
                        that.endOfScroll = false;
                        that.programPercentValue = value;
                        that.cdRef.detectChanges();
                        that.updatePercentValue(value);
                    }
                }
            });
        }
    }
}
