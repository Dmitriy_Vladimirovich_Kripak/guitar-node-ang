import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router } from '@angular/router';
import { NotificationStatusService } from '../../services/notification-status.service'
import { ErrorModel } from 'common/error.model'
@Component({  
    selector: 'tree-error-grid',
    templateUrl: './tree-error-grid.component.html'
})

export class TreeGridComponent {  
    @Input() set errorArray(errors: any) {
        this.errorList = errors;
    }
    @Input() set hideError(isShow: boolean) {
        this.hideThis = isShow;
    }
    errorList: Array<ErrorModel> = new Array();
    public filterQuery = "";
    public rowsOnPage = 10;
    public sortBy = "Name";
    public sortOrder = "asc";
    public hideThis: boolean;

    public constructor(private parentRouter: Router) {       
    }
    ngOnInit(): void {
    }  
    showInner() {
        this.hideThis = false;
    }

}
