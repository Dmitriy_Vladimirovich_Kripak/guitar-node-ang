import { Component, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import { DataFilterPipe } from '../../services/data-filter.pipe';
import { Router } from '@angular/router';
import { NotificationStatusService } from '../../services/notification-status.service'
import { ErrorModel } from 'common/error.model'
import { TreeGridComponent } from './tree-error-grid.component'
@Component({  
    selector: 'all-error',
    templateUrl: './all-errors.component.html'
})

export class AllErrorComponent {  
    @Input() set errorArray(errors: any) {
        this.errorList = errors;
    }
    errorList: Array<ErrorModel> = new Array();
    indexSubError: any;
    public filterQuery = "";
    public rowsOnPage = 10;
    public sortBy = "Name";
    public sortOrder = "asc";    
    icons: string = "+";
    public constructor(private parentRouter: Router, private notificationStatusService: NotificationStatusService) {       
        this.notificationStatusService.SendErrorList.subscribe((array) => {
            this.errorList = array;
        });
        this.notificationStatusService.getErrorList();
    }

    ngOnInit(): void {
    }  
    showInner(index: any, el: ElementRef) {
        if (this.indexSubError == index)
            this.indexSubError = -1;                   
        else          
            this.indexSubError = index;
    }

}
