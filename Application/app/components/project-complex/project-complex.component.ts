import { Component, OnInit, Input, Output, ChangeDetectorRef, EventEmitter, ViewChild, AfterViewInit, HostListener, ElementRef } from '@angular/core';
import { EnumHelperService } from '../../common/enum-helper.service';
import {
    CalculationType, ProjectDeliveryStrategyFactor, LocationFactor, BuildingType, SoilCondition, SiteConstraint, ArchitecturalExpectation,
    PerformanceExpectation, Plumbing, HVAC, BuildingPlant, FireProtection, Electrical, EscalateTo, FacilityRemediation, FacilityDemolition, SitePreparation,
    SiteDevelopment, SiteInfrastructureServices, SiteServices, SiteInfrastructureCivil, SiteCivil, C10_E20SettingsViewModel, FloorRoofConstractionPossibleValues,
} from '../../calculation/view.models';
import { ProjectViewResultModel } from '../../calculation/project-view-result.model';
import { ProjectViewModel } from '../../calculation/project.model';
import { FormGroup } from '@angular/forms';
import { ACalculationService } from '../../calculation/a-calulation.service';
import { IMyDpOptions, IMyDateModel, IMyDate } from 'mydatepicker';
import { NotificationStatusService } from '../../services/notification-status.service';
import { ProjectModel } from '../../manage/project/project.model';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageService } from 'services/storage-service';
import { ProjectComplexValueModel } from 'manage/project/projectComplexValue.model';
import { ProjectService } from 'services/project.service ';
import { CampusInfoModel } from 'models/campusInfo.model';
import { DialogModalService } from 'services/dialog-modal.service';
import { retry } from 'rxjs/operator/retry';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Element } from '@angular/compiler';



@Component({
    selector: 'project-page',
    templateUrl: './project-complex.component.html',
    styleUrls: ['./project-complex.component.css']
})
export class ProjectComplexComponent implements AfterViewInit {

    model: ProjectViewModel;
    disableGeneraFile: boolean;
    pageIsLoad: boolean;
    project: ProjectModel;
    campusInfo: CampusInfoModel;
    resultView: ProjectViewResultModel;
    floorRoofConstractionPossibleValues: FloorRoofConstractionPossibleValues;
    locationFactors: any[];
    projectDeliveryStrategyFactors: any[];
    buildingTypes: any[];
    soilConditions: any[];
    architecturalExpectations: any[];
    performanceExpectations: any[];
    siteConstraints: any[];
    plumbings: any[];
    HVACs: any[];
    buildingPlants: any[];
    fireProtections: any[];
    electricals: any[];
    escalateToValues: any[];
    facilityRemediation: any[];
    facilityDemolition: any[];
    sitePreparation: any[];
    siteDevelopment: any[];
    siteInfrastructureServices: any[];
    siteServices: any[];
    siteInfrastructureCivil: any[];
    siteCivil: any[];
    floorConstructionTypes: any[];
    roofConstructionTypes: any[];
    isResult: boolean;
    title: string;
    label: string;
    isDisplayFixedTotals: boolean;
    constructionPercent: string;
    escalationPercent: string;
    isChildFormValid: boolean;
    childFormErrMsg: string;
    minEstimateDate: any;

    @Output() resultViewEmit: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('projectForm') projectForm: FormGroup;

    //To fix view by scrolling
    @HostListener("window:scroll", ["$event"])
    onWindowScroll(eventObject) {
        document.documentElement.scrollTop >= 190 ?
            this.isDisplayFixedTotals = true :
            this.isDisplayFixedTotals = false;
    }

    /*------------------------------COREL+SHELL variables--------------------------------*/
    viewSettings: C10_E20SettingsViewModel;
    assignableProgramsStatus: string;
    grossUpProgramsStatus: string;
    totalAssignableProgramsASF: number;
    totalGrossUpProgramsASF: number;

    stepSize: number;
    buildingEfficiencyASF: number;
    designFeeASF: number;
    ffeASF: number;
    technologyASF: number;
    contingencyASF: number;
    miscASF: number;
    contingenciesDesignASF: number;
    minBuildingEfficiency: number;
    maxBuildingEfficiency: number;
    shellSpaceIndex: number;
    isInit: boolean;
    allowCalculate: boolean;
    status: string;

    /*------------------------------END COREL+SHELL variables--------------------------------*/

    /*------------------------------- Taxes Escalations Fees variables---------------------------------------*/

    yourModelDate: Date;

    /*------------------------------------END Taxes Escalations Fees variables----------------------------------*/

    /*------------------------------- Soft Costs variables---------------------------------------*/
    minDesignFee: number;
    maxDesignFee: number;
    minFFE: number;
    maxFFE: number;
    minTechnology: number;
    maxTechnology: number;
    minContingency: number;
    maxContingency: number;
    minMisc: number;
    maxMisc: number;
    minContingenciesDesign: number;
    maxContingenciesDesign: number;
    /*------------------------------- END Soft Costs variables---------------------------------------*/

    /*------------------------------- Component lifecycle methods------------------------------------*/

    constructor(private service: ACalculationService,
        private enumHelperService: EnumHelperService,
        private notificationService: NotificationStatusService,
        private cdRef: ChangeDetectorRef,
        private router: Router,
        private activateRoute: ActivatedRoute,
        private storageService: StorageService,
        private projectService: ProjectService,
        private dialogService: DialogModalService, ) {        
        this.setFieldsToDefault();
        this.contingenciesToPercent(this.model.Z920Percent, this.model.Z930Percent);
    }

    ngOnInit() {
       this.setMinimumEstimateDate();
        this.activateRoute.params.subscribe(params => {
            let projectId = +params['id'];
            if (projectId) {
                this.projectService.getProjectById(projectId, this.storageService.getUser().Id).subscribe(response => {
                    if (response.Status == "Ok") {
                        this.model = response.Data.ProjectValue;
                        this.setEstimateDate(response.Data.ProjectValue.EstimateDateStr, response.Data.ProjectValue.ConstructionStartDateStr);
                        this.softCostsInit(this.model);
                        this.project = response.Data.Project;
                        if (!this.project.Institution)
                            this.project.Institution = this.storageService.campusInfo.InstitutionName;
                        this.onBuildingTypeChange(this.model.BuildingType);
                        this.title = "Project:"
                        this.label = this.project.Name;
                    }
                    else {
                        if (response.Error.InnerException) {
                            var exception = response.Error.InnerException;
                            var longErrMsg = "";
                            //Get all inner exception messages
                            while (true) {
                                longErrMsg += exception.Message + "\n";
                                if (!exception.InnerException) {
                                    break;
                                }
                                exception = exception.InnerException
                            }
                            this.notificationService.displayErrorMessage(longErrMsg, "Edit project");
                        }
                        else
                            this.notificationService.displayErrorMessage(response.Error, "Edit project");
                    }
                    this.allowCalculate = true;
                    this.initiateCalc();
                });
                this.disableGeneraFile = true;
            }
            else {
                this.project.Institution = this.storageService.campusInfo.InstitutionName;
                this.title = "";
                this.label = "Create New Project"
                this.onBuildingTypeChange(this.model.BuildingType);
                this.softCostsInit(this.model);
                this.setEstimateDate();
            }
       
        });
        this.campusInfo = this.storageService.campusInfo;
    }

    ngAfterViewInit() {
        this.pageIsLoad = true;

        $('.datePick').datepicker({
            format: "dd/mm/yyyy"
        })
    }

    /*-------------------------------End of Component lifecycle methods------------------------------------*/

    setFieldsToDefault() {
        this.model = new ProjectViewModel();
        this.disableGeneraFile = true;
        this.pageIsLoad = false;
        this.project = new ProjectModel();
        this.campusInfo = new CampusInfoModel();
        this.resultView = new ProjectViewResultModel();
        this.floorRoofConstractionPossibleValues = new FloorRoofConstractionPossibleValues();
        this.locationFactors = EnumHelperService.getValues(LocationFactor, true);
        this.projectDeliveryStrategyFactors = EnumHelperService.getValues(ProjectDeliveryStrategyFactor, true);
        this.buildingTypes = EnumHelperService.getValues(BuildingType, true);
        this.soilConditions = EnumHelperService.getValues(SoilCondition, true);
        this.architecturalExpectations = EnumHelperService.getValues(ArchitecturalExpectation, true);
        this.performanceExpectations = EnumHelperService.getValues(PerformanceExpectation, true);
        this.siteConstraints = EnumHelperService.getValues(SiteConstraint, true);
        this.plumbings = EnumHelperService.getValues(Plumbing, true);
        this.HVACs = EnumHelperService.getValues(HVAC, true);
        this.buildingPlants = EnumHelperService.getValues(BuildingPlant, true);
        this.fireProtections = EnumHelperService.getValues(FireProtection, true);
        this.electricals = EnumHelperService.getValues(Electrical, true);
        this.escalateToValues = EnumHelperService.getValues(EscalateTo, true);
        this.facilityRemediation = EnumHelperService.getValues(FacilityRemediation, true);
        this.facilityDemolition = EnumHelperService.getValues(FacilityDemolition, true);
        this.sitePreparation = EnumHelperService.getValues(SitePreparation, true);
        this.siteDevelopment = EnumHelperService.getValues(SiteDevelopment, true);
        this.siteInfrastructureServices = EnumHelperService.getValues(SiteInfrastructureServices, true);
        this.siteServices = EnumHelperService.getValues(SiteServices, true);
        this.siteInfrastructureCivil = EnumHelperService.getValues(SiteInfrastructureCivil, true);
        this.siteCivil = EnumHelperService.getValues(SiteCivil, true);
        this.floorConstructionTypes = this.floorRoofConstractionPossibleValues.FloorPossibleTypes.Item(BuildingType[this.model.BuildingType]);
        this.floorConstructionTypes = EnumHelperService.addSpacesToBuildingType(this.floorConstructionTypes);
        this.roofConstructionTypes = this.floorRoofConstractionPossibleValues.RoofPossibleTypes.Item(BuildingType[this.model.BuildingType]);
        this.roofConstructionTypes = EnumHelperService.addSpacesToBuildingType(this.roofConstructionTypes);
        this.isResult = false;
        this.title = "";
        this.label = "";
        this.constructionPercent = "";
        this.escalationPercent = "";
        this.isChildFormValid = true;
        this.childFormErrMsg = "";

        /*------------------------------COREL+SHELL variables--------------------------------*/
        this.viewSettings = new C10_E20SettingsViewModel();
        this.assignableProgramsStatus = '';
        this.grossUpProgramsStatus = '';
        this.totalAssignableProgramsASF = 0;
        this.totalGrossUpProgramsASF = 0;

        this.stepSize = 0.001;
        this.minBuildingEfficiency = 0.5;
        this.maxBuildingEfficiency = 0.75;
        this.shellSpaceIndex = 0;
        this.isInit = true;
        this.allowCalculate = false;
        this.status = '';

        /*------------------------------END COREL+SHELL variables--------------------------------*/

        /*------------------------------- Taxes Escalations Fees variables---------------------------------------*/

        this.yourModelDate = new Date();

        /*------------------------------------END Taxes Escalations Fees variables----------------------------------*/

        /*------------------------------- Soft Costs variables---------------------------------------*/
        this.minDesignFee = 0.00;
        this.maxDesignFee = 0.20;
        this.minFFE = 0.00;
        this.maxFFE = 0.20;
        this.minTechnology = 0.00;
        this.maxTechnology = 0.20;
        this.minContingency = 0.00;
        this.maxContingency = 0.20;
        this.minMisc = 0;
        this.maxMisc = 0.20;
        this.minContingenciesDesign = 0;
        this.maxContingenciesDesign = 1;
    }

    /*-------------------------------DatePicker-------------------------*/

    private setMinimumEstimateDate(){
        this.service.getBaselineVariables(CalculationType.Z920BC).subscribe((res) => {
            if (res.Status == "Ok") {
                let baseLineDataDate = res.Data.BaselineVariables[5].Value;
                this.minEstimateDate = this.decrementDate(baseLineDataDate);
            }
            else {
                this.minEstimateDate = {
                    year: 2017,
                    month: 12,
                    day: 31
                }
            }
            this.myDatePickerOptions.disableUntil = this.minEstimateDate;
        });      
    }

    private decrementDate(data: any): IMyDate {
        var initialDate = Date.parse(data) - 1;
        var time = new Date();
        time.setTime(initialDate);

        return {
            year: time.getUTCFullYear(),
            month: time.getUTCMonth() + 1,
            day: time.getUTCDate()
        };
    }

    onEstimateDateChanged(event: IMyDateModel): void {
        this.model.EstimateDateStr = event.formatted.replace(/\./g, "/");
    }
    onConstructionStartDateChanged(event: IMyDateModel): void {
        this.model.ConstructionStartDateStr = event.formatted.replace(/\./g, "/");
    }

    setRangeValue(nameOfPropetry, value) {
        this.model[nameOfPropetry] = value;
        console.log(nameOfPropetry + "-" + this.model[nameOfPropetry]);
        this.cdRef.detectChanges()
    }

    private myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'mm.dd.yyyy'        
    };

    /*-------------------------------END DatePicker-------------------------*/

    /*----------------Construction/Escalation calculation------------------*/
    setContingenciesDesign(val) {
        this.model.ContingenciesDesign = val;
    }

    contingenciesToPercent(Z920Percent: number, Z930Percent: number) {
        Z920Percent ? this.model.Z920Percent = +(Z920Percent * 100).toFixed(2) : this.model.Z920Percent = 15.75;
        Z930Percent ? this.model.Z930Percent = +(Z930Percent * 100).toFixed(2) : this.model.Z930Percent = 3.00;
        this.updateGFAValue();
    }

    setEstimateDate(dataDateStr: string = null, constructionStartDate: string = null) {
        if (!dataDateStr) {
            this.service.getBaselineVariables(CalculationType.Z920BC).subscribe((res) => {
                if (res.Status == "Ok") {
                    let baseLineDataDate = res.Data.BaselineVariables[5].Value;
                    var dateArray = baseLineDataDate.split("/", 3);
                    this.model.EstimateDate = {
                        date: {
                            month: +dateArray[0].replace("0", ""),
                            day: +dateArray[1].replace("0", ""), year: +dateArray[2]
                        }
                    };

                    this.model.EstimateDateStr = baseLineDataDate;
                    //Set the default value of ConstructionStartDate for new proj (EstimateDate + 1 year)
                    this.model.ConstructionStartDate = {
                        date: {
                            month: +dateArray[0].replace("0", ""),
                            day: +dateArray[1].replace("0", ""), year: (+dateArray[2] + 1)
                        }
                    };
                    this.model.ConstructionStartDateStr = dateArray[0] + "/" + dateArray[1] + "/" + (+dateArray[2] + 1);
                }
                else {
                    if (res.Error.InnerException)
                        this.notificationService.displayErrorMessage(res.Error.Message + " " +
                            res.Error.InnerException.Message, "Get View Settings");
                    else
                        this.notificationService.displayErrorMessage(res.Error, "Get View Settings");
                }
                this.allowCalculate = true;
                this.initiateCalc();
            });
        }
        else {
            var dateArray = dataDateStr.split("/", 3);
            this.model.EstimateDate = {
                date: {
                    month: dateArray[0].replace("0", ""),
                    day: dateArray[1].replace("0", ""),
                    year: dateArray[2]
                }
            };
            this.model.EstimateDateStr = dataDateStr;

            var startDateArray = constructionStartDate.split("/", 3);
            this.model.ConstructionStartDate = {
                date: {
                    month: startDateArray[0].replace("0", ""),
                    day: startDateArray[1].replace("0", ""),
                    year: startDateArray[2]
                }
            };
            this.model.ConstructionStartDateStr = constructionStartDate;
        }
    }

    /*----------------End Construction/Escalation calculation------------------*/

    calc(projectForm: FormGroup, eventObject: any = null) {
        this.parseDouble(eventObject);
        if (this.isFormValidBeforeCalculation(projectForm, eventObject) && this.allowCalculate) {
            this.service.calcProject(this.model).subscribe((response) => {
                if (response.Status == "Ok") {
                    this.resultView = response.Data;
                    this.model.Z920Percent = +(this.resultView.Z920Percent * 100).toFixed(2);
                    this.model.Z930Percent = +(this.resultView.Z930Percent * 100).toFixed(2);
                    this.isResult = true;
                    this.notificationService.displayOkMessage("Project was calculated", "Success");
                }
                else {
                    if (response.Error.InnerException)
                        this.notificationService.displayErrorMessage(response.Error.Message + " " + response.Error.InnerException.Message, "Calculate All Formula");
                    else
                        this.notificationService.displayErrorMessage(response.Error, "Calculate All Formula");
                }

            }, (error) => {
                this.notificationService.displayErrorMessage(error, "Calculate All Formula");
            });
            this.resultViewEmit.emit(this.model);
            if (this.project.Name) {
                this.title = this.project.Name !== "" ? "Project:" : "";
                this.label = this.project.Name !== "" ? this.project.Name : "Create New Project";
                this.cdRef.detectChanges();
            }
        }
    }

    initiateCalc() {
        this.calc(this.projectForm);
    }

    /*------------------------------COREL+SHELL METHODS --------------------------------*/

    public options = {
        position: ["bottom", "right"],
        timeOut: 0,
        lastOnBottom: true,
    };

    softCostsInit(model: ProjectViewModel) {
        this.model.DesignFeeASF = +(model.DesignFee * 100).toFixed(1);
        this.model.FfeASF = +(model.FFE * 100).toFixed(1);
        this.model.TechnologyASF = +(model.Technology * 100).toFixed(1);
        this.model.ContingencyASF = +(model.Contingency * 100).toFixed(1);
        this.model.MiscASF = +(model.Misc * 100).toFixed(1);
        this.model.ContingenciesDesignASF = +(model.ContingenciesDesign * 100).toFixed(1);
    }

    notificationAboutCalculate() {
        this.notificationService.displayOkMessage('For update formula You should click "Calculate" button ', "Warning");
        this.resultViewEmit.emit(this.model);
    }

    onBuildingTypeChange(newValue: number) {
        this.model.BuildingType = newValue;
        this.updateBuildingEfficiencyASF();
        this.service.getC10E20ViewSettings(this.model.BuildingType).subscribe((res) => {
            if (res.Status == "Ok") {
                this.viewSettings = res.Data;
                this.isInit = false;
                this.prepareAssignablyProgramValues();
                this.updateUIAfterChanges();
                this.cdRef.detectChanges();
            }
            else {
                if (res.Error.InnerException)
                    this.notificationService.displayErrorMessage(res.Error.Message + " " + res.Error.InnerException.Message, "Get View Settings");
                else
                    this.notificationService.displayErrorMessage(res.Error, "Get View Settings");
            }
        }, (error) => {
            this.notificationService.displayErrorMessage(error, "Get View Settings");
        });

        /*------------------Possible Values By BuildingType-------------------------*/

        this.floorConstructionTypes = this.floorRoofConstractionPossibleValues.FloorPossibleTypes.Item(BuildingType[this.model.BuildingType]);
        this.roofConstructionTypes = this.floorRoofConstractionPossibleValues.RoofPossibleTypes.Item(BuildingType[this.model.BuildingType]);

        this.model.FloorConstructionType = this.floorConstructionTypes[0];
        this.model.RoofConstructionType = this.roofConstructionTypes[0];

        /*------------------End Possible Values By BuildingType---------------------*/
    }

    assignableProgramASFValueUpdated(newValue: number, index: number) {
        if (newValue < 0)
            newValue = - newValue;
        if (this.isInit) return;
        this.model['AssignableProgram' + index.toString() + 'ASF'] = newValue;
        if (index != this.shellSpaceIndex) {
            //  shellSpace program = BuildingEfficiency - all the others programs
            //  one program is shellSpace, so all programs (1 +..+ 8) - shellSpace program = all programs except shellSpace program
            this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] = this.buildingEfficiencyASF -
                (this.model.AssignableProgram1ASF + this.model.AssignableProgram2ASF + this.model.AssignableProgram3ASF + this.model.AssignableProgram4ASF
                    + this.model.AssignableProgram5ASF + this.model.AssignableProgram6ASF + this.model.AssignableProgram7ASF + this.model.AssignableProgram8ASF
                    - this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF']
                );
            if (this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] < 0) {
                this.model['AssignableProgram' + this.shellSpaceIndex.toString() + 'ASF'] = 0;
            }
        }
        for (var i = 1; i <= 8; i++) {
            this.model['AssignableProgram' + i.toString()] = parseFloat((this.model['AssignableProgram' + i.toString() + 'ASF'] / this.model.GrossSquareFeet).toFixed(3));
        }

        this.cdRef.detectChanges();
        this.updateUIAfterChanges();
    }

    grossUpProgramASFValueUpdated(newValue: number, index: number) {
        if (this.isInit) return;

        this.model['GrossUpProgram' + index.toString() + 'ASF'] = newValue;
        if (index != 4) {
            let restBuildingEfficiencyASF = this.model.GrossSquareFeet - this.buildingEfficiencyASF;
            this.model.GrossUpProgram4ASF = restBuildingEfficiencyASF - (this.model.GrossUpProgram1ASF + this.model.GrossUpProgram2ASF + this.model.GrossUpProgram3ASF);
            if (this.model.GrossUpProgram4ASF < 0) {
                this.model.GrossUpProgram4ASF = 0;
            }
        }
        for (var i = 1; i <= 4; i++) {
            this.model['GrossUpProgram' + i.toString()] = parseFloat((this.model['GrossUpProgram' + i.toString() + 'ASF'] / this.model.GrossSquareFeet).toFixed(3));
        }

        this.cdRef.detectChanges();
        this.updateUIAfterChanges();
    }

    updateBuildingEfficiency() {
        this.model.BuildingEfficiency = parseFloat((this.buildingEfficiencyASF / this.model.GrossSquareFeet).toFixed(3));
    }

    updateBuildingEfficiencyASF() {
        this.buildingEfficiencyASF = Math.round((this.model.BuildingEfficiency * this.model.GrossSquareFeet));
    }

    updateGFAValue() {
        this.updateBuildingEfficiencyASF();
        this.model.AssignableProgram1ASF = Math.round((this.model.AssignableProgram1 * this.model.GrossSquareFeet));
        this.model.AssignableProgram2ASF = Math.round((this.model.AssignableProgram2 * this.model.GrossSquareFeet));
        this.model.AssignableProgram3ASF = Math.round((this.model.AssignableProgram3 * this.model.GrossSquareFeet));
        this.model.AssignableProgram4ASF = Math.round((this.model.AssignableProgram4 * this.model.GrossSquareFeet));
        this.model.AssignableProgram5ASF = Math.round((this.model.AssignableProgram5 * this.model.GrossSquareFeet));
        this.model.AssignableProgram6ASF = Math.round((this.model.AssignableProgram6 * this.model.GrossSquareFeet));
        this.model.AssignableProgram7ASF = Math.round((this.model.AssignableProgram7 * this.model.GrossSquareFeet));
        this.model.AssignableProgram8ASF = Math.round((this.model.AssignableProgram8 * this.model.GrossSquareFeet));

        this.model.DesignFeeASF = Math.round((this.model.DesignFee * 100));
        this.model.FfeASF = Math.round((this.model.FFE * 100));
        this.model.TechnologyASF = Math.round((this.model.Technology * 100));
        this.model.ContingencyASF = Math.round((this.model.Contingency * 100));
        this.model.MiscASF = Math.round((this.model.Misc * 100));
        this.model.ContingenciesDesignASF = Math.round((this.model.ContingenciesDesign * 100));

        this.model.GrossUpProgram1ASF = Math.round((this.model.GrossUpProgram1 * this.model.GrossSquareFeet));
        this.model.GrossUpProgram2ASF = Math.round((this.model.GrossUpProgram2 * this.model.GrossSquareFeet));
        this.model.GrossUpProgram3ASF = Math.round((this.model.GrossUpProgram3 * this.model.GrossSquareFeet));
        this.model.GrossUpProgram4ASF = Math.round((this.model.GrossUpProgram4 * this.model.GrossSquareFeet));

        this.calc(this.projectForm);
    }

    updateUIAfterChanges() {
        this.updateBuildingEfficiency();
        this.calculateAllPrograms();
        this.updateAssignableStatus();
        this.updateGrossUpStatus();
        if (this.assignableProgramsStatus) {
            this.notificationService.displayErrorMessage(this.assignableProgramsStatus, "Program Parameters");
            this.assignableProgramsStatus = null;
        }
        if (this.grossUpProgramsStatus) {
            this.notificationService.displayErrorMessage(this.grossUpProgramsStatus, "Program Parameters");
            this.grossUpProgramsStatus = null;
        }
        else {
            this.calc(this.projectForm);
        }
    }

    calculateAllPrograms() {
        this.totalAssignableProgramsASF = 0;
        for (var i = 1; i <= 8; i++) {
            var temp = this.model['AssignableProgram' + i.toString() + 'ASF'];
            this.totalAssignableProgramsASF = this.totalAssignableProgramsASF + temp;
        }
        this.totalGrossUpProgramsASF = 0;
        for (var i = 1; i <= 4; i++) {
            this.totalGrossUpProgramsASF = this.totalGrossUpProgramsASF + this.model['GrossUpProgram' + i.toString() + 'ASF'];
        }
    }

    prepareAssignablyProgramValues() {
        for (var i = 1; i <= 8; i++) {
            this.model['AssignableProgram' + i.toString() + 'ASF'] = Math.round((this.model['AssignableProgram' + i.toString()] * this.model.GrossSquareFeet));
        }
        for (var i = 1; i <= 4; i++) {
            this.model['GrossUpProgram' + i.toString() + 'ASF'] = Math.round((this.model['GrossUpProgram' + i.toString()] * this.model.GrossSquareFeet));
        }

        for (var i = 1; i <= 8; i++) {
            if (this.isUsingByIndex(i)) {
                //last visible index is index of shellSpace program
                this.shellSpaceIndex = i;
            }
            else {// set invisible programs to 0
                this.model['AssignableProgram' + i.toString()] = 0;
            }

        }
    }

    isUsingByIndex(index: number) {
        return this.viewSettings['AssignableProgram' + index.toString() + 'IsUsing'];
    }

    updateAssignableStatus() {
        this.assignableProgramsStatus = '';
        if (this.totalAssignableProgramsASF < (this.minBuildingEfficiency * this.model.GrossSquareFeet))
            this.assignableProgramsStatus = 'Sum of all assignable programs should be equal or more than ' + ((Math.round(this.minBuildingEfficiency * 10000)) / 100).toString() + ' %';
        if (this.totalAssignableProgramsASF > this.buildingEfficiencyASF)
            this.assignableProgramsStatus = 'Sum of all assignable programs should be equal or less than ' + ((Math.round(this.model.BuildingEfficiency * 10000)) / 100).toString() + ' %';

    }

    updateGrossUpStatus() {
        this.grossUpProgramsStatus = '';
        if (this.totalGrossUpProgramsASF < ((1 - this.maxBuildingEfficiency) * this.model.GrossSquareFeet))
            this.grossUpProgramsStatus = 'Sum of all Gross Up programs should be equal or more than ' + ((Math.round((1 - this.maxBuildingEfficiency) * 10000)) / 100).toString() + ' %';
        if (this.totalGrossUpProgramsASF > (this.model.GrossSquareFeet - this.buildingEfficiencyASF))
            this.grossUpProgramsStatus = 'Sum of all Gross Up programs should be equal or less than ' + ((Math.round((1 - this.model.BuildingEfficiency) * 10000)) / 100).toString() + ' %';

    }
   
    /*------------------------------END COREL+SHELL METHODS --------------------------------*/

    /*------------------------------------Building Constraction/Site Development/Site Utilities settings----------------------------------------*/
    changeValueCheckBox(element: HTMLInputElement) {
        this.model.SiteInfrastructureElectrical = element.checked ? true : false;
    }

    /*------------------------------- END Building Constraction/Site Development/Site Utilities settings -------------------------------------*/

    numberOfLevelsUpdated(newValue: any) {
        this.model.NumberOfLevels = newValue.numberOfLevels;
        this.model.NumberOfBelowGradeLevels = newValue.numberOfBelowGradeLevels;
        this.model.NumberOfAboveGradeLevels = newValue.numberOfAboveGradeLevels;
        this.model.BuildingFootprint = Math.round(newValue.buildingFootprint);
        this.model.GrossSquareFeet = newValue.grossSquareFeet;
        this.isChildFormValid = newValue.isValid;
        this.childFormErrMsg = newValue.errMsg;

        if (this.isChildFormValid) {
            this.recalCladdingRatio();
            this.updateGFAValue();
        }
    }

    claddingRatioUpdated(newValue: any) {
        this.model.CladdingRatio = newValue.claddingRatio;
        this.model.FFHeights = newValue.fFHeights;
        this.model.FloorPlateWidth = newValue.floorPlateWidth;
        this.model.BasementFFHeights = newValue.basementFFHeights;
    }

    recalCladdingRatio() {
        if (this.model.FloorPlateWidth > 0 && this.model.FFHeights > 0 && this.model.GrossSquareFeet > 0 && this.model.NumberOfLevels > 0 && this.model.NumberOfBelowGradeLevels > 0 && this.model.NumberOfAboveGradeLevels && this.model.BasementFFHeights) {
            this.model.CladdingRatio = ((((this.model.GrossSquareFeet / this.model.NumberOfLevels) / this.model.FloorPlateWidth + this.model.FloorPlateWidth) * 2 * this.model.BasementFFHeights * this.model.NumberOfBelowGradeLevels * 1.15) +
                ((this.model.GrossSquareFeet / this.model.NumberOfLevels) / this.model.FloorPlateWidth + this.model.FloorPlateWidth) * 2 * this.model.NumberOfAboveGradeLevels * this.model.FFHeights * 1.15) / this.model.GrossSquareFeet;
            this.model.CladdingRatio = parseFloat(this.model.CladdingRatio.toFixed(3));
        }
    }

    openReportPage() {
        if (!this.project.Id) {
            this.notificationService.displayErrorMessage("Can't generate PDF for unsaved project", "Generate PDF");
            return;
        }
        this.storageService.currentPorjectForGenerate = this.project;
        this.router.navigate(['/report-detail']);
    }

    saveProject(form: FormGroup) {
        this.validationForm(form);
        if (this.isFormValidBeforeCalculation(form)) {
            let complexProject: ProjectComplexValueModel = new ProjectComplexValueModel();
            complexProject.project = this.project;
            complexProject.projectValue = this.model;

            this.projectService.createProject(complexProject).subscribe(response => {
                if (response.Data && response.Data.Project.Id && response.Data.Project.Id > 0) {
                    this.notificationService.displayOkMessage("Project was saved", "Success");
                    this.resultView = response.Data.CalcResult;
                    this.project = response.Data.Project;
                    this.label = this.project.Name;
                }

                else {
                    if (response.Error.InnerException)
                        this.notificationService.displayErrorMessage(response.Error.Message + " " + response.Error.InnerException.Message, "Save Project");
                    else
                        this.notificationService.displayErrorMessage(response.Error, "Save Project");
                }
            }, (error: any) => {
                this.notificationService.displayErrorMessage(error, "Save Project");
            });
        }
    }

    saveAsProject(form: FormGroup, projectName) {
        if (projectName) {
            this.project.Name = projectName;
        }
        if (this.validationForm(form) &&
            this.isFormValidBeforeCalculation(form)) {
            this.project.Id = 0;
            let complexProject: ProjectComplexValueModel = new ProjectComplexValueModel();
            complexProject.project = this.project;
            complexProject.projectValue = this.model;

            this.projectService.createProject(complexProject).subscribe(response => {

                if (response.Data)
                    this.project = response.Data.Project;

                if (this.project.Id && this.project.Id > 0) {
                    this.title = "Project:";
                    this.label = this.project.Name;
                    this.notificationService.displayOkMessage("New project was saved", "Success");
                }

                else {
                    if (response.Error.InnerException)
                        this.notificationService.displayErrorMessage(response.Error.Message + " " + response.Error.InnerException.Message, "Save Project");
                    else
                        this.notificationService.displayErrorMessage(response.Error, "Save Project");
                }
            }, (error: any) => {
                this.notificationService.displayErrorMessage(error, "Save Project");
            });
        }
    }

    saveAsProjectModel(form: FormGroup) {
        this.dialogService.showProjectNameModal().subscribe(result => {
            if (!result)
                return;
            this.saveAsProject(form, result);
        })
    }

    changedASFValue(newValue) {
        if (newValue.value) {
            this.buildingEfficiencyASF = newValue.valuASF;
            this.model.BuildingEfficiency = newValue.value;
        }
        else {
            this.buildingEfficiencyASF = newValue;
        }

        this.cdRef.detectChanges();
        this.updateUIAfterChanges();
    }

    changedDesignFeeASF(newValue: number, property: string) {
        this.model[property] = newValue / 100;
    }

    parseDouble(event: any) {
        if (event && event.currentTarget.value) {
            event.currentTarget.value[0] === '.' ?
                event.currentTarget.value = "0" + event.currentTarget.value :
                "";
        }
    }

    isFormValidBeforeCalculation(personForm: FormGroup, eventObject: any = null): boolean {
        if (!this.isChildFormValid) {
            this.notificationService.displayErrorMessage(this.childFormErrMsg, "Project Calculation");
            return false;
        }

        if (personForm && !personForm.valid) {
            var validMsg;

            for (var control in personForm.controls) {
                if (personForm.controls[control].invalid)
                    validMsg = "Incorrect value of " + control;
            }

            this.notificationService.displayErrorMessage(validMsg, "Project Calculation");
            return false;
        }
        else if (eventObject && eventObject.target) {
            eventObject.target.validity.valid == false ?
                this.notificationService.displayErrorMessage("Incorrect value of " + eventObject.target.name, "Project Calculation") :
                "";
            return eventObject.target.validity.valid;
        }
        else if (this.model.ContingenciesDesign > 1) {
            this.notificationService.displayErrorMessage("Incorrect value of Design", "Project Calculation")
            return false;
        }
        return true;
    }

    validationForm(personForm: FormGroup) {
        if (!this.project.Institution) {
            this.notificationService.displayErrorMessage("Project Institution is empty", "Save Project");
            return false;
        }
        if (!this.project.Name) {
            this.notificationService.displayErrorMessage("Project Name is empty", "Save Project");
            return false;
        }
        if (!this.project.CampusPrecint) {
            this.notificationService.displayErrorMessage("Campus Precint is empty", "Save Project");
            return false;
        }
        if (!personForm.valid) {
            this.notificationService.displayErrorMessage("Form is not valid", "Save Project");
            return false;
        }
        else {
            return true;
        }
    }

    resetProjectValues() {
        this.setFieldsToDefault();
        this.contingenciesToPercent(this.model.Z920Percent, this.model.Z930Percent);
        this.ngOnInit();

        this.cdRef.detectChanges();
    }
}
