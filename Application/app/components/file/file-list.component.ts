import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service ';
import { StorageService } from 'services/storage-service';
import { NotificationStatusService } from 'services/notification-status.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FileService } from 'services/file-service';
import { FileInfoModel } from 'models/file-info.model';

@Component({
    selector: 'file-list-component',
    templateUrl: './file-list.component.html',   
    styleUrls: ['./file-list.component.css'] 
})
export class ListFileInfoComponent {   
    
    listFileInfo:Array<FileInfoModel> = new Array();

    constructor(private projectService:ProjectService,
                private storage:StorageService,
                private route: Router,
                private activeRoute:ActivatedRoute,
                private fileService:FileService,
                private notificationService: NotificationStatusService) { 
    }  
    ngOnInit()
    {
        this.fileService.getFileInfos(false).subscribe(response =>{
            this.listFileInfo = response.Data;
        })
    }
    fileInfoDetail(id:number){
        var route = '/report-detail/'+id;
        this.route.navigate([route]);
    }     
    showAll(){
        this.fileService.getFileInfos(true).subscribe(response =>{
            this.listFileInfo = response.Data;
        })
    }

    deleteFileInfo(id:number,index:number)
    {
        this.fileService.deleteFileInfo(id).subscribe(response =>{
            let isDelete:boolean = response.Data;
            if(isDelete)
                this.notificationService.displayOkMessage("PDF file was deleted", "Success");       
                this.listFileInfo.splice(index, 1);   
            }, (error: any) => {

            this.notificationService.displayErrorMessage(error, "PDF file saving");  
        })
    }

    editProject(id: number) {

        var finOut = id;
        this.route.navigate(['/project-edit/' + id]);
    }
}
