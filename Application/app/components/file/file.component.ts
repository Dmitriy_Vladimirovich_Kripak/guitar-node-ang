import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../services/project.service ';
import { StorageService } from 'services/storage-service';
import { NotificationStatusService } from 'services/notification-status.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FileService } from 'services/file-service';
import { FileInfoModel } from 'models/file-info.model';
import * as FileSaver from 'file-saver'; 
import { DialogModalService } from 'services/dialog-modal.service';

@Component({
    selector: 'file-component',
    templateUrl: './file.component.html',    
    styleUrls: ['./file.component.css'] 
})
export class FileComponent implements OnDestroy {   
    
    pdfSrc:any;
    fileId:number;
    fileBlob:any;
    private fileInfo:FileInfoModel;
    isSaveShow:boolean;

    constructor(private projectService:ProjectService,
                private storage:StorageService,
                private activeRoute:ActivatedRoute,
                private route: Router,
                private fileService:FileService,
                private dialogService: DialogModalService, 
                private notificationService: NotificationStatusService)  { 
    }  
    ngOnInit()
    {
        this.isSaveShow = false;
        this.activeRoute.params.subscribe(params => {
            this.fileId = +params['id']; 
            if(this.fileId)           
            {                
                this.fileService.getFileById(this.fileId).subscribe(response =>{
                    this.pdfSrc = window.URL.createObjectURL(response.json()); 
                    this.fileBlob = new Blob([response.blob()], { 
                        type: 'application/pdf' 
                    });
                })       
                this.fileService.getFileInfo(this.fileId).subscribe(response =>{
                    this.fileInfo = response.Data;
                })          
            }
            else {
                this.fileService.getPDF(this.storage.currentPorjectForGenerate).subscribe(response => {     
                    this.pdfSrc = window.URL.createObjectURL(response.json()); 
                    this.fileBlob = new Blob([response.blob()], { 
                        type: 'application/pdf' 
                    });
                    this.isSaveShow = true;
                })
                
            }
        });       
       
      
    } 

    saveFile(){
        this.dialogService.showFileNameModal(this.storage.currentPorjectForGenerate).subscribe(result => {
            if(!result)
                return;
            this.fileService.savePDF(this.storage.currentPorjectForGenerate, result).subscribe(response =>{
                this.fileInfo = response.Data;
                if (this.fileInfo.Id > 0)
                {
                    this.notificationService.displayOkMessage("PDF file was saved", "Success");
                    this.isSaveShow = false;
                }                
                else
                    this.notificationService.displayErrorMessage(response.Error, "PDF file saving");        
    
                }, (error: any) => {
                    this.notificationService.displayErrorMessage(error, "PDF file saving");  
            })
        })
        
    }

    returnToList(){
        this.route.navigate(['/published-reports']);
    }

    printFile(){
      const iframe = <any>document.createElement('iframe');
      iframe.style.display = 'none';
      iframe.src = this.pdfSrc;
      document.body.appendChild(iframe);
      iframe.contentWindow.print();
    }

    downloadFile(){        
        let name;
        if(this.fileInfo)
        {
            name = this.fileInfo.Label + '-' + Date.now();
        }
        else{
            name = this.storage.currentPorjectForGenerate + '-' + Date.now();
        }
        FileSaver.saveAs(this.fileBlob, );
    }
    ngOnDestroy() {
        this.storage.currentPorjectForGenerate = null;
    }

    returnToEntryPage() {
        this.route.navigate(['/project-edit/' + this.storage.currentPorjectForGenerate.Id]);
    }
}
