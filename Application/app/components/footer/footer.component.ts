import { Component, OnInit } from '@angular/core';
import { Data } from '@angular/router/src/config';

@Component({
    selector: 'footer-component',
    templateUrl: './footer.component.html',    
})
export class FooterComponent {
    public currentDate: number;

    constructor() {
        this.currentDate = new Date().getFullYear();
    }  
    ngOnInit()
    {
        
    } 
}
