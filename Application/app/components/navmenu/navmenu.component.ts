import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { UserInfo } from 'manage/model/user-info';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',    
})
export class NavMenuComponent {    
    
    @Input() userInfo:UserInfo;

    role:Array<string> = new Array();
    
    constructor() {               
    }  
    ngOnInit()
    {
        this.userInfo.Roles.forEach(role =>{
            this.role.push(role);
        })  
    }    
}
