import { Component, OnInit, OnDestroy } from '@angular/core';
@Component({
    selector: 'faq-component',
    templateUrl: './faq.component.html'
})
export class FAQComponent {

    constructor() {
    }
    ngAfterViewInit() {
        $('.flexslider').flexslider({
            animation: "fade",
        });

        function toggleChevron(e) {
            $(e.target)
                .prev('.panel-heading')
                .find("i.indicator")
                .toggleClass('fa fa-minus-circle fa fa-plus-circle');
        }
        $('#accordion').on('hidden.bs.collapse', toggleChevron);
        $('#accordion').on('shown.bs.collapse', toggleChevron);
    }
}
