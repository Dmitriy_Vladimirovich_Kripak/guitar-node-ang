export class CampusInfoModel{
    
    public Id:number;

    public InstitutionName: string;

    public InstitutionType: string;

    public Location: string;

    public ArchitechName: string;

    public CampusProject: string;

    public LocationEnum:number;
}