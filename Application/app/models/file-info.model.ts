import { ProjectModel } from "manage/project/project.model";

export class FileInfoModel{
    public  Id:number;

    public  UserId:string;

    public  File:any;      

    public  Date:string;

    public  Label:string;

    public  Project:ProjectModel = new ProjectModel();
}