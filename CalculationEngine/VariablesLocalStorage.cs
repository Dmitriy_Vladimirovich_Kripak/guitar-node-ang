﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine
{
    public class VariablesLocalStorage : IReadLocalStorage, IUpdateLocalStorage
    {
        IDictionary<string, string> _variables;
        bool _initialized = false;

        public bool Initialized
        {
            get
            {
                return _initialized;
            }
        }

        public void Update(Tuple<string, string> newVariable)
        {
            if (string.IsNullOrWhiteSpace(newVariable.Item1) || string.IsNullOrWhiteSpace(newVariable.Item2))
            {
                throw new Exception(string.Format("New variable has wrong name: '{0}' or value '{1}'", newVariable.Item1, newVariable.Item2));
            }
            if (_variables.ContainsKey(newVariable.Item1))
            {
                _variables[newVariable.Item1] = newVariable.Item2;
            }
            else
            {
                _variables.Add(newVariable.Item1, newVariable.Item2);
            }
        }

        public void Update(IDictionary<string, string> newVariables)
        {
            if (_variables == null)
            {
                if (newVariables == null
                    || newVariables.Keys.Any(k => string.IsNullOrWhiteSpace(k))
                    || newVariables.Values.Any(v => string.IsNullOrWhiteSpace(v))
                    )
                {
                    throw new Exception("Baseline variables are null or have empty parameters");
                }
                _variables = new Dictionary<string, string>(newVariables);
            }
            else if (newVariables != null)
            {
                foreach (var newVariable in newVariables)
                {
                    Update(new Tuple<string, string>(newVariable.Key, newVariable.Value));
                }
            }
            _initialized = true;
        }

        public IDictionary<string, string> GetAllValues()
        {
            return new Dictionary<string, string>(_variables);
        }

        public bool GetValueAsBool(string variableName)
        {
            bool retVal;
            var stringValue = this[variableName]?? string.Empty;
            
            if (stringValue.Trim() == "0") return false;
            if (stringValue.Trim() == "1") return true;
            if (!bool.TryParse(stringValue, out retVal))
            {
                throw new Exception(string.Format("Variable: '{0}' has value: '{1}' which is not an boolean", variableName, stringValue));
            }
            return retVal;
        }

        public int GetValueAsInt(string variableName)
        {
            int retVal;
            if (!int.TryParse(this[variableName], out retVal))
            {
                throw new Exception(string.Format("Variable: '{0}' has value: '{1}' which is not an integer", variableName, this[variableName]));
            }
            return retVal;
        }

        public double GetValueAsDouble(string variableName)
        {
            double retVal;           
            if (!double.TryParse(this[variableName], NumberStyles.Any, CultureInfo.InvariantCulture, out retVal))
            {
                throw new Exception(string.Format("Variable: '{0}' has value: '{1}' which is not a float", variableName, this[variableName]));
            }
            return retVal;
        }

        public string GetValueAsString(string variableName)
        {
            return this[variableName];
        }

        //Now it supports the following types only: bool, int, double, string 
        public T GetValue<T>(string variableName)
        {
            var type = typeof(T);
            object value;
            if (type == typeof(double)) value = GetValueAsDouble(variableName);
            else if (type == typeof(bool)) value = GetValueAsBool(variableName);
            else if (type == typeof(int)) value = GetValueAsInt(variableName);
            else if (type == typeof(string)) value = GetValueAsString(variableName);
            else
                throw new Exception(string.Format("You are trying to get Variable: '{0}' as {1} type which is not supported", variableName, type.ToString()));
            return (T)value;
        }

        private string this[string index]
        {
            get
            {
                if (!_initialized)
                    throw new Exception("Baseline variables are not initialized!");
                if (string.IsNullOrWhiteSpace(index))
                    throw new Exception(string.Format("Wrong name of variable: '{0}'", index));
                if (!_variables.ContainsKey(index))
                    throw new Exception(string.Format("Baseline variables doesn't contain variable: '{0}'", index));

                return _variables[index];
            }
        }

    }
}
