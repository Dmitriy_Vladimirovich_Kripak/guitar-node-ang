﻿using Application.Common;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer.C10_E20
{
    public class D50FormulaCalculator : BaseFormulaCalculator<D50ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public D50FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }
        /// Example:
        /// 
        /// Interior Construction $/GSF = 
        /// ( Core part ) x 
        /// 
        public override double CalculateFormula(D50ViewModel userInputs)
        {
            var coreResult = new C10_E20CoreFormulaCalculator<D50ViewModel>()
                .CalculateCorePartOfFormula(userInputs, _localStorage, CalculationScenarioNames.D50);

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            double architecturalRatio = 0;
            switch (userInputs.ArchitecturalExpectation)
            {
                case ArchitecturalExpectation.Iconic:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicD50);
                    break;
                case ArchitecturalExpectation.CampusStandard:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardD50);
                    break;
                case ArchitecturalExpectation.LowCost:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostD50);
                    break;
                default:
                    throw new Exception("Unknown type of Architectural Expectation");
            }

            double performanceRatio = 0;
            switch (userInputs.PerformanceExpectation)
            {
                case PerformanceExpectation.LEEDPlatinumEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD50);
                    break;
                case PerformanceExpectation.LEEDGoldEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentD50);
                    break;
                case PerformanceExpectation.LEEDSilverEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentD50);
                    break;
                default:
                    throw new Exception("Unknown type of Performance Expectation");
            }

            double electricalRatio = 0;
            switch (userInputs.Electrical)
            {
                case Electrical.Typical:
                    electricalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ElectricalTypical);
                    break;
                case Electrical.Comprehensive:
                    electricalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ElectricalComprehensive);
                    break;
                default:
                    throw new Exception("Unknown type of Electrica");
            }
            return NormalizeToHundredth(coreResult * locationRatio * projectDeliveryRatio * architecturalRatio * performanceRatio * electricalRatio);
        }
    }
}
