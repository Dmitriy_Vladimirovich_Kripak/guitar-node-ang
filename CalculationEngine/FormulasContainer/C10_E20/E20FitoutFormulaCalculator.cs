﻿using Application.Common;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer.C10_E20
{
    public class E20FitoutFormulaCalculator : BaseFormulaCalculator<E20FitoutViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public E20FitoutFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }
        /// Example:
        /// 
        /// Interior Construction $/GSF = 
        /// ( Core part ) x 
        /// 
        public override double CalculateFormula(E20FitoutViewModel userInputs)
        {
            var coreResult = new C10_E20CoreFormulaCalculator<E20FitoutViewModel>()
                .CalculateCorePartOfFormula(userInputs, _localStorage, CalculationScenarioNames.E20Fitout);

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
            double architecturalRatio = 0;
            switch (userInputs.ArchitecturalExpectation)
            {
                case ArchitecturalExpectation.Iconic:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicE20Fitout);
                    break;
                case ArchitecturalExpectation.CampusStandard:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardE20Fitout);
                    break;
                case ArchitecturalExpectation.LowCost:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostE20Fitout);
                    break;
                default:
                    throw new Exception("Unknown type of Architectural Expectation");
            }

            return NormalizeToHundredth(coreResult * locationRatio * projectDeliveryRatio * architecturalRatio);
        }
    }
}
