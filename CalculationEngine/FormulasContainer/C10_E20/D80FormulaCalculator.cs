﻿using Application.Common;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer.C10_E20
{
    public class D80FormulaCalculator : BaseFormulaCalculator<D80ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public D80FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }
        /// Example:
        /// 
        /// Interior Construction $/GSF = 
        /// ( Core part ) x 
        /// 
        public override double CalculateFormula(D80ViewModel userInputs)
        {
            var coreResult = new C10_E20CoreFormulaCalculator<D80ViewModel>()
                .CalculateCorePartOfFormula(userInputs, _localStorage, CalculationScenarioNames.D80);

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            double architecturalRatio = 0;
            switch (userInputs.ArchitecturalExpectation)
            {
                case ArchitecturalExpectation.Iconic:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicD80);
                    break;
                case ArchitecturalExpectation.CampusStandard:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardD80);
                    break;
                case ArchitecturalExpectation.LowCost:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostD80);
                    break;
                default:
                    throw new Exception("Unknown type of Architectural Expectation");
            }

            double performanceRatio = 0;
            switch (userInputs.PerformanceExpectation)
            {
                case PerformanceExpectation.LEEDPlatinumEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD80);
                    break;
                case PerformanceExpectation.LEEDGoldEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentD80);
                    break;
                case PerformanceExpectation.LEEDSilverEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentD80);
                    break;
                default:
                    throw new Exception("Unknown type of Performance Expectation");
            }

            double HVACRatio = 0;
            switch (userInputs.HVAC)
            {
                case HVAC.Air:
                    HVACRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.HVACAir);
                    break;
                case HVAC.Hybrid:
                    HVACRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.HVACHybrid);
                    break;
                case HVAC.Hydronic:
                    HVACRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.HVACHydronic);
                    break;
                default:
                    throw new Exception("Unknown type of HVAC");
            }

            double buildingPlantRatio = 0;
            switch (userInputs.BuildingPlant)
            {
                case BuildingPlant.Local:
                    buildingPlantRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.BuildingPlantLocal);
                    break;
                case BuildingPlant.Remote:
                    buildingPlantRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.BuildingPlantRemote);
                    break;
                default:
                    throw new Exception("Unknown type of Building Plant");
            }

            return NormalizeToHundredth(coreResult * locationRatio * projectDeliveryRatio * architecturalRatio * performanceRatio * HVACRatio * buildingPlantRatio);
        }
    }
}
