﻿using Application.Common;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer.C10_E20
{
    public class C10_E20CoreFormulaCalculator<IN> where IN : BaseC10_E20ViewModel
    {
        /// 
        /// Program 1 ASF x Program 1 $/ASF + Program 2 ASF x Program 2 $/ASF + …+ Shell Spaces x Shell Space $/ASF + 
        /// % Restrooms x Sum (Program 1 ASF + Program 2 ASF + …+ Shell Space ASF) x Restrooms $/ASF +  
        /// % Circulation x Sum (Program 1 ASF + Program 2 ASF + …+ Shell Space ASF) x Circulation $/ASF + 
        /// % MEP x Sum (Program 1 ASF + Program 2 ASF + …+ Shell Space ASF) x MEP $/ASF + 
        /// % Walls and Shafts x Sum (Program 1 ASF + Program 2 ASF + …+ Shell Space ASF) x Walls and Shafts $/ASF
        /// 
        public double CalculateCorePartOfFormula(IN userInputs, IReadLocalStorage localStorage, string calculationScenarioName)
        {
            var buildingTypeName = userInputs.BuildingType.ToString();

            bool assignableProgram1IsUsing = GetValue<bool>(localStorage, GetIsUsing(buildingTypeName, 1));
            bool assignableProgram2IsUsing = GetValue<bool>(localStorage, GetIsUsing(buildingTypeName, 2));
            bool assignableProgram3IsUsing = GetValue<bool>(localStorage, GetIsUsing(buildingTypeName, 3));
            bool assignableProgram4IsUsing = GetValue<bool>(localStorage, GetIsUsing(buildingTypeName, 4));
            bool assignableProgram5IsUsing = GetValue<bool>(localStorage, GetIsUsing(buildingTypeName, 5));
            bool assignableProgram6IsUsing = GetValue<bool>(localStorage, GetIsUsing(buildingTypeName, 6));
            bool assignableProgram7IsUsing = GetValue<bool>(localStorage, GetIsUsing(buildingTypeName, 7));
            bool assignableProgram8IsUsing = GetValue<bool>(localStorage, GetIsUsing(buildingTypeName, 8));
            string assignableProgram1Caption = GetValue<string>(localStorage, GetCaption(buildingTypeName, 1));
            string assignableProgram2Caption = GetValue<string>(localStorage, GetCaption(buildingTypeName, 2));
            string assignableProgram3Caption = GetValue<string>(localStorage, GetCaption(buildingTypeName, 3));
            string assignableProgram4Caption = GetValue<string>(localStorage, GetCaption(buildingTypeName, 4));
            string assignableProgram5Caption = GetValue<string>(localStorage, GetCaption(buildingTypeName, 5));
            string assignableProgram6Caption = GetValue<string>(localStorage, GetCaption(buildingTypeName, 6));
            string assignableProgram7Caption = GetValue<string>(localStorage, GetCaption(buildingTypeName, 7));
            string assignableProgram8Caption = GetValue<string>(localStorage, GetCaption(buildingTypeName, 8));
            double assignableProgram1ASF = GetValue<double>(localStorage, GetAssignable(calculationScenarioName, buildingTypeName, 1));
            double assignableProgram2ASF = GetValue<double>(localStorage, GetAssignable(calculationScenarioName, buildingTypeName, 2));
            double assignableProgram3ASF = GetValue<double>(localStorage, GetAssignable(calculationScenarioName, buildingTypeName, 3));
            double assignableProgram4ASF = GetValue<double>(localStorage, GetAssignable(calculationScenarioName, buildingTypeName, 4));
            double assignableProgram5ASF = GetValue<double>(localStorage, GetAssignable(calculationScenarioName, buildingTypeName, 5));
            double assignableProgram6ASF = GetValue<double>(localStorage, GetAssignable(calculationScenarioName, buildingTypeName, 6));
            double assignableProgram7ASF = GetValue<double>(localStorage, GetAssignable(calculationScenarioName, buildingTypeName, 7));
            double assignableProgram8ASF = GetValue<double>(localStorage, GetAssignable(calculationScenarioName, buildingTypeName, 8));

            double grossUpProgram1ASF = GetValue<double>(localStorage, GetGrossUp(calculationScenarioName, buildingTypeName, 1));
            double grossUpProgram2ASF = GetValue<double>(localStorage, GetGrossUp(calculationScenarioName, buildingTypeName, 2));
            double grossUpProgram3ASF = GetValue<double>(localStorage, GetGrossUp(calculationScenarioName, buildingTypeName, 3));
            double grossUpProgram4ASF = GetValue<double>(localStorage, GetGrossUp(calculationScenarioName, buildingTypeName, 4));

            ValidateInputsAndBaselineVariables(userInputs
                , assignableProgram1IsUsing
                , assignableProgram2IsUsing
                , assignableProgram3IsUsing
                , assignableProgram4IsUsing
                , assignableProgram5IsUsing
                , assignableProgram6IsUsing
                , assignableProgram7IsUsing
                , assignableProgram8IsUsing
                , assignableProgram1Caption
                , assignableProgram2Caption
                , assignableProgram3Caption
                , assignableProgram4Caption
                , assignableProgram5Caption
                , assignableProgram6Caption
                , assignableProgram7Caption
                , assignableProgram8Caption
                , assignableProgram1ASF
                , assignableProgram2ASF
                , assignableProgram3ASF
                , assignableProgram4ASF
                , assignableProgram5ASF
                , assignableProgram6ASF
                , assignableProgram7ASF
                , assignableProgram8ASF
                , grossUpProgram1ASF
                , grossUpProgram2ASF
                , grossUpProgram3ASF
                , grossUpProgram4ASF
                );


            double totalAssignable =
                 userInputs.AssignableProgram1 +
                 userInputs.AssignableProgram2 +
                 userInputs.AssignableProgram3 +
                 userInputs.AssignableProgram4 +
                 userInputs.AssignableProgram5 +
                 userInputs.AssignableProgram6 +
                 userInputs.AssignableProgram7 +
                 userInputs.AssignableProgram8;

            double shellSpace = userInputs.BuildingEfficiency - totalAssignable;

            double grossUpRatio = 1 / totalAssignable;

            double assignableRatio =
                 userInputs.AssignableProgram1 * assignableProgram1ASF +
                 userInputs.AssignableProgram2 * assignableProgram2ASF +
                 userInputs.AssignableProgram3 * assignableProgram3ASF +
                 userInputs.AssignableProgram4 * assignableProgram4ASF +
                 userInputs.AssignableProgram5 * assignableProgram5ASF +
                 userInputs.AssignableProgram6 * assignableProgram6ASF +
                 userInputs.AssignableProgram7 * assignableProgram7ASF +
                 userInputs.AssignableProgram8 * assignableProgram8ASF;

            double grossUpProgram1Ratio = grossUpRatio * userInputs.GrossUpProgram1 * totalAssignable * grossUpProgram1ASF;
            double grossUpProgram2Ratio = grossUpRatio * userInputs.GrossUpProgram2 * totalAssignable * grossUpProgram2ASF;
            double grossUpProgram3Ratio = grossUpRatio * userInputs.GrossUpProgram3 * totalAssignable * grossUpProgram3ASF;
            double grossUpProgram4Ratio = grossUpRatio * userInputs.GrossUpProgram4 * totalAssignable * grossUpProgram4ASF;

            double result = assignableRatio + 
                grossUpProgram1Ratio + grossUpProgram2Ratio + grossUpProgram3Ratio + grossUpProgram4Ratio;

            return result;
        }

        private T GetValue<T>(IReadLocalStorage localStorage, string name)
        {
            return localStorage.GetValue<T>(name);
        }

        private string GetIsUsing(string buildingType, int programNumber)
        {
            return BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, programNumber, true, false);
        }

        private string GetCaption(string buildingType, int programNumber)
        {
            return BaselineVariableNames.GetC10_E20CaptionVarName(buildingType, programNumber, true, true);
        }

        private string GetAssignable(string calculationName, string buildingType, int programNumber)
        {
            return BaselineVariableNames.GetC10_E20AssignableVarName(calculationName, buildingType, programNumber);
        }

        private string GetGrossUp(string calculationName, string buildingType, int programNumber)
        {
            return BaselineVariableNames.GetC10_E20GrossUpVarName(calculationName, buildingType, programNumber);
        }

        private void ValidateInputsAndBaselineVariables(
            IN userInputs
            , bool assignableProgram1IsUsing
            , bool assignableProgram2IsUsing
            , bool assignableProgram3IsUsing
            , bool assignableProgram4IsUsing
            , bool assignableProgram5IsUsing
            , bool assignableProgram6IsUsing
            , bool assignableProgram7IsUsing
            , bool assignableProgram8IsUsing
            , string assignableProgram1Caption
            , string assignableProgram2Caption
            , string assignableProgram3Caption
            , string assignableProgram4Caption
            , string assignableProgram5Caption
            , string assignableProgram6Caption
            , string assignableProgram7Caption
            , string assignableProgram8Caption
            , double assignableProgram1ASF
            , double assignableProgram2ASF
            , double assignableProgram3ASF
            , double assignableProgram4ASF
            , double assignableProgram5ASF
            , double assignableProgram6ASF
            , double assignableProgram7ASF
            , double assignableProgram8ASF
            , double grossUpProgram1ASF
            , double grossUpProgram2ASF
            , double grossUpProgram3ASF
            , double grossUpProgram4ASF
            )
        {
            double minAssignable = 0.5;
            double maxAssignable = 0.75;
            double minGrossUp = 0.25;
            double maxGrossUp = 0.5;

            Action<string, bool, double, double> checkProgrammSettingsAndValues = (Caption, IsUsing, ASFValue, UserValue) =>
            {
                if(ASFValue < 0 )
                {
                    throw new Exception(string.Format("System configured that {0} has negative value {1} but it should be zero or positive", Caption, ASFValue));
                }
                if (UserValue < 0)
                {
                    throw new Exception(string.Format("User entered value {0} for programm {1} but it should be zero or positive", UserValue, Caption));
                }
                if (!IsUsing && UserValue != 0)
                    throw new Exception(string.Format("{0} has value {1} but system configured that it is not using", Caption, UserValue));
            };

            checkProgrammSettingsAndValues(assignableProgram1Caption, assignableProgram1IsUsing, assignableProgram1ASF, userInputs.AssignableProgram1);
            checkProgrammSettingsAndValues(assignableProgram2Caption, assignableProgram2IsUsing, assignableProgram2ASF, userInputs.AssignableProgram2);
            checkProgrammSettingsAndValues(assignableProgram3Caption, assignableProgram3IsUsing, assignableProgram3ASF, userInputs.AssignableProgram3);
            checkProgrammSettingsAndValues(assignableProgram4Caption, assignableProgram4IsUsing, assignableProgram4ASF, userInputs.AssignableProgram4);
            checkProgrammSettingsAndValues(assignableProgram5Caption, assignableProgram5IsUsing, assignableProgram5ASF, userInputs.AssignableProgram5);
            checkProgrammSettingsAndValues(assignableProgram6Caption, assignableProgram6IsUsing, assignableProgram6ASF, userInputs.AssignableProgram6);
            checkProgrammSettingsAndValues(assignableProgram7Caption, assignableProgram7IsUsing, assignableProgram7ASF, userInputs.AssignableProgram7);
            checkProgrammSettingsAndValues(assignableProgram8Caption, assignableProgram8IsUsing, assignableProgram8ASF, userInputs.AssignableProgram8);

            if (grossUpProgram1ASF < 0) throw new Exception(string.Format("System configured that GrossUp1Assignable$/ASF has negative value {0} but it should be zero or positive", grossUpProgram1ASF));
            if (grossUpProgram2ASF < 0) throw new Exception(string.Format("System configured that GrossUp2Assignable$/ASF has negative value {0} but it should be zero or positive", grossUpProgram2ASF));
            if (grossUpProgram3ASF < 0) throw new Exception(string.Format("System configured that GrossUp3Assignable$/ASF has negative value {0} but it should be zero or positive", grossUpProgram3ASF));
            if (grossUpProgram4ASF < 0) throw new Exception(string.Format("System configured that GrossUp4Assignable$/ASF has negative value {0} but it should be zero or positive", grossUpProgram4ASF));

            if (userInputs.GrossUpProgram1 < 0
                || userInputs.GrossUpProgram2 < 0
                || userInputs.GrossUpProgram3 < 0
                || userInputs.GrossUpProgram4 < 0
                )
                throw new Exception(string.Format("All Assignable and GrossUp Programs should be equal or greater than zero"));

            if (userInputs.BuildingEfficiency < minAssignable || userInputs.BuildingEfficiency > maxAssignable)
            {
                throw new Exception("Building Efficiency should be between 50% and 75%");
            }

            var allAssignable =
                (userInputs.AssignableProgram1 +
                 userInputs.AssignableProgram2 +
                 userInputs.AssignableProgram3 +
                 userInputs.AssignableProgram4 +
                 userInputs.AssignableProgram5 +
                 userInputs.AssignableProgram6 +
                 userInputs.AssignableProgram7 +
                 userInputs.AssignableProgram8);

            var allGrossUp =
                (userInputs.GrossUpProgram1 +
                 userInputs.GrossUpProgram2 +
                 userInputs.GrossUpProgram3 +
                 userInputs.GrossUpProgram4);

            allAssignable = Math.Round(allAssignable, 3);
            allGrossUp = Math.Round(allGrossUp, 3);

            if (minAssignable > allAssignable )
            {
                throw new Exception("All Assignable Programs should be equal or greater than 50%");
            }

            if (userInputs.BuildingEfficiency < allAssignable)
            {
                throw new Exception("Building Efficiency should be equal or greater than all Assignable Programs");
            }

            if (1 < (userInputs.BuildingEfficiency + allGrossUp))
            {
                throw new Exception("Building Efficiency and all Gross Up Programs cannot exceed 100%");
            }

            if (minGrossUp > allGrossUp || maxGrossUp < allGrossUp )
            {
                throw new Exception("All Gross Up Programs should be between 25% and 50%");
            }
        }

    }
}