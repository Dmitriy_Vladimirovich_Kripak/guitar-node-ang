﻿using Application.Common;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer.C10_E20
{
    public class D20FormulaCalculator : BaseFormulaCalculator<D20ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public D20FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }
        /// Example:
        /// 
        /// Interior Construction $/GSF = 
        /// ( Core part ) x 
        /// 
        public override double CalculateFormula(D20ViewModel userInputs)
        {
            var coreResult = new C10_E20CoreFormulaCalculator<D20ViewModel>()
                .CalculateCorePartOfFormula(userInputs, _localStorage, CalculationScenarioNames.D20);

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            double architecturalRatio = 0;
            switch (userInputs.ArchitecturalExpectation)
            {
                case ArchitecturalExpectation.Iconic:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicD20);
                    break;
                case ArchitecturalExpectation.CampusStandard:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardD20);
                    break;
                case ArchitecturalExpectation.LowCost:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostD20);
                    break;
                default:
                    throw new Exception("Unknown type of Architectural Expectation");
            }

            double performanceRatio = 0;
            switch (userInputs.PerformanceExpectation)
            {
                case PerformanceExpectation.LEEDPlatinumEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentD20);
                    break;
                case PerformanceExpectation.LEEDGoldEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentD20);
                    break;
                case PerformanceExpectation.LEEDSilverEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentD20);
                    break;
                default:
                    throw new Exception("Unknown type of Performance Expectation");
            }

            double plumbingRatio = 0;
            switch (userInputs.Plumbing)
            {
                case Plumbing.Typical:
                    plumbingRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.PlumbingTypical);
                    break;
                case Plumbing.Flexible:
                    plumbingRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.PlumbingFlexible);
                    break;
                default:
                    throw new Exception("Unknown type of Plumbing");
            }
            return NormalizeToHundredth(coreResult * locationRatio * projectDeliveryRatio * architecturalRatio * performanceRatio * plumbingRatio);
        }
    }
}
