﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z920BCFormulaCalculator : BaseFormulaCalculator<Z920BCViewModel, double>
    {
        IReadLocalStorage _localStorage;
        IFormulasContainer _formulasContainer;

        public Z920BCFormulaCalculator(IReadLocalStorage localStorage, IFormulasContainer formulasContainer)
        {
            _localStorage = localStorage;
            _formulasContainer = formulasContainer;
        }

        /// Example:
        public override double CalculateFormula(Z920BCViewModel userInputs)
        {
            var escalation = _formulasContainer.CalculateZ920Rate(userInputs);
            return NormalizeToHundredth(escalation * (userInputs.A10F30SumOfBC + userInputs.Z910BC));
        }
    }
}
