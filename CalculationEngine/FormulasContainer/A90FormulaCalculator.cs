﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class A90FormulaCalculator : BaseFormulaCalculator<A90ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public A90FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example
        /// 
        /// Subgrade Related Activities ($/GSF) = (Footprint Areas) x (Number of Basement Levels) x (Basement F-F Heights) / (27) x (Substructure Excavation $/CY) / GSF
        /// 
        /// So, in the case of an Academic building of 5 stories built on rippable rock, 
        /// with a cladding ratio of 0.6, f-f height of 20’, one basement level, and a constrained site,
        /// the following would be the calc.
        /// Subgrade Related Activities($/GSF) = (100,000/5 x 1 x 20 / 27 x $35/CY) / 100,000 = $5.19/GSF
        ///                     
        public override double CalculateFormula(A90ViewModel userInputs)
        {
            if(userInputs.NumberOfLevels < 1 || userInputs.NumberOfLevels > 100)
            {
                throw new Exception(string.Format("The number of levels must be from 1 to 100, actual result is: {0}", userInputs.NumberOfLevels));
            }

            if (userInputs.NumberOfBelowGradeLevels < 0 || userInputs.NumberOfBelowGradeLevels > 5)
            {
                throw new Exception(string.Format("The number of basement levels must be from 0 to 5, actual result is: {0}", userInputs.NumberOfBelowGradeLevels));
            }
            if (userInputs.CladdingRatio < 0 || userInputs.CladdingRatio > 2)
                throw new Exception(string.Format("The Cladding Ratio must be from 0 to 2, actual result is: {0}", userInputs.CladdingRatio));

            double soilExcavationAndHaulAway = 0;

            switch (userInputs.SoilCondition)
            {
                case SoilCondition.Soil:
                    soilExcavationAndHaulAway = _localStorage.GetValueAsDouble(BaselineVariableNames.EarthworkExcavationHaulAwayCYSoil);
                    break;
                case SoilCondition.RippableRock:
                    soilExcavationAndHaulAway = _localStorage.GetValueAsDouble(BaselineVariableNames.EarthworkExcavationHaulAwayCYRippableRock);
                    break;
                case SoilCondition.SolidRock:
                    soilExcavationAndHaulAway = _localStorage.GetValueAsDouble(BaselineVariableNames.EarthworkExcavationHaulAwayCYSolidRock);
                    break;
                default:
                    throw new Exception("Unknown type of Soil Condition");


            }

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
         

         
            

            var result = (userInputs.BuildingFootprint * userInputs.CladdingRatio * soilExcavationAndHaulAway) * projectDeliveryRatio * locationRatio / (double)userInputs.GrossSquareFeet;
            return NormalizeToHundredth(result);
        }
    }
}
