﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z910SDFormulaCalculator : BaseFormulaCalculator<Z910SDViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public Z910SDFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(Z910SDViewModel userInputs)
        {
            return NormalizeToHundredth(userInputs.ContingenciesDesign*userInputs.G10G60Sum);
        }
    }
}
