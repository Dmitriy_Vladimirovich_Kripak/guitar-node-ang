﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class G20SDFormulaCalculator : BaseFormulaCalculator<G20SDViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public G20SDFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(G20SDViewModel userInputs)
        {

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

          
            double siteDevelopment = 0;
            switch (userInputs.SiteDevelopment)
            {
                case SiteDevelopment.Simple:
                    siteDevelopment = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteDevelopmentSimple);                 
                    break;
                case SiteDevelopment.Moderate:
                    siteDevelopment = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteDevelopmentModerate);
                    break;
                case SiteDevelopment.Extensive:
                    siteDevelopment = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteDevelopmentExtensive);
                    break;
            }

            return NormalizeToHundredth((userInputs.DevelopedSiteArea * siteDevelopment * locationRatio * projectDeliveryRatio) / userInputs.DevelopedSiteArea);

        }
    }
}
