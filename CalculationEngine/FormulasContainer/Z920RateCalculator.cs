﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z920RateCalculator : BaseFormulaCalculator<BaseZ920ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public Z920RateCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        public DateTime CalculateDataDate(BaseZ920ViewModel userInputs)
        {
            var dataDateStr = this.CalculateStrDataDate(userInputs);
            DateTime dataDate;
            if (!DateTime.TryParseExact(dataDateStr, "MM/dd/yyyy", CultureInfo.InvariantCulture,
                           DateTimeStyles.None, out dataDate))
            {
                throw new Exception(string.Format("Data date is incorrect: {0}", dataDateStr));
            }

            return dataDate;
        }

        public string CalculateStrDataDate(BaseZ920ViewModel userInputs)
        {
            var dataDateStr = _localStorage.GetValueAsString(BaselineVariableNames.DataDate);

            if (dataDateStr == null || dataDateStr == string.Empty)
                throw new Exception("Can't get value of DataDate");

            return dataDateStr;
        }

        /// Example:
        public override double CalculateFormula(BaseZ920ViewModel userInputs)
        {
            string dataDateStr = _localStorage.GetValueAsString(BaselineVariableNames.DataDate);
            DateTime dataDate;
            if (!DateTime.TryParseExact(dataDateStr, "MM/dd/yyyy", CultureInfo.InvariantCulture,
                           DateTimeStyles.None, out dataDate))
            {
                throw new Exception(string.Format("Data date is incorrect: {0}", dataDateStr));
            }

            if (userInputs.EstimateDate < dataDate || userInputs.EstimateDate.Year != dataDate.Year)
            {
                throw new Exception(string.Format("Estimation date {0} must be equal or greater than data date {1} and must be in the same year", userInputs.EstimateDate, dataDate));
            }

            if (userInputs.ConstructionStartDate < userInputs.EstimateDate)
            {
                throw new Exception(string.Format("Construction Start date {0} must be equal or greater than Estimation date {1} ", userInputs.ConstructionStartDate, userInputs.EstimateDate));
            }

            if (userInputs.DurationInMonths <= 0)
            {
                throw new Exception(string.Format("Duration In Months {0} must be equal or greater than 1", userInputs.DurationInMonths));
            }

            double escalation = 0;
            switch (userInputs.EscalateTo)
            {
                case EscalateTo.EstimateDate:
                    escalation = CalcToEstimation(userInputs.EstimateDate, dataDate);
                    break;
                case EscalateTo.ConstructionStartDate:
                    escalation = CalcToConstruction(userInputs.EstimateDate, userInputs.ConstructionStartDate, dataDate);
                    break;
                case EscalateTo.ConstructionMidpoint:
                    escalation = CalcToConstruction(userInputs.EstimateDate, userInputs.ConstructionStartDate, dataDate, userInputs.DurationInMonths / 2);
                    break;
                case EscalateTo.ConstructionDuration:
                    escalation = CalcToConstruction(userInputs.EstimateDate, userInputs.ConstructionStartDate, dataDate, userInputs.DurationInMonths);
                    break;
                default:
                    throw new Exception("Unknown type of Escalate To");
            }

            return escalation;

        }

        private double CalcToEstimation(DateTime estimateDate, DateTime dataDate)
        {
            var months = GetMonths(dataDate, estimateDate);
            var rate1 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate1Year) * months / 12;

            return rate1;
        }

        private double CalcToConstruction(DateTime estimateDate, DateTime constructionStartDate, DateTime dataDate, int durationMonth = 0)
        {
            //var toEstimation = CalcToEstimation(estimateDate, dataDate);
            var months = GetMonths(dataDate, constructionStartDate);

            var rate = CalcToDate(dataDate, months + durationMonth);
            return rate;
        }


        private double CalcToDate(DateTime dataDate, int months)
        {
            var monthsInCurrentYear = 12 - dataDate.Month + 1;
            //current year only
            if (months <= monthsInCurrentYear)
            {
                var rate1 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate1Year) * months / 12;
                return rate1;
            }
            //+ next year
            else if (months <= 12 + monthsInCurrentYear)
            {
                var rate1 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate1Year) * monthsInCurrentYear / 12;
                var rate2 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate2Year) * (months - monthsInCurrentYear) / 12;
                return (1 + rate1) * (1 + rate2) - 1;
            }
            else if (months <= 24 + monthsInCurrentYear)
            {
                var rate1 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate1Year) * monthsInCurrentYear / 12;
                var rate2 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate2Year);
                var rate3 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate3Year) * (months - 12 - monthsInCurrentYear) / 12;
                return (1 + rate1) * (1 + rate2) * (1 + rate3) - 1;
            }
            else if (months <= 35 + monthsInCurrentYear)
            {
                var rate1 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate1Year) * monthsInCurrentYear / 12;
                var rate2 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate2Year);
                var rate3 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate3Year);
                var rate4 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate4Year) * (months - 24 - monthsInCurrentYear) / 12;
                return (1 + rate1) * (1 + rate2) * (1 + rate3) * (1 + rate4) - 1;
            }
            //for 5th year and the others years we will add multiplyer (1 + rateOfYear)
            else
            {
                var rate1 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate1Year) * monthsInCurrentYear / 12;
                var rate2 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate2Year);
                var rate3 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate3Year);
                var rate4 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate4Year);
                var rate5 = _localStorage.GetValueAsDouble(BaselineVariableNames.DefaultEscalationRate5Year);

                var restMonths = months - 36 - monthsInCurrentYear;
                var restFullYears = restMonths / 12;
                restMonths = restMonths - restFullYears * 12; //mounths in the last year can be from 0 till 11;

                double restYearsRate = 1;
                if (restFullYears > 0)
                {
                    for (var i = 0; i < restFullYears; i++)
                    {
                        restYearsRate = restYearsRate * (1 + rate5);
                    }
                }

                double restMonthsRate = 1;
                if (restMonths > 0)
                {
                    restMonthsRate = 1 + rate5 * restMonths / 12;
                }

                return (1 + rate1) * (1 + rate2) * (1 + rate3) * (1 + rate4) * restYearsRate * restMonthsRate - 1;
            }
        }

        private int GetMonths(DateTime first, DateTime second)
        {
            var years = (second.Year - first.Year);
            var months = (second.Month - first.Month);
            var days = (second.Day - first.Day) > 15 ? 1 : 0;
            return years * 12 + months + days;
        }

    }

}
