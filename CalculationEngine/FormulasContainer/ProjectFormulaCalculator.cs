﻿using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class ProjectFormulaCalculator
    {
        IReadLocalStorage _localStorage;      
        public ProjectFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
           
        }
       
        public double CalculateA10F30SumOfBC(ProjectResultModel userInputs)
        {
            double result = userInputs.A10 + userInputs.A20 + userInputs.A40 + userInputs.A60 + userInputs.A90 + userInputs.B10 + userInputs.B20 + userInputs.B30 + userInputs.C10
                + userInputs.C20 + userInputs.D10 + userInputs.D20 + userInputs.D30 + userInputs.D40 + userInputs.D50 + userInputs.D60 + userInputs.D70 + userInputs.D80 
                + userInputs.E10 + userInputs.E20 + userInputs.F10BC + userInputs.F20BC + userInputs.F30BC;

            return Math.Round(result, 3);
        }

        public double CalculateA10F30SumOfFitouts(ProjectResultModel userInputs)
        {
            double result = userInputs.C10Fitout + userInputs.C20Fitout + userInputs.D20Fitout + userInputs.D30Fitout + userInputs.D40Fitout + userInputs.D50Fitout
                + userInputs.D60Fitout + userInputs.D70Fitout + userInputs.D80Fitout + userInputs.E10Fitout + userInputs.E20Fitout;

            return Math.Round(result, 3);
        }

        public  double CalculateA10F30Sum(ProjectResultModel userInputs)
        {
            double result = userInputs.A10 + userInputs.A20 + userInputs.A40 + userInputs.A60 + userInputs.A90 + userInputs.B10 + userInputs.B20 + userInputs.B30 + userInputs.C10 + userInputs.C10Fitout + userInputs.C20
                + userInputs.C20Fitout + userInputs.D10 + userInputs.D20 + userInputs.D20Fitout + userInputs.D30 + userInputs.D30Fitout +userInputs.D40 + userInputs.D40Fitout + userInputs.D50 + 
                userInputs.D50Fitout + userInputs.D60 + userInputs.D60Fitout + userInputs.D70 + userInputs.D70Fitout + userInputs.D80 + userInputs.D80Fitout + userInputs.E10 + userInputs.E10Fitout + userInputs.E20+
                userInputs.E20Fitout + userInputs.F10BC + userInputs.F20BC + userInputs.F30BC;
            return Math.Round(result,3);
        }

        public double CalculateG10G60Sum(ProjectResultModel userInputs)
        {
            double result = userInputs.G10SD + userInputs.G20SD + userInputs.G60SD;
            return Math.Round(result, 3);
        }

        public double CalculateG30G50SUSum(ProjectResultModel userInputs)
        {
            double result = userInputs.G30SU + userInputs.G40SU+ userInputs.G50SU;
            return Math.Round(result, 3);
        }

        public double CalculateA_Substructure(ProjectResultModel userInputs)
        {
            return userInputs.A10 + userInputs.A20 + userInputs.A40 + userInputs.A60 + userInputs.A90;
        }

        public double CalculateB_Shell(ProjectResultModel userInputs)
        {
            return userInputs.B10 + userInputs.B20 + userInputs.B30;
        }

        public double CalculateC_Interiors(ProjectResultModel userInputs)
        {
            return userInputs.C10 + userInputs.C10Fitout + userInputs.C20 + userInputs.C20Fitout;
        }

        public double CalculateD_Services(ProjectResultModel userInputs)
        {
            return userInputs.D10 + userInputs.D20 + userInputs.D20Fitout + userInputs.D30 + userInputs.D30Fitout
                + userInputs.D40 + userInputs.D40Fitout + userInputs.D50 + userInputs.D50Fitout + userInputs.D60
                + userInputs.D60Fitout + userInputs.D70 + userInputs.D70Fitout + userInputs.D80 + userInputs.D80Fitout;
        }

        public double CalculateE_EquipmentFurnishings(ProjectResultModel userInputs)
        {
            return userInputs.E10 + userInputs.E10Fitout + userInputs.E20 + userInputs.E20Fitout;
        }

        public double CalculateF_ConstructionDemolition(ProjectResultModel userInputs)
        {
            return userInputs.F10BC + userInputs.F20BC + userInputs.F30BC;
        }

        public double CalculateSubtotalDirectCost_building(ProjectResultModel userInputs)
        {
            return userInputs.A_Substructure + userInputs.B_Shell + userInputs.C_Interiors
                + userInputs.D_Services + userInputs.E_EquipmentFurnishings + userInputs.F_ConstructionDemolition;
        }

        public double CalculateContractorMarkups_building(ProjectResultModel userInputs)
        {
            return userInputs.Z10BC + userInputs.Z10Fitout + userInputs.Z70BC + userInputs.Z70Fitout
                + userInputs.Z940BC + userInputs.Z940Fitout;
        }

        public double CalculateContingencies_building(ProjectResultModel userInputs)
        {
            return userInputs.Z910BC + userInputs.Z910Fitout + userInputs.Z920BC + userInputs.Z920Fitout
                + userInputs.Z930BC + userInputs.Z930Fitout;
        }

        public double CalculateTotalBuildingConstruction(ProjectResultModel userInputs)
        {
            return CalculateSubtotalDirectCost_building(userInputs) + CalculateContractorMarkups_building(userInputs)
                + CalculateContingencies_building(userInputs);
        }

        public double CalculateSiteDevelopment_site(ProjectResultModel userInputs)
        {
            return userInputs.G10SD + userInputs.G20SD + userInputs.G60SD;
        }

        public double CalculateSiteUtilities_site(ProjectResultModel userInputs)
        {
            return userInputs.G30SU + userInputs.G40SU + userInputs.G50SU;
        }

        public double CalculateSubtotalDirectCost_site(ProjectResultModel userInputs)
        {
            return CalculateSiteDevelopment_site(userInputs) + CalculateSiteUtilities_site(userInputs);
        }

        public double CalculateContractorMarkups_site(ProjectResultModel userInputs)
        {
            return userInputs.Z10SD + userInputs.Z10SU + userInputs.Z70SD + userInputs.Z70SU
                + userInputs.Z940SD + userInputs.Z940SU;
        }

        public double CalculateContingencies_site(ProjectResultModel userInputs)
        {
            return userInputs.Z910SD + userInputs.Z910SU + userInputs.Z920SD + userInputs.Z920SU
                + userInputs.Z930SD + userInputs.Z930SU;
        }

        public double CalculateTotalSiteConstruction_site(ProjectResultModel userInputs)
        {
            return CalculateSubtotalDirectCost_site(userInputs) + CalculateContractorMarkups_site(userInputs)
                + CalculateContingencies_site(userInputs);
        }

        public double CalculateTotalConstructionCosts(ProjectResultModel userInputs)
        {
            return CalculateTotalBuildingConstruction(userInputs) + CalculateTotalSiteConstruction_site(userInputs);
        }

        public double CalculateSoftCosts(ProjectResultModel userInputs)
        {
            return (userInputs.DesignFee + userInputs.FFE + userInputs.Technology + userInputs.Contingency + userInputs.Misc)
                * CalculateTotalConstructionCosts(userInputs);
        }

        public double CalculateTotalProjectCosts(ProjectResultModel userInputs)
        {
            return CalculateTotalConstructionCosts(userInputs) + CalculateSoftCosts(userInputs);
        }
    }
}
