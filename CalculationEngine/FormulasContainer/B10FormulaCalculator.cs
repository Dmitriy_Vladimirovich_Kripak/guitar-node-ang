﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class B10FormulaCalculator : BaseFormulaCalculator<B10ViewModel, double>
    {
        IReadLocalStorage _localStorage;
        public readonly Dictionary<BuildingType, string[]> possibleFloorConstructionTypes = new Dictionary<BuildingType, string[]>();
        public readonly Dictionary<BuildingType, string[]> possibleRoofConstructionTypes = new Dictionary<BuildingType, string[]>();

        public B10FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;

            InitPossibleTypeValues();
        }
        /// Example:
        /// 
        /// Superstructure ($/GSF) = (
        /// (GSF – Footprint) x (Building Type Floor Construction Structure Type $/SF) + 
        /// (Footprint) x (Horizontal Articulation Factor) x(Building Type Roof Construction Structure Type $/SF) + 
        /// ((Egress Stairs $/FLT) x(Building Type FLT/SF) x GSF) + ((Ornamental Stairs $/FLT) x(Building Type FLT/SF) x GSF)
        /// )/GSF
        /// 
        /// So, in the case of an Academic building of 5 stories built on rippable rock, 
        /// with a cladding ratio of 0.6, f-f height of 20’, one basement level, 
        /// and a constrained site, the following would be the calc.
        /// Superstructure($/GSF) = (
        /// (100,000-100,000/5) x($50/SF) + 
        /// ((100,000/5) x(1.2) x($45/SF)) + 
        /// (($40,000/FLT) x(0.00007) X(100,000)) + (($120,000/FLT) x(0.00003) X(100,000))
        /// )/100,000 = $57.2/GSF
        /// 

        public override double CalculateFormula(B10ViewModel userInputs)
        {
            if(userInputs.NumberOfLevels < 1 || userInputs.NumberOfLevels > 100)
            {
                throw new Exception(string.Format("The number of levels must be from 1 to 100, actual result is: {0}", userInputs.NumberOfLevels));
            }
            double floorConstructionValue = ValidateAndGetFloorConstructionValue(userInputs.BuildingType, userInputs.FloorConstructionType);
            double roofConstructionValue = ValidateAndGetRoofConstructionValue(userInputs.BuildingType, userInputs.RoofConstructionType);

            double egressStairValue = 0;
            double ornamentalStairValue = 0;
            double egressFlightRatio = 0;
            double ornamentalFlightRatio = 0;

            switch (userInputs.BuildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    egressStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairAcademicClassroomEgressFLT);
                    ornamentalStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairAcademicClassroomOrnamentalFLT);
                    egressFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightAcademicClassroomEgressRatio);
                    ornamentalFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightAcademicClassroomOrnamentalRatio);
                    break;
                case BuildingType.LaboratoryBuilding:
                    egressStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairLaboratoryEgressFLT);
                    ornamentalStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairLaboratoryOrnamentalFLT);
                    egressFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightLaboratoryEgressRatio);
                    ornamentalFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightLaboratoryOrnamentalRatio);
                    break;
                case BuildingType.StudentHousing:
                    egressStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairStudentHousingEgressFLT);
                    ornamentalStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairStudentHousingOrnamentalFLT);
                    egressFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightStudentHousingEgressRatio);
                    ornamentalFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightStudentHousingOrnamentalRatio);
                    break;
                case BuildingType.Dining:
                    egressStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairDiningEgressFLT);
                    ornamentalStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairDiningOrnamentalFLT);
                    egressFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightDiningEgressRatio);
                    ornamentalFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightDiningOrnamentalRatio);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    egressStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairParkingGarageBelowGradeEgressFLT);
                    ornamentalStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairParkingGarageBelowGradeOrnamentalFLT);
                    egressFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightParkingGarageBelowGradeEgressRatio);
                    ornamentalFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightParkingGarageBelowGradeOrnamentalRatio);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    egressStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairParkingGarageAboveGradeEgressFLT);
                    ornamentalStairValue = _localStorage.GetValueAsDouble(BaselineVariableNames.StairParkingGarageAboveGradeOrnamentalFLT);
                    egressFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightParkingGarageAboveGradeEgressRatio);
                    ornamentalFlightRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.FlightParkingGarageAboveGradeOrnamentalRatio);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");
            }
            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
            double footprint = (double)userInputs.GrossSquareFeet / (double)userInputs.NumberOfLevels;
            double floorCalculation = ((double)userInputs.GrossSquareFeet - footprint) * floorConstructionValue;
            double roofCalculation = footprint * roofConstructionValue * userInputs.HorizontalArticulationFactor;

            double egressOrnamentalCalculation =
                egressStairValue * egressFlightRatio * (double)userInputs.GrossSquareFeet
                + ornamentalStairValue * ornamentalFlightRatio * (double)userInputs.GrossSquareFeet;


            return NormalizeToHundredth((roofCalculation + floorCalculation + egressOrnamentalCalculation) * projectDeliveryRatio  * locationRatio / (double)userInputs.GrossSquareFeet);
        }

        private double ValidateAndGetFloorConstructionValue(BuildingType buildingType, string floorConstructionType)
        {
            if (!possibleFloorConstructionTypes[buildingType].Contains(floorConstructionType))
            {
                throw new Exception(string.Format("Invalid Floor Construction Type:{0} for Building Type: {1}, possible values are: {2}"
                    , floorConstructionType, buildingType, String.Join(", ", possibleFloorConstructionTypes[buildingType])));
            }
            double value = 0;
            switch (buildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionAcademicClassroomConcreteFlatSlabSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionAcademicClassroomConcreteSlabAndBeamSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionAcademicClassroomSteelBracedFrameSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][3]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionAcademicClassroomSteelMomentFrameSF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionLaboratoryConcreteFlatSlabSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionLaboratoryConcreteSlabAndBeamSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionLaboratorySteelBracedFrameSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][3]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionLaboratorySteelMomentFrameSF);
                    break;
                case BuildingType.StudentHousing:
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionStudentHousingConcreteFlatSlabSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionStudentHousingConcreteSlabAndBeamSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionStudentHousingSteelBracedFrameSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][3]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionStudentHousingSteelMomentFrameSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][4]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionStudentHousingWoodFrameSF);
                    break;
                case BuildingType.Dining:
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionDiningConcreteFlatSlabSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionDiningConcreteSlabAndBeamSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionDiningSteelBracedFrameSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][3]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionDiningSteelMomentFrameSF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteFlatSlabSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionParkingGarageBelowGradePrecastSF);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteFlatSlabSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF);
                    if (floorConstructionType == possibleFloorConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.FloorConstructionParkingGarageAboveGradePrecastSF);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");

            }
            if (value == 0) throw new Exception(string.Format("Unexpected value of {0} baseline variable", floorConstructionType));
            return value;
        }

        private double ValidateAndGetRoofConstructionValue(BuildingType buildingType, string roofConstructionType)
        {
            if(!possibleRoofConstructionTypes[buildingType].Contains(roofConstructionType))
            {
                throw new Exception(string.Format("Invalid Roof Construction Type:{0} for Building Type: {1}, possible values are: {2}"
                    , roofConstructionType, buildingType, String.Join(", ", possibleRoofConstructionTypes[buildingType])));
            }
            double value = 0;
            switch(buildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionAcademicClassroomConcreteFlatSlabSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionAcademicClassroomConcreteSlabAndBeamSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionAcademicClassroomSteelBracedFrameSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][3]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionAcademicClassroomSteelMomentFrameSF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionLaboratoryConcreteFlatSlabSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionLaboratoryConcreteSlabAndBeamSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionLaboratorySteelBracedFrameSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][3]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionLaboratorySteelMomentFrameSF);
                    break;
                case BuildingType.StudentHousing:
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionStudentHousingConcreteFlatSlabSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionStudentHousingConcreteSlabAndBeamSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionStudentHousingSteelBracedFrameSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][3]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionStudentHousingSteelMomentFrameSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][4]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionStudentHousingWoodFrameSF);
                    break;
                case BuildingType.Dining:
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionDiningConcreteFlatSlabSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionDiningConcreteSlabAndBeamSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionDiningSteelBracedFrameSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][3]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionDiningSteelMomentFrameSF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteFlatSlabSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionParkingGarageBelowGradeConcreteSlabAndBeamSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionParkingGarageBelowGradePrecastSF);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][0]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteFlatSlabSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][1]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionParkingGarageAboveGradeConcreteSlabAndBeamSF);
                    if (roofConstructionType == possibleRoofConstructionTypes[buildingType][2]) value = _localStorage.GetValueAsDouble(BaselineVariableNames.RoofConstructionParkingGarageAboveGradePrecastSF);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");

            }
            if(value == 0) throw new Exception(string.Format("Unexpected value of {0} baseline variable", roofConstructionType));
            return value;
        }

        private void InitPossibleTypeValues()
        {
            //FloorConstructionTypes
            possibleFloorConstructionTypes.Add(
                BuildingType.AcademicClassroomBuilding
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", });
            possibleFloorConstructionTypes.Add(
                BuildingType.LaboratoryBuilding
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", });
            possibleFloorConstructionTypes.Add(
                BuildingType.StudentHousing
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", "WoodFrame" });
            possibleFloorConstructionTypes.Add(
                BuildingType.Dining
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", });
            possibleFloorConstructionTypes.Add(
                BuildingType.ParkingGarageBelowGrade
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "Precast", });
            possibleFloorConstructionTypes.Add(
                BuildingType.ParkingGarageAboveGrade
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "Precast", });

            //RoofConstructionTypes
            possibleRoofConstructionTypes.Add(
                BuildingType.AcademicClassroomBuilding
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", });
            possibleRoofConstructionTypes.Add(
                BuildingType.LaboratoryBuilding
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", });
            possibleRoofConstructionTypes.Add(
                BuildingType.StudentHousing
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", "WoodFrame" });
            possibleRoofConstructionTypes.Add(
                BuildingType.Dining
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "SteelBracedFrame", "SteelMomentFrame", });
            possibleRoofConstructionTypes.Add(
                BuildingType.ParkingGarageBelowGrade
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "Precast", });
            possibleRoofConstructionTypes.Add(
                BuildingType.ParkingGarageAboveGrade
                , new string[] { "ConcreteFlatSlab", "ConcreteSlabAndBeam", "Precast", });
        }

    }
}
