﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class G10SDFormulaCalculator : BaseFormulaCalculator<G10SDViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public G10SDFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(G10SDViewModel userInputs)
        {

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            double sitePreparation = 0;
            double siteDemolition = 0;
            switch (userInputs.SitePreparation)
            {
                case SitePreparation.Greenfield:
                    sitePreparation = _localStorage.GetValueAsDouble(BaselineVariableNames.SitePreparationGreenfield);
                    siteDemolition = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteDemolitionGreenfield);
                    break;
                case SitePreparation.Brownfield:
                    sitePreparation = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteDemolitionBrownfield);
                    siteDemolition = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteDemolitionBrownfield);
                    break;
            }

            return NormalizeToHundredth((userInputs.GrossSiteArea * (sitePreparation + siteDemolition) * locationRatio * projectDeliveryRatio) / userInputs.DevelopedSiteArea);

        }
    }
}
