﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class A10FormulaCalculator: BaseFormulaCalculator<A10ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public A10FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        /// A10 Foundations ($/GSF) = (Soil Conditions Selection) x (Number of Levels Factor) x (Building Type Footing $ /GSF)
        /// 
        /// So, in the case of an Academic building of 5 stories built on rippable rock, the following would be the calc.
        /// A10 Foundations($/GSF) = (1) x(0.5*($20/GSF)+0.5*($10/GSF)) = $15/GSF
        ///
        public override double CalculateFormula(A10ViewModel userInputs)
        {
            if(userInputs.NumberOfLevels < 1 || userInputs.NumberOfLevels > 100)
            {
                throw new Exception(string.Format("The number of levels must be from 1 to 100, actual result is: {0}", userInputs.NumberOfLevels));
            }

            double buildingTypeCalc = 0;
            switch (userInputs.SoilCondition)
            {
                case SoilCondition.Soil:
                    buildingTypeCalc = CalcSoil(userInputs.BuildingType);
                    break;
                case SoilCondition.RippableRock:
                    buildingTypeCalc = CalcRippableRock(userInputs.BuildingType);
                    break;
                case SoilCondition.SolidRock:
                    buildingTypeCalc = CalcSolidRock(userInputs.BuildingType);
                    break;
                default:
                    throw new Exception("Unknown type of Soil Condition");
            }
            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
            double levelsFactor = _localStorage.GetValueAsDouble(BaselineVariableNames.NumberOfLevelsFactor) + userInputs.NumberOfLevels * 0.01;

            return NormalizeToHundredth((buildingTypeCalc * levelsFactor) * locationRatio * projectDeliveryRatio);

        }

        private double CalcSoil(BuildingType type)
        {
            double deep = 0;
            switch (type)
            {
                case BuildingType.AcademicClassroomBuilding:
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingDeepGSF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingDeepGSF);
                    break;
                case BuildingType.StudentHousing:
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingDeepGSF);
                    break;
                case BuildingType.Dining:
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningDeepGSF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeDeepGSF);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageAboveGradeDeepGSF);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");
            }

            return deep;
        }

        private double CalcRippableRock(BuildingType type)
        {
            double shallow = 0;
            double deep = 0;
            switch (type)
            {
                case BuildingType.AcademicClassroomBuilding:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingShallowGSF);
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingDeepGSF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingShallowGSF);
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingDeepGSF);
                    break;
                case BuildingType.StudentHousing:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingShallowGSF);
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingDeepGSF);
                    break;
                case BuildingType.Dining:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningShallowGSF);
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningDeepGSF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeShallowGSF);
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeDeepGSF);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageAboveGradeShallowGSF);
                    deep = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageAboveGradeDeepGSF);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");
            }

            return 0.5*shallow + 0.5*deep;
        }

        private double CalcSolidRock(BuildingType type)
        {
            double shallow = 0;
            switch (type)
            {
                case BuildingType.AcademicClassroomBuilding:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingShallowGSF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingShallowGSF);
                    break;
                case BuildingType.StudentHousing:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingShallowGSF);
                    break;
                case BuildingType.Dining:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningShallowGSF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeShallowGSF);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    shallow = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageAboveGradeShallowGSF);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");
            }

            return shallow;
        }
    }
}
