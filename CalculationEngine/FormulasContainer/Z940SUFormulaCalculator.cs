﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z940SUFormulaCalculator : BaseFormulaCalculator<Z940SUViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public Z940SUFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(Z940SUViewModel userInputs)
        {

            double valueBuildingTypePrjectStrategyFactor = GetBuildingTypePrjectStrategyFactor(userInputs.ProjectDeliveryStrategyFactor, userInputs.BuildingType);

            return NormalizeToHundredth(valueBuildingTypePrjectStrategyFactor * (userInputs.G30G50SUSum + userInputs.Z910SU + userInputs.Z920SU + userInputs.Z930SU + userInputs.Z10SU + userInputs.Z70SU));

        }

        private double GetBuildingTypePrjectStrategyFactor(ProjectDeliveryStrategyFactor projectDeliveryStrategyFactor, BuildingType buildingType)
        {
            double result = 0;
            switch (buildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.LaboratoryBuilding:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.StudentHousing:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.Dining:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                default:
                    throw new Exception("Unknown type of  Building ");

            }

            return result;
        }

    }
}
