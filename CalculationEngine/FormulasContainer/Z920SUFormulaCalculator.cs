﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z920SUFormulaCalculator : BaseFormulaCalculator<Z920SUViewModel, double>
    {
        IReadLocalStorage _localStorage;
        IFormulasContainer _formulasContainer;

        public Z920SUFormulaCalculator(IReadLocalStorage localStorage, IFormulasContainer formulasContainer)
        {
            _localStorage = localStorage;
            _formulasContainer = formulasContainer;
        }

        /// Example:
        public override double CalculateFormula(Z920SUViewModel userInputs)
        {
            var escalation = _formulasContainer.CalculateZ920Rate(userInputs);
            return NormalizeToHundredth(escalation * (userInputs.G30G50SUSum + userInputs.Z910SU));
        }
    }
}
