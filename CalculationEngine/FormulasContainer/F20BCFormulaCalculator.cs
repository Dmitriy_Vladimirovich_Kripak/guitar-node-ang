﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class F20BCFormulaCalculator : BaseFormulaCalculator<F20BCViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public F20BCFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(F20BCViewModel userInputs)
        {

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            double facilityRemediation = 0;
            switch (userInputs.FacilityRemediation)
            {
                case FacilityRemediation.Complex:
                    facilityRemediation = _localStorage.GetValueAsDouble(BaselineVariableNames.FacilityRemediationComplex);
                    break;
                case FacilityRemediation.Simple:
                    facilityRemediation = _localStorage.GetValueAsDouble(BaselineVariableNames.FacilityRemediationSimple);
                    break;
                default:
                    throw new Exception("Unknown type of Facility Remediation");
            }

            double facilityRemediationRate = _localStorage.GetValueAsDouble(BaselineVariableNames.FacilityRemediationRate);

            return NormalizeToHundredth((facilityRemediationRate * userInputs.AreaOfFacilityToBeRemediated * locationRatio * projectDeliveryRatio * facilityRemediation) / userInputs.GrossSquareFeet);

        }
    }
}
