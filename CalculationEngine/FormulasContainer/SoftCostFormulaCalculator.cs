﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class SoftCostFormulaCalculator : BaseFormulaCalculator<SoftCostViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public SoftCostFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(SoftCostViewModel userInputs)
        {

            if(userInputs.DesignFee < 0 || userInputs.DesignFee > 0.2)
                throw new Exception("Design Fees should be between 0% - 20%");
            if (userInputs.FFE < 0 || userInputs.FFE > 0.2)
                throw new Exception("FFE should be between 0% - 20%");
            if (userInputs.Technology < 0 || userInputs.Technology > 0.2)
                throw new Exception("AV / Technology  should be between 0% - 20%");
            if (userInputs.Contingency < 0 || userInputs.Contingency > 0.2)
                throw new Exception("Contingency  should be between 0% - 20%");
            if (userInputs.Misc < 0 || userInputs.Misc > 0.2)
                throw new Exception("Misc  should be between 0% - 20%");

            return NormalizeToHundredth(userInputs.TotalConstructionCosts * (userInputs.DesignFee + userInputs.FFE + userInputs.Technology + userInputs.Contingency + userInputs.Misc));
        }
    }
}
