﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class BuildingTypeProjectStrategyFactorCalculator
    {
        IReadLocalStorage _localStorage;

        public BuildingTypeProjectStrategyFactorCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        //TODO: need to refactor object userInputs!!!
        public double GetFactor(object userInputs)
        {
            Type type = userInputs.GetType();
            ProjectDeliveryStrategyFactor projectDeliveryStrategyFactor = (ProjectDeliveryStrategyFactor)type.GetProperty("ProjectDeliveryStrategyFactor").GetValue(userInputs);
            BuildingType buildingType = (BuildingType)type.GetProperty("BuildingType").GetValue(userInputs);
            double result = 0;
            switch (buildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.LaboratoryBuilding:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.StudentHousing:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.Dining:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ940BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            if (type == typeof(Z10BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ10BC);
                            if (type == typeof(Z70BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC);
                            if (type == typeof(Z930BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ930BC);
                            if (type == typeof(Z940BCViewModel))
                                result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ940BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                default:
                    throw new Exception("Unknown type of  Building ");

            }

            return result;
        }
    }
}