﻿using Application.Models.Calculation;

namespace CalculationEngine.FormulasContainer
{
    public class Z920FitoutFormulaCalculator : BaseFormulaCalculator<Z920FitoutViewModel, double>
    {
        IReadLocalStorage _localStorage;
        IFormulasContainer _formulasContainer;

        public Z920FitoutFormulaCalculator(IReadLocalStorage localStorage, IFormulasContainer formulasContainer)
        {
            _localStorage = localStorage;
            _formulasContainer = formulasContainer;
        }

        public override double CalculateFormula(Z920FitoutViewModel userInputs)
        {
            var escalation = _formulasContainer.CalculateZ920Rate(userInputs);
            return NormalizeToHundredth(escalation * (userInputs.A10F30SumOfFitouts + userInputs.Z910Fitout));
        }
    }
}
