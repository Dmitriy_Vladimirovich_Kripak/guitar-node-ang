﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z70BCFormulaCalculator : BaseFormulaCalculator<Z70BCViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public Z70BCFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(Z70BCViewModel userInputs)
        {

            double valueBuildingTypePrjectStrategyFactor = GetBuildingTypePrjectStrategyFactor(userInputs.ProjectDeliveryStrategyFactor, userInputs.BuildingType);

            return NormalizeToHundredth(valueBuildingTypePrjectStrategyFactor * (userInputs.A10F30SumOfBC + userInputs.Z910BC + userInputs.Z920BC + userInputs.Z930BC + userInputs.Z10BC));

        }

        private double GetBuildingTypePrjectStrategyFactor(ProjectDeliveryStrategyFactor projectDeliveryStrategyFactor, BuildingType buildingType)
        {
            double result = 0;
            switch (buildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ70BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.LaboratoryBuilding:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ70BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.StudentHousing:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ70BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.Dining:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ70BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ70BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ70BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ70BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                default:
                    throw new Exception("Unknown type of  Building ");

            }

            return result;
        }
    }
}
