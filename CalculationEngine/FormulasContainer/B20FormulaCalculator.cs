﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class B20FormulaCalculator : BaseFormulaCalculator<B20ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public B20FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        /// 
        /// Exterior Vertical Enclosures ($/GSF) = (
        /// ((Cladding Ratio) x ((Number of Above Grade Levels +1) / (Number of levels +1) x GSF)) x (1-Glazing Ratio) x (Solid Wall $/SF) x (Vertical Articulation Factor) x (Architectural Expectations x Performance Expectations, Sustainability Goal)) + 
        /// ((Cladding Ratio) x ((Number of Above Grade Levels +1) / (Number of levels +1) x GSF)) x (Glazing Ratio) x (Glazed Wall $/SF) x (Vertical Articulation Factor) x (Architectural Expectations x Performance Expectations, Sustainability Goals))
        /// )/GSF
        /// 
        /// In this example, based on parameters identified above, the calc would be the following:
        /// Exterior Vertical Enclosures($/GSF) = (
        /// ((0.6) x((4 +1) / (5+1) x 100,000)) x(1-0.3) x($80/SF) x(1.15) x(1.0 x 1.15)) + 
        /// ((0.6) x((4 +1) / (5 +1) x 100,000)) x(0.3) x($100/SF) x(1.15) x(1.0 x 1.15))
        /// )/100,000
        /// 
        public override double CalculateFormula(B20ViewModel userInputs)
        {
            if(userInputs.NumberOfLevels < 1 || userInputs.NumberOfLevels > 100)
            {
                throw new Exception(string.Format("The number of levels must be from 1 to 100, actual result is: {0}", userInputs.NumberOfLevels));
            }
            if (userInputs.NumberOfAboveGradeLevels < 0 || userInputs.NumberOfAboveGradeLevels > 100)
            {
                throw new Exception(string.Format("The number of Above Grade levels must be from 0 to 100, actual result is: {0}", userInputs.NumberOfAboveGradeLevels));
            }
            if (userInputs.VerticalArticulationFactor< 1 || userInputs.VerticalArticulationFactor > 2)
            {
                throw new Exception(string.Format("The Vertical Articulation Factor must be from 1 to 2, actual result is: {0}", userInputs.VerticalArticulationFactor));
            }

            if (userInputs.CladdingRatio < 0 || userInputs.CladdingRatio > 2)
            {
                throw new Exception(string.Format("The Cladding Ratio must be from 0 to 2, actual result is: {0}", userInputs.CladdingRatio));
            }
            if (userInputs.GlazingRatio < 0 || userInputs.GlazingRatio > 1)
            {
                throw new Exception(string.Format("The Glazing Ratio must be from 0 to 1, actual result is: {0}", userInputs.GlazingRatio));
            }

            double solidWallRatio = 0;
            double glazedWallRatio = 0;
            switch (userInputs.BuildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    solidWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingSolidWallSF);
                    glazedWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingGlazedWallSF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    solidWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingSolidWallSF);
                    glazedWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingGlazedWallSF);
                    break;
                case BuildingType.StudentHousing:
                    solidWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingSolidWallSF);
                    glazedWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingGlazedWallSF);
                    break;
                case BuildingType.Dining:
                    solidWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningSolidWallSF);
                    glazedWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningGlazedWallSF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    solidWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeSolidWallSF);
                    glazedWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeGlazedWallSF);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    solidWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageAboveGradeSolidWallSF);
                    glazedWallRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageAboveGradeGlazedWallSF);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");
            }
            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
            double architecturalRatio = 0;
            switch (userInputs.ArchitecturalExpectation)
            {
                case ArchitecturalExpectation.Iconic:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicB20);
                    break;
                case ArchitecturalExpectation.CampusStandard:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardB20);
                    break;
                case ArchitecturalExpectation.LowCost:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostB20);
                    break;
                default:
                    throw new Exception("Unknown type of Architectural Expectation");
            }

            double performanceRatio = 0;
            switch (userInputs.PerformanceExpectation)
            {
                case PerformanceExpectation.LEEDPlatinumEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB20);
                    break;
                case PerformanceExpectation.LEEDGoldEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentB20);
                    break;
                case PerformanceExpectation.LEEDSilverEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentB20);
                    break;
                default:
                    throw new Exception("Unknown type of Architectural Expectation");
            }

            double verticalArticulationRatio = userInputs.VerticalArticulationFactor * architecturalRatio * performanceRatio;
            double aboveGradeLevelsRatio = userInputs.CladdingRatio * (double)userInputs.GrossSquareFeet * (double)(userInputs.NumberOfAboveGradeLevels + 1) / (double)(userInputs.NumberOfLevels + 1);
            double solidWallCalc = aboveGradeLevelsRatio * (1 - userInputs.GlazingRatio) * solidWallRatio * verticalArticulationRatio;
            double glazedWallCalc = aboveGradeLevelsRatio * userInputs.GlazingRatio * glazedWallRatio * verticalArticulationRatio;

            return NormalizeToHundredth((solidWallCalc + glazedWallCalc)  * locationRatio * projectDeliveryRatio / (double)userInputs.GrossSquareFeet);
        }


    }
}
