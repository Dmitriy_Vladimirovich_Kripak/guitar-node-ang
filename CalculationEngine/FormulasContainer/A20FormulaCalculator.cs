﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class A20FormulaCalculator: BaseFormulaCalculator<A20ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public A20FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        /// 
        /// Subgrade Enclosures ($/GSF) = ((Cladding Ratio) x (Number of Below Grade Levels / Number of levels x GSF)) x 
        /// (Basement Walls $/SF + 
        /// (Retention $/SF + (Earthwork $/CY x 5 / 27)) 
        /// OR + 
        /// (Number of Below Grade Levels x F-F Height x Earthwork $/CY)/2/27))
        /// ) / GSF
        /// 
        /// So, in the case of an Academic building of 5 stories built on rippable rock,
        /// with a cladding ratio of 0.6, f-f height of 20’, one basement level, and a constrained site, the following would be the calc.
        /// Subgrade Enclosures($/GSF) = ((0.6) x(1/5 x 100,000)) x($50/SF + $65/SF + (5 x $40/CY / 27)))/100,000 = $14.68/GSF
        /// 
        /// ...and a open site:
        /// Subgrade Enclosures ($/GSF) = ((0.6) x (1/5 x 100,000)) x ($50/SF + (1x20x$40/CY/2/27))))/100,000 = $7.78/GSF
        /// 
        public override double CalculateFormula(A20ViewModel userInputs)
        {
            if(userInputs.NumberOfLevels < 1 || userInputs.NumberOfLevels > 100)
            {
                throw new Exception(string.Format("The number of levels must be from 1 to 100, actual result is: {0}", userInputs.NumberOfLevels));
            }

            if (userInputs.NumberOfBelowGradeLevels < 0 || userInputs.NumberOfBelowGradeLevels > 5)
            {
                throw new Exception(string.Format("The number of basement levels must be from 0 to 5, actual result is: {0}", userInputs.NumberOfBelowGradeLevels));
            }

            if (userInputs.CladdingRatio < 0 || userInputs.CladdingRatio > 2)
            {
                throw new Exception(string.Format("The Cladding Ratio must be from 0 to 2, actual result is: {0}", userInputs.CladdingRatio));
            }

            double siteConstraints = 0;
            switch (userInputs.SiteConstraint)
            {
                case SiteConstraint.Constrained:
                    siteConstraints = CalcConstrained(userInputs);
                    break;
                case SiteConstraint.Open:
                    siteConstraints = CalcOpen(userInputs);
                    break;
                default:
                    throw new Exception("Unknown type of Soil Condition");
            }

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            double ratio = userInputs.CladdingRatio * ((double)userInputs.NumberOfBelowGradeLevels / (double)userInputs.NumberOfLevels) * (double)userInputs.GrossSquareFeet;
            var result = ratio * (_localStorage.GetValueAsDouble(BaselineVariableNames.BasementWallsSF) + siteConstraints) * projectDeliveryRatio * locationRatio / (double)userInputs.GrossSquareFeet;
            return NormalizeToHundredth(result);

        }

        private double CalcConstrained(A20ViewModel model)
        {
            return
                _localStorage.GetValueAsDouble(BaselineVariableNames.RetentionSF) +
            (_localStorage.GetValueAsDouble(BaselineVariableNames.EarthworkExcavationBackfillCY) * (double) model.NumberOfLevels / 27);

        }


        private double CalcOpen(A20ViewModel model)
        {
            return
                ((double)model.NumberOfBelowGradeLevels * (double)model.FFHeights * _localStorage.GetValueAsDouble(BaselineVariableNames.EarthworkExcavationBackfillCY))/ (2 * 27);
        }
    }
}