﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public abstract class BaseFormulaCalculator<IN, OUT>
    {
        public abstract OUT CalculateFormula(IN userInputs);
                
        public double NormalizeToHundredth(double input)
        {
            return Convert.ToInt32(input * 100) / 100.0;
        }
    }
}