﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class D10FormulaCalculator : BaseFormulaCalculator<D10ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public D10FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }
        /// Example:
        /// Conveying Formula
        /// Conveying $/GSF = 
        /// (Building Type Passenger $/STP) x(Building Type Passenger STP/SF) + 
        /// (Building Type Service $/STP) x(Building Type Service STP/SF)
        /// 
        /// Conveying $/GSF = ($50,000/STP) x(0.00005) + ($70,000/STP) x(0.00003)
        /// 
        public override double CalculateFormula(D10ViewModel userInputs)
        {
            double buildingTypePassengerSTP = 0;
            double buildingTypeServiceSTP = 0;
            double buildingTypePassengerSTP_SF = 0;
            double buildingTypeServiceSTP_SF = 0;
            switch (userInputs.BuildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    buildingTypePassengerSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorAcademicClassroomPassengerSTP);
                    buildingTypeServiceSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorAcademicClassroomServiceSTP);
                    buildingTypePassengerSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorAcademicClassroomPassengerSTP_SF);
                    buildingTypeServiceSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorAcademicClassroomServiceSTP_SF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    buildingTypePassengerSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorLaboratoryPassengerSTP);
                    buildingTypeServiceSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorLaboratoryServiceSTP);
                    buildingTypePassengerSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorLaboratoryPassengerSTP_SF);
                    buildingTypeServiceSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorLaboratoryServiceSTP_SF);
                    break;
                case BuildingType.StudentHousing:
                    buildingTypePassengerSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorStudentHousingPassengerSTP);
                    buildingTypeServiceSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorStudentHousingServiceSTP);
                    buildingTypePassengerSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorStudentHousingPassengerSTP_SF);
                    buildingTypeServiceSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorStudentHousingServiceSTP_SF);
                    break;
                case BuildingType.Dining:
                    buildingTypePassengerSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorDiningPassengerSTP);
                    buildingTypeServiceSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorDiningServiceSTP);
                    buildingTypePassengerSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorDiningPassengerSTP_SF);
                    buildingTypeServiceSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorDiningServiceSTP_SF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    buildingTypePassengerSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorParkingGarageBelowGradePassengerSTP);
                    buildingTypeServiceSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorParkingGarageBelowGradeServiceSTP);
                    buildingTypePassengerSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorParkingGarageBelowGradePassengerSTP_SF);
                    buildingTypeServiceSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorParkingGarageBelowGradeServiceSTP_SF);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    buildingTypePassengerSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorParkingGarageAboveGradePassengerSTP);
                    buildingTypeServiceSTP = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorParkingGarageAboveGradeServiceSTP);
                    buildingTypePassengerSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorParkingGarageAboveGradePassengerSTP_SF);
                    buildingTypeServiceSTP_SF = _localStorage.GetValueAsDouble(BaselineVariableNames.ElevatorParkingGarageAboveGradeServiceSTP_SF);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");
            }
            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            return NormalizeToHundredth((buildingTypePassengerSTP * buildingTypePassengerSTP_SF + buildingTypeServiceSTP * buildingTypeServiceSTP_SF) * projectDeliveryRatio * locationRatio);
        }
    }
}
