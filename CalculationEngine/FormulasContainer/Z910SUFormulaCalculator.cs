﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z910SUFormulaCalculator : BaseFormulaCalculator<Z910SUViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public Z910SUFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(Z910SUViewModel userInputs)
        {
            return NormalizeToHundredth(userInputs.ContingenciesDesign*userInputs.G30G50SUSum);
        }
    }
}
