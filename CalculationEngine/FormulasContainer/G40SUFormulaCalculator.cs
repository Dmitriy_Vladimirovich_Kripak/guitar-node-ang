﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class G40SUFormulaCalculator : BaseFormulaCalculator<G40SUViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public G40SUFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(G40SUViewModel userInputs)
        {
            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            double siteInfrastructureServices = 0;
            switch (userInputs.SiteInfrastructureServices)
            {
                case SiteInfrastructureServices.Proximate:
                    siteInfrastructureServices = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteInfrastructureServicesProximate);
                    break;
                case SiteInfrastructureServices.Remote:
                    siteInfrastructureServices = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteInfrastructureServicesRemote);
                    break;
                default:
                    throw new Exception("Unknown type of Site Infrastructure Services");
            }

            double siteInfrastructureElectrical = 0;
            if (userInputs.SiteInfrastructureElectrical)
                siteInfrastructureElectrical = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteInfrastructureElectrical);

            double siteLighting = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteLighting);

            return NormalizeToHundredth((((siteInfrastructureServices * siteInfrastructureElectrical) + (userInputs.DevelopedSiteArea) * siteLighting) * locationRatio * projectDeliveryRatio) / userInputs.DevelopedSiteArea);

        }



    }
}
