﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class A60FormulaCalculator : BaseFormulaCalculator<A60ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public A60FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        public override double CalculateFormula(A60ViewModel userInputs)
        {
            if (userInputs.BuildingFootprint <= 0)
            {
                throw new Exception(string.Format("The Footprint Areas should be more than 0 , actual result is: {0}", userInputs.NumberOfLevels));
            }
            if (userInputs.CladdingRatio < 0 || userInputs.CladdingRatio > 2)
                throw new Exception(string.Format("The Cladding Ratio must be from 0 to 2, actual result is: {0}", userInputs.CladdingRatio));

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }


            double foundationDrainage = _localStorage.GetValueAsDouble(BaselineVariableNames.FoundationDrainage);

            double result = ((userInputs.BuildingFootprint * userInputs.CladdingRatio) / userInputs.BasementFFHeights + userInputs.BuildingFootprint * 0.05) * foundationDrainage * projectDeliveryRatio * locationRatio / (double)userInputs.GrossSquareFeet;

            return NormalizeToHundredth(result);
        }
    }
}
