﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class TotalProjectCostFormulaCalculator : BaseFormulaCalculator<TotalProjectCostViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public TotalProjectCostFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(TotalProjectCostViewModel userInputs)
        {
            return NormalizeToHundredth(userInputs.TotalConstructionCosts + userInputs.SoftCosts);
        }
    }
}
