﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class G30SUFormulaCalculator : BaseFormulaCalculator<G30SUViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public G30SUFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(G30SUViewModel userInputs)
        {
            double siteServicesHotWater = 0;
            double siteServicesSteamCondensate = 0;
            double siteServicesChilledWaterSupply = 0;
            double siteCivilWaterFire = 0;
            double siteCivilSanitarySewer = 0;
            double siteCivilStormSewer = 0;
            double siteCivilGas = 0;

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
            double siteInfrastructureServices = 0;
            switch (userInputs.SiteInfrastructureServices)
            {
                case SiteInfrastructureServices.Remote:
                    siteInfrastructureServices = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteInfrastructureServicesRemote);
                    break;
                case SiteInfrastructureServices.Proximate:
                    siteInfrastructureServices = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteInfrastructureServicesProximate);
                    break;
             
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
           
            double buildingPlant = 0;
            switch (userInputs.BuildingPlant)
            {
                case BuildingPlant.Local:
                    buildingPlant = _localStorage.GetValueAsDouble(BaselineVariableNames.BuildingPlantLocalG30SU);
                    break;
                case BuildingPlant.Remote:
                    buildingPlant = _localStorage.GetValueAsDouble(BaselineVariableNames.BuildingPlantRemoteG30SU);
                    break;              

                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
            double siteInfrastructureCivil = 0;
            switch (userInputs.SiteInfrastructureCivil)
            {
                case SiteInfrastructureCivil.Remote:
                    siteInfrastructureCivil = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteInfrastructureCivilRemote);
                    break;
                case SiteInfrastructureCivil.Proximate:
                    siteInfrastructureCivil = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteInfrastructureCivilProximate);
                    break;

                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
            if(userInputs.SiteServicesChilledWaterSupply)
               siteServicesChilledWaterSupply = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteServicesChilledWaterSupply);

            if (userInputs.SiteServicesHotWater)
                siteServicesHotWater = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteServicesHotWaterSupply);

            if (userInputs.SiteServicesSteamCondensate)
                siteServicesSteamCondensate = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteServicesSteamCondensate);



            if (userInputs.SiteCivilWaterFire)
                siteCivilWaterFire = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteCivilWaterFire);

            if (userInputs.SiteCivilStormSewer)
                siteCivilStormSewer = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteCivilStormSewer);

            if (userInputs.SiteCivilSanitarySewer)
                siteCivilSanitarySewer = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteCivilSanitarySewer);

            if (userInputs.SiteCivilGas)
                siteCivilGas = _localStorage.GetValueAsDouble(BaselineVariableNames.SiteCivilGas);

            return NormalizeToHundredth(((buildingPlant * (siteInfrastructureServices * siteServicesChilledWaterSupply + siteInfrastructureServices * siteServicesHotWater
               + siteInfrastructureServices * siteServicesSteamCondensate) + siteInfrastructureCivil * siteCivilWaterFire +
               siteInfrastructureCivil * siteCivilStormSewer + siteInfrastructureCivil * siteCivilSanitarySewer + siteInfrastructureCivil * siteCivilGas) * locationRatio * projectDeliveryRatio) / userInputs.DevelopedSiteArea); 

        }



    }
}
