﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z910FitoutFormulaCalculator : BaseFormulaCalculator<Z910FitoutViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public Z910FitoutFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(Z910FitoutViewModel userInputs)
        {
            return NormalizeToHundredth(userInputs.ContingenciesDesign * userInputs.A10F30SumOfFitouts);
        }
    }
}
