﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class A40FormulaCalculator : BaseFormulaCalculator<A40ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public A40FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }
        /// Example
        /// 
        /// Slabs-on-Grade ($/GSF) = (Footprint Areas) x (Building Type Slab on Grade $/SF) / GSF
        /// 
        /// So, in the case of an Academic building of 5 stories built on rippable rock, 
        /// with a cladding ratio of 0.6, f-f height of 20’, one basement level, and a constrained site, 
        /// the following would be the calc.
        /// Slabs-on-Grade($/GSF) = (100,000/5) x($10/SF) / 100,000 = $2/SF
        ///                 
        public override double CalculateFormula(A40ViewModel userInputs)
        {
            if(userInputs.NumberOfLevels < 1 || userInputs.NumberOfLevels > 100)
            {
                throw new Exception(string.Format("The number of levels must be from 1 to 100, actual result is: {0}", userInputs.NumberOfLevels));
            }

            double buildingTypeSlab = 0;
            switch (userInputs.BuildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    buildingTypeSlab = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingSlabSF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    buildingTypeSlab = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingSlabSF);
                    break;
                case BuildingType.StudentHousing:
                    buildingTypeSlab = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingSlabSF);
                    break;
                case BuildingType.Dining:
                    buildingTypeSlab = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningSlabSF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    buildingTypeSlab = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeSlabSF);
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    buildingTypeSlab = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageAboveGradeSlabSF);
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");
            }

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            

            double result = userInputs.BuildingFootprint * buildingTypeSlab * projectDeliveryRatio * locationRatio / (double)userInputs.GrossSquareFeet;

            return NormalizeToHundredth(result);
        }
    }
}
