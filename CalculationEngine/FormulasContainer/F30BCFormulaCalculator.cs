﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class F30BCFormulaCalculator : BaseFormulaCalculator<F30BCViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public F30BCFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(F30BCViewModel userInputs)
        {

            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }

            double facilityDemolition = 0;
            switch (userInputs.FacilityDemolition)
            {
                case FacilityDemolition.Complex:
                    facilityDemolition = _localStorage.GetValueAsDouble(BaselineVariableNames.FacilityDemolitionComplex);
                    break;
                case FacilityDemolition.Simple:
                    facilityDemolition = _localStorage.GetValueAsDouble(BaselineVariableNames.FacilityDemolitionSimple);
                    break;
                default:
                    throw new Exception("Unknown type of Facility Demolition");
            }

            double facilityDemolitionRate = _localStorage.GetValueAsDouble(BaselineVariableNames.FacilityDemolitionRate);

            return NormalizeToHundredth((facilityDemolitionRate * userInputs.AreaOfFacilityToBeDemolished * locationRatio * projectDeliveryRatio * facilityDemolition) / userInputs.GrossSquareFeet);

        }
    }
}
