﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class B30FormulaCalculator : BaseFormulaCalculator<B30ViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public B30FormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        /// Exterior Horizontal Enclosures($/GSF) = 
        /// (
        /// (Footprint) x(Horizontal Articulation Factor) x(Roof Terrace Ratio) x
        /// (Building Type Terrace Roof $/SF) x(Architectural Expectations x Performance Expectations, Sustainability Goal)
        /// + 
        /// (
        /// (Footprint) x(Horizontal Articulation Factor) x(Roof Glazing Ratio) x
        /// (Building Type Glazed Roof $/SF) x(Architectural Expectations x Performance Expectations, Sustainability Goal)
        /// ) + 
        /// (
        ///     (
        ///     (Footprint) x(Horizontal Articulation Factor) - ((Footprint) x(Horizontal Articulation Factor) x(Roof Terrace Ratio)) -
        ///     ((Footprint) x(Horizontal Articulation Factor) x (Roof Glazing Ratio))
        ///     ) x (Building Type Solid Roof $/SF) x (Architectural Expectations x Performance Expectations, Sustainability Goal)
        /// ) + 
        /// ((Footprint) x(Horizontal Articulation Factor) – (Footprint)) x(Building Type Soffit $/SF))
        /// )/GSF
        ///
        /// 
        /// In this example, based on parameters identified above, the calc would be the following:
        ///	Exterior Horizontal Enclosures($/GSF) = 
        ///	(
        ///	(100,000/5) x (1.2) x (0.1) x (50) x (1.0x1.15)) 
        ///	+ 
        ///	((100,000/5) x (1.2) x (0.05) x (175) x (1.0x1.5)) 
        ///	+ 
        ///	(
        ///	((100,000/5) x (1.2) - ((100,000/5) x (1.2) x (0.1)) - ((100,000/5) x (1.2) x (0.1))) 
        ///	x (17.5) x (1.0x1.15)
        ///	) + 
        ///	((100,000/5) x (1.2) – (100,000/5)) x (45))
        ///	)/100,000
        /// 
        public override double CalculateFormula(B30ViewModel userInputs)
        {
            if(userInputs.NumberOfLevels < 1 || userInputs.NumberOfLevels > 100)
            {
                throw new Exception(string.Format("The number of levels must be from 1 to 100, actual result is: {0}", userInputs.NumberOfLevels));
            }
            if (userInputs.HorizontalArticulationFactor < 1 || userInputs.HorizontalArticulationFactor > 2)
            {
                throw new Exception(string.Format("The Horizontal Articulation Factor must be from 1 to 2, actual result is: {0}", userInputs.HorizontalArticulationFactor));
            }
            if (userInputs.RoofGlazingRatio < 0 || userInputs.RoofGlazingRatio > 1)
            {
                throw new Exception(string.Format("The Roof Glazing Ratio must be from 0 to 1, actual result is: {0}", userInputs.RoofGlazingRatio));
            }
            if (userInputs.RoofTerraceRatio < 0 || userInputs.RoofTerraceRatio > 1)
            {
                throw new Exception(string.Format("The Roof Terrace Ratio must be from 0 to 1, actual result is: {0}", userInputs.RoofTerraceRatio));
            }

            double solidRoofRatio = 0;
            double glazedRoofRatio = 0;
            double terraceRoofRatio = 0;
            double soffitRoofRatio = 0;
            switch (userInputs.BuildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    solidRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingSolidRoofSF);
                    glazedRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingGlazedRoofSF);
                    terraceRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingTerraceRoofSF);
                    soffitRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.AcademicClassroomBuildingSoffitRoofSF);
                    break;
                case BuildingType.LaboratoryBuilding:
                    solidRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingSolidRoofSF);
                    glazedRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingGlazedRoofSF);
                    terraceRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingTerraceRoofSF);
                    soffitRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LaboratoryBuildingSoffitRoofSF);
                    break;
                case BuildingType.StudentHousing:
                    solidRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingSolidRoofSF);
                    glazedRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingGlazedRoofSF);
                    terraceRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingTerraceRoofSF);
                    soffitRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.StudentHousingSoffitRoofSF);
                    break;
                case BuildingType.Dining:
                    solidRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningSolidRoofSF);
                    glazedRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningGlazedRoofSF);
                    terraceRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningTerraceRoofSF);
                    soffitRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DiningSoffitRoofSF);
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    solidRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeSolidRoofSF);
                    glazedRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageBelowGradeGlazedRoofSF);
                    terraceRoofRatio = 0;
                    soffitRoofRatio = 0;
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    solidRoofRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ParkingGarageAboveGradeSolidRoofSF);
                    glazedRoofRatio = 0;
                    terraceRoofRatio = 0;
                    soffitRoofRatio = 0;
                    break;
                default:
                    throw new Exception("Unknown type of Building Type");
            }
            double locationRatio = 0;
            switch (userInputs.LocationFactor)
            {
                case LocationFactor.LosAngeles:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorLosAngeles);
                    break;
                case LocationFactor.NationalAverage:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorNationalAverage);
                    break;
                case LocationFactor.TBD:
                    locationRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.LocationFactorTBD);
                    break;
                default:
                    throw new Exception("Unknown type of Location Factor");
            }


            double projectDeliveryRatio = 0;
            switch (userInputs.ProjectDeliveryStrategyFactor)
            {
                case ProjectDeliveryStrategyFactor.DesignBidBuild:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuild);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1);
                    break;
                case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2);
                    break;
                case ProjectDeliveryStrategyFactor.CMARNational:
                    projectDeliveryRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNational);
                    break;
                default:
                    throw new Exception("Unknown type of Project Delivery Strategy Factor");
            }
            double architecturalRatio = 0;
            switch (userInputs.ArchitecturalExpectation)
            {
                case ArchitecturalExpectation.Iconic:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationIconicB30);
                    break;
                case ArchitecturalExpectation.CampusStandard:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationCampusStandardB30);
                    break;
                case ArchitecturalExpectation.LowCost:
                    architecturalRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorArchitecturalExpectationLowCostB30);
                    break;
                default:
                    throw new Exception("Unknown type of Architectural Expectation");
            }

            double performanceRatio = 0;
            switch (userInputs.PerformanceExpectation)
            {
                case PerformanceExpectation.LEEDPlatinumEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDPlatinumEquivalentB30);
                    break;
                case PerformanceExpectation.LEEDGoldEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDGoldEquivalentB30);
                    break;
                case PerformanceExpectation.LEEDSilverEquivalent:
                    performanceRatio = _localStorage.GetValueAsDouble(BaselineVariableNames.DesignFactorPerformanceExpectationLEEDSilverEquivalentB30);
                    break;
                default:
                    throw new Exception("Unknown type of Architectural Expectation");
            }

         

            double terraceRoofCalc = userInputs.BuildingFootprint * userInputs.HorizontalArticulationFactor * userInputs.RoofTerraceRatio * terraceRoofRatio * architecturalRatio * performanceRatio;
            double glazedRoofCalc = userInputs.BuildingFootprint * userInputs.HorizontalArticulationFactor * userInputs.RoofGlazingRatio * glazedRoofRatio * architecturalRatio * performanceRatio;
            double solidRoofCalc = userInputs.BuildingFootprint * (userInputs.HorizontalArticulationFactor - userInputs.HorizontalArticulationFactor * userInputs.RoofTerraceRatio - userInputs.HorizontalArticulationFactor * userInputs.RoofGlazingRatio);
            double solidRoofCalcMultiplyRatio = solidRoofCalc* solidRoofRatio * architecturalRatio * performanceRatio;
            double soffitRoofCalc = (userInputs.BuildingFootprint * userInputs.HorizontalArticulationFactor - userInputs.BuildingFootprint) * soffitRoofRatio ;

            return NormalizeToHundredth((terraceRoofCalc + glazedRoofCalc + solidRoofCalcMultiplyRatio + soffitRoofCalc) * locationRatio * projectDeliveryRatio / (double)userInputs.GrossSquareFeet);
        }
    }
}
