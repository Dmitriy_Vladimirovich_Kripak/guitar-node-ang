﻿using Application.Common;
using Application.Models.Calculation;
using System;

namespace CalculationEngine.FormulasContainer
{
    public class Z930FitoutFormulaCalculator : BaseFormulaCalculator<Z930FitoutViewModel, double>
    {
        IReadLocalStorage _localStorage;

        public Z930FitoutFormulaCalculator(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        /// Example:
        public override double CalculateFormula(Z930FitoutViewModel userInputs)
        {

            double valueBuildingTypePrjectStrategyFactor = GetBuildingTypePrjectStrategyFactor(userInputs.ProjectDeliveryStrategyFactor, userInputs.BuildingType);

            return NormalizeToHundredth(valueBuildingTypePrjectStrategyFactor * (userInputs.A10F30SumOfFitouts + userInputs.Z910Fitout + userInputs.Z920Fitout));

        }

        private double GetBuildingTypePrjectStrategyFactor(ProjectDeliveryStrategyFactor projectDeliveryStrategyFactor, BuildingType buildingType)
        {
            double result = 0;
            switch (buildingType)
            {
                case BuildingType.AcademicClassroomBuilding:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildAcademicClassroomZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1AcademicClassroomZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2AcademicClassroomZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalAcademicClassroomZ930BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.LaboratoryBuilding:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildLaboratoryZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1LaboratoryZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2LaboratoryZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalLaboratoryZ930BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.StudentHousing:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildStudentHousingZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1StudentHousingZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2StudentHousingZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalStudentHousingZ930BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.Dining:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildDiningZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1DiningZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2DiningZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalDiningZ930BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.ParkingGarageBelowGrade:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageBelowGradeZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageBelowGradeZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageBelowGradeZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageBelowGradeZ930BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                case BuildingType.ParkingGarageAboveGrade:
                    switch (projectDeliveryStrategyFactor)
                    {
                        case ProjectDeliveryStrategyFactor.DesignBidBuild:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorDesignBidBuildParkingGarageAboveGradeZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier1:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier1ParkingGarageAboveGradeZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARRegionalTier2:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARRegionalTier2ParkingGarageAboveGradeZ930BC);
                            break;
                        case ProjectDeliveryStrategyFactor.CMARNational:
                            result = _localStorage.GetValueAsDouble(BaselineVariableNames.ProjectDeliveryStrategyFactorCMARNationalParkingGarageAboveGradeZ930BC);
                            break;
                        default:
                            throw new Exception("Unknown type of  Project Delivery Strategy Factor");
                    }
                    break;
                default:
                    throw new Exception("Unknown type of  Building ");

            }

            return result;
        }
    }
}
