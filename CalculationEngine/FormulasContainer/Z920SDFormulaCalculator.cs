﻿using Application.Common;
using Application.Models.Calculation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine.FormulasContainer
{
    public class Z920SDFormulaCalculator : BaseFormulaCalculator<Z920SDViewModel, double>
    {
        IReadLocalStorage _localStorage;
        IFormulasContainer _formulasContainer;

        public Z920SDFormulaCalculator(IReadLocalStorage localStorage, IFormulasContainer formulasContainer)
        {
            _localStorage = localStorage;
            _formulasContainer = formulasContainer;
        }

        /// Example:
        public override double CalculateFormula(Z920SDViewModel userInputs)
        {
            var escalation = _formulasContainer.CalculateZ920Rate(userInputs);
            return NormalizeToHundredth(escalation * (userInputs.G10G60Sum + userInputs.Z910SD));

        }
    }
}
