﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine
{
    public interface IUpdateLocalStorage
    {
        void Update(IDictionary<string, string> newVariables);

        void Update(Tuple<string, string> newVariable);

        bool Initialized { get; }
    }
}
