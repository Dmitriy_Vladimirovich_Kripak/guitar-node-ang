﻿using Application.Common;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using CalculationEngine.FormulasContainer;
using CalculationEngine.FormulasContainer.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine
{
    public class FormulasCalculatorContainer : IFormulasContainer
    {
        private IReadLocalStorage _localStorage;
        A10FormulaCalculator _a10FormulaCalculator;
        A20FormulaCalculator _a20FormulaCalculator;
        A40FormulaCalculator _a40FormulaCalculator;
        A60FormulaCalculator _a60FormulaCalculator;
        A90FormulaCalculator _a90FormulaCalculator;
        B10FormulaCalculator _b10FormulaCalculator;
        B20FormulaCalculator _b20FormulaCalculator;
        B30FormulaCalculator _b30FormulaCalculator;
        D10FormulaCalculator _d10FormulaCalculator;
        C10FormulaCalculator _c10FormulaCalculator;
        C10FitoutFormulaCalculator _c10FitoutFormulaCalculator;
        C20FormulaCalculator _c20FormulaCalculator;
        C20FitoutFormulaCalculator _c20FitoutFormulaCalculator;
        D20FormulaCalculator _d20FormulaCalculator;
        D20FitoutFormulaCalculator _d20FitoutFormulaCalculator;
        D30FormulaCalculator _d30FormulaCalculator;
        D30FitoutFormulaCalculator _d30FitoutFormulaCalculator;
        D40FormulaCalculator _d40FormulaCalculator;
        D40FitoutFormulaCalculator _d40FitoutFormulaCalculator;
        D50FormulaCalculator _d50FormulaCalculator;
        D50FitoutFormulaCalculator _d50FitoutFormulaCalculator;
        D60FormulaCalculator _d60FormulaCalculator;
        D60FitoutFormulaCalculator _d60FitoutFormulaCalculator;
        D70FormulaCalculator _d70FormulaCalculator;
        D70FitoutFormulaCalculator _d70FitoutFormulaCalculator;
        D80FormulaCalculator _d80FormulaCalculator;
        D80FitoutFormulaCalculator _d80FitoutFormulaCalculator;
        E10FormulaCalculator _e10FormulaCalculator;
        E10FitoutFormulaCalculator _e10FitoutFormulaCalculator;
        E20FormulaCalculator _e20FormulaCalculator;
        E20FitoutFormulaCalculator _e20FitoutFormulaCalculator;


        F10BCFormulaCalculator _f10BCFormulaCalculator;
        F20BCFormulaCalculator _f20BCFormulaCalculator;
        F30BCFormulaCalculator _f30BCFormulaCalculator;
        Z10BCFormulaCalculator _z10BCFormulaCalculator;
        Z70BCFormulaCalculator _z70BCFormulaCalculator;
        Z910BCFormulaCalculator _z910BCFormulaCalculator;
        Z920BCFormulaCalculator _z920BCFormulaCalculator;
        Z930BCFormulaCalculator _z930BCFormulaCalculator;
        Z940BCFormulaCalculator _z940BCFormulaCalculator;

        Z10FitoutFormulaCalculator _z10FitoutFormulaCalculator;
        Z70FitoutFormulaCalculator _z70FitoutFormulaCalculator;
        Z910FitoutFormulaCalculator _z910FitoutFormulaCalculator;
        Z920FitoutFormulaCalculator _z920FitoutFormulaCalculator;
        Z930FitoutFormulaCalculator _z930FitoutFormulaCalculator;
        Z940FitoutFormulaCalculator _z940FitoutFormulaCalculator;

        G10SDFormulaCalculator _g10SDFormulaCalculator;
        G20SDFormulaCalculator _g20SDFormulaCalculator;
        G60SDFormulaCalculator _g60SDFormulaCalculator;
        Z10SDFormulaCalculator _z10SDFormulaCalculator;
        Z70SDFormulaCalculator _z70SDFormulaCalculator;
        Z910SDFormulaCalculator _z910SDFormulaCalculator;
        Z920SDFormulaCalculator _z920SDFormulaCalculator;
        Z930SDFormulaCalculator _z930SDFormulaCalculator;
        Z940SDFormulaCalculator _z940SDFormulaCalculator;
        G30SUFormulaCalculator _g30SUFormulaCalculator;
        G40SUFormulaCalculator _g40SUFormulaCalculator;
        G50SUFormulaCalculator _g50SUFormulaCalculator;
        Z10SUFormulaCalculator _z10SUFormulaCalculator;
        Z70SUFormulaCalculator _z70SUFormulaCalculator;
        Z910SUFormulaCalculator _z910SUFormulaCalculator;
        Z920SUFormulaCalculator _z920SUFormulaCalculator;
        Z930SUFormulaCalculator _z930SUFormulaCalculator;
        Z940SUFormulaCalculator _z940SUFormulaCalculator;
        SoftCostFormulaCalculator _softCostFormulaCalculator;
        TotalProjectCostFormulaCalculator _totalProjectCostFormulaCalculator;
        ProjectFormulaCalculator _projectFormulaCalculator;
        BuildingTypeProjectStrategyFactorCalculator _buildingTypeProjectStrategyFactorCalculator;
        Z920RateCalculator _z920RateCalculator;

        public FormulasCalculatorContainer(IReadLocalStorage localStorage)
        {
            _localStorage = localStorage;
            _a10FormulaCalculator = new A10FormulaCalculator(_localStorage);
            _a20FormulaCalculator = new A20FormulaCalculator(_localStorage);
            _a40FormulaCalculator = new A40FormulaCalculator(_localStorage);
            _a60FormulaCalculator = new A60FormulaCalculator(_localStorage);
            _a90FormulaCalculator = new A90FormulaCalculator(_localStorage);
            _b10FormulaCalculator = new B10FormulaCalculator(_localStorage);
            _b20FormulaCalculator = new B20FormulaCalculator(_localStorage);
            _b30FormulaCalculator = new B30FormulaCalculator(_localStorage);
            _d10FormulaCalculator = new D10FormulaCalculator(_localStorage);
            _c10FormulaCalculator = new C10FormulaCalculator(_localStorage);
            _c10FitoutFormulaCalculator = new C10FitoutFormulaCalculator(_localStorage);
            _c20FormulaCalculator = new C20FormulaCalculator(_localStorage);
            _c20FitoutFormulaCalculator = new C20FitoutFormulaCalculator(_localStorage);
            _d20FormulaCalculator = new D20FormulaCalculator(_localStorage);
            _d20FitoutFormulaCalculator = new D20FitoutFormulaCalculator(_localStorage);
            _d30FormulaCalculator = new D30FormulaCalculator(_localStorage);
            _d30FitoutFormulaCalculator = new D30FitoutFormulaCalculator(_localStorage);
            _d40FormulaCalculator = new D40FormulaCalculator(_localStorage);
            _d40FitoutFormulaCalculator = new D40FitoutFormulaCalculator(_localStorage);
            _d50FormulaCalculator = new D50FormulaCalculator(_localStorage);
            _d50FitoutFormulaCalculator = new D50FitoutFormulaCalculator(_localStorage);
            _d60FormulaCalculator = new D60FormulaCalculator(_localStorage);
            _d60FitoutFormulaCalculator = new D60FitoutFormulaCalculator(_localStorage);
            _d70FormulaCalculator = new D70FormulaCalculator(_localStorage);
            _d70FitoutFormulaCalculator = new D70FitoutFormulaCalculator(_localStorage);
            _d80FormulaCalculator = new D80FormulaCalculator(_localStorage);
            _d80FitoutFormulaCalculator = new D80FitoutFormulaCalculator(_localStorage);
            _e10FormulaCalculator = new E10FormulaCalculator(_localStorage);
            _e10FitoutFormulaCalculator = new E10FitoutFormulaCalculator(_localStorage);
            _e20FormulaCalculator = new E20FormulaCalculator(_localStorage);
            _e20FitoutFormulaCalculator = new E20FitoutFormulaCalculator(_localStorage);
            _z920BCFormulaCalculator = new Z920BCFormulaCalculator(_localStorage, this);
            _f10BCFormulaCalculator = new F10BCFormulaCalculator(_localStorage);
            _f20BCFormulaCalculator = new F20BCFormulaCalculator(_localStorage);
            _f30BCFormulaCalculator = new F30BCFormulaCalculator(_localStorage);
            _z910BCFormulaCalculator = new Z910BCFormulaCalculator(_localStorage);
            _z930BCFormulaCalculator = new Z930BCFormulaCalculator(_localStorage);
            _z10BCFormulaCalculator = new Z10BCFormulaCalculator(_localStorage);
            _z70BCFormulaCalculator = new Z70BCFormulaCalculator(_localStorage);
            _z940BCFormulaCalculator = new Z940BCFormulaCalculator(_localStorage);
            _z10FitoutFormulaCalculator = new Z10FitoutFormulaCalculator(_localStorage);
            _z70FitoutFormulaCalculator = new Z70FitoutFormulaCalculator(_localStorage);
            _z910FitoutFormulaCalculator = new Z910FitoutFormulaCalculator(_localStorage);
            _z920FitoutFormulaCalculator = new Z920FitoutFormulaCalculator(_localStorage, this);
            _z930FitoutFormulaCalculator = new Z930FitoutFormulaCalculator(_localStorage);
            _z940FitoutFormulaCalculator = new Z940FitoutFormulaCalculator(_localStorage);
            _g10SDFormulaCalculator = new G10SDFormulaCalculator(_localStorage);
            _g20SDFormulaCalculator = new G20SDFormulaCalculator(_localStorage);
            _g60SDFormulaCalculator = new G60SDFormulaCalculator(_localStorage);
            _z10SDFormulaCalculator = new Z10SDFormulaCalculator(_localStorage);
            _z70SDFormulaCalculator = new Z70SDFormulaCalculator(_localStorage);
            _z910SDFormulaCalculator = new Z910SDFormulaCalculator(_localStorage);
            _z920SDFormulaCalculator = new Z920SDFormulaCalculator(_localStorage, this);
            _z930SDFormulaCalculator = new Z930SDFormulaCalculator(_localStorage);
            _z940SDFormulaCalculator = new Z940SDFormulaCalculator(_localStorage);
            _g30SUFormulaCalculator = new G30SUFormulaCalculator(_localStorage);
            _g40SUFormulaCalculator = new G40SUFormulaCalculator(_localStorage);
            _g50SUFormulaCalculator = new G50SUFormulaCalculator(_localStorage);
            _z10SUFormulaCalculator = new Z10SUFormulaCalculator(_localStorage);
            _z70SUFormulaCalculator = new Z70SUFormulaCalculator(_localStorage);
            _z910SUFormulaCalculator = new Z910SUFormulaCalculator(_localStorage);
            _z920SUFormulaCalculator = new Z920SUFormulaCalculator(_localStorage, this);
            _z930SUFormulaCalculator = new Z930SUFormulaCalculator(_localStorage);
            _z940SUFormulaCalculator = new Z940SUFormulaCalculator(_localStorage);
            _softCostFormulaCalculator = new SoftCostFormulaCalculator(_localStorage);
            _totalProjectCostFormulaCalculator = new TotalProjectCostFormulaCalculator(_localStorage);
            _projectFormulaCalculator = new ProjectFormulaCalculator(_localStorage);
            _buildingTypeProjectStrategyFactorCalculator = new BuildingTypeProjectStrategyFactorCalculator(_localStorage);
            _z920RateCalculator = new Z920RateCalculator(_localStorage);
        }

        public DateTime CalculateDataDate(BaseZ920ViewModel userInputs)
        {
            return _z920RateCalculator.CalculateDataDate(userInputs);
        }
        public string CalculateStrDataDate(BaseZ920ViewModel userInputs)
        {
            return _z920RateCalculator.CalculateStrDataDate(userInputs);
        }

        public double CalculateA10Foundations(A10ViewModel userInputs)
        {
            return _a10FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateA20Foundations(A20ViewModel userInputs)
        {
            return _a20FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateA40Foundations(A40ViewModel userInputs)
        {
            return _a40FormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateA60Foundations(A60ViewModel userInputs)
        {
            return _a60FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateA90Foundations(A90ViewModel userInputs)
        {
            return _a90FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateB10Foundations(B10ViewModel userInputs)
        {
            return _b10FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateB20Foundations(B20ViewModel userInputs)
        {
            return _b20FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateB30Foundations(B30ViewModel userInputs)
        {
            return _b30FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD10Foundations(D10ViewModel userInputs)
        {
            return _d10FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateC10Foundations(C10ViewModel userInputs)
        {
            return _c10FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateC10FitoutFoundations(C10FitoutViewModel userInputs)
        {
            return _c10FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateC20Foundations(C20ViewModel userInputs)
        {
            return _c20FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateC20FitoutFoundations(C20FitoutViewModel userInputs)
        {
            return _c20FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD20Foundations(D20ViewModel userInputs)
        {
            return _d20FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD20FitoutFoundations(D20FitoutViewModel userInputs)
        {
            return _d20FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD30Foundations(D30ViewModel userInputs)
        {
            return _d30FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD30FitoutFoundations(D30FitoutViewModel userInputs)
        {
            return _d30FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD40Foundations(D40ViewModel userInputs)
        {
            return _d40FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD40FitoutFoundations(D40FitoutViewModel userInputs)
        {
            return _d40FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD50Foundations(D50ViewModel userInputs)
        {
            return _d50FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD50FitoutFoundations(D50FitoutViewModel userInputs)
        {
            return _d50FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD60Foundations(D60ViewModel userInputs)
        {
            return _d60FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD60FitoutFoundations(D60FitoutViewModel userInputs)
        {
            return _d60FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD70Foundations(D70ViewModel userInputs)
        {
            return _d70FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD70FitoutFoundations(D70FitoutViewModel userInputs)
        {
            return _d70FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD80Foundations(D80ViewModel userInputs)
        {
            return _d80FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateD80FitoutFoundations(D80FitoutViewModel userInputs)
        {
            return _d80FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateE10Foundations(E10ViewModel userInputs)
        {
            return _e10FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateE10FitoutFoundations(E10FitoutViewModel userInputs)
        {
            return _e10FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateE20Foundations(E20ViewModel userInputs)
        {
            return _e20FormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateE20FitoutFoundations(E20FitoutViewModel userInputs)
        {
            return _e20FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateF10BC(F10BCViewModel userInputs)
        {
            return _f10BCFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateF20BC(F20BCViewModel userInputs)
        {
            return _f20BCFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateF30BC(F30BCViewModel userInputs)
        {
            return _f30BCFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ10BC(Z10BCViewModel userInputs)
        {
            return _z10BCFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ70BC(Z70BCViewModel userInputs)
        {
            return _z70BCFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ910BC(Z910BCViewModel userInputs)
        {
            return _z910BCFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ920BC(Z920BCViewModel userInputs)
        {
            return _z920BCFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ930BC(Z930BCViewModel userInputs)
        {
            return _z930BCFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ940BC(Z940BCViewModel userInputs)
        {
            return _z940BCFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateG10SD(G10SDViewModel userInputs)
        {
            return _g10SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateG20SD(G20SDViewModel userInputs)
        {
            return _g20SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateG60SD(G60SDViewModel userInputs)
        {
            return _g60SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ10SD(Z10SDViewModel userInputs)
        {
            return _z10SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ70SD(Z70SDViewModel userInputs)
        {
            return _z70SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ910SD(Z910SDViewModel userInputs)
        {
            return _z910SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ920SD(Z920SDViewModel userInputs)
        {
            return _z920SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ930SD(Z930SDViewModel userInputs)
        {
            return _z930SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ940SD(Z940SDViewModel userInputs)
        {
            return _z940SDFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateG30SU(G30SUViewModel userInputs)
        {
            return _g30SUFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateG40SU(G40SUViewModel userInputs)
        {
            return _g40SUFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateG50SU(G50SUViewModel userInputs)
        {
            return _g50SUFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ10SU(Z10SUViewModel userInputs)
        {
            return _z10SUFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ70SU(Z70SUViewModel userInputs)
        {
            return _z70SUFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ910SU(Z910SUViewModel userInputs)
        {
            return _z910SUFormulaCalculator.CalculateFormula(userInputs);
        }
        public double CalculateZ920SU(Z920SUViewModel userInputs)
        {
            return _z920SUFormulaCalculator.CalculateFormula(userInputs);

        }
        public double CalculateZ930SU(Z930SUViewModel userInputs)
        {
            return _z930SUFormulaCalculator.CalculateFormula(userInputs);

        }
        public double CalculateZ940SU(Z940SUViewModel userInputs)
        {
            return _z940SUFormulaCalculator.CalculateFormula(userInputs);

        }
        public double CalculateSoftCost(SoftCostViewModel userInputs)
        {
            return _softCostFormulaCalculator.CalculateFormula(userInputs);

        }
        public double CalculateTotalProjectCost(TotalProjectCostViewModel userInputs)
        {
            return _totalProjectCostFormulaCalculator.CalculateFormula(userInputs);

        }
        public double CalculateA10F30SumOfBC(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateA10F30SumOfBC(userInputs);

        }
        public double CalculateA10F30SumOfFitouts(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateA10F30SumOfFitouts(userInputs);

        }
        public double CalculateA10F30Sum(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateA10F30Sum(userInputs);

        }
        public double CalculateG10G60Sum(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateG10G60Sum(userInputs);
        }
        public double CalculateG30G50SUSum(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateG30G50SUSum(userInputs);
        }

        public double GetBuildingTypeProjectStrategyFactor(object userInputs)
        {
            return _buildingTypeProjectStrategyFactorCalculator.GetFactor(userInputs);
        }

        public double CalculateZ920Rate(BaseZ920ViewModel userInputs)
        {
            return _z920RateCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ10Fitout(Z10FitoutViewModel userInputs)
        {
            return _z10FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ70Fitout(Z70FitoutViewModel userInputs)
        {
            return _z70FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ910Fitout(Z910FitoutViewModel userInputs)
        {
            return _z910FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ920Fitout(Z920FitoutViewModel userInputs)
        {
            return _z920FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ930Fitout(Z930FitoutViewModel userInputs)
        {
            return _z930FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateZ940Fitout(Z940FitoutViewModel userInputs)
        {
            return _z940FitoutFormulaCalculator.CalculateFormula(userInputs);
        }

        public double CalculateA_Substructure(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateA_Substructure(userInputs);
        }

        public double CalculateB_Shell(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateB_Shell(userInputs);
        }

        public double CalculateC_Interiors(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateC_Interiors(userInputs);
        }

        public double CalculateD_Services(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateD_Services(userInputs);
        }

        public double CalculateE_EquipmentFurnishings(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateE_EquipmentFurnishings(userInputs);
        }

        public double CalculateF_ConstructionDemolition(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateF_ConstructionDemolition(userInputs);
        }

        public double CalculateSubtotalDirectCost_building(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateSubtotalDirectCost_building(userInputs);
        }

        public double CalculateContractorMarkups_building(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateContractorMarkups_building(userInputs);
        }

        public double CalculateContingencies_building(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateContingencies_building(userInputs);
        }

        public double CalculateTotalBuildingConstruction(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateTotalBuildingConstruction(userInputs);
        }

        public double CalculateSiteDevelopment_site(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateSiteDevelopment_site(userInputs);
        }

        public double CalculateSiteUtilities_site(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateSiteUtilities_site(userInputs);
        }

        public double CalculateSubtotalDirectCost_site(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateSubtotalDirectCost_site(userInputs);
        }

        public double CalculateContractorMarkups_site(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateContractorMarkups_site(userInputs);
        }

        public double CalculateContingencies_site(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateContingencies_site(userInputs);
        }

        public double CalculateTotalSiteConstruction_site(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateTotalSiteConstruction_site(userInputs);
        }

        public double CalculateTotalConstructionCosts(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateTotalConstructionCosts(userInputs);
        }

        public double CalculateSoftCosts(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateSoftCosts(userInputs);
        }

        public double CalculateTotalProjectCosts(ProjectResultModel userInputs)
        {
            return _projectFormulaCalculator.CalculateTotalProjectCosts(userInputs);
        }
    }
}
