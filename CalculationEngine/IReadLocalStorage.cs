﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationEngine
{
    public interface IReadLocalStorage
    {
        bool GetValueAsBool(string variableName);
        int GetValueAsInt(string variableName);
        double GetValueAsDouble(string variableName);
        string GetValueAsString(string variableName);
        //now it supports bool, int, double, string only
        T GetValue<T>(string variableName);
        IDictionary<string,string> GetAllValues();
    }
}
