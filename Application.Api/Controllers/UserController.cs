﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Models;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Application.Entities;
using Application.Models.Manage;
using System.Web;

namespace Application.Api.Controllers
{
    [Authorize]
    public class UserController : ApplicationApiController
    {
        private IUserBS _userService;
        
        public UserController(IUserBS userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]        
        [Route("api/User/getAllUsers")]
        public IHttpActionResult GetAllUser()
        {
            return InvokeOfBSEmptyParameters(_userService.GetAllUsers);
        }
        [HttpGet]
        [Authorize]
        [Route("api/User/UserRole")]
        public IHttpActionResult GetUserRole()
        {
            return InvokeOfBS(_userService.GetUserRole, HttpContext.Current.User.Identity.Name);      
        }      

        [HttpGet]
        [Authorize]
        [Route("api/User/UserInfo")]
        public IHttpActionResult GetUserInfo()
        {
            return InvokeOfBS(_userService.GetUserInfo, HttpContext.Current.User.Identity.Name);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]      
        [Route("api/User/{id}")]       
        public IHttpActionResult GetUserById(string  id)
        {
            return InvokeOfBS(_userService.GetUserById, id); 
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("api/User/changeUserBlockStatus/")]
        public IHttpActionResult ChangeUserBlockStatus(UserView model)
        {
            return InvokeOfBS(_userService.ChangeUserBlockStatus, model);   
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("api/User/updateUser/")]
        public IHttpActionResult UpdateUserInfo(UserView model)
        {
            return InvokeOfBS(_userService.UpdateUserInfo, model);
        }
    }
}
