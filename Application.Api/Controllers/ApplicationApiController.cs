﻿using Application.Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Application.Api.Controllers
{
    public class ApplicationApiController: ApiController
    {
        private ApplicationResultObject result;
        protected IHttpActionResult InvokeOfBS<IN, OUT>(Func<IN, OUT> method, IN model)
        {
            result = new ApplicationResultObject();
            try
            {               
                result.Data = method(model);
                result.Status = "Ok";
                return Ok(result);
            }
            catch (Exception  e)
            {
                result.Status = "Error";
                result.Error = e;               
                return Ok(result);
            }
        }

        protected IHttpActionResult InvokeOfBSEmptyParameters<OUT>(Func<OUT> method)
        {            
            result = new ApplicationResultObject();
            try
            {
                result.Data = method();
                result.Status = "Ok";
                return Ok(result);
            }
            catch (Exception e)
            {
                result.Status = "Error";
                result.Error = e;
                return Ok(result);
            }
        }

        protected IHttpActionResult InvokeOfBSEmptyReturnType<IN>(Action<IN> method, IN model)
        {
            result = new ApplicationResultObject();
            try
            {
                method(model);
                result.Status = "Ok";
                return Ok(result);
            }
            catch (Exception e)
            {
                result.Status = "Error";
                result.Error = e;
                return Ok(result);
            }
        }


        protected IHttpActionResult InvokeOfBSWithSeveralParams<F,S,OUT>(Func<F,S,OUT> method, F firstParam, S secondParam)
        {
            result = new ApplicationResultObject();
            try
            {
                result.Data = method(firstParam, secondParam);
                result.Status = "Ok";
                return Ok(result);
            }
            catch (Exception e)
            {
                result.Status = "Error";
                result.Error = e;
                return Ok(result);
            }
        }

    }
}
