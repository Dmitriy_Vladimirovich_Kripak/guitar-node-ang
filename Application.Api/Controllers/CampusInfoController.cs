﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Models.DTO;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Application.Api.Controllers
{
    public class CampusInfoController : ApplicationApiController
    {
        private ICampusInfo _campusInfo;

        public CampusInfoController(ICampusInfo campusInfo)
        {
            _campusInfo = campusInfo;
        }        

        [HttpPut]       
        [Authorize(Roles = "Admin")]
        [Route("api/CampusInfo")]
        public IHttpActionResult UpdateProject(CampusInfoDTO model)
        {
            return InvokeOfBS(_campusInfo.Update, model);

        }

        [HttpGet]
        [Authorize]
        [Route("api/CampusInfo/{campusId}")]
        public IHttpActionResult GetProject(int campusId)
        {
            return InvokeOfBS(_campusInfo.Get, campusId);

        }

        [HttpGet]
        [Authorize]
        [Route("api/CampusInfo")]
        public IHttpActionResult GetProject()
        {
            return InvokeOfBSEmptyParameters(_campusInfo.Get);

        }
    }
}
