﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Models;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Application.Entities;
using Application.Models.Manage;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;

namespace Application.Api.Controllers
{
    [Authorize]
    public class ProjectController : ApplicationApiController
    {
        private IProjectBS _projectService;
        private ICalculationProjectBS _calculationBMS;
        public ProjectController(IProjectBS projectService, ICalculationProjectBS calculationBMS)
        {
            _projectService = projectService;
            _calculationBMS = calculationBMS;
        }

        [HttpGet]
        [Route("api/Project/getAllProjects")]
        public IHttpActionResult GetAllProjects()
        {
            return InvokeOfBS(_projectService.GetAllProject, HttpContext.Current.User.Identity.Name); 
        }

        
        [HttpPost]        
        [Route("api/Project/createProject")]
        public IHttpActionResult CreateProject(ComplexProjectValue model)
        {
            model.Project.UserName = HttpContext.Current.User.Identity.Name;
            return InvokeOfBS(_projectService.AddProject, model);          

        }
        [HttpPost]        
        [Route("api/Project/saveAsProject")]
        public IHttpActionResult SavrAsProject(ComplexProjectValue model)
        {
            return InvokeOfBS(_projectService.AddProject, model);  
        }

        [HttpPost]        
        [Route("api/Project/getProject")]
        public IHttpActionResult GetProjectById([FromBody]JObject model)
        {
            var project = new ProjectModel() { Id = (int)model["projectId"], UserId = model["userId"].ToString() };
            return InvokeOfBS(_projectService.GetProjectValues, project);           

        }        

        [HttpDelete]
        [Route("api/Project/delete/{projectId}")]
        public IHttpActionResult GetProjectById(int projectId)
        {            
            return InvokeOfBS(_projectService.DeleteProject, projectId);
        }

        [HttpPost]
        [Route("api/Project/getPagedProject")]
        public IHttpActionResult GetPagedProject([FromBody] PaginationRequest paginationModel)
        {
            var userName = HttpContext.Current.User.Identity.Name;

            return InvokeOfBSWithSeveralParams(_projectService.GetPagedListProject, paginationModel, userName);
        }
    }
}
