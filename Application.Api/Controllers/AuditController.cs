﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Models;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Application.Entities;
using Application.Models.Manage;
using System.Web;
using Application.Models.AuditTrail;

namespace Application.Api.Controllers
{
    [Authorize]
    public class AuditController : ApplicationApiController
    {
        private IAuditTrailBS _auditTrailBS;
        
        public AuditController(IAuditTrailBS auditTrailBS)
        {
            _auditTrailBS = auditTrailBS;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]        
        [Route("api/Audit/getAuditTrailInfo")]
        public IHttpActionResult GetAuditTrailInfo()
        {
            return InvokeOfBSEmptyParameters<IList<AuditTrailViewModel>>(_auditTrailBS.GetAuditTrailInfo);
        }
        
        [HttpPost]
        [Authorize(Roles = "Admin")]        
        [Route("api/Audit/getAuditTrailInfoListForPaging")]
        public IHttpActionResult GetAuditTrailInfoForPaging([FromBody] PaginationRequest paginationModel)
        {
            return InvokeOfBS<PaginationRequest, PaginationAuditTrailsViewModel>(_auditTrailBS.GetAuditTrailInfoForPaging, paginationModel);
        }
    }
}
