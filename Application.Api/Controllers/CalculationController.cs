﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Models;
using Application.Models.Calculation;
using Application.Models.Calculation.C10_E20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Application.Api.Controllers
{
    public class CalculationController : ApplicationApiController
    {
        private ICalculationBS _calculationBS;
        private ICalculationProjectBS _calculationBMS;
        public CalculationController(ICalculationBS calculationBS, ICalculationProjectBS calculationBMS)
        {
            _calculationBS = calculationBS;
            _calculationBMS = calculationBMS;
        }

        [Authorize]
        [HttpGet]
        [Route("api/Calculation/getBaselineVariables")]
        public IHttpActionResult GetBaselineVariables(CalculationType calculationType)
        {
            return InvokeOfBS<CalculationType, BaselineVariablesViewModel>(_calculationBS.GetBaselineVariables, calculationType);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/updateVariable")]
        public IHttpActionResult Post_UpdateVariable(BaselineVariableViewModel model)
        {
            return InvokeOfBS<BaselineVariableViewModel, string>(_calculationBS.UpdateVariable, model);
        }
        
        [Authorize]
        [HttpGet]
        [Route("api/Calculation/getC10E20ViewSettings")]
        public IHttpActionResult GetC10E20ViewSettings(BuildingType buildingType)
        {
            return InvokeOfBS<BuildingType, C10_E20SettingsViewModel>(_calculationBS.GetC10_E20ViewSettings, buildingType);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcA10")]
        public IHttpActionResult Post_CalcA10(A10ViewModel model)
        {
            return InvokeOfBS<A10ViewModel, string>(_calculationBS.CalcA10, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcA20")]
        public IHttpActionResult Post_CalcA20(A20ViewModel model)
        {
            return InvokeOfBS<A20ViewModel, string>(_calculationBS.CalcA20, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcA40")]
        public IHttpActionResult Post_CalcA40(A40ViewModel model)
        {
            return InvokeOfBS<A40ViewModel, string>(_calculationBS.CalcA40, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcA60")]
        public IHttpActionResult Post_CalcA60(A60ViewModel model)
        {
            return InvokeOfBS<A60ViewModel, string>(_calculationBS.CalcA60, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcA90")]
        public IHttpActionResult Post_CalcA90(A90ViewModel model)
        {
            return InvokeOfBS<A90ViewModel, string>(_calculationBS.CalcA90, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcB10")]
        public IHttpActionResult Post_CalcB10(B10ViewModel model)
        {
            return InvokeOfBS<B10ViewModel, string>(_calculationBS.CalcB10, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcB20")]
        public IHttpActionResult Post_CalcB20(B20ViewModel model)
        {
            return InvokeOfBS<B20ViewModel, string>(_calculationBS.CalcB20, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcB30")]
        public IHttpActionResult Post_CalcB30(B30ViewModel model)
        {
            return InvokeOfBS<B30ViewModel, string>(_calculationBS.CalcB30, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD10")]
        public IHttpActionResult Post_CalcD10(D10ViewModel model)
        {
            return InvokeOfBS<D10ViewModel, string>(_calculationBS.CalcD10, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcC10")]
        public IHttpActionResult Post_CalcC10(C10ViewModel model)
        {
            return InvokeOfBS<C10ViewModel, string>(_calculationBS.CalcC10, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcC10Fitout")]
        public IHttpActionResult Post_CalcC10Fitout(C10FitoutViewModel model)
        {
            return InvokeOfBS<C10FitoutViewModel, string>(_calculationBS.CalcC10Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcC20")]
        public IHttpActionResult Post_CalcC20(C20ViewModel model)
        {
            return InvokeOfBS<C20ViewModel, string>(_calculationBS.CalcC20, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcC20Fitout")]
        public IHttpActionResult Post_CalcC20Fitout(C20FitoutViewModel model)
        {
            return InvokeOfBS<C20FitoutViewModel, string>(_calculationBS.CalcC20Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD20")]
        public IHttpActionResult Post_CalcD20(D20ViewModel model)
        {
            return InvokeOfBS<D20ViewModel, string>(_calculationBS.CalcD20, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD20Fitout")]
        public IHttpActionResult Post_CalcD20Fitout(D20FitoutViewModel model)
        {
            return InvokeOfBS<D20FitoutViewModel, string>(_calculationBS.CalcD20Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD30")]
        public IHttpActionResult Post_CalcD30(D30ViewModel model)
        {
            return InvokeOfBS<D30ViewModel, string>(_calculationBS.CalcD30, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD30Fitout")]
        public IHttpActionResult Post_CalcD30Fitout(D30FitoutViewModel model)
        {
            return InvokeOfBS<D30FitoutViewModel, string>(_calculationBS.CalcD30Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD40")]
        public IHttpActionResult Post_CalcD40(D40ViewModel model)
        {
            return InvokeOfBS<D40ViewModel, string>(_calculationBS.CalcD40, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD40Fitout")]
        public IHttpActionResult Post_CalcD40Fitout(D40FitoutViewModel model)
        {
            return InvokeOfBS<D40FitoutViewModel, string>(_calculationBS.CalcD40Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD50")]
        public IHttpActionResult Post_CalcD50(D50ViewModel model)
        {
            return InvokeOfBS<D50ViewModel, string>(_calculationBS.CalcD50, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD50Fitout")]
        public IHttpActionResult Post_CalcD50Fitout(D50FitoutViewModel model)
        {
            return InvokeOfBS<D50FitoutViewModel, string>(_calculationBS.CalcD50Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD60")]
        public IHttpActionResult Post_CalcD60(D60ViewModel model)
        {
            return InvokeOfBS<D60ViewModel, string>(_calculationBS.CalcD60, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD60Fitout")]
        public IHttpActionResult Post_CalcD60Fitout(D60FitoutViewModel model)
        {
            return InvokeOfBS<D60FitoutViewModel, string>(_calculationBS.CalcD60Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD70")]
        public IHttpActionResult Post_CalcD70(D70ViewModel model)
        {
            return InvokeOfBS<D70ViewModel, string>(_calculationBS.CalcD70, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD70Fitout")]
        public IHttpActionResult Post_CalcD70Fitout(D70FitoutViewModel model)
        {
            return InvokeOfBS<D70FitoutViewModel, string>(_calculationBS.CalcD70Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD80")]
        public IHttpActionResult Post_CalcD80(D80ViewModel model)
        {
            return InvokeOfBS<D80ViewModel, string>(_calculationBS.CalcD80, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcD80Fitout")]
        public IHttpActionResult Post_CalcD80Fitout(D80FitoutViewModel model)
        {
            return InvokeOfBS<D80FitoutViewModel, string>(_calculationBS.CalcD80Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcE10")]
        public IHttpActionResult Post_CalcE10(E10ViewModel model)
        {
            return InvokeOfBS<E10ViewModel, string>(_calculationBS.CalcE10, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcE10Fitout")]
        public IHttpActionResult Post_CalcE10Fitout(E10FitoutViewModel model)
        {
            return InvokeOfBS<E10FitoutViewModel, string>(_calculationBS.CalcE10Fitout, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcE20")]
        public IHttpActionResult Post_CalcE20(E20ViewModel model)
        {
            return InvokeOfBS<E20ViewModel, string>(_calculationBS.CalcE20, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcE20Fitout")]
        public IHttpActionResult Post_CalcE20Fitout(E20FitoutViewModel model)
        {
            return InvokeOfBS<E20FitoutViewModel, string>(_calculationBS.CalcE20Fitout, model);
        }       
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcF10BC")]
        public IHttpActionResult Post_CalcF10BC(F10BCViewModel model)
        {
            return InvokeOfBS<F10BCViewModel, string>(_calculationBS.CalcF10BC, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcF20BC")]
        public IHttpActionResult Post_CalcF20BC(F20BCViewModel model)
        {
            return InvokeOfBS<F20BCViewModel, string>(_calculationBS.CalcF20BC, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcF30BC")]
        public IHttpActionResult Post_CalcF30BC(F30BCViewModel model)
        {
            return InvokeOfBS<F30BCViewModel, string>(_calculationBS.CalcF30BC, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ10BC")]
        public IHttpActionResult Post_CalcZ10BC(Z10BCViewModel model)
        {
            return InvokeOfBS<Z10BCViewModel, string>(_calculationBS.CalcZ10BC, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ70BC")]
        public IHttpActionResult Post_CalcZ70BC(Z70BCViewModel model)
        {
            return InvokeOfBS<Z70BCViewModel, string>(_calculationBS.CalcZ70BC, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ910BC")]
        public IHttpActionResult Post_CalcZ910BC(Z910BCViewModel model)
        {
            return InvokeOfBS<Z910BCViewModel, string>(_calculationBS.CalcZ910BC, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ920BC")]
        public IHttpActionResult Post_CalcZ920BC(Z920BCViewModel model)
        {
            return InvokeOfBS<Z920BCViewModel, string>(_calculationBS.CalcZ920BC, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ930BC")]
        public IHttpActionResult Post_CalcZ930BC(Z930BCViewModel model)
        {
            return InvokeOfBS<Z930BCViewModel, string>(_calculationBS.CalcZ930BC, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ940BC")]
        public IHttpActionResult Post_CalcZ940BC(Z940BCViewModel model)
        {
            return InvokeOfBS<Z940BCViewModel, string>(_calculationBS.CalcZ940BC, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcG10SD")]
        public IHttpActionResult Post_CalcG10SD(G10SDViewModel model)
        {
            return InvokeOfBS<G10SDViewModel, string>(_calculationBS.CalcG10SD, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcG20SD")]
        public IHttpActionResult Post_CalcG20SD(G20SDViewModel model)
        {
            return InvokeOfBS<G20SDViewModel, string>(_calculationBS.CalcG20SD, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcG60SD")]
        public IHttpActionResult Post_CalcG60SD(G60SDViewModel model)
        {
            return InvokeOfBS<G60SDViewModel, string>(_calculationBS.CalcG60SD, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ10SD")]
        public IHttpActionResult Post_CalcZ10SD(Z10SDViewModel model)
        {
            return InvokeOfBS<Z10SDViewModel, string>(_calculationBS.CalcZ10SD, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ70SD")]
        public IHttpActionResult Post_CalcZ70SD(Z70SDViewModel model)
        {
            return InvokeOfBS<Z70SDViewModel, string>(_calculationBS.CalcZ70SD, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ910SD")]
        public IHttpActionResult Post_CalcZ910SD(Z910SDViewModel model)
        {
            return InvokeOfBS<Z910SDViewModel, string>(_calculationBS.CalcZ910SD, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ920SD")]
        public IHttpActionResult Post_CalcZ920SD(Z920SDViewModel model)
        {
            return InvokeOfBS<Z920SDViewModel, string>(_calculationBS.CalcZ920SD, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ930SD")]
        public IHttpActionResult Post_CalcZ930SD(Z930SDViewModel model)
        {
            return InvokeOfBS<Z930SDViewModel, string>(_calculationBS.CalcZ930SD, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ940SD")]
        public IHttpActionResult Post_CalcZ940SD(Z940SDViewModel model)
        {
            return InvokeOfBS<Z940SDViewModel, string>(_calculationBS.CalcZ940SD, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcG30SU")]
        public IHttpActionResult Post_CalcG30SU(G30SUViewModel model)
        {
            return InvokeOfBS<G30SUViewModel, string>(_calculationBS.CalcG30SU, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcG40SU")]
        public IHttpActionResult Post_CalcG40SU(G40SUViewModel model)
        {
            return InvokeOfBS<G40SUViewModel, string>(_calculationBS.CalcG40SU, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcG50SU")]
        public IHttpActionResult Post_CalcG50SU(G50SUViewModel model)
        {
            return InvokeOfBS<G50SUViewModel, string>(_calculationBS.CalcG50SU, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ10SU")]
        public IHttpActionResult Post_CalcZ10SU(Z10SUViewModel model)
        {
            return InvokeOfBS<Z10SUViewModel, string>(_calculationBS.CalcZ10SU, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ70SU")]
        public IHttpActionResult Post_CalcZ70SU(Z70SUViewModel model)
        {
            return InvokeOfBS<Z70SUViewModel, string>(_calculationBS.CalcZ70SU, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ910SU")]
        public IHttpActionResult Post_CalcZ910SU(Z910SUViewModel model)
        {
            return InvokeOfBS<Z910SUViewModel, string>(_calculationBS.CalcZ910SU, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ920SU")]
        public IHttpActionResult Post_CalcZ920SU(Z920SUViewModel model)
        {
            return InvokeOfBS<Z920SUViewModel, string>(_calculationBS.CalcZ920SU, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ930SU")]
        public IHttpActionResult Post_CalcZ930SU(Z930SUViewModel model)
        {
            return InvokeOfBS<Z930SUViewModel, string>(_calculationBS.CalcZ930SU, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcZ940SU")]
        public IHttpActionResult Post_CalcZ940SU(Z940SUViewModel model)
        {
            return InvokeOfBS<Z940SUViewModel, string>(_calculationBS.CalcZ940SU, model);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcSoftCost")]
        public IHttpActionResult Post_CalcSoftCost(SoftCostViewModel model)
        {
            return InvokeOfBS<SoftCostViewModel, string>(_calculationBS.CalcSoftCost, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcTotalProjectCost")]
        public IHttpActionResult Post_CalcTotalProjectCost(TotalProjectCostViewModel model)
        {
            return InvokeOfBS<TotalProjectCostViewModel, string>(_calculationBS.CalcTotalProjectCost, model);
        }
        [Authorize]
        [HttpPost]
        [Route("api/Calculation/calcProject")]
        public IHttpActionResult Post_CalcProject(ProjectViewModel model)
        {
            return InvokeOfBS<ProjectViewModel, ProjectResultModel>(_calculationBMS.CalcProject, model);
        }
    }
}
