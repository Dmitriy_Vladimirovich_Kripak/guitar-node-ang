﻿using Application.BLLInterfaces.BusinessServicesInterfaces;
using Application.Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Application.Api.Controllers
{
    [Authorize]
    public class FileApiController : ApplicationApiController
    {
        private IProjectBS _projectService;
        private ICalculationProjectBS _calculationBMS;
        private IFileBS _fileBS;

        public FileApiController(IProjectBS projectService, ICalculationProjectBS calculationBMS, IFileBS fileBS)
        {
            _projectService = projectService;
            _calculationBMS = calculationBMS;
            _fileBS = fileBS;
        }

        [HttpGet]
        [Route("api/File/getFile/{id}")]
        public HttpResponseMessage GetFile(int id)
        {
            var fileInfo =  _fileBS.GetById(id);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(fileInfo.File)
            };
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileInfo.Project.Name + "-GeneratedPDF"
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");           

            return response;
        }

        [HttpGet]
        [Route("api/File/getFileInfo/{id}")]
        public IHttpActionResult GetFileInfo(int id)
        {
            return InvokeOfBS(_fileBS.GetById, id);
        }

        [HttpDelete]
        [Route("api/File/deleteFileInfo/{id}")]
        public IHttpActionResult DeleteFileInfo(int id)
        {
            return InvokeOfBS(_fileBS.DeleteFile, id);
        }

        [HttpGet]
        [Route("api/File/getAllFileInfo/{showAll}")]
        public IHttpActionResult GetAllProjects(bool showAll = false)
        {
            var userName = HttpContext.Current.User.Identity.Name;
            return InvokeOfBSWithSeveralParams(_fileBS.GetAll, showAll, userName);
        }

        [HttpPost]
        [Route("api/File/getPDF")]
        public HttpResponseMessage GetPDF(ProjectModel model)
        {
            try
            {
                if (!_projectService.IsProjectExist(model))
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                var complexProjectInfo = _projectService.GetProjectValues(model);

                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(_projectService.GetPDFByProject(_calculationBMS.CalcProject(complexProjectInfo.ProjectValue), complexProjectInfo.Project))
                };
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = complexProjectInfo.Project.Name + "-GeneratedPDF"
                };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                return response;
            }
            catch (Exception e)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message),
                    ReasonPhrase = "PDF File was not created. " + e.Message
                };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;

            }

        }

        [HttpPost]
        [Route("api/File/saveFilePDF/{fileName}")]
        public IHttpActionResult SavePDF(ProjectModel model,string fileName)
        {
            return InvokeOfBSWithSeveralParams(_fileBS.SaveFile, model, fileName);
        }


    }
}
